
\section{Methodology\label{sec:Methods}}

This Section outlines the methodology and the steps used in the analysis.
Sections~\ref{sec:two_pc} describes the two-particle correlation method
  and Section~\ref{sub:template_fits} describes the template-fit method
  used to obtain the \vn.

\subsection{Two particle correlations\label{sec:two_pc}}
This analysis uses the two-particle correlation method for measuring
  long-range correlations.
The methods and their implementation have been described in detail in
  a previous internal note~\cite{Jia:1349038}.
Here a short summary is presented for completeness.
All plots shown in this subsection to describe the method are taken
  from the Run-1 \PbPb\ analysis internal note~\cite{Jia:1349038}.

In two-particle correlations the yield of associated (or partner)
  particles (labeled `$b$') w.r.t. reference particles (labeled `$a$')
  in relative azimuthal angle $\dphi=\phia-\phib$ and pseudo-rapidity
  separation $\deta=\etaa-\etab$ are measured.
The two particles involved in the correlations can be selected using
  different criteria, for example different $\pT$ ranges
  (hard-soft correlations), different rapidity
  (forward -backward correlation), different charge sign
  (same sign or opposite sign correlation) or even different species.
In this analysis, the two particles are charged hadrons measured by
  the ATLAS tracking system, they cover the full azimuth and
  $\pm2.5$ in $\eta$.
Thus, the pair acceptance coverage is $\pm5.0$ units in $\deta$.

In order to account for detector acceptance effects, the correlation
  is constructed from the ratio of distributions where the reference and
  associated particles are taken from the same event
  (same-event distributions or foreground distributions)
  and distributions where the reference and associated particles
  are taken from two different events (mixed-event
  distributions or background distributions):
\begin{equation}
C(\deta,\dphi)=%
\frac{\langle\frac{d^{2}N_{\mathrm{same}}^{{\mathrm{pairs}}}(\deta,\dphi)}{d\dphi d\deta}\rangle_{{\mathrm{evt}}}}%
{\langle\frac{d^{2}N_{\mathrm{mixed}}^{{\mathrm{pairs}}}(\deta,\dphi)}{d\dphi d\deta}\rangle_{{\mathrm{evt}}}},
\end{equation}
where, the $\langle...\rangle_{{evt}}$ indicates averaging over
many events (or mixed-events).
The mixed-event distribution is constructed from particles from two
  different events that have similar multiplicity and $z$-vertex,
  so that the detector acceptance effects are similar for
  the two events.
The mixed-event distribution includes the effects of detector
  inefficiencies and non-uniformity, but contains no physical
  correlations, while the same-event distribution includes both detector
  acceptance effects as well as physical correlations.
Upon dividing by the mixed-event distribution, the acceptance effects
  cancel out and only the physical correlations remain
  (see Appendix II of~\cite{Jia:1349038} for the proof of this procedure).
Further more, in order to account for the effects of tracking efficiency
  $\epsilon(\pt,\eta)$, each pair is weighted by
  $\frac{1}{\epsilon(\pta,\etaa)\epsilon(\ptb,\etab)}$
  while calculating the same- and mixed-background distributions.

The event-mixing principle can be most clearly demonstrated using
  central Pb+Pb collision data where multiplicities are large.
Figure~\ref{fig:2pc_method_2d} shows examples of the same-event
  distribution, the mixed-event distribution and the correlation function
  for (0-5)\% central Pb+Pb collisions, for $\ptab\in(2,3)$~\GeV.
The mixed-event distribution has an almost triangular shape along the
  $\Delta\eta$ axis since it reflects a convolution of two single
  particle distributions that are approximately flat in $\eta$
  ($dN/d\eta$ is approximately flat for $-2.5<\eta<2.5$~\cite{ATLAS:2011ag}).
Apart from this triangular shape, there are small modulations in both the
  $\Delta\eta$ and $\Delta\phi$ directions due to non-uniform acceptance
  of the detector (arising from holes, dead modules etc.), which are not
  visible because of the large triangular modulation.
A similar triangular shape is also observed for the foreground distribution
  as it is also largely dominated by pair acceptance along $\Delta\eta$.
Upon dividing the same-event pair distribution by the mixed-event pair
  distribution, all the acceptance effects (including detector non-uniformity)
  cancel out leaving behind the physical correlations.

\begin{figure}
\begin{centering}
\includegraphics[width=1\linewidth]{fig_pool/methodology/2pc_method}
\par\end{centering}
\caption{
The distribution for same event pairs (left), mixed event pairs (middle)
  and correlation function (right) for $\ptab\in(2,3)$~\GeV\ for the
  (0-5)\% centrality interval in Pb+Pb collisions.
\label{fig:2pc_method_2d}}
\end{figure}

To investigate the $\Delta\phi$ dependence in more detail, one-dimensional
  (1D) $\Delta\phi$ correlation functions can be constructed from the
  ratio of the same-event and mixed-event pair-distributions both projected
  to $\Delta\phi$, corresponding to a range in $\Delta\eta$:
\begin{equation}
  C(\Delta\phi)=\frac{\langle\int\frac{d^{2}N_{\mathrm{same}}^{{\mathrm{pairs}}}(\Delta\phi,\Delta\eta)}{d\dphi d\deta}d\Delta\eta\rangle_{{\mathrm{evt}}}}{\langle\int\frac{d^{2}N_{\mathrm{mixed}}^{{\mathrm{pairs}}}(\Delta\phi,\Delta\eta)}{d\dphi d\deta}d\Delta\eta\rangle_{{\mathrm{evt}}}}\equiv\frac{\langle dN_{{\mathrm{same}}}^{{\mathrm{pairs}}}(\dphi)/d\dphi\rangle_{{\mathrm{evt}}}}{\langle dN_{{\mathrm{mixed}}}^{{\mathrm{pairs}}}(\dphi)d\dphi\rangle_{{\mathrm{evt}}}}.
\label{eq:1d2pc}
\end{equation}
Figure~\ref{fig:2pc_method_1d} shows such 1D correlations for several
  $|\Delta\eta|$ slices along with the corresponding same-event and
  mixed-event pair distributions.
These plots also demonstrate how the mixed-event distribution
  corrects for detector effects.

\begin{figure}
\begin{centering}
\includegraphics[width=1\linewidth]{fig_pool/methodology/fig_21}
\par\end{centering}
\caption{The distribution for same-event pairs and mixed-event pairs projected
along $\Delta\phi$ for several $|\Delta\eta|$ slices in the (0-5)\%
centrality interval with $\ptab\in(2,3)$~\GeV. The last panel shows
the correlation functions. Figure taken from previous conf note \cite{ATLAS:2011wfa}.
\label{fig:2pc_method_1d}}
\end{figure}

Similar to the single particle distribution Eq.\eqref{eq:single}, the
  2PC in relative azimuthal angle $\Delta\phi=\phi^{a}-\phi^{b}$ can
  be expanded in $\Delta\phi$ as a Fourier series as:
\begin{equation}
C(\Delta\phi)=C_{0}\left(1+\Sigma_{n=1}^{\infty}v_{n,n}(\pta,\ptb)\cos(n\Delta\phi)\right),\label{eq:2pc_fourier}
\end{equation}
where $\pta$ and $\ptb$ label the $\pt$ ranges of the particles used
  in the correlation.
It can be shown that for single particle distributions described by Eq.~\eqref{eq:single},
  the Fourier coefficients of the 2PC in factorize as~\cite{Jia:1349038}:
\begin{equation}
v_{n,n}(\pta,\ptb)=v_{n}(\pta)v_{n}(\ptb).\label{eq:factorization}
\end{equation}
The factorization of $v_{n,n}$ given by Eq.~\eqref{eq:factorization}
  is expected to break at high $\pt$ when the flow picture
  begins to break.
The factorization is also expected to break when the $\Delta\eta$
  separation between the particles is small, as such particles may
  have additional correlations such as HBT or may have been produced
  from decay of the same parent particle.
In the phase-space region where Eq.~\eqref{eq:factorization} holds,
  the $v_{n}(\ptb)$ can be evaluated from the measured $v_{n,n}$ as:
\begin{equation}
v_{n}(\ptb)=\frac{v_{n,n}(\pta,\ptb)}{v_{n}(\pta)}=\frac{v_{n,n}(\pta,\ptb)}{\sqrt{v_{n,n}(\pta,\pta)}},\label{eq:factorization_2}
\end{equation}
where in the denominator we have used the fact that $v_{n,n}(\pta,\pta)=v_{n}(\pta)v_{n}(\pta)$

For multiplicity dependence, both \pta\ and \ptb\ are required
  with range of $0.4 < \pt\ 4$ GeV.
For \pt\ dependence (as function of \ptb), \pta\ is required to be within $0.4 < \pt\ 4$.





\subsection{Combinatorial correction \label{sub:combinatorial_correction}}
In general, reconstructed jets are composed of not just particles produced from
  hard scattering, but also particles that originate from the soft underlying event.
In order to accurately measure the correlation between the hard scattering
  products and the particles from the underlying event,
  the combinatorial background (particles from the underlying event within the
  jet compared to those away from jets) must be subtracted from the measurements.
To account for combinatorial correction in the measurement of two-particle
  correlations, a new correlation is constructed using particles from other
  events, which are referred to as ``pool events".
This correlation is then subtracted from the foreground correlation.
To ensure that the pool events are similar to the current event, pool events are
  required to have similar event multiplicity (within 10 multiplicities),
  similar z-vertex position (within 20 mm), and
  they must pass the same total energy (TE) trigger group as the current event.
To obtain the correlation for the combinatorial correction, particles in the pool
  events that are located around the valid jet positions are correlated with
  particles in the pool events after applying the same $\eta$ rejection ranges.
This allows to subtract the combinatorial background effect to the measurement
  and obtain the true correlation.
An example of this method is illustrated in  Figure~\ref{fig:combCorrCartoon}.

\begin{figure}[H]
  \center{
    \includegraphics[width=0.9\linewidth]{fig_pool_afterHP/methodology/CombCorrCartoon}
  }
  \caption{
    Cartoon for combinatorial correction. Dashed circles indicate the same jet
    location as in the current event. Same jet rejection ranges are applied
    in pool events.
  }
  \label{fig:combCorrCartoon}
\end{figure}





\subsection{Template Fit Method \label{sub:template_fits}}
One drawback of the \tpc\ method is that in peripheral events with
  low multiplicity or for \tpcs\ involving particles at high-\pt, the
  measured \vn\ can be biased by correlations arising from back-to-back
  dijets that are not rejected by the $|\deta|>2$ requirement.
This issue is much more severe in smaller collision systems,
  such as \pPb\ and especially in \pp\ collisions -- where
  \tpc\ even at large \deta\ is completely dominated by the back-to-back
  dijet correlations.
To address this issue, a template fitting procedure was developed
  by ATLAS and used to measure the \vn\ in \pp\ and \pPb\
  collisions~\cite{HION-2015-09,HION-2016-01,HION-2017-11}.
The template fit method as used in \pp\ and \pPb\
  analyses~\cite{HION-2015-09,HION-2016-01,HION-2017-11} assumes that:
\begin{enumerate}
\item The shape of the dijet contribution to the \tpc\ does not change
  from low- to high-multiplicity events, only its relative contribution
  to the \tpc\ changes.
\item The \tpc\ for low multiplicity events is dominated
  by the dijet contribution.
\end{enumerate}

With the above assumptions, the correlation $\ctwophi$ in
  higher-multiplicity events is then described by a template fit,
  \ctempl, consisting of two components: a scale factor, $F$,
  times the correlation measured in low-multiplicity correlations,
  \cperi, which accounts for the dijet-correlation, and a
  genuine long-range harmonic modulation, \cridge:
\begin{eqnarray}
\ctempl &= & F\cperi+G\left(1+2\Sigma_{n=2}v_{n,n}\cos(n\dphi)\right)\label{eq:template}\\
           &\equiv& F\cperi+\cridge,\nonumber
% \label{eq:template}
\end{eqnarray}
where the coefficient $F$ and the \vnn\ are fit parameters adjusted to
  reproduce the $\ctwophi$.
The coefficient $G$ is not a free parameter, but is fixed by the
  requirement that the integral of the $\ctempl(\dphi)$ and $C(\dphi)$
  are equal.
Unlike in Eq.~\eqref{eq:2pc_fourier}, the sum over \vnn\ in
  Eq.~\eqref{eq:template} starts from $n$=2, this is because
  the $n$=1 term almost completely arises because of back to
  back dijets,  and is already accounted for by the
  inclusion of the scaled peripheral reference.

The previous ATLAS template-fit analyses~\cite{HION-2015-09,HION-2016-01,HION-2017-11}
  were successful in extracting the \vnn\ in \pp\ collisions
  which was not possible previously.
They also demonstrated that the \vnn\ obtained by the template fit
  method in peripheral events are found to be less biased by di-jets
  and factorized, and thus could be used to obtain the \vn\ via Eq.~\eqref{eq:factorization}.


\begin{figure}[H]
\center{
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/templateFit/inclusive/can_fits_cent40_50_pericent10_30_fgTag0}%
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/templateFit/inclusive/can_fits_cent60_70_pericent10_30_fgTag0}%
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/templateFit/inclusive/can_fits_cent80_90_pericent10_30_fgTag0}
\caption{
Template fits for inclusive case in different multiplicity bins using
  Eq.~\eqref{eq:template}, for 13~\TeV\ \pp\ data.
The $\ntrkreccorr\in[10,30)$ bin is used to determine the $\cperi(\dphi)$.
The $F\cperi(\dphi)$ has been shifted up by $G$, and the $\cridge$ term
  has been shifted up by $F\cperi(0)$ for easier comparison.
The plots are for $\ptab\in$(0.5,4.0)~\GeV.
\label{fig:Fit_example}
}}
\end{figure}

Figure~\ref{fig:Fit_example} shows an example of how the
  template fitting works.
The black data-points indicate the measured \tpc\ in
  high-multiplicity events.
The red line is the fit function.
The open circles indicate the scaled low-multiplicity
  reference: $C\cperi$, which accounts for most of the
  away-side peak in the measured \tpc.
The blue dashed line indicates \cridge: the long-range correlation
 which is described by the cosine modulation.





\subsection{Statistical uncertainty \label{sub:stat_un}}
The statistical uncertainties in the above extracted correlation coefficients
  are based on the assumption that all pairs entering the two-particle
  correlation histogram are statistically independent.
This assumption is incorrect as particles in one events are correlated.
Each particle might be re-used multiple times to form different correlation pairs.
Therefore, the uncertainties from the old pair-by-pair filling procedure
  is underestimated.
One way to improve the calculation of uncertainties is to treat them fully
  correlated with each other, counting the pairs at different $\Delta\phi$
  on per-event base and filling them event-by-event.
Then use the standard error of the mean on each $\Delta\phi$ bin as
  new uncertainties.
Figure~\ref{fig:cov_matrix} is an example covariance of bins in 1D2PCs
  in one of the \JH\ multiplicity bin.
The diagonal values are used to calculate the standard error of
  the mean (SEM) for each bins in 1D2PC.

\begin{figure}[H]
  \center{\includegraphics[width=0.60\linewidth]{fig_pool_afterHP/methodology/statistical_uncertainty/can_covariance_cent100_110}}
  \caption{
    Covariance matrix of bins in 1D2PC plots in \JH\ case.
  }
  \label{fig:cov_matrix}
\end{figure}

However, this method is not optimal either.
In this case, all particles in one event are assumed to be fully correlated.
While, particles from different processes should be treated differently.
There is also off diagonal terms shown in Figure~\ref{fig:cov_matrix}.
The SEM error does not account for bin-to-bin correlations.
These effects are discussed in previous photo-nuclear 2PC paper \cite{Seidlitz:2668489}.
A bootstrapping procedure is also used here to account for
  both Poisson statistical errors and bin-to-bin correlations.
The pseudo-experiments use the same set of nominal events re-sampled
  by a random number of times, drawn from a Poisson distribution of mean one.

Figure~\ref{fig:bootstrap_1d2pcbins_inclusive} shows the result of \dphi\ bins in
  1D2PC conducted on 100 bootstrapped pseudo-experiments.
The RMS is used on the final template fit result plots.
Figure~\ref{fig:bootstrap_v22_inclusive} shows the result of v22 in different
  multiplicity bins.
The the mean value and RMS is used as the final v22 to obtain v2 results.

\begin{figure}[H]
  \center{\includegraphics[width=0.95\linewidth]{fig_pool_afterHP/bootstrapping/inclusive/can_mult100_110_Data_pp_mu5_eff1_trig9_pileup5_trkqual1_rejME1_jet0_jc0_jetcut0_ifHaveJet2_ifUECorr1_corrtype0_multtype0}}
  \caption{
    The distribution of the \dphi\ bins from the bootstrapping for events with
      multiplicity from 100 to 110.
    The x-axis is $C(\Delta\phi)$ value in 1D2PC.
    The distributions are shown for the hadron-hadron correlation case as example.
    Each panel is a different \dphi\ bin.
    The RMS-widths and mean values of the distributions are stated on the plots.
  }
  \label{fig:bootstrap_1d2pcbins_inclusive}
\end{figure}


\begin{figure}[H]
  \center{\includegraphics[width=0.60\linewidth]{fig_pool_afterHP/bootstrapping/inclusive/can_bs_v22_pericent55_Data_pp_mu5_eff1_trig9_pileup5_trkqual1_rejME1_jet0_jc0_jetcut0_ifHaveJet2_ifUECorr1_corrtype0_multtype0}}
  \caption{
    The distribution of the v22 values from the bootstrapping.
    The x-axis is v22 value.
    The $10^{-3}$ label on x-axis is eaten by ROOT when making plots.
    The distributions are shown for the hadron-hadron correlation case.
    Each panel is a different multiplicity interval.
    The RMS-widths and mean values of the distributions are stated on the plots.
  }
  \label{fig:bootstrap_v22_inclusive}
\end{figure}
