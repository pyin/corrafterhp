
\section{Introduction}



In heavy-ion collisions, two particle correlations~(2PC) in relative
  azimuthal angle $\Delta\phi=\phi^{{\mathrm{a}}}-\phi^{{\mathrm{b}}}$ and pseudo-rapidity
  separation $\Delta\eta=\eta^{{\mathrm{a}}}-\eta^{{\mathrm{b}}}$ show distinct
  long-range correlations along $\Delta\eta$~\cite{Aamodt:2011by,Chatrchyan:2012wg,HION-2011-01}.
Figure~\ref{fig:2pc_demo} shows 2PC in mid-central%
\footnote{
  In heavy ion collisions, the term ``centrality'' is used as an experimental
    proxy that quantifies the degree of overlap (or impact parameter: \bimp),
    between the colliding nuclei.
  Head-on collisions where the nuclei have significant overlap (or small \bimp)
    are called ``central'' events, while ``peripheral'' events are
    collisions with a smaller overlap (or large \bimp).
  The centrality is determined by measuring the total multiplicity or energy
    deposited in a reference detector and categorizing the events into
    percentile classes based on this.
  Thus (0--1)\% centrality would represent the 1\% of all events with highest
    multiplicity while (99--100)\% centrality would represent the 1\% of all
    events with the lowest multiplicity.%
}
  (20--30)\%, and peripheral (70--80)\%, \PbPb\ collisions from Ref.~\cite{HION-2011-01}.
In the left panel a small near-side jet-peak is visible at $(\deta,\dphi)\sim(0,0)$.
  Long-range correlations along $\deta$ are seen on both the near-side
  ($\dphi\sim0$), called the ``ridge'', and also on away-side $\dphi\sim\pi$.
Detailed analysis of these long-range correlations demonstrated that
  these structures were in-fact a simple manifestation of the single-particle
  anisotropies \vn, which originate due to the hydrodynamic expansion
  of the medium~\cite{HION-2011-01}. The \vn\ are defined by parameterizing
  the azimuthal distribution of particles produced in heavy-ion collisions as:

\begin{equation}
\frac{{\mathrm{d}}N}{{\mathrm{d}}\phi}\propto\left(1+2\Sigma_{n=1}^{\infty}\vn\cos(n(\phi-\Phi_{n}))\right),
\label{eq:single}
\end{equation}
where the \vn\ and $\Phi_{n}$ denote the magnitude and orientation
  of the single-particle anisotropies.
It has been shown that the long-range correlations in central and
  mid-central A+A collisions, roughly covering (0--70)\% centrality
  range, arise from the convolution of single-particle distributions~\cite{HION-2011-01}.
In the peripheral~(70--80)\% centrality events shown in Fig.~\ref{fig:2pc_demo},
  the near-side jet peak is much more prominent, and is truncated to
  make the other structures more prominent.
The ridge correlation on the near-side is much weaker and shows
  considerable dependence on $\deta$.
The weakening of the ridge in peripheral collisions is understood as the
  decrease in collective behavior as the system-size becomes smaller.
In the peripheral events, the correlation on the away-side is not
  because of collective dynamics, but largely due to back to
  back jets~\cite{HION-2011-01}.

\begin{figure}[h]
\begin{centering}
\includegraphics[bb=0bp 138bp 142bp 282bp,clip,width=0.33\linewidth]{fig_pool/introduction/fig_08}%
\includegraphics[bb=142bp 0bp 284bp 138bp,clip,width=0.33\linewidth]{fig_pool/introduction/fig_08}%
\includegraphics[bb=426bp 0bp 567bp 142bp,clip,width=0.33\linewidth]{fig_pool/introduction/fig_08}
\par
\end{centering}
\caption{Two-Particle $\Delta\eta$-$\Delta\phi$ correlations in mid-central
(20--30)\% and peripheral (70--80)\% \PbPb\ collisions~\cite{HION-2011-01}.\label{fig:2pc_demo}}
\end{figure}


Because of their hydrodynamic origin, such long-range correlations
  were not expected to be seen in smaller colliding systems, where hydrodynamic
  or collective phenomena were not expected to develop (similar to the
  diminishing long-range correlations in peripheral A+A collisions).
In 2010 the CMS Collaboration showed the presence of a long-range component on
  the near-side for 2PC in high-multiplicity \pp\ collisions at $\sqs=7$~\TeV,
  with more than 100 charged particles~\cite{Khachatryan:2010gv}.
The CMS results are shown in Fig.\ref{fig:2pc_pp_CMS}.
The left panel shows the 2PC for min-bias events where one can only see the
  near-side jet peak and the away-side jet.
The right panel shows the 2PC for events with more than 110 reconstructed tracks
  where a weak (compared to A+A), but clearly observable ridge correlation is also seen.

\begin{figure}[h]
\begin{centering}
\includegraphics[bb=284bp 274bp 567bp 547bp,clip,width=0.4\linewidth]{fig_pool/introduction/CMS_pp}%
\includegraphics[bb=284bp   0bp 567bp 274bp,clip,width=0.4\linewidth]{fig_pool/introduction/CMS_pp}
\par
\end{centering}
\caption{Two-Particle correlations in min-bias (left) and high-multiplicity
\pp\ collisions at $\sqs=7$~\TeV~\cite{Khachatryan:2010gv} (right).\label{fig:2pc_pp_CMS}}
\end{figure}


A few theoretical calculations that anticipated collective phenomena
  in \pp\ collisions at the LHC, existed prior to the CMS measurement~\cite{Bozek:2009dt,Prasad:2009bx,CasalderreySolana:2009uk}. And several
  results were published after the CMS measurements came out \cite{Shuryak:2010wp,Werner:2010ss,Bozek:2010pb,Bozek:2011if}.
These calculations argue that the ridge in \pp\ collisions arises
  from the mechanism similar to heavy-ion collisions: i.e. elliptic
  flow, with the initial eccentricities produced by fluctuations of
  the sub-nucleonic structures.
Other models argued that the \pp\ ridge is due to initial state effects~\cite{Dumitru:2010iy,Kovner:2010xk,Altinoluk:2011qy,Dusling:2012iga,Levin:2011fb,Strikman:2011cx},
  e.g. processes with multiple partons, produced semi-classically which
  color connect partons across a large $\eta$ range.
These processes can be enhanced due to gluon saturation effects in
  high-multiplicity \pp\ collisions where the gluon density is high~\cite{Dumitru:2010iy,Kovner:2010xk}.
The initial-state effect picture was the more commonly accepted
  mechanism behind the $pp$ ridge in the heavy-ion community.

In the original CMS measurement in Ref.\cite{Khachatryan:2010gv},
  only the long-range correlation on the near-side ($\Delta\phi$\textasciitilde{}0)
  was studied.
This was because of the presence of significant contribution of back to
  back dijets made the study of the genuine ridge-like correlation
  difficult on the away-side ($\Delta\phi\sim\pi$).
A much more detailed study of the full $\dphi$ structure of long-range
  correlations in $pp$ collisions at $\sqrt{s}$=2.76 and 13~\TeV, after
  removing the dijet contribution, was published by ATLAS \cite{HION-2015-09}.
This was followed by a second set of results where the
  measurement was extended to $\sqs=5.02$~\TeV\ \pp\ collisions~\cite{HION-2016-01}.
In these measurements it was shown that the long-range correlations in \pp\
  collisions are in fact produced from single-particle anisotropies just
  like in heavy-ion collisions.
Starting from the 2PC, the single-particle anisotropies \vn\ were extracted.
It was shown that the \vn\ in \pp\ collisions were consistent with no
  dependence on \sqs.
The \pt\ dependence of the \vtwo\ was shown to be qualitatively similar to that
  measured in \pA\ and A+A collisions.
And very surprisingly it was shown that the \vn\ were independent of the
  charged-particle multiplicity ($\ntrkrec$).

These measurements supported the ``collective effects'' origin
  scenario for the long-range correlations, like in heavy ion collisions.
However, additional measurements are necessary to establish the origin
  of the long-range correlation.
%This present analysis aims to tackle some of these issues.
This note presents flow measurements in \pp\ collisions while actively rejecting
  particles associated with jets.
If the long-range correlations arise due to hard or semi-hard processes,
  then removing particles associated with jets from the analysis would
  weaken the long-rage correlation.
On the other hand, no modification in the long-range correlation when
  removing tracks associated with jets would further support the
  flow origin for the correlations.
%It may also be ``flow like'', where the correlation would be weak when
%  one of the particles in the 2PC is associated with a jet.
%In this case, we can study if the soft-fragments of the jet attain anisotropy
%  from interaction with the underlying event (UE).
%When correlating particles that well separated in eta from jets,
%  the correlation should be weakened as well.


The outline of the note is as follows:
  Section~\ref{sec:Datasets_trigger_and_tracking_cuts} describes the
  dataset, event-selection cuts and tracking cuts used in the analysis.
Section~\ref{sec:Methods} describes the two-particle correlation procedure and
  the template-fit method, which is used to measure the flow harmonics
  $v_n$ in this analysis.
Section~\ref{sec:jets} describes the criteria for selecting the jets and
  the procedure  by which tracks associated with the jets are removed
  from the analysis.
Several jet performance studies are also discussed.
The systematic uncertainties associated with the measurement are covered
  in Section~\ref{sec:Systematics}.
% Section~\ref{sec:checks} includes the dependence of the results on few
%   parameters of the measurement.
% Varying these parameters can introduce changes in the underlying physics
%   effects that are being studied in this analysis, and thus these variations
%   should not be considered as sources of systematic uncertainties.
In Section~\ref{sec:Analysis_Result} the details of the measurements are
  described together with the results.
Finally Section~\ref{sec:summary} summarizes the final results of the analysis.
