
\section{Jet reconstruction and corrections\label{sec:jets}}

The analysis uses Particle-Flow jets with the anti-kt algorithm using R = 0.4.
In this section, we introduce the calibration, cleaning, and cuts used in this analysis.
We also provide a detailed discussion of a groomed jet \pt\ definition, \ptp,
  which is required to remove underlying event modulation in the \JH\ study.


\subsection{PFlow jet calibration}
EMPFlow jet calibration is performed following this twiki page:
\url{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ApplyJetCalibrationR21#Small_R_Jets_Calibration_for_r21}.
Specific parameters used are:
\begin{itemize}
  \item \textbf{Jet Algorithm} : AntiKt4EMPFlow
  \item \textbf{Configuration file} :  JES\_MC16Recommendation\_Consolidated\_PFlow\_Apr2019\_Rel21.config
  \item \textbf{Calibration sequence for Data} : JetArea\_Residual\_EtaJES\_GSC\_Insitu
  \item \textbf{Calibration area} :  00-04-82
  \item \textbf{isData parameter in constructor} : true
\end{itemize}



\subsection{PFlow jet cleaning}
Jet cleaning is performed following this twiki page:
\url{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HowToCleanJetsR21}, with CutLevet of ``LooseBad".



\subsection{PFlow jet JVT cut}
To further remove pile-up jets, Jet Vertex Tagger (JVT) is used by suggestion for
  particle flow jets, following this twiki page:
\url{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PileupJetRecommendations#Jet_Pile_up_Tagging_Recommen_AN1}.



\subsection{PFlow jet time cut}
Figure~\ref{fig:PFlowJets2nd_EtaT_corr} show jet time vs jet eta.
An extra band appear around $|t| \sim 25$ ns, specifically at low multiplicity.
This is due to out-of-time pileup from another bunch-crossing.
A timing cut of $|t| < 5$ ns is applied to reduce this out-of-time pileup.

\begin{figure}[H]
\center{
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaT/can_h2_PFlowJets2nd_EtaT_orig_cent10_30}%
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaT/can_h2_PFlowJets2nd_EtaT_orig_cent30_40}%
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaT/can_h2_PFlowJets2nd_EtaT_orig_cent40_50}
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaT/can_h2_PFlowJets2nd_EtaT_orig_cent50_60}%
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaT/can_h2_PFlowJets2nd_EtaT_orig_cent60_70}%
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaT/can_h2_PFlowJets2nd_EtaT_orig_cent70_80}
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaT/can_h2_PFlowJets2nd_EtaT_orig_cent80_90}%
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaT/can_h2_PFlowJets2nd_EtaT_orig_cent90_100}%
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaT/can_h2_PFlowJets2nd_EtaT_orig_cent100_110}
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaT/can_h2_PFlowJets2nd_EtaT_orig_cent110_120}%
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaT/can_h2_PFlowJets2nd_EtaT_orig_cent120_130}%
\includegraphics[width=0.33\linewidth]{fig_pool_afterHP/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaT/can_h2_PFlowJets2nd_EtaT_orig_cent130_140}
}
\caption{
Jet time vs $\eta$ distribution of pflow jets with $\pt\>15$ GeV.
\label{fig:PFlowJets2nd_EtaT_corr}
}
\end{figure}





\subsection{Groomed jet \pt definition \label{sec:new_jet_pt}}
Particles coming out from soft productions are the underlying event in this analysis.
The soft productions are modulated with respect to the event angle.
The global modulation is basically due to the elliptic flow.
When jets sit on the peak of underlying event modulation, as pointed by red arrow
  in Figure~\ref{fig:modulation_cartoon}, the reconstructed energy will be larger
  than it is if sitting at the bottom of the modulation (pointed by blue arrow).
The modulation is in the range of $\mathcal{O}$(10 MeV) -- $\mathcal{O}$(100 MeV)
  and can affect \JH\ correlation in this analysis.
% , therefore, jets with high enough
%   energy will not be affected by this modulation effect.
% This analysis examines jets with a \pt\ of around 40 GeV, which are significantly
%   impacted by the modulation.

\begin{figure}[H]
  \center{
  \includegraphics[width=0.95\linewidth]{fig_pool_afterHP/jetMonitors/modulation_cartoon}
  }
  \caption{
    Left plot is a demonstration of particle $\phi$ distribution with respect to
      event plane angle.
    More particles are expected at 0 and $\pi$ due to the presence of elliptic flow.
    Middle and left plots are two extreme examples cartoon demonstrating when a
      jet is setting at the crest (left) and at the trough (middle).
  }
  \label{fig:modulation_cartoon}
\end{figure}

To reduce the effect of underlying event modulation, a groomed jet \pt\ is proposed
  by summing constituents (both charge and neutral particle flow objects) with \pt\ greater than 4 GeV:
\begin{eqnarray}
p_{\mathrm{T}}^{\mathrm{G}}=\left|\sum{\boldsymbol{p}_{\mathrm{T}}^{>\,4\,\mathrm{GeV}}}\right|
\end{eqnarray}
Then the same calibration factor is applied to this \ptp to take the jet
  calibrations into account.
It is used to reduce as many soft production particles as possible
  and get a cleaner metric when selecting jets.
Details about the choice of 4 GeV threshold for constituents and modulation effects
  are further shown in Embedding Study Section~\ref{sec:embedding}.
The 4 GeV threshold is only used for the groomed jet \pt\ definition, not for
  any other selections in this analysis.
It only used to select jets for the two-particle correlation study.
When constructing pairs for 2PC, all \pt\ particles within selected jets are used.
Figure~\ref{fig:ptp_calibpt} shows groomed jet energy, \ptp, vs. original
  calibrated jet \pt\ at low multiplicity range.
The average of the data displays a strong correlation, and the slope is almost constant.
This indicates that it is not dependent on the jet energy, but rather appears
  to be more of a jet energy scale effect.

\begin{figure}[H]
  \center{
  \includegraphics[width=0.48\linewidth]{fig_pool_afterHP/jetMonitors/ptp_calibpt}%
  \includegraphics[width=0.48\linewidth]{fig_pool_afterHP/jetMonitors/pjx_ptp40}
  }
  \caption{
    Jet \ptp\ vs. calibrated \pt\ with multiplicity of 10 to 30 (left).
    Calibrated jet \pt\ spectrum with \ptp\ above 40, projected from left plot.
    The black points are the average of the distribution.
  }
  \label{fig:ptp_calibpt}
\end{figure}

A crosscheck using PYTHIA 8 is also done using Monash 2013 settings with MPI off
  and ISR on.
Figure~\ref{fig:ptp_calibpt_PYTHIA8}(a) shows similar groomed jet energy, \ptp,
  vs. reconstructed jet \pt\ at low multiplicity range from PYTHIA 8 events.
It has a similar slope as it is in Data plot above.
After modifying the jet spectrum in PYTHIA 8 to be the same as Data, the projection
  from PYTHIA 8 is shown in Figure~\ref{fig:ptp_calibpt_PYTHIA8}(b), which
  shows a good agreement with data.

\begin{figure}[H]
  \center{
  \includegraphics[width=0.48\linewidth]{fig_pool_afterHP/jetMonitors/can_PYTHIA_h2_PFlowJets_calibPT_PTAbvN_cent10_30}%
  \includegraphics[width=0.48\linewidth]{fig_pool_afterHP/jetMonitors/can_pjxWithScale_overlay_h2_PFlowJets_calibPT_PTAbvN_cent10_30}
  }
  \caption{
    Jet \ptp\ vs. calibrated \pt\ with multiplicity of 10 to 30 (left) in PYTHIA 8.
    Reconstructed jet \pt\ spectrum with \ptp\ above 40, projected from left plot.
    The black points are the average of the distribution.
  }
  \label{fig:ptp_calibpt_PYTHIA8}
\end{figure}





\subsection{Correcting Jets constituents from the underlying event \label{sec:ue_corr}}
The number of constituents are corrected to account for the
 underlying event (\UE) tracks.
This corrected number is only used for event multiplicity correction in the next
  section.
These corrections are evaluated in fine intervals of multiplicity
  and groups of TE triggers.
The correction for the number of constituents is evaluated as:
\begin{eqnarray}
N^{\mathrm{constituents}}_{\mathrm{corrected}} = N^{\mathrm{constituents}}_{\mathrm{original}} - \pi r^{2}\rho(\eta,\phi,\{\nchrec\})
\label{eq:correction1}
\end{eqnarray}
where $N^{\mathrm{constituents}}_{\mathrm{original}}$ count charged jet constituents from all \pt,
  r = 0.4 (corresponding to the antikt4 jets), and
  $\rho$ is the average number-density of tracks in events with similar
  multiplicity, as the event containing the jet, evaluated as a function of $\eta$ and $\phi$:
\begin{eqnarray}
\rho(\eta,\phi,\{\nchrec\})=\left\langle\frac{dN^{\mathrm{trk}}}{d\eta d\phi}\right\rangle_{\mathrm{Events}} (\{\nchrec\})
\label{eq:correction2}
\end{eqnarray}









\subsection{Correcting the event multiplicity to account for UE tracks in jets \label{sec:mult_correction}}
The final state particles from an event can be classified into two groups:
  the particles associated with jets and the underlying event particles.
To better represent results of flow features from rejecting particles associated
  with jets, it is appropriate to assign events with similar number of underlying
  event particles -- instead of total number of particles -- into the same multiplicity bin.
To estimate the number of underlying event particles, the number of constituent particles
  in jets with jet $\ptp > 15\GeV$ summed in an event, are removed from the
  measured multiplicity calculation.
The corrected multiplicity is given by:
\begin{eqnarray}
\text{N}_{\text{ch}}^{\text{rec,corr}}&=&\text{N}_{\text{ch}}^{\text{rec}}-\sum_{\mathrm{jets}} N^{\mathrm{constituents}}_{\mathrm{corrected}}  \\
                                  &\equiv&\text{N}_{\text{ch}}-\Delta \text{N}_{\text{ch}}
\label{eq:mult_corr}
\end{eqnarray}
where $N^{\mathrm{constituents}}_{\mathrm{corrected}}$ is defined in Eq.~\ref{eq:correction1}.
All subsequent results are presented using the corrected multiplicity
  given in Eq.~\eqref{eq:mult_corr}.
Figure~\ref{fig:multiplicity_correction_corrected_original} shows the
  distribution of $\Delta \text{N}_{\text{ch}}$ as well as the
  correlation between the corrected and uncorrected event-multiplicity.
In the correlation plot, a set of events is seen to be distributed
  along the diagonal; these are the set of events where there were
  no jets above the \ptp\ threshold.
%A small gap can be seen separating the set of events without jets and
%  the set of events with jets, as once a jet passing the selection
%  requirements is present, the correction $\Delta \text{N}_{\text{ch}}$ is $geq$2.

\begin{figure}[H]
\center{
  \includegraphics[width=0.5\linewidth]{fig_pool_afterHP/methodology/UECorrection/NtrkCorrection_perEvent}%
  \includegraphics[width=0.5\linewidth]{fig_pool_afterHP/methodology/UECorrection/multiplicity_original_corr}
  }
  \caption{
  The left panel shows the distribution of the multiplicity correction
    ($\Delta \text{N}_{\text{ch}}$) from each event.
  The right panel shows the relation between corrected multiplicity
    ($\text{N}_{\text{ch}}^{\text{corr}}$) and original multiplicity
    ($\text{N}_{\text{ch}}$).
  \label{fig:multiplicity_correction_corrected_original}
  }
\end{figure}






\subsection{Reject particles associated with jets\label{sec:jet_track_removal}}
It is not straightforward to remove particles associated with jets from the 2PC analysis
  by simply rejecting all tracks within a R=0.4 cone of the jet axis.
Such a conical rejection would introduce artificial structures along the $\dphi$
  direction in the 1D-2PCs.
Instead, tracks within a $\pm1$ pseudorapidity separation from any
  particle-flow jets with $\ptp > $15~\GeV\ are
  dropped from the 2PC analysis.
This rejection procedure removes slices of the detector acceptance along $\eta$,
  and thus does not introduce any acceptance effects along $\dphi$.

Figure~\ref{fig:carton} demonstrates this procedure.
This cartoon depicts an event with five reconstructed jets (an extreme example where
  in data only 0.8\% events have more than 2 jets.)
Measured charged tracks are shown as colored points within $|\eta| < 2.5$.
Jets A and E do not have measured tracks because they are reconstructed from
  calorimeter particle flow objects outside the ID acceptance.
Jet D also lacks measured tracks and is composed only of particle flow objects.
The yellow bands represent the $\eta$ regions around jets A, B, C, D, and E
  that are rejected.
The blue solid points represent the surviving particles, referred to as "
  blue particles" throughout the rest of the note.
Gray tracks represent rejected particles not used in the two-particle
  correlation, while red tracks indicate the "jet constituents" used in the \JH\ case.
Note that the tracks  within $\pm1\ \eta$ unit of the jet-axis are removed only
  from the 2PCs, not from the multiplicity counting.
  % for the multiplicity counting, only the tracks associated with the jets
  % are removed (Eq.~\eqref{eq:mult_corr}).


\begin{figure}[H]
  \center{
  \includegraphics[width=0.80\linewidth]{fig_pool_afterHP/methodology/ParticleRejectionCartoon}
  }
  \caption{
    Figures demonstrating removal of tracks associated with jets,
      balance jet requirement, and isolation requirement.
    The circles represent pflow jets that have \ptp above 15~\GeV.
    Particles within a $\pm1$ $\eta$ range of any pflow jets
      are removed from the correlation.
    The rejection region is indicated by the yellow bands.
    The unshaded (clear or white) region indicates the $\eta$ region
      from which tracks are used in the correlations analysis.
    In this cartoon, blue solid points are the particles survived
      from the rejection.
    \label{fig:carton}
  }
\end{figure}


\subsection{Jet constituents selections\label{sec:red_particle_selection}}
In this section, selections of jet constituents (``red particles'' described in
  previous section in Figure~\ref{fig:carton}) are presented.
These jet constituents are only used in the the event case of \JH, not in other cases.
Only charged tracks are considered in the two particle correlation procedure.
It is critical to select clean jet constituents when do the correlation
  between jet constituents and particles away from jets.
During a hard scattering process, typically two jets will be produced
  due to the conservation law.
Sometimes, jets might split into multiple low $\pt$ jets.
To get a clean selection of jet constituents, these side cases need
  to be excluded.
Therefore, charged tracks from jets that meet the following criteria are used on the jets:
\begin{enumerate}
    \item $\ptp > 40$ GeV. (The reason of choosing 40 GeV is discussed later in Section~\ref{sec:embed_2pc}).
    \item $|\eta| < 2.1$
    \item It must have a balance jet with $\ptp > 15$ GeV and $|\Delta\phi| > 5\pi/6$.
    \item It must not have any jet $\ptp > 15$ GeV within 1 radial distance.
\end{enumerate}
By requiring the 2nd selection, it is possible to make sure all charged tracks
  of R = 0.4 jets are measured within ID, which is $|\eta| < 2.5$.
In Figure~\ref{fig:carton}, the 3rd selection rule is shown as the green band.
Jet C is considered as a balance jet with jet B.
Jet C is rejected due to D is considered as a neighbor jet around it and failed
  the isolation requirement.
In this cartoon event, only constituents of jet B are used in
  the jet-hadron correlation part. These constituents will be named as
  ``Red particles" in the rest of the note.


