
\section{PYTHIA Embedding \label{sec:embedding}}
% Previous studies have shown that the ``collective flow'' in heavy-ion collisions
%   leads to a significant modulation.
The elliptic flow modulation is a crucial aspect in this analysis.
Jet energies can be greatly affected by this event modulation.
Jets positioned at the crest of the modulation tend to have a relatively higher
  energy compared to those located at the trough (as shown previously in Figure~\ref{fig:modulation_cartoon}).
When picking jet constituents with any \pt\ threshold, the selection is bias by
  the significant underlying event modulation.
Such bias becomes small as the jet \pt\ becoming high.
To study the effect of underlying event modulation, a \PYTHIA\ 8 truth event
  embedding study is performed.
Monash 2013 settings with MPI off and ISR on are used to produce truth hard QCD
  PYTHIA events.
50 million PYTHIA 8 events are generated.
In this case, truth events are also filtered at truth level with requiring a jet
  with \pt\ greater than 15 GeV and must have a balance jet with \pt\ greater
  than 10 GeV and $|\dphi| > 5\pi/6$.
Then truth events are embedded into data events that passing trigger-group-1
  and trigger-group-2 and the same selections from all 4 years with information
  of charge and neutral pflow objects.
Each data event is embedded by a random PYTHIA 8 event.
About 350 million data events are used for the embedding.
Jets are reconstructed from all particles with the anti-$k_{\mathrm{T}}$
  clustering algorithm with a distance parameter of 0.4.
Embedding study follows the same jet selections as described in
  Section~\ref{sec:red_particle_selection} with two additional requirements:
\begin{enumerate}
    \item Reconstructed jets must near a truth jet location.
    \item Reconstructed jets do not overlap with any data jets with \pt\ > 15 GeV.
\end{enumerate}
Figure~\ref{fig:embedding_cartoon} demonstrates a general embedding events.
Truth purple particles are contaminated by underlying event's green and orange
  particles.
The final reconstructed jet energy is larger than the truth jet \pt.
And the additional amount of energy is modulated from the underlying event.

\begin{figure}[H]
  \center{
    \includegraphics[width=0.5\linewidth]{fig_pool_afterHP/embedding/embedding_cartoon}
  }
  \caption{
    A cartoon demonstrates embedding procedure. Green dots indicate neutral
      pflow objects. Orange dots indicate charge pflow objects. And purple dots
      indicate truth PTYHIA particles. Red big circles indicate reconstructed
      jets.
  }
  \label{fig:embedding_cartoon}
\end{figure}


To visualize the underlying event modulation, one can plot the difference
  between the reconstructed jet $\phi$ and the event plane angle calculated with
  underlying event particles.
Since PYTHIA 8 events are fully independent from data events, the distribution of
  such $\phi - \Psi_2$ should be flat if we select directly from truth jets, as shown
  in Figure~\ref{fig:embed_truthJetPhiToPsi2}.
\begin{figure}[H]
  \center{
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/TruthJetOnly/can_h2_tjet_noCut_dPT_dphiToPsi2_1st_projy_cent10_30}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/TruthJetOnly/can_h2_tjet_noCut_dPT_dphiToPsi2_1st_projy_cent70_80}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/TruthJetOnly/can_h2_tjet_noCut_dPT_dphiToPsi2_1st_projy_cent110_120}
  }
  \caption{
    Distribution of the difference between truth jet $\phi$ and event plane angle
      $\Psi_2$.
    Different panel is from different multiplicity.
  }
  \label{fig:embed_truthJetPhiToPsi2}
\end{figure}



However, when considering data particles with truth particles together during
  the jet reconstruction, the distribution of the difference modulated as shown in
  the bottom row of Figure~\ref{fig:embed_recoJetPhiToPsi2_oldPt}.
\begin{figure}[H]
  \center{
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_OldPt/can_1D_cent10_30_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig0_pileup5_trkqual1_rejME1_jet4_jc9_jetcut4_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_OldPt/can_1D_cent70_80_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig0_pileup5_trkqual1_rejME1_jet4_jc9_jetcut4_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_OldPt/can_1D_cent110_120_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig0_pileup5_trkqual1_rejME1_jet4_jc9_jetcut4_ifHaveJet2_ifUECorr0_corrtype1_multtype0}
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_OldPt/can_h2_jet_OlIsoBlnc_dPT_dphiToDataPsi2_1st_projy_cent10_30}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_OldPt/can_h2_jet_OlIsoBlnc_dPT_dphiToDataPsi2_1st_projy_cent70_80}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_OldPt/can_h2_jet_OlIsoBlnc_dPT_dphiToDataPsi2_1st_projy_cent110_120}
  }
  \caption{
    Top row shows the 1D2PC using old jet \pt\ definition.
    Bottom row shows the distribution of the difference between reconstructed
      jet $\phi$ using data and truth particle together and event plane angle
      $\Psi_2$ calculated only using data particles and requiring them to be
      away from any jets.
  }
  \label{fig:embed_recoJetPhiToPsi2_oldPt}
\end{figure}




\subsection{PYTHIA Embedding: Modulation correction using event plane angle and Q-vec}
One ideal way to deal with the modulation is using the event plane angle and size of
  v2 to correct it event by event.
Using Q vectors from particles away from jets:
\begin{equation}
\left\langle Q_{x}^{\mathrm{blue}} \right\rangle = \frac{\Sigma p_{\mathrm{T}}^{i}\cos2\phi^{i}}{\Sigma p_{\mathrm{T}}^{i}},
\left\langle Q_{y}^{\mathrm{blue}} \right\rangle = \frac{\Sigma p_{\mathrm{T}}^{i}\sin2\phi^{i}}{\Sigma p_{\mathrm{T}}^{i}}
\end{equation}

\begin{equation}
\Psi_{2}^{\mathrm{blue}} = \frac{1}{2}\arctan\left(\left\langle Q_{y}^{\mathrm{blue}} \right\rangle, \left\langle Q_{x}^{\mathrm{blue}} \right\rangle \right),
v_{2}^{\mathrm{blue}} = \sqrt{\left\langle Q_{y}^{\mathrm{blue}} \right\rangle^2 + \left\langle Q_{x}^{\mathrm{blue}} \right\rangle^2}
\end{equation}

Then the jet \pt\ modulation correction is calculated as:
\begin{equation}
\begin{aligned}
p_{\mathrm{T}}^{\mathrm{jet,corrected}} &=  p_{\mathrm{T}}^{\mathrm{jet,orig}} - \Delta p_{\mathrm{T}}^{\mathrm{jet}} \\
&= p_{\mathrm{T}}^{\mathrm{jet,orig}} - \Delta p_{\mathrm{T}}^{\mathrm{jet}}(\eta,\phi) \cdot \left(1 + 2v_{2}^{\mathrm{blue}}\cos\left(2\left(\phi^{\mathrm{jet}} - \Psi_{2}^{\mathrm{blue}}\right)\right)\right)
\end{aligned}
\end{equation}

where $\Delta p_{\mathrm{T}}^{\mathrm{jet}}(\eta,\phi)$ is obtained with the
  same method as described in Section~\ref{sec:ue_corr}.
However, after applying modulation correction from this method, the modulation
  effect is not fully reduced, as shown in Figure~\ref{fig:embedding_modulation_corr1}.
\begin{figure}[H]
  \center{
    % \includegraphics[width=0.8\linewidth]{fig_pool_afterHP/embedding/modulation_corr_EPA_5060}
    % \includegraphics[width=0.8\linewidth]{fig_pool_afterHP/embedding/modulation_corr_EPA}
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_ModCorr/can_h2_jet_OlIsoBlnc_dPT_dphiToDataPsi2_1st_projy_cent10_30}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_ModCorr/can_h2_jet_OlIsoBlnc_dPT_dphiToDataPsi2_1st_projy_cent70_80}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_ModCorr/can_h2_jet_OlIsoBlnc_dPT_dphiToDataPsi2_1st_projy_cent110_120}
  }
  \caption{
    $\Delta\phi$ between reconstructed jet $\phi$ and event plane angle $\Psi_{2}$
      calculated only using data particles and requiring them to be away from
      any jets.
    % Top rows corresponds to multiplicity of 50--60.
    % Bottom row corresponds to multiplicity of 100--120.
  }
  \label{fig:embedding_modulation_corr1}
\end{figure}

This is understood to be due to lack of resolution of event plane angle as
  shown in Figure~\ref{fig:embedding_EPA_res}.
\begin{figure}[H]
  \center{
    \includegraphics[width=0.5\linewidth]{fig_pool_afterHP/embedding/event_plane_resolution}
  }
  \caption{
    Event plane angle resolution. Event plane angle is calculated using data
      particles only and requiring 1 unit away from any jets in $\eta$.
  }
  \label{fig:embedding_EPA_res}
\end{figure}



\subsection{PYTHIA Embedding: Modulation correction using grooming tool}
Another method is suggested by using grooming tool from Fastjet package.
A SoftDrop is applied in this method with z\_cut = 0.1 and $\beta = 0$.
Figure~\ref{fig:embed_grooming} shows the $\phi - \Psi_2$ with grooming.
Modulation is not reduced by grooming.
\begin{figure}[H]
  \center{
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_Grooming/can_1D_cent10_30_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig0_pileup5_trkqual1_rejME1_jet4_jc9_jetcut4_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_Grooming/can_1D_cent70_80_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig0_pileup5_trkqual1_rejME1_jet4_jc9_jetcut4_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_Grooming/can_1D_cent110_120_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig0_pileup5_trkqual1_rejME1_jet4_jc9_jetcut4_ifHaveJet2_ifUECorr0_corrtype1_multtype0}
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_Grooming/can_h2_jet_OlIsoBlnc_dPT_dphiToDataPsi2_1st_projy_cent10_30}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_Grooming/can_h2_jet_OlIsoBlnc_dPT_dphiToDataPsi2_1st_projy_cent70_80}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_Grooming/can_h2_jet_OlIsoBlnc_dPT_dphiToDataPsi2_1st_projy_cent110_120}
  }
  \caption{
    Distribution of the difference between reconstructed jet $\phi$ using data
      and truth particle together and event plane angle $\Psi_2$
      calculated only using data particles and requiring them to be away from
      any jets.
    Grooming is applied using SoftDrop with z\_cut = 0.1 and $\beta = 0$.
    The modulation is not reduced
  }
  \label{fig:embed_grooming}
\end{figure}





\subsection{PYTHIA Embedding: Selecting jets based on the energy from high \pt\ constituents \label{sec:embed_2345GeV}}
Since the modulation effect are dominated by low \pt\ particles from the soft
  collectivity process, one can select high \pt\ particles to reduce the
  contamination from low \pt\ particles.
A groomed jet \pt\ definition is proposed here, by summing particles with \pt\
  greater than a certain amount:
\begin{equation}
p_{\mathrm{T}}^{\mathrm{G}} = \sum p_{\mathrm {T}}^{> x\ \mathrm{GeV}}
\end{equation}
The X GeV threshold is only used for the groomed jet \pt\ definition, not for
  any other selections in this analysis.
It only used to select jets for the two-particle correlation study.
When constructing pairs for 2PC, all \pt\ particles within selected jets are used.
Figure~\ref{fig:embedding_sum_abv_2345} shows $\phi - \Psi_2$ using different
  minimum \pt\ particles.
% the azimuthal angle of jets
%   relative to the measured event plane calculated by selecting
%   jet with \ptp\ above 25 GeV.
The modulation effect is well reduced when summing constituents with \pt\
  greater than 4 GeV.
This also removes auto-correlation effects as particles used in 2PC are
  less than 4 GeV, which is different from the particles used in this groomed
  jet \pt\ definition.
\begin{figure}[H]
  \center{
    \includegraphics[width=0.95\linewidth]{fig_pool_afterHP/embedding/sum_above_2345_5060}
    \includegraphics[width=0.95\linewidth]{fig_pool_afterHP/embedding/sum_above_2345}
    % \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_NewJetPt/jet4099_abv2/can_h2_jet_dPT_dphiToDataPsi2_1st_projy_cent50_60}%
    % \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_NewJetPt/jet4099_abv3/can_h2_jet_dPT_dphiToDataPsi2_1st_projy_cent50_60}%
    % \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_NewJetPt/jet4099_abv4/can_h2_jet_dPT_dphiToDataPsi2_1st_projy_cent50_60}
    % \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_NewJetPt/jet4099_abv2/can_h2_jet_dPT_dphiToDataPsi2_1st_projy_cent100_120}%
    % \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_NewJetPt/jet4099_abv3/can_h2_jet_dPT_dphiToDataPsi2_1st_projy_cent100_120}%
    % \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/JetPhiToPsi/RecoJet_NewJetPt/jet4099_abv4/can_h2_jet_dPT_dphiToDataPsi2_1st_projy_cent100_120}
  }
  \caption{
    The distribution of the azimuthal angle of jets relative to the event plane.
    Plots are self normalized.
    From left to right shows \ptp\ by summing constituents with \pt\ greater than 2, 3, 4 GeV.
    Top rows corresponds to multiplicity of 50--60.
    Bottom row corresponds to multiplicity of 100--120.
  }
  \label{fig:embedding_sum_abv_2345}
\end{figure}






\subsection{PYTHIA Embedding: Two particle correlation \label{sec:embed_2pc}}
Another way to visualize the modulation effect is by analyzing 1D2PCs.
Suppose there is no modulation in the underlying event, the particles distribution
  along $\phi$ would be flat.
Since, PYTHIA truth events are fully independent from data events, when
  correlating truth jet constituents with flat distributed particles, the 1D2PC
  would be a delta shape ideally at away side (around $\pi$) and flat at near
  side (around 0).
If there is modulation in the underlying event, the particles distribution would
  be sinusoidal like, and contribute a near side ridge in 1D2PC.
Figure~\ref{fig:embed_1d2pc_2599} shows 1D2PCs with jet selections to be
  $25 < \ptp\ < 100$ GeV.
No near side ridge is seen in most of multiplicity ranges.
A set of similar plots using old jet \pt\ is included in Appendix~\ref{sec:embed1d2pcOldJetPt}.

\begin{figure}[H]
  \center{
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet2599/can_1D_cent10_30_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut2_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet2599/can_1D_cent40_50_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut2_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet2599/can_1D_cent50_60_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut2_ifHaveJet2_ifUECorr0_corrtype1_multtype0}
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet2599/can_1D_cent60_70_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut2_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet2599/can_1D_cent70_80_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut2_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet2599/can_1D_cent80_90_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut2_ifHaveJet2_ifUECorr0_corrtype1_multtype0}
  }
  \caption{
    Two-particle correlations in $\dphi$ or 0.5~\GeV $<\ptab <4$~\GeV
      and $2<|\deta|<5$.
    Each panel shows the correlation function for a different multiplicity
      interval.
    The plots correspond to the  \JH\ case (see Section~\ref{sec:event_sets}).
    Jet selections require \ptp\ to be greater than 25 GeV and less than 100 GeV.
  }
  \label{fig:embed_1d2pc_2599}
\end{figure}


For better understanding of the contribution with respect to different jet \ptp\
  threshold, Figure~\ref{fig:embed_1d2pc_2530} - Figure~\ref{fig:embed_1d2pc_4099}
  show 1D2PC with different jet \ptp\ requirements.
When selecting jet with $25 < \ptp\ < 30$ GeV and $30 < \ptp\ < 40$ GeV, there
  are still some visible ridge at near side.
In the end, jet selections with $40 < \ptp\ < 100$ is used to reduce the
  modulation effect of underlying events.
A set of similar plots using old jet \pt\ is included in Appendix~\ref{sec:embed1d2pcOldJetPt}.

\begin{figure}[H]
  \center{
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet2530/can_1D_cent10_30_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut6_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet2530/can_1D_cent40_50_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut6_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet2530/can_1D_cent50_60_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut6_ifHaveJet2_ifUECorr0_corrtype1_multtype0}
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet2530/can_1D_cent60_70_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut6_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet2530/can_1D_cent70_80_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut6_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet2530/can_1D_cent80_90_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut6_ifHaveJet2_ifUECorr0_corrtype1_multtype0}
  }
  \caption{
    Two-particle correlations in $\dphi$ or 0.5~\GeV $<\ptab <4$~\GeV
      and $2<|\deta|<5$.
    Each panel shows the correlation function for a different multiplicity
      interval.
    The plots correspond to the  \JH\ case (see Section~\ref{sec:event_sets}).
    Jet selections require \ptp\ to be greater than 25 GeV and less than 30 GeV.
  }
  \label{fig:embed_1d2pc_2530}
\end{figure}

\begin{figure}[H]
  \center{
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet3040/can_1D_cent10_30_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut7_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet3040/can_1D_cent40_50_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut7_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet3040/can_1D_cent50_60_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut7_ifHaveJet2_ifUECorr0_corrtype1_multtype0}
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet3040/can_1D_cent60_70_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut7_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet3040/can_1D_cent70_80_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut7_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet3040/can_1D_cent80_90_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig5_pileup5_trkqual1_rejME1_jet4_jc9_jetcut7_ifHaveJet2_ifUECorr0_corrtype1_multtype0}
  }
  \caption{
    Two-particle correlations in $\dphi$ or 0.5~\GeV $<\ptab <4$~\GeV
      and $2<|\deta|<5$.
    Each panel shows the correlation function for a different multiplicity
      interval.
    The plots correspond to the  \JH\ case (see Section~\ref{sec:event_sets}).
    Jet selections require \ptp\ to be greater than 30 GeV and less than 40 GeV.
  }
  \label{fig:embed_1d2pc_3040}
\end{figure}

\begin{figure}[H]
  \center{
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet4099/can_1D_cent10_30_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig3_pileup5_trkqual1_rejME1_jet4_jc9_jetcut4_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet4099/can_1D_cent40_50_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig3_pileup5_trkqual1_rejME1_jet4_jc9_jetcut4_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet4099/can_1D_cent50_60_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig3_pileup5_trkqual1_rejME1_jet4_jc9_jetcut4_ifHaveJet2_ifUECorr0_corrtype1_multtype0}
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet4099/can_1D_cent60_70_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig3_pileup5_trkqual1_rejME1_jet4_jc9_jetcut4_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet4099/can_1D_cent70_80_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig3_pileup5_trkqual1_rejME1_jet4_jc9_jetcut4_ifHaveJet2_ifUECorr0_corrtype1_multtype0}%
    \includegraphics[width=0.3\linewidth]{fig_pool_afterHP/embedding/1D2PC/jet4099/can_1D_cent80_90_pta10_ptb5_40_ch2_eta1_Data_pp_mu5_eff1_trig3_pileup5_trkqual1_rejME1_jet4_jc9_jetcut4_ifHaveJet2_ifUECorr0_corrtype1_multtype0}
  }
  \caption{
    Two-particle correlations in $\dphi$ or 0.5~\GeV $<\ptab <4$~\GeV
      and $2<|\deta|<5$.
    Each panel shows the correlation function for a different multiplicity
      interval.
    The plots correspond to the  \JH\ case (see Section~\ref{sec:event_sets}).
    Jet selections require \ptp\ to be greater than 40 GeV and less than 100 GeV.
  }
  \label{fig:embed_1d2pc_4099}
\end{figure}

