
\section{Systematic uncertainties and cross-checks\label{sec:Systematics}}

In this section the systematic uncertainties associated with the
  template fit \vn-measurements are discussed.
The uncertainties are independently evaluated for the \inclusive, \AllEvents,
  \NoJet, \WithJet\ and \JH\ case.
Four sources are considered: tracking efficiency variations, tracking cuts,
  event-mixing, and peripheral reference choice.
Relative errors are used for \inclusive, \AllEvents, \NoJet\ and \WithJet\ cases.
Absolute errors are used for \JH\ case.
Smoothing is done for \WithJet\ case.
For the ratios, correlated uncertainties are cancelled out.


\subsection{Systematics summary}
Table~\ref{tab:sysErrTable} summarizes the final systematic uncertainties.
\begin{table}[H]
\begin{center}
\begin{tabular}{|l |c |c |c |c |c |}
\hline
systematic sources    & h-h           & AllEvts       & NoJetEvts     & WithJetEvts  & h-jet constituents \\
\hline
Tracking efficiency   & 0.3\%         & 0.3\%         & 0.3\%         & 0.6\%        & 0.0028             \\
\hline
Tracking cuts         & 0.2\%--0.6\%  & 0.2\%--0.6\%  & 0.2\%--0.6\%  & 0.7\%        & 0.005              \\
\hline
event-mixing          & 0.05\%--0.3\% & 0.02\%--0.2\% & 0.02\%--0.2\% & 1.0\%        & 0.008              \\
\hline
pile-up               & 0.3\%         & 0.3\%--3.0\%  & 0.3\%--3.0\%  & 0.3\%--3.0\% & 0.003              \\
\hline
peripheral reference  & 1.4\%--0.3\%  & 1.7\%--0.3\%  & 1.8\%--0.3\%  & 2.6\%        & 0.01               \\
\hline
\end{tabular}
\end{center}
\caption{
Summary of systematic uncertainties.
For ``h-h'', ``AllEvts'', ``NoJetEvts'', and ``WithJetEvts'', relative errors are used.
Column 2 to 5 shows relative errors.
Column 6 shows absolute errors.
% For ``h-jet constituents'', absolute errors are used.
\label{tab:sysErrTable}
}
\end{table}


\subsection{Tracking efficiency variations\label{sec:trkEffSys}}
In this analysis, tracking efficiency is evaluated as a function of
  \pt, $\eta$ and centrality.
Previous flow measurements have shown that $v_n$ strongly depends on
  \pt: it increases then decreases as \pt\ increases.
Meanwhile, differential flow measurements also shows that $v_n$ is
  weakly dependent of $\eta$.
Due to these reasons, the \pt\ weighting in tracking efficiency
  could introduce uncertainty to the results.

To evaluate the impact of the uncertainty in the tracking efficiency on
  the final \vn, the following variations are performed in the efficiency:
\begin{itemize}
\item Default $\epsilon$: particles weighted by tracking efficiency;
%\item Check: particles not applying tracking efficiency;
\item Check :  efficiency $\epsilon_{\mathrm{var}}$: the tracking efficiency
       at high \pt\ is increased to its maximum within uncertainty;
       while the tracking efficiency at low \pt\ is decreased to its
       minimum within uncertainty;
% \item Check 2 lower efficiency $\epsilon_-$: tracking efficiency
%       at high \pt\ is decreased to its minimum within uncertainty;
%       while the tracking efficiency at low \pt\ is increased to its
%       maximum within uncertainty;
\end{itemize}
where the variation can be parameterized as:
\begin{equation}
\epsilon_{\mathrm{var}}(\pT) \equiv \epsilon(\pT) + 0.06\frac{\epsilon(\pT)-\epsilon(\pT^\text{low})}{\epsilon(\pT^\text{high})-\epsilon(\pT^\text{low})} - 0.03
 \end{equation}
where $p_\text{T}^\text{low}$ is 0.5~\GeV while $p_\text{T}^\text{high}$ is 5.0~\GeV.
This formulation for the systematic variation for the efficiency
  is used in multiple previous correlation analyses~\cite{HION-2016-01,HION-2016-06,HION-2015-09},
  and results in an efficiency that is reduced by 3\% at the lower-\pt\
  and increased by 3\% at the higher-\pt, 3\% being the uncertainty in
  the overall tracking efficiency.
Figure ~\ref{fig:sysErr_trackEff_multdept} compares the \vn\ for the alternated
  and default tracking efficiencies and no track efficiency for 0.5--4.0~\GeV\
  \pt\ interval for the all cases.
Figure~\ref{fig:sysErr_trackEff_ptbdept} shows a similar comparison for \ptb\
  dependence.

\begin{figure}[H]
  \center{
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackEffSystematic/can_v2_multDept_TrkEff_sys0}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackEffSystematic/can_v2_multDept_TrkEff_sys1}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackEffSystematic/can_v2_multDept_TrkEff_sys2}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackEffSystematic/can_v2_multDept_TrkEff_sys3}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackEffSystematic/can_v2_multDept_TrkEff_sys4}
  }
  \caption{
    Comparison of template fit \vn\ obtained with different tracking efficiency conditions.
    The figures from top to bottom are using event set of \inclusive, \AllEvents,
      \NoJet, \WithJet, and \JH.
    The plots are for multiplicity dependence with 0.5--4.0~\GeV\ \pt\ interval.
    The middle panels show the ratio of the \vn\ obtained with track efficiency
      condition to the case without track efficiency.
    The right panels show the difference of the \vn\ obtained with track efficiency
      condition to the case without track efficiency.
    For clarity, statistical uncertainties are not plotted on the ratio plots.
  }
  \label{fig:sysErr_trackEff_multdept}
\end{figure}

\begin{figure}[H]
  \center{
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackEffSystematic/can_v2_ptDept_ptb60_cent55_TrkEff_sys0}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackEffSystematic/can_v2_ptDept_ptb60_cent55_TrkEff_sys1}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackEffSystematic/can_v2_ptDept_ptb60_cent55_TrkEff_sys2}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackEffSystematic/can_v2_ptDept_ptb60_cent55_TrkEff_sys3}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackEffSystematic/can_v2_ptDept_ptb60_cent55_TrkEff_sys4}
  }
  \caption{
    Comparison of template fit \vn\ obtained with different tracking efficiency conditions.
    The figures from top to bottom are using event set of \inclusive, \AllEvents,
      \NoJet, \WithJet, and \JH.
    The plots are for \ptb\ dependence with 40-150 multiplicity interval.
    The middle panels show the ratio of the \vn\ obtained with track efficiency
      condition to the case without track efficiency.
    The right panels show the difference of the \vn\ obtained with track efficiency
      condition to the case without track efficiency.
    For clarity, statistical uncertainties are not plotted on the ratio plots.
  }
  \label{fig:sysErr_trackEff_ptbdept}
\end{figure}









\newpage
\subsection{Dependence of tracking cuts\label{sec:sysErrorTrkCut}}
By varying tracking cuts the balance between the real and fake tracks can be
  modified in the sample.
For that purpose a different set of cuts were used for tracks.
The alternated case is similar to the the selections mentioned in Section~\ref{sec:Track_selections}
  but with some modifications:
\begin{enumerate}
    \item $|d_{0}|$ w.r.t beam-line $<$1.0 mm.
    \item $|z_{0}\times\sin(\theta)|$ w.r.t. primary-vertex $<$1.0 mm.
\end{enumerate}
Figure ~\ref{fig:sysErr_trackQual_multdept} compares the \vn\ for the alternated
  and default tracking cuts and no track efficiency for 0.5--4.0~\GeV\
  \pt\ interval for the all cases.
Figure~\ref{fig:sysErr_trackQual_ptbdept} shows a similar comparison for \ptb\
  dependence.

\begin{figure}[H]
  \center{
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackQualSystematic/can_v2_multDept_TrkQual_sys0}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackQualSystematic/can_v2_multDept_TrkQual_sys1}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackQualSystematic/can_v2_multDept_TrkQual_sys2}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackQualSystematic/can_v2_multDept_TrkQual_sys3}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackQualSystematic/can_v2_multDept_TrkQual_sys4}
  }
  \caption{
    Comparison of template fit \vn\ obtained with different tracking cut conditions.
    The figures from top to bottom are \inclusive, \AllEvents, \NoJet, \WithJet, and \JH.
    The plots are for multiplicity dependence with 0.5--4.0~\GeV\ \pt\ interval.
    The middle panels show the ratio.
    The right panels show the difference.
    For clarity, statistical uncertainties are not plotted on the ratio plots.
  }
  \label{fig:sysErr_trackQual_multdept}
\end{figure}

\begin{figure}[H]
  \center{
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackQualSystematic/can_v2_ptDept_ptb60_cent55_TrkQual_sys0}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackQualSystematic/can_v2_ptDept_ptb60_cent55_TrkQual_sys1}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackQualSystematic/can_v2_ptDept_ptb60_cent55_TrkQual_sys2}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackQualSystematic/can_v2_ptDept_ptb60_cent55_TrkQual_sys3}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/TrackQualSystematic/can_v2_ptDept_ptb60_cent55_TrkQual_sys4}
  }
  \caption{
    Comparison of template fit \vn\ obtained with different tracking cut conditions.
    The figures from top to bottom are using event set of \inclusive, \AllEvents,
      \NoJet, \WithJet, and \JH.
    The plots are for \ptb\ dependence with 40-150 multiplicity interval.
    The middle panels show the ratio of the \vn\ obtained with alternated to the
      default case.
    The right panels show the difference of the \vn\ obtained with alternated to the
      default case.
    For clarity, statistical uncertainties are not plotted on the ratio plots.
  }
  \label{fig:sysErr_trackQual_ptbdept}
\end{figure}









\newpage
\subsection{Event-mixing}
Previous analysis uses the event-mixing procedure to account for the
  pair-acceptance effects.
It is seen that mixed event curves shown a relative flat distribution comparing
  with foreground plots in Section~\ref{sec:fgbgcheck}.
Therefore, event-mixing is not performed in the final result, but as part of
  systematic uncertainties.
The systematic uncertainties associated with the event-mixing correction
  are conservatively estimated by not applying the event-mixing correction
  vs when using the mixing variation in the \vn\.
Figure~\ref{fig:sysErr_evtmixing_multdept} compares the
  \vn\ for different event mixing for 0.5--4.0~\GeV\ \pt\ interval
  for multiplicity dependence.
Figure~\ref{fig:sysErr_evtmixing_ptbdept} shows similar plots
  for \ptb\ dependence.

\begin{figure}[H]
  \center{
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/EvtMixing/can_v2_multDept_EvtMixing_sys0}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/EvtMixing/can_v2_multDept_EvtMixing_sys1}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/EvtMixing/can_v2_multDept_EvtMixing_sys2}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/EvtMixing/can_v2_multDept_EvtMixing_sys3}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/EvtMixing/can_v2_multDept_EvtMixing_sys4}
  }
  \caption{
    Comparison of template fit \vn\ obtained with different event mixing conditions.
    The figures from top to bottom are using event set of \inclusive, \AllEvents,
      \NoJet, \WithJet, and \JH.
    The plots are for multiplicity dependence with 0.5--4.0~\GeV\ \pt\ interval.
    The middle panels show the ratio of the \vn\ obtained with the event mixing
      procedure to without event mixing procedure.
    The right panels show the difference of the \vn\ obtained with the event mixing
      procedure to without event mixing procedure.
    For clarity, statistical uncertainties are not plotted on the ratio plots.
  }
  \label{fig:sysErr_evtmixing_multdept}
\end{figure}

\begin{figure}[H]
  \center{
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/EvtMixing/can_v2_ptDept_ptb60_cent55_EvtMixing_sys0}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/EvtMixing/can_v2_ptDept_ptb60_cent55_EvtMixing_sys1}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/EvtMixing/can_v2_ptDept_ptb60_cent55_EvtMixing_sys2}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/EvtMixing/can_v2_ptDept_ptb60_cent55_EvtMixing_sys3}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/EvtMixing/can_v2_ptDept_ptb60_cent55_EvtMixing_sys4}
  }
  \caption{
    Comparison of template fit \vn\ obtained with different event mixing conditions.
    The figures from top to bottom are event sets of \inclusive, \AllEvents,
      \NoJet, \WithJet, and \JH.
    The plots are for \ptb\ dependence with 40-150 multiplicity interval.
    The middle panels show the ratio of the \vn\ obtained with the event mixing
      procedure to without event mixing procedure.
    The right panels show the difference of the \vn\ obtained with the event mixing
      procedure to without event mixing procedure.
    For clarity, statistical uncertainties are not plotted on the ratio plots.
  }
  \label{fig:sysErr_evtmixing_ptbdept}
\end{figure}





\newpage
\subsection{Pile-up rejections}
To account for pile-up effect, events with more than one reconstructed vertex
  are rejected from the analysis.
Additionally, the analysis also requires that the standard deviation of the
  $|z_{0}\times\sin(\theta)|$ distribution of the tracks in an event be smaller
  than 0.25 mm.
This additional selection was detailed in Section~\ref{sec:evt_selection}
Conservatively, the difference in the $v_2$ when not applying this additional
  selection is used for this systematic uncertainty.
Figure~\ref{fig:sysErr_pileup_multdept} compares the $v_2$ with and without this
  requirement on the $|z_{0}\times\sin(\theta)|$ for the 0.5–4.0~\GeV\ \pt\
  interval as a function of multiplicity.
Figure~\ref{fig:sysErr_pileup_ptbdept} shows similar plots for the \ptb\ dependence.
This uncertainty is taken to be identical for the AllEvents, NoJet and WithJet
  cases, since these use the same set of events.
For this reason Figures~\ref{fig:sysErr_pileup_multdept}--\ref{fig:sysErr_pileup_ptbdept}
  do not show the NoJet and WithJet case.


\begin{figure}[H]
  \center{
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/pileupSystematic/can_v2_multDept_z0RMS_sys0}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/pileupSystematic/can_v2_multDept_z0RMS_sys1}
    % \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/pileupSystematic/can_v2_multDept_z0RMS_sys2}
    % \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/pileupSystematic/can_v2_multDept_z0RMS_sys3}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/pileupSystematic/can_v2_multDept_z0RMS_sys4}
  }
  \caption{
    Comparison of template fit \vn\ obtained with different pileup conditions.
    The figures from top to bottom are using event set of \inclusive, \AllEvents, and \JH.
    The plots are for multiplicity dependence with 0.5--4.0~\GeV\ \pt\ interval.
    The middle panels show the ratio.
    The right panels show the difference.
    For clarity, statistical uncertainties are not plotted on the ratio plots.
  \label{fig:sysErr_pileup_multdept}
  }
\end{figure}

\begin{figure}[H]
  \center{
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/pileupSystematic/can_v2_ptDept_ptb60_cent55_z0RMS_sys0}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/pileupSystematic/can_v2_ptDept_ptb60_cent55_z0RMS_sys1}
    % \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/pileupSystematic/can_v2_ptDept_ptb60_cent55_z0RMS_sys2}
    % \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/pileupSystematic/can_v2_ptDept_ptb60_cent55_z0RMS_sys3}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/pileupSystematic/can_v2_ptDept_ptb60_cent55_z0RMS_sys4}
  }
  \caption{
    Comparison of template fit \vn\ obtained with different pileup conditions.
    The figures from top to bottom are using event set of \inclusive, \AllEvents, and \JH.
    The plots are for \ptb\ dependence with 40-150 multiplicity interval.
    The middle panels show the ratio of the \vn\ obtained with the event mixing
    The middle panels show the ratio.
    The right panels show the difference.
    For clarity, statistical uncertainties are not plotted on the ratio plots.
  \label{fig:sysErr_pileup_ptbdept}
  }
\end{figure}






\newpage
\subsection{Peripheral reference variations}
Results obtained from template fits depend on the multiplicity interval
  used to construct the peripheral references.
By varying the multiplicity interval used for evaluating peripheral
  references, we can estimate the systematic uncertainties arising
  from any breakdown of the assumptions used template fitting method.
%In most cases there are not much variations in the template-\vn\
%  over the 0--50$\%$ centrality range when changing the peripheral reference.
%However in peripheral events the variations grow significantly with centrality.
This variation in the template-\vn\ is included as the uncertainty associated with
  the template-fit procedure.
%In most cases systematic the uncertainties in central events are around
%  1--2$\%$ in $v_{2}$ and $v_{3}$, while in $v_{4}$ and $v_{5}$, the
%  uncertainties are larger.
This uncertainty is largest for the low-multiplicity events, and decreases
  systematically for higher multiplicities.

Figure~\ref{fig:sysErr_periphbin_multdept} compares the \vn\ for different
  peripheral reference bins for 0.5--4.0~\GeV\ \pt\ interval
  for multiplicity dependence.
Similar plots for \ptb\ dependence are shown in Figures~\ref{fig:sysErr_periphbin_ptbdept}.

\begin{figure}[H]
  \center{
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/PeriphBinSystematic/can_v2_multDept_PeriphBin_sys0}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/PeriphBinSystematic/can_v2_multDept_PeriphBin_sys1}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/PeriphBinSystematic/can_v2_multDept_PeriphBin_sys2}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/PeriphBinSystematic/can_v2_multDept_PeriphBin_sys3}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/PeriphBinSystematic/can_v2_multDept_PeriphBin_sys4}
  }
  \caption{
    Comparison of template fit \vn\ obtained with different peripheral reference bins.
    The figures from top to bottom are \inclusive, \AllEvents, \NoJet, \WithJet, and \JH.
    The plots are for \ptb\ dependence with 40-150 multiplicity interval.
    The middle panels show the ratio.
    The right panels show the difference.
    For clarity, statistical uncertainties are not plotted on the ratio plots.
  }
  \label{fig:sysErr_periphbin_multdept}
\end{figure}

\begin{figure}[H]
  \center{
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/PeriphBinSystematic/can_v2_ptDept_ptb60_PeriphBin_sys0}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/PeriphBinSystematic/can_v2_ptDept_ptb60_PeriphBin_sys1}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/PeriphBinSystematic/can_v2_ptDept_ptb60_PeriphBin_sys2}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/PeriphBinSystematic/can_v2_ptDept_ptb60_PeriphBin_sys3}
    \includegraphics[width=0.82\linewidth]{fig_pool_afterHP/systematics/PeriphBinSystematic/can_v2_ptDept_ptb60_PeriphBin_sys4}
  }
  \caption{
    Comparison of template fit \vn\ obtained with different peripheral reference bins.
    The figures from top to bottom are event sets of \inclusive, \AllEvents,
      \NoJet, \WithJet, and \JH.
    The plots are for multiplicity dependence with 0.5--4.0~\GeV\ \pt\ interval.
    The middle panels show the ratio of the \vn\ obtained with the alternate
      peripheral reference bins to the default peripheral reference bin case (10--30).
    The right panels show the difference of the \vn\ obtained with the alternate
      peripheral reference bins to the default peripheral reference bin case (10--30).
    For clarity, statistical uncertainties are not plotted on the ratio plots.
  }
  \label{fig:sysErr_periphbin_ptbdept}
\end{figure}





\newpage
\subsection{Cross-check: Same-event Mixed-event overlay\label{sec:fgbgcheck}}
Figure~\ref{fig:fgbg_inclusive} - Figure~\ref{fig:fgbg_JH} show 1D2PC
  same-event distribution in black overlay with mixed-event distribution
  as gray points.
Mixed-event distribution is scaled to having the same integral as
  same-event distribution.
The variation on mixed-event distribution is very small relative to
  the modulation of same-event distribution.
Therefore, the mixed-event distribution is no longer used to divide
  the same-event distribution in the Template Fit step.


\begin{figure}[H]
  \center{
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/Inclusive/can_nchDept_1D_fgbg_cent10_30_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/Inclusive/can_nchDept_1D_fgbg_cent40_50_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/Inclusive/can_nchDept_1D_fgbg_cent60_70_pta10_ptb38}
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/Inclusive/can_nchDept_1D_fgbg_cent80_90_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/Inclusive/can_nchDept_1D_fgbg_cent100_110_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/Inclusive/can_nchDept_1D_fgbg_cent110_120_pta10_ptb38}
  }
  \caption{
    Same-event Mixed-event overlay in \inclusive\ case. Black points are fg distribution. Gray points are bg distribution.
  }
  \label{fig:fgbg_inclusive}
\end{figure}

\begin{figure}[H]
  \center{
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/AllEvents/can_nchDept_1D_fgbg_cent10_30_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/AllEvents/can_nchDept_1D_fgbg_cent40_50_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/AllEvents/can_nchDept_1D_fgbg_cent60_70_pta10_ptb38}
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/AllEvents/can_nchDept_1D_fgbg_cent80_90_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/AllEvents/can_nchDept_1D_fgbg_cent100_110_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/AllEvents/can_nchDept_1D_fgbg_cent110_120_pta10_ptb38}
  }
  \caption{
    Same-event Mixed-event overlay in \AllEvents\ case. Black points are fg distribution. Gray points are bg distribution.
  }
  \label{fig:fgbg_AllEvents}
\end{figure}

\begin{figure}[H]
  \center{
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/NoJets/can_nchDept_1D_fgbg_cent10_30_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/NoJets/can_nchDept_1D_fgbg_cent40_50_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/NoJets/can_nchDept_1D_fgbg_cent60_70_pta10_ptb38}
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/NoJets/can_nchDept_1D_fgbg_cent80_90_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/NoJets/can_nchDept_1D_fgbg_cent100_110_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/NoJets/can_nchDept_1D_fgbg_cent110_120_pta10_ptb38}
  }
  \caption{
    Same-event Mixed-event overlay in \NoJet\ case. Black points are fg distribution. Gray points are bg distribution.
  }
  \label{fig:fgbg_NoJet}
\end{figure}

\begin{figure}[H]
  \center{
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/WithJets/can_nchDept_1D_fgbg_cent10_30_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/WithJets/can_nchDept_1D_fgbg_cent40_50_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/WithJets/can_nchDept_1D_fgbg_cent60_70_pta10_ptb38}
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/WithJets/can_nchDept_1D_fgbg_cent80_90_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/WithJets/can_nchDept_1D_fgbg_cent100_110_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/WithJets/can_nchDept_1D_fgbg_cent110_120_pta10_ptb38}
  }
  \caption{
    Same-event Mixed-event overlay in \WithJet\ case. Black points are fg distribution. Gray points are bg distribution.
  }
  \label{fig:fgbg_WithJet}
\end{figure}

\begin{figure}[H]
  \center{
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/JH/can_nchDept_1D_fgbg_cent10_30_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/JH/can_nchDept_1D_fgbg_cent40_50_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/JH/can_nchDept_1D_fgbg_cent60_70_pta10_ptb38}
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/JH/can_nchDept_1D_fgbg_cent80_90_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/JH/can_nchDept_1D_fgbg_cent100_110_pta10_ptb38}%
    \includegraphics[width=0.33\linewidth]{fig_pool_afterHP/results/fgbgoverlay/JH/can_nchDept_1D_fgbg_cent110_120_pta10_ptb38}
  }
  \caption{
    Same-event Mixed-event overlay in \JH\ case. Black points are fg distribution. Gray points are bg distribution.
  }
  \label{fig:fgbg_JH}
\end{figure}










\newpage
\subsection{Cross-check: Dependence on jet \ptp\ thresholds\label{sec:varyingJetPt}}
Figure~\ref{fig:crosscheck_jet354050} shows the comparison results among
  various jet \ptp\ requirements.
``$\ptp\ > 40$ GeV'' and ``$\ptp\ > 50$ GeV'' used the same set of triggers.
While ``$\ptp\ > 35$ GeV'' case does not use j45 triggers as it's not efficient
  at 35~\GeV.
% ``$\ptp\ > 40$ GeV'' case is about 65\% of events of ``$\ptp\ > 35$ GeV'' case at high multiplicity.
``$\ptp\ > 50$ GeV'' case is about 46\% of events of ``$\ptp\ > 40$ GeV'' case at high multiplicity.
It can be observed that variations in the jet \ptp\ value of over 10\% have no
  significant impact on the values of $v_2$.
This indicates that the results of this study are not sensitive to the jet
  \ptp\ thresholds.
Any crude variation of the JES would be much smaller than the shift in \ptp\ used
  to generate those other selections, which give identical distributions.


\begin{figure}[H]
  \center{
    \includegraphics[width=0.45\linewidth]{fig_pool_afterHP/systematics/JetPtp354050/can_v2_multDept}%
    \includegraphics[width=0.45\linewidth]{fig_pool_afterHP/systematics/JetPtp354050/can_v2_ptDept_ptb60_cent55}
  }
  \caption{
    Comparison of template fit \vn\ obtained with different jet \ptp\ threshold.
    Purple cross corresponds to \JH\ case with jet $\ptp\ > 35$ GeV.
    Yellow cross corresponds to \JH\ case with jet $\ptp\ > 40$ GeV, which is default.
    Green cross corresponds to \JH\ case with jet $\ptp\ > 50$ GeV.
  }
  \label{fig:crosscheck_jet354050}
\end{figure}



