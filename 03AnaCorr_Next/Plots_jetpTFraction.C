#include "Riostream.h"
#include "TFile.h"
#include "TF1.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include <TStyle.h>

#include "bins.h"

void Plots_jetpTFraction(DEFAULT_SETTINGS) {
    string base = BASENAME;
    char name [600];
    TFile *input;
    sprintf(name , "../02AnaCorr/Condor/01Rootfiles/%s.root", base.c_str());
    input  = new TFile(name);
    if (input->IsZombie()) {std::cout << name << " Not Found" << std::endl; throw std::exception();}

    TH1D *hcent;
    TH1D *h_jet_pT[Bins::NCENT + Bins::NCENT_ADD];

    char hname[100];
    //Read in All Histograms
    for (int icent = 0; icent < Bins::NCENT; icent++) {
        sprintf(hname, "h_jet_pT_cent%.2d", icent);
        h_jet_pT[icent] = (TH1D*)input->Get(hname);
        if (!h_jet_pT[icent]) {std::cout << hname << " Not found" << std::endl; throw std::exception();}
    }

    //Add more multiplicity bins
    for (int icent = Bins::NCENT; icent < Bins::NCENT + Bins::NCENT_ADD; icent++) {
        for (int icent_add = Bins::cent_add_lo[icent - Bins::NCENT]; icent_add < Bins::cent_add_up[icent - Bins::NCENT]; icent_add++) {
            TH1D* h_jet_pT_add = h_jet_pT[icent_add];

            if (icent_add == Bins::cent_add_lo[icent - Bins::NCENT]) {
                sprintf(hname, "h_jet_pT_cent%.2d", icent); h_jet_pT[icent] = (TH1D*)h_jet_pT_add->Clone(hname); h_jet_pT[icent]->SetTitle(hname);
            }
            else {
                h_jet_pT[icent]->Add(h_jet_pT_add);
            }
        }
    }

    sprintf(hname, "hcent");
    hcent = (TH1D*)input->Get(hname);
    if (!hcent) {std::cout << hname << " Not found" << std::endl; throw std::exception();}

    //Write All Histograms
    std::vector<TCanvas*> m_can_vec;
    // std::vector<int> m_centralities = Bins::GetCentIndex({0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150});
    std::vector<int> m_centralities    = {Bins::GetCentIndex(  0,   5), Bins::GetCentIndex(  5,  10), Bins::GetCentIndex( 10,  15), Bins::GetCentIndex( 15,  20),
                                          Bins::GetCentIndex( 20,  25), Bins::GetCentIndex( 25,  30), Bins::GetCentIndex(  0,  20), Bins::GetCentIndex( 20,  30),
                                          Bins::GetCentIndex( 30,  40), Bins::GetCentIndex( 40,  50), Bins::GetCentIndex( 50,  60), Bins::GetCentIndex( 60,  70),
                                          Bins::GetCentIndex( 70,  80), Bins::GetCentIndex( 80,  90), Bins::GetCentIndex( 90, 100), Bins::GetCentIndex(100, 110),
                                          Bins::GetCentIndex(110, 120), Bins::GetCentIndex(120, 130), Bins::GetCentIndex(130, 140), Bins::GetCentIndex(140, 150)
                                         };

    for (int icent = 0; icent < (int) m_centralities.size(); icent++) {
        int cent_lo = Bins::GetCentVals(m_centralities[icent]).first;
        int cent_hi = Bins::GetCentVals(m_centralities[icent]).second;

        int nEvents = hcent->GetBinContent(m_centralities[icent] + 1);
        if (m_centralities[icent] == 40) nEvents = hcent->GetBinContent(39) + hcent->GetBinContent(40);
        if (m_centralities[icent] == 41) nEvents = hcent->GetBinContent(37) + hcent->GetBinContent(38);
        if (m_centralities[icent] == 42) nEvents = hcent->GetBinContent(35) + hcent->GetBinContent(36);
        if (m_centralities[icent] == 43) nEvents = hcent->GetBinContent(33) + hcent->GetBinContent(34);
        if (m_centralities[icent] == 44) nEvents = hcent->GetBinContent(31) + hcent->GetBinContent(32);
        if (m_centralities[icent] == 45) nEvents = hcent->GetBinContent(29) + hcent->GetBinContent(30);
        if (m_centralities[icent] == 46) nEvents = hcent->GetBinContent(39) + hcent->GetBinContent(38) + hcent->GetBinContent(37) + hcent->GetBinContent(36);

        sprintf(name, "can_h_jet_fraction_cent%.2d_%.2d", cent_lo, cent_hi);
        TCanvas* c1 = new TCanvas(name, name, 700, 500);
        c1->SetLeftMargin  (0.12);
        c1->SetTopMargin   (0.04);
        c1->SetBottomMargin(0.12);
        c1->SetRightMargin (0.04);
        m_can_vec.push_back(c1);
        c1->SetLogy();

        h_jet_pT[m_centralities[icent]]->GetYaxis()->SetTitleOffset(1.0);
        h_jet_pT[m_centralities[icent]]->GetXaxis()->SetTitleOffset(1.0);
        h_jet_pT[m_centralities[icent]]->GetXaxis()->SetTitle("p_{T}^{jet,corrected}");
        h_jet_pT[m_centralities[icent]]->GetYaxis()->SetTitle("#frac{1}{N_{evt}} #frac{N}{dp_{T}}");
        h_jet_pT[m_centralities[icent]]->Scale(1. / nEvents / h_jet_pT[m_centralities[icent]]->GetXaxis()->GetBinWidth(1), "nosw2");
        h_jet_pT[m_centralities[icent]]->SetMaximum(h_jet_pT[m_centralities[icent]]->GetMaximum() * 1.5);
        h_jet_pT[m_centralities[icent]]->Draw();


        float X, Y, SIZE;
        X = 0.5, Y = 0.9, SIZE = 18;
        int lowbin = h_jet_pT[m_centralities[icent]]->GetXaxis()->FindFixBin(6.0001);
        float fraction = h_jet_pT[m_centralities[icent]]->Integral(lowbin, 200) / h_jet_pT[m_centralities[icent]]->Integral();
        sprintf(name, "Fraction of jet > 6 GeV = %f", fraction);
        Common::myText2(X, Y, 1, name, SIZE, 43);

        lowbin = h_jet_pT[m_centralities[icent]]->GetXaxis()->FindFixBin(7.0001);
        fraction = h_jet_pT[m_centralities[icent]]->Integral(lowbin, 200) / h_jet_pT[m_centralities[icent]]->Integral();
        sprintf(name, "Fraction of jet > 7 GeV = %f", fraction);
        Common::myText2(X, Y - 0.06 * 1, 1, name, SIZE, 43);

        lowbin = h_jet_pT[m_centralities[icent]]->GetXaxis()->FindFixBin(8.0001);
        fraction = h_jet_pT[m_centralities[icent]]->Integral(lowbin, 200) / h_jet_pT[m_centralities[icent]]->Integral();
        sprintf(name, "Fraction of jet > 8 GeV = %f", fraction);
        Common::myText2(X, Y - 0.06 * 2, 1, name, SIZE, 43);

        lowbin = h_jet_pT[m_centralities[icent]]->GetXaxis()->FindFixBin(9.0001);
        fraction = h_jet_pT[m_centralities[icent]]->Integral(lowbin, 200) / h_jet_pT[m_centralities[icent]]->Integral();
        sprintf(name, "Fraction of jet > 9 GeV = %f", fraction);
        Common::myText2(X, Y - 0.06 * 3, 1, name, SIZE, 43);

        lowbin = h_jet_pT[m_centralities[icent]]->GetXaxis()->FindFixBin(10.0001);
        fraction = h_jet_pT[m_centralities[icent]]->Integral(lowbin, 200) / h_jet_pT[m_centralities[icent]]->Integral();
        sprintf(name, "Fraction of jet > 10 GeV = %f", fraction);
        Common::myText2(X, Y - 0.06 * 4, 1, name, SIZE, 43);

        lowbin = h_jet_pT[m_centralities[icent]]->GetXaxis()->FindFixBin(12.0001);
        fraction = h_jet_pT[m_centralities[icent]]->Integral(lowbin, 200) / h_jet_pT[m_centralities[icent]]->Integral();
        sprintf(name, "Fraction of jet > 12 GeV = %f", fraction);
        Common::myText2(X, Y - 0.06 * 5, 1, name, SIZE, 43);

        lowbin = h_jet_pT[m_centralities[icent]]->GetXaxis()->FindFixBin(15.0001);
        fraction = h_jet_pT[m_centralities[icent]]->Integral(lowbin, 200) / h_jet_pT[m_centralities[icent]]->Integral();
        sprintf(name, "Fraction of jet > 15 GeV = %f", fraction);
        Common::myText2(X, Y - 0.06 * 6, 1, name, SIZE, 43);



        X = 0.18, Y = 0.9, SIZE = 21;
        Common::myText2(X       , Y      , 1, "ATLAS"         , SIZE, 73);
        Common::myText2(X + 0.14, Y      , 1, Common::Internal, SIZE, 43);
        Common::myText2(X       , Y - .06, 1, DataSetEnums::DATASETLABEL[1] + " " + DataSetEnums::DATASETENERGY[1], SIZE, 43);
        if (l_multiplicity_type == 0) sprintf(name, "%d#leq#it{N}_{ ch}^{ rec}<%d", cent_lo, cent_hi);
        if (l_multiplicity_type == 1) sprintf(name, "%d#leq E_{T}^{ FCal}<%d", cent_lo, cent_hi);
        Common::myText2(X, Y - .12, 1, name, SIZE, 63);
    }
    Common::SaveCanvas(m_can_vec, base);
}
