#include "Riostream.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "vector"
#include "TLine.h"

#include "bins.h"

TFile *input;
vector<TCanvas*> m_can_vec;

void Plots_NchPerTrigger(DEFAULT_SETTINGS) {
    string base = BASENAME;
    char name [600];
    sprintf(name , "../02AnaCorr/01Rootfiles/%s.root", base.c_str());
    input  = new TFile(name);
    std::cout << "Opening: " << name << std::endl;

    vector<string> trigName {
        "b_HLT_mb_sptrk",
        "b_HLT_noalg_mb_L1MBTS_1_1",
        "b_HLT_noalg_mb_L1MBTS_1",
        "b_HLT_noalg_mb_L1MBTS_2"    ,
        "b_HLT_noalg_mb_L1RD0_FILLED",
        "b_HLT_j10_L1MBTS_2"         ,
        "b_HLT_j10_L1RD0_FILLED"     ,
        "b_HLT_j20_L1J12"            ,
        "b_HLT_j20_L1MBTS_2"         ,
        "b_HLT_j20_L1RD0_FILLED"     ,
        "b_HLT_j45_L1J12"            ,
        "b_HLT_noalg_mb_L1TE5"       ,
        "b_HLT_noalg_mb_L1TE10"      ,
        "b_HLT_noalg_mb_L1TE20"      ,
        "b_HLT_noalg_mb_L1TE30"      ,
        "b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1",
        "b_HLT_mb_sp600_trk40_hmt_L1TE5",
        "b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5",
        "b_HLT_mb_sp600_trk40_hmt_L1TE10",
        "b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10",
        "b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1",
        "b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1",
        "b_HLT_mb_sp700_trk50_hmt_L1TE5",
        "b_HLT_mb_sp900_trk50_hmt_L1TE5",
        "b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5",
        "b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5",
        "b_HLT_mb_sp700_trk50_hmt_L1TE10",
        "b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10",
        "b_HLT_mb_sp700_trk50_hmt_L1TE20",
        "b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20",
        "b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1",
        "b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1",
        "b_HLT_mb_sp900_trk60_hmt_L1TE5",
        "b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5",
        "b_HLT_mb_sp900_trk60_hmt_L1TE10",
        "b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10",
        "b_HLT_mb_sp900_trk60_hmt_L1TE20",
        "b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20",
        "b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1",
        "b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1",
        "b_HLT_mb_sp1000_trk70_hmt_L1TE5",
        "b_HLT_mb_sp1100_trk70_hmt_L1TE5",
        "b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5",
        "b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5",
        "b_HLT_mb_sp1000_trk70_hmt_L1TE10",
        "b_HLT_mb_sp1100_trk70_hmt_L1TE10",
        "b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10",
        "b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10",
        "b_HLT_mb_sp1100_trk70_hmt_L1TE20",
        "b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20",
        "b_HLT_mb_sp1100_trk70_hmt_L1TE30",
        "b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30",
        "b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1",
        "b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1",
        "b_HLT_mb_sp1200_trk80_hmt_L1TE5",
        "b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5",
        "b_HLT_mb_sp1200_trk80_hmt_L1TE10",
        "b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10",
        "b_HLT_mb_sp1200_trk80_hmt_L1TE15",
        "b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15",
        "b_HLT_mb_sp1200_trk80_hmt_L1TE20",
        "b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20",
        "b_HLT_mb_sp1200_trk80_hmt_L1TE30",
        "b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30",
        "b_HLT_mb_sp1200_trk90_hmt_L1TE5",
        "b_HLT_mb_sp1400_trk90_hmt_L1TE5",
        "b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5",
        "b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5",
        "b_HLT_mb_sp1400_trk90_hmt_L1TE10",
        "b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10",
        "b_HLT_mb_sp1400_trk90_hmt_L1TE15",
        "b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15",
        "b_HLT_mb_sp1400_trk90_hmt_L1TE20",
        "b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20",
        "b_HLT_mb_sp1400_trk90_hmt_L1TE30",
        "b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30",
        "b_HLT_mb_sp1400_trk90_hmt_L1TE40",
        "b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40",
        "b_HLT_mb_sp1200_trk100_hmt_L1TE5",
        "b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5",
        "b_HLT_mb_sp1600_trk100_hmt_L1TE5",
        "b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5",
        "b_HLT_mb_sp1600_trk100_hmt_L1TE10",
        "b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10",
        "b_HLT_mb_sp1600_trk100_hmt_L1TE15",
        "b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15",
        "b_HLT_mb_sp1600_trk100_hmt_L1TE20",
        "b_HLT_mb_sp1400_trk100_hmt_L1TE20",
        "b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20",
        "b_HLT_mb_sp1600_trk100_hmt_L1TE25",
        "b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25",
        "b_HLT_mb_sp1600_trk100_hmt_L1TE30",
        "b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30",
        "b_HLT_mb_sp1600_trk100_hmt_L1TE40",
        "b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40",
        "b_HLT_mb_sp1600_trk100_hmt_L1TE50",
        "b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50",
        "b_HLT_mb_sp1700_trk110_hmt_L1TE10",
        "b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10",
        "b_HLT_mb_sp1800_trk110_hmt_L1TE10",
        "b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10",
        "b_HLT_mb_sp1800_trk110_hmt_L1TE15",
        "b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15",
        "b_HLT_mb_sp1700_trk110_hmt_L1TE20",
        "b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20",
        "b_HLT_mb_sp1800_trk110_hmt_L1TE20",
        "b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20",
        "b_HLT_mb_sp1800_trk110_hmt_L1TE25",
        "b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25",
        "b_HLT_mb_sp1700_trk110_hmt_L1TE30",
        "b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30",
        "b_HLT_mb_sp1800_trk110_hmt_L1TE30",
        "b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30",
        "b_HLT_mb_sp1700_trk110_hmt_L1TE40",
        "b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40",
        "b_HLT_mb_sp1700_trk110_hmt_L1TE50",
        "b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50",
        "b_HLT_mb_sp1700_trk110_hmt_L1TE60",
        "b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60",
        "b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5",
        "b_HLT_mb_sp1900_trk120_hmt_L1TE10",
        "b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10",
        "b_HLT_mb_sp2100_trk120_hmt_L1TE10",
        "b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10",
        "b_HLT_mb_sp2100_trk120_hmt_L1TE15",
        "b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15",
        "b_HLT_mb_sp1900_trk120_hmt_L1TE20",
        "b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20",
        "b_HLT_mb_sp2100_trk120_hmt_L1TE20",
        "b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20",
        "b_HLT_mb_sp2100_trk120_hmt_L1TE25",
        "b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25",
        "b_HLT_mb_sp1900_trk120_hmt_L1TE30",
        "b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30",
        "b_HLT_mb_sp2100_trk120_hmt_L1TE30",
        "b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30",
        "b_HLT_mb_sp1900_trk120_hmt_L1TE40",
        "b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40",
        "b_HLT_mb_sp1900_trk120_hmt_L1TE50",
        "b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50",
        "b_HLT_mb_sp1900_trk120_hmt_L1TE60",
        "b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60",
        "b_HLT_mb_sp1900_trk120_hmt_L1TE70",
        "b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70",
        "b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5",
        "b_HLT_mb_sp2300_trk130_hmt_L1TE15",
        "b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15",
        "b_HLT_mb_sp2100_trk130_hmt_L1TE20",
        "b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20",
        "b_HLT_mb_sp2300_trk130_hmt_L1TE20",
        "b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20",
        "b_HLT_mb_sp2300_trk130_hmt_L1TE25",
        "b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25",
        "b_HLT_mb_sp2100_trk130_hmt_L1TE30",
        "b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30",
        "b_HLT_mb_sp2300_trk130_hmt_L1TE30",
        "b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30",
        "b_HLT_mb_sp2100_trk130_hmt_L1TE40",
        "b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40",
        "b_HLT_mb_sp2300_trk130_hmt_L1TE40",
        "b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40",
        "b_HLT_mb_sp2100_trk130_hmt_L1TE50",
        "b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50",
        "b_HLT_mb_sp2100_trk130_hmt_L1TE60",
        "b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60",
        "b_HLT_mb_sp2100_trk130_hmt_L1TE70",
        "b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70",
        "b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10",
        "b_HLT_mb_sp2200_trk140_hmt_L1TE20",
        "b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20",
        "b_HLT_mb_sp2200_trk140_hmt_L1TE30",
        "b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30",
        "b_HLT_mb_sp2200_trk140_hmt_L1TE40",
        "b_HLT_mb_sp2500_trk140_hmt_L1TE40",
        "b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40",
        "b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40",
        "b_HLT_mb_sp2200_trk140_hmt_L1TE50",
        "b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50",
        "b_HLT_mb_sp2200_trk140_hmt_L1TE60",
        "b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60",
        "b_HLT_mb_sp2200_trk140_hmt_L1TE70",
        "b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70",
        "b_HLT_mb_sp2400_trk150_hmt_L1TE20",
        "b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20",
        "b_HLT_mb_sp2400_trk150_hmt_L1TE30",
        "b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30",
        "b_HLT_mb_sp2400_trk150_hmt_L1TE40",
        "b_HLT_mb_sp2700_trk150_hmt_L1TE40",
        "b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40",
        "b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40",
        "b_HLT_mb_sp2400_trk150_hmt_L1TE50",
        "b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50",
        "b_HLT_mb_sp2400_trk150_hmt_L1TE60",
        "b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60",
        "b_HLT_mb_sp2400_trk150_hmt_L1TE70",
        "b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70",
        "b_HLT_mb_sp2900_trk160_hmt_L1TE40",
        "b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40"
    };

    float X, Y, SIZE;
    for (string s : trigName) {
        char hname[100];
        // sprintf(hname, "h1_Nch_perTrig_orig_%s", s.c_str());
        // TH1D* hist1 = (TH1D*)input->Get(hname);
        // if (!hist1) {std::cout << hname << " Not found" << std::endl; throw std::exception();}
        // sprintf(hname, "h1_Nch_perTrig_%s", s.c_str());
        // TH1D* hist2 = (TH1D*)input->Get(hname);
        // if (!hist2) {std::cout << hname << " Not found" << std::endl; throw std::exception();}


        sprintf(hname, "h1_Nch_perTrig_HLT2_%s", s.c_str());
        TH1D* hist1 = (TH1D*)input->Get(hname);
        if (!hist1) {std::cout << hname << " Not found" << std::endl; throw std::exception();}
        if (hist1->Integral() < 1) continue;

        sprintf(name, "can_%s", s.c_str());
        TCanvas *c1 = new TCanvas(name, name, 700, 500);
        c1->SetLeftMargin  (0.10);
        c1->SetTopMargin   (0.05);
        c1->SetBottomMargin(0.15);
        c1->SetRightMargin (0.05);
        c1->SetGridx();
        c1->SetGridy();
        c1->SetLogy();
        m_can_vec.push_back(c1);
        hist1->GetYaxis()->SetTitleOffset(1.0);
        hist1->GetXaxis()->SetTitleOffset(1.0);
        hist1->GetXaxis()->SetRangeUser(0, 200);
        hist1->GetYaxis()->SetRangeUser(0.5, 1E8);
        hist1->GetXaxis()->SetTitle("N_{trk}");
        hist1->SetLineColor(kBlack);
        // hist2->SetLineColor(kRed);
        hist1->Draw();
        // hist2->Draw("same");
        // Common::myText2(0.12, 0.83, 1, s, 24, 43);

        X = 0.12, Y = 0.89, SIZE = 24;
        Common::myText2(X       , Y       , 1, "ATLAS"   , SIZE, 73);
        Common::myText2(X + 0.14, Y       , 1, Common::Internal, SIZE, 43);
        Common::myText2(X       , Y - 0.07, 1, s.substr(2), SIZE, 43);
    }

    Common::SaveCanvas(m_can_vec, "");
}
