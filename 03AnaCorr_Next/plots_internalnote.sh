

root -b -l -q 'Plots_1DGlobalHist.C+(2,0,5,1,3,5,1,1,2,9,4,2,1,1,0,  "h_mu_0")'
root -b -l -q 'Plots_1DGlobalHist.C+(2,0,5,1,3,5,1,1,2,9,4,2,1,1,0,  "hNtrk_corrected")'
mv figs/*.png figs/datasets


root -b -l -q "Plots_MuonElecFraction.C+(2,0,5,1,3,5,1,1,2,9,4,2,1,1,0)"
mv figs/*.png figs/datasets/MuonElecFraction

root -b -l -q Plots_UENch.C++
mv figs/*.png figs/methodology/UECorrection







# ================================================================================
# Jet monitor plots
#
# pflow jet eta vs phi monitors
root -b -l -q 'Plots_allJetEtaPhi.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets_EtaPhi_orig"        , 0, 0)'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets_EtaPhi_orig
root -b -l -q 'Plots_allJetEtaPhi.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets2nd_EtaPhi_orig"     , 0, 0)'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaPhi_orig
root -b -l -q 'Plots_allJetEtaPhi.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets_EtaPhi"             , 1, 1)'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets_EtaPhi
root -b -l -q 'Plots_allJetEtaPhi.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets2nd_EtaPhi"          , 1, 1)'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaPhi
root -b -l -q 'Plots_allJetEtaPhi.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets_EtaPhi_rejected"    , 0, 0)'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets_EtaPhi_rejected
root -b -l -q 'Plots_allJetEtaPhi.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets2nd_EtaPhi_rejected" , 0, 0)'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaPhi_rejected
root -b -l -q 'Plots_allJetEtaPhi.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets_EtaPhi_jvtRej"      , 0, 0)'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets_EtaPhi_jvtRej
root -b -l -q 'Plots_allJetEtaPhi.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets2nd_EtaPhi_jvtRej"   , 0, 0)'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaPhi_jvtRej

root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets_EtaJvt")'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets_EtaJvt
root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets2nd_EtaJvt")'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaJvt
root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets_EtaT_corr")'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets_EtaT_corr
root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets2nd_EtaT_corr")'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaT_corr
root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJetsPT_uncalib_calib")'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJetsPT_uncalib_calib
root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJetsPT_uncalib_dPT")'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJetsPT_uncalib_dPT
root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets_PT_ETA")'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets_PT_ETA
root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets_PT_SumPartPt")'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets_PT_SumPartPt

root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets_EtaSumHEC")'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets_EtaSumHEC
root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets2nd_EtaSumHEC")'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaSumHEC
root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets_EtaSumGap")'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets_EtaSumGap
root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_PFlowJets2nd_EtaSumGap")'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/PFlowJets2nd_EtaSumGap
# root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,5,2,1,1,0,  "h2_dR_CaloJettoPflowJet_0")'
# mv figs/*.png figs/jetMonitors/pflowJetMonitor/dR_CaloJettoPflowJet_0
# root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,5,2,1,1,0,  "h2_dR_CaloJettoPflowJet_1")'
# mv figs/*.png figs/jetMonitors/pflowJetMonitor/dR_CaloJettoPflowJet_1
# root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,5,2,1,1,0,  "h2_dR_CaloJettoPflowJet_2")'
# mv figs/*.png figs/jetMonitors/pflowJetMonitor/dR_CaloJettoPflowJet_2
# root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,5,2,1,1,0,  "h2_dR_CaloJettoPflowJet_3")'
# mv figs/*.png figs/jetMonitors/pflowJetMonitor/dR_CaloJettoPflowJet_3

root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_CaloJetPT_PflowJetPT")'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/CaloJetPT_PflowJetPT
root -b -l -q 'Plots_allJetRegularMonitor.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,  "h2_Ratio_CaloJetPT_PflowJetPT")'
mv figs/*.png figs/jetMonitors/pflowJetMonitor/Ratio_CaloJetPT_PflowJetPT



root -b -l -q "Plots_NchPerTrigger.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0)"
mv figs/*.png figs/datasets/perTriggerNch






# ================================================================================
# Results
#
# 2D 2PC
root -b -l -q "Plots_2D_2PC.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,      1)"
mv figs/*.png figs/results/2D_2PC

# 1D 2PC
root -b -l -q "Plots_1D_2PC.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,      1,  0)"
mv figs/*.png figs/results/1D_2PC/multDept
root -b -l -q "Plots_1D_2PC.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,      1,  1)"
mv figs/*.png figs/results/1D_2PC/pTDept

# Template fit
root -b -l -q "Plots_TemplateFits.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,    0,1,    0)"
mv figs/*.png figs/results/templateFit/multDept
root -b -l -q "Plots_TemplateFits.C+(8,0,9,1,22,9,1,1,2,0,6,2,1,1,0,    0,1,    1)"
mv figs/*.png figs/results/templateFit/pTDept

# vn
root -b -l -q "Plots_Vn_newData_CompareMultiCases.C+(0,0,1,2,  0,1,  0)"
root -b -l -q "Plots_Vn_newData_CompareMultiCases.C+(0,1,1,2,  0,1,  0)"
mv figs/*.png figs/results/vn
