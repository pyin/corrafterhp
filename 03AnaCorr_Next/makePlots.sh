# mkdir jet25_30
mkdir jet25_30/fillReco25
root -b -l -q "Plots_all1D2Dhist.C+(2,0,5,1,5,5,1,1,4,9,6,2,0,1,0)"
mv figs/*.png jet25_30/fillReco25

# mkdir jet30_40
mkdir jet30_40/fillReco25
root -b -l -q "Plots_all1D2Dhist.C+(2,0,5,1,5,5,1,1,4,9,7,2,0,1,0)"
mv figs/*.png jet30_40/fillReco25

# mkdir jet40_50
mkdir jet40_50/fillReco25
root -b -l -q "Plots_all1D2Dhist.C+(2,0,5,1,5,5,1,1,4,9,8,2,0,1,0)"
mv figs/*.png jet40_50/fillReco25

# mkdir jet50_99
mkdir jet50_99/fillReco25
root -b -l -q "Plots_all1D2Dhist.C+(2,0,5,1,5,5,1,1,4,9,9,2,0,1,0)"
mv figs/*.png jet50_99/fillReco25

# mkdir jet25_99_noNewTrig
mkdir jet25_99_noNewTrig/fillReco25
root -b -l -q "Plots_all1D2Dhist.C+(2,0,5,1,0,5,1,1,4,9,2,2,0,1,0)"
mv figs/*.png jet25_99_noNewTrig/fillReco25
