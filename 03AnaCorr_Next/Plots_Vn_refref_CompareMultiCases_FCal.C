#include "TFile.h"
#include "TH1.h"
#include "TCanvas.h"
#include "map"
#include "bins.h"
#include "common.C"
#include "TStyle.h"
#include "TGaxis.h"
#include "TLine.h"
#include "SystErrors_FCal.C"
#include "RatioSystErrors_FCal.C"

std::vector<TCanvas*>    m_can_vec;
map<std::string, double> m_format = Common::StandardFormat();

void InitializeCase(int i_vn_type);
void Plots_Vn   (int i_vn_type, int ihar, int periphLow, int periphHigh);
void Plots_Vn_pT(int i_vn_type, int ihar, int periphLow, int periphHigh);
void parsePaperPoints();
void ReassignRatioStatErrorMult(TH1* InputHist, std::string base, int ihar, int ipt2 , int icent_periph, std::vector<int> centbins);
void ReassignRatioStatErrorPT  (TH1* InputHist, std::string base, int ihar, int icent, int icent_periph, std::vector<int> ptbbins);

SystErrors *m_systerrors_inclusive = nullptr;
SystErrors *m_systerrors_AllEvents = nullptr;
SystErrors *m_systerrors_NoJet     = nullptr;
SystErrors *m_systerrors_WithJet   = nullptr;

RatioSystErrors *m_ratiosys_AllEvents = nullptr;
RatioSystErrors *m_ratiosys_NoJet     = nullptr;
RatioSystErrors *m_ratiosys_WithJet   = nullptr;

TH1D *h_pp13_stat;
TH1D *h_pp13_syst;

struct collection {
    std::string base;
    std::string base_ref;
    std::string label;
    int   eventSet  = -1;
    TFile *file     = nullptr;
    TFile *file_ref = nullptr;
    TH1   *hist     = nullptr;
    TH1   *hist_sys = nullptr;
    collection(std::string b, std::string b_ref, std::string l, int es) {
        base     = b;
        base_ref = b_ref;
        label    = l;
        eventSet = es;
    }
};

std::vector<collection> m_dataset;
std::vector<int> m_centralities = {17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
std::vector<int> m_ptb          = Bins::GetPtbIndex( {{0.5, 5.0}, {0.5, 1.0}, {1.0, 1.5}, {1.5, 2.0}, {2.0, 2.5}, {2.5, 3.0}, {3.0, 3.5}, {3.5, 4.0}, {4, 6}, {6, 8}} );
const int ideta = Bins::GetDetaIndex(2.0, 5.0);

int m_data_type;
int m_correlation_type;
int m_case;
int m_if_have_jet;
int m_multType;
int m_fgTag;
int m_do_sysError;

enum {
    CASE_COMPARE_DR = 0,
    CASE_CHECK      = 1,
};

enum {
    TYPE_CENT_DEP = 0,
    TYPE_PT_DEP   = 1,
};

void Plots_Vn_refref_CompareMultiCases_FCal(int icase = CASE_COMPARE_DR, int dep_type = TYPE_CENT_DEP, int i_vn_type = Bins::VN_TEMPLATE_PEDESTAL, int i_if_have_jet = 0, int i_multType = 0, int fgTag = 0, int do_sysError = 0) {
    gStyle->SetErrorX(0.001);
    m_format["XNdivisions"] = 510;
    m_case        = icase;
    m_if_have_jet = i_if_have_jet;
    m_multType    = i_multType;
    m_fgTag       = fgTag;
    m_do_sysError = do_sysError;

    InitializeCase(i_vn_type);
    parsePaperPoints();

    float periph_lo = 0;
    float periph_hi = 20;

    if (m_multType == 1) {
        periph_lo = 5;
        periph_hi = 15;
    }

    if (dep_type == TYPE_CENT_DEP) {
        Plots_Vn(i_vn_type, 2, periph_lo, periph_hi);
        Plots_Vn(i_vn_type, 3, periph_lo, periph_hi);
        Plots_Vn(i_vn_type, 4, periph_lo, periph_hi);
    }
    else if (dep_type == TYPE_PT_DEP) {
        Plots_Vn_pT(i_vn_type, 2, periph_lo, periph_hi);
        Plots_Vn_pT(i_vn_type, 3, periph_lo, periph_hi);
        Plots_Vn_pT(i_vn_type, 4, periph_lo, periph_hi);
    }

    char name[600];
    sprintf(name, "%s", DataSetEnums::Filename[m_data_type].c_str());
    Common::SaveCanvas(m_can_vec, name);
}

void InitializeCase(int i_vn_type)
{
    // -----------------------------------------------------------------------------------------
    if (m_case == CASE_COMPARE_DR) {
        // More cases
        // More comparesions
        m_data_type        = DataSetEnums::DATA_LOW_AND_INTERMEDIATE_MU;
        m_correlation_type = DataSetEnums::HADRON_NOJET_CORRELATIONS;

        std::string base_ref, base2_ASSOASSO_AllEvents, base2_ASSOASSO_NoJet, base2_ASSOASSO_WithJet;
        if (m_multType == 0) {
            base_ref                 = DataSetEnums::BaseName(1, 1, 2, 8, 3, 6, 2, 1, 0, 0);
            base2_ASSOASSO_AllEvents = DataSetEnums::BaseName(1, 1, 2, 8, 3, 6, 2, 1, 2, 0);
            base2_ASSOASSO_NoJet     = DataSetEnums::BaseName(1, 1, 2, 8, 3, 6, 0, 1, 2, 0);
            base2_ASSOASSO_WithJet   = DataSetEnums::BaseName(1, 1, 2, 8, 3, 6, 1, 1, 2, 0);
        }

        if (m_multType == 1) {
            base_ref                 = DataSetEnums::BaseName(1, 1, 2, 5, 3, 6, 2, 1, 0, 1);
            base2_ASSOASSO_AllEvents = DataSetEnums::BaseName(1, 1, 2, 5, 3, 6, 2, 1, 2, 1);
            base2_ASSOASSO_NoJet     = DataSetEnums::BaseName(1, 1, 2, 5, 3, 6, 0, 1, 2, 1);
            base2_ASSOASSO_WithJet   = DataSetEnums::BaseName(1, 1, 2, 5, 3, 6, 1, 1, 2, 1);
        }


        m_dataset.push_back(collection(base_ref, base_ref, "Inclusive", 3));
        if (m_if_have_jet == 2) {m_dataset.push_back(collection(base2_ASSOASSO_AllEvents, base2_ASSOASSO_AllEvents, "AllEvents", 2));}
        if (m_if_have_jet == 0) {m_dataset.push_back(collection(base2_ASSOASSO_NoJet    , base2_ASSOASSO_NoJet    , "NoJet"    , 0));}
        if (m_if_have_jet == 1) {m_dataset.push_back(collection(base2_ASSOASSO_WithJet  , base2_ASSOASSO_WithJet  , "WithJet"  , 1));}

        if (m_do_sysError) {
            m_systerrors_inclusive = new SystErrors(base_ref                , m_data_type, 2);
            m_systerrors_AllEvents = new SystErrors(base2_ASSOASSO_AllEvents, m_data_type, 2);
            m_systerrors_NoJet     = new SystErrors(base2_ASSOASSO_NoJet    , m_data_type, 0);
            m_systerrors_WithJet   = new SystErrors(base2_ASSOASSO_WithJet  , m_data_type, 1);

            m_ratiosys_AllEvents = new RatioSystErrors(base2_ASSOASSO_AllEvents, m_data_type, 2);
            m_ratiosys_NoJet     = new RatioSystErrors(base2_ASSOASSO_NoJet    , m_data_type, 0);
            m_ratiosys_WithJet   = new RatioSystErrors(base2_ASSOASSO_WithJet  , m_data_type, 1);
        }
    }
    // -----------------------------------------------------------------------------------------

    if (m_case == CASE_CHECK) {
        // More cases
        // More comparesions
        m_data_type        = DataSetEnums::DATA_LOW_AND_INTERMEDIATE_MU;
        m_correlation_type = DataSetEnums::HADRON_NOJET_CORRELATIONS;
        m_centralities     = Bins::GetCentIndex({0, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 200, 220, 240});

        std::string base_ref = DataSetEnums::BaseName(1, 1, 2, 8, 0, 0, 0, 0, 0, 0);

        m_dataset.push_back(collection(base_ref, base_ref, "H-H Inclusive", 3));
    }



    for (auto& l_dataset : m_dataset) {
        char name[600];
        if      (i_vn_type == Bins::VN_TEMPLATE_PEDESTAL) sprintf(name, "01RootFiles/%s_fgTag0_TemplateFits_pedestal_vnn.root" , l_dataset.base.c_str());
        else if (i_vn_type == Bins::VN_TEMPLATE         ) sprintf(name, "01RootFiles/%s_fgTag0_TemplateFits_vnn.root"          , l_dataset.base.c_str());
        else if (i_vn_type == Bins::VN_DIRECT           ) sprintf(name, "01RootFiles/%s_fgTag0_Direct_vnn.root"                , l_dataset.base.c_str());
        else if (i_vn_type == Bins::VN_PERISUB          ) sprintf(name, "01RootFiles/%s_fgTag0_PeripheralSub_vnn.root"         , l_dataset.base.c_str());
        TFile* file = new TFile(name , "read");
        Common::CheckFile(file, name);
        l_dataset.file = file;
        l_dataset.file_ref = file;
    }
}



void Plots_Vn(int i_vn_type, int ihar, int periphLow, int periphHigh)
{
    char name [600], name1[600];
    const int ipt1  = Bins::GetPtaIndex(0.5, 5.0);
    const int ipt2  = Bins::GetPtbIndex(0.5, 5.0);
    const int ich   = Bins::COMBINED_CHARGE;
    int icent_periph;
    if (i_vn_type == Bins::VN_DIRECT) icent_periph = Bins::NO_PERIPHERAL_BIN;
    else                              icent_periph = Bins::GetCentIndex(periphLow, periphHigh);

    if (ihar == 2) m_centralities = Bins::GetCentIndex({0, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150});
    else           m_centralities = Bins::GetCentIndex({0, 20, 40, 60, 80, 100, 120, 140});

    if (ihar == 2 && m_if_have_jet == 1) m_centralities = Bins::GetCentIndex({0, 20, 40, 60, 80, 100, 120, 140});

    if (m_multType == 1) {
        if (ihar == 2) m_centralities = Bins::GetCentIndex({0, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120});
        else           m_centralities = Bins::GetCentIndex({0, 20, 40, 60, 80, 100, 120});

        if (ihar == 2 && m_if_have_jet == 1) m_centralities = Bins::GetCentIndex({0, 20, 40, 60, 80, 100, 120});
    }

    // for (int vnn_vn : {0, 1}) {
    for (int vnn_vn : {1}) {
        if (vnn_vn == 1) sprintf(name, "can_v%d_SystemCompare_centdep_pta%d_ptb%d_ch%d_eta%d_pericent%d_type%d_ifHaveJet%d_multType%d_fgTag%d"  , ihar      , ipt1, ipt2, ich, ideta, icent_periph, i_vn_type, m_if_have_jet, m_multType, m_fgTag);
        else             sprintf(name, "can_v%d%d_SystemCompare_centdep_pta%d_ptb%d_ch%d_eta%d_pericent%d_type%d_ifHaveJet%d_multType%d_fgTag%d", ihar, ihar, ipt1, ipt2, ich, ideta, icent_periph, i_vn_type, m_if_have_jet, m_multType, m_fgTag);

        TCanvas *Can = Common::StandardCanvas2(name);
        m_can_vec.push_back(Can);

        //------------------------------------------------------------------------------
        //Fill
        float max = 0;
        float min = 0;
        for (auto& l_dataset : m_dataset) {
            TFile* l_InFile         = l_dataset.file;
            TFile* l_InFile_ref     = l_dataset.file_ref;
            std::string data_label  = l_dataset.label;
            int eventSet            = l_dataset.eventSet;

            if (vnn_vn == 1) sprintf(name, "h_v%d_centdep_pta%d_ptb%d_ch%d_eta%d_pericent%d_type%d_%s",    ihar      , ipt1, ipt2, ich, ideta, icent_periph, i_vn_type, data_label.c_str());
            else             sprintf(name, "h_v%d%d_centdep_pta%d_ptb%d_ch%d_eta%d_pericent%d_type%d_%s",  ihar, ihar, ipt1, ipt2, ich, ideta, icent_periph, i_vn_type, data_label.c_str());
            TH1D* hist = Bins::CentdepHist(m_centralities, name); //bins.h
            if (vnn_vn == 0) sprintf(name, "v_{%d,%d}", ihar, ihar);
            else             sprintf(name, "v_{%d}"   , ihar      );
            hist->GetYaxis()->SetTitle(name);
            hist->GetXaxis()->SetTitle("#it{N}_{ch}^{rec,corr}");
            if (m_multType == 1) hist->GetXaxis()->SetTitle("E_{T}^{FCal} [GeV]");
            Common::FormatHist(hist, m_format);

            for (unsigned int i = 0; i < m_centralities.size(); i++) {
                if (hist->GetBinLowEdge(i + 2) > 150) continue; //leave space for TLegends

                //skip bins below icent_periph from the Template fit vn values
                if (i_vn_type == Bins::VN_TEMPLATE ||
                        i_vn_type == Bins::VN_TEMPLATE_PEDESTAL ||
                        i_vn_type == Bins::VN_PERISUB) {
                    double cent_lo = hist->GetBinLowEdge(i + 1);
                    if (cent_lo < Bins::GetCentVals(icent_periph).second - 0.001) continue;
                }
                std::pair<float, float> vnn = Bins::GetVnn  (m_centralities[i], ipt1, ipt2, ich, ideta, ihar, icent_periph, l_InFile);
                if (vnn_vn == 1)        vnn = Bins::GetVnPtb(m_centralities[i], ipt1, ipt2, ich, ideta, ihar, icent_periph, l_InFile, l_InFile);
                hist->SetBinContent(i + 1, vnn.first );
                hist->SetBinError  (i + 1, vnn.second);
            }

            if (hist->GetMaximum() > max && hist->GetMaximum() <  0.5) max = hist->GetMaximum();
            if (hist->GetMinimum() < min && hist->GetMinimum() > -0.5) min = hist->GetMinimum();
            l_dataset.hist = hist;

            // -----------------------------------------------------------------------------
            // Systematic errors
            if (m_do_sysError) {
                TH1* h_vn_sys = nullptr;
                if (eventSet == 3) h_vn_sys = m_systerrors_inclusive->AddSystErrorCentDepHist(ihar, ipt2, hist, SystErrors::SYSTEMATIC_TOTAL);
                if (eventSet == 2) h_vn_sys = m_systerrors_AllEvents->AddSystErrorCentDepHist(ihar, ipt2, hist, SystErrors::SYSTEMATIC_TOTAL);
                if (eventSet == 0) h_vn_sys = m_systerrors_NoJet    ->AddSystErrorCentDepHist(ihar, ipt2, hist, SystErrors::SYSTEMATIC_TOTAL);
                if (eventSet == 1) h_vn_sys = m_systerrors_WithJet  ->AddSystErrorCentDepHist(ihar, ipt2, hist, SystErrors::SYSTEMATIC_TOTAL);
                l_dataset.hist_sys = h_vn_sys;
            }
            // -----------------------------------------------------------------------------
        }
        //------------------------------------------------------------------------------


        //Draw
        //------------------------------------------------------------------------------
        int idraw = 0, isty = 0;
        TH1* h_main = nullptr;
        TH1* h_rmain = nullptr;
        for (auto& l_dataset : m_dataset) {
            std::string base        = l_dataset.base;
            std::string data_label  = l_dataset.label;
            TH1D* hist              = (TH1D*) l_dataset.hist;
            TH1D* hist_sys          = (TH1D*) l_dataset.hist_sys;
            int eventSet            = l_dataset.eventSet;
            // if (m_multType == 0) Common::ScaleXaxis(hist    , 1.18);
            // if (m_multType == 0) Common::ScaleXaxis(hist_sys, 1.18);
            if (idraw == 0) h_main = hist;

            int col    [6] = {kBlue    , kRed    , kGreen + 1, kOrange + 1, kCyan + 1, kMagenta    };
            int col_sys[6] = {kBlue - 9, kRed - 9, kGreen - 9, kOrange - 9, kCyan - 9, kMagenta - 9};
            int sty[6] = {25,      20,         22,        20,          20,       20};
            Common::format(hist, col[isty], sty[isty]);

            if (hist_sys && vnn_vn == 1 && m_do_sysError) {
                Common::format(hist_sys, col_sys[isty], sty[isty]);
                hist_sys->SetLineWidth(15);
                hist_sys->SetMarkerSize(0.1);
                if (idraw == 0) h_main = hist_sys;
            }

            if (idraw > 0) {
                Can->cd(2);

                TH1D* hist_ratio = (TH1D*)hist->Clone(Common::UniqueName().c_str());
                Common::FormatHist(hist_ratio, m_format);
                hist_ratio->Divide(m_dataset[0].hist);
                hist_ratio->GetYaxis()->SetTitle("ratio");
                ReassignRatioStatErrorMult(hist_ratio, base, ihar, ipt2, icent_periph, m_centralities);
                h_rmain = hist_ratio;

                if (m_do_sysError) {
                    TH1* h_vn_ratiosys = nullptr;
                    if (eventSet == 2) h_vn_ratiosys = m_ratiosys_AllEvents->AddSystErrorCentDepHist(ihar, ipt2, hist_ratio, RatioSystErrors::SYSTEMATIC_TOTAL);
                    if (eventSet == 0) h_vn_ratiosys = m_ratiosys_NoJet    ->AddSystErrorCentDepHist(ihar, ipt2, hist_ratio, RatioSystErrors::SYSTEMATIC_TOTAL);
                    if (eventSet == 1) h_vn_ratiosys = m_ratiosys_WithJet  ->AddSystErrorCentDepHist(ihar, ipt2, hist_ratio, RatioSystErrors::SYSTEMATIC_TOTAL);

                    Common::format(h_vn_ratiosys, col_sys[isty], sty[isty]);
                    h_vn_ratiosys->SetLineWidth(15);
                    h_vn_ratiosys->SetMarkerSize(0.1);

                    if (idraw == 1) h_rmain = h_vn_ratiosys;

                    if (idraw == 1) {
                        h_vn_ratiosys->Draw();
                        hist_ratio   ->Draw("same");
                    }
                    else {
                        h_vn_ratiosys->Draw("same");
                        hist_ratio   ->Draw("same");
                    }
                }
                else {
                    if (idraw == 1) hist_ratio->Draw();
                    else            hist_ratio->Draw("same");
                }

                if (idraw == 1) {
                    double line_min = 0.1;
                    double line_max = h_rmain->GetBinLowEdge(h_rmain->GetNbinsX() + 1);
                    TLine *line  = new TLine(line_min,  1, line_max,  1);
                    line->SetLineStyle(2);
                    line->Draw();
                }

                if (ihar == 2) {
                    if (vnn_vn == 1) {
                        if (m_if_have_jet == 1) h_rmain->GetYaxis()->SetRangeUser(0.9, 1.5);
                        else h_rmain->GetYaxis()->SetRangeUser(0.9, 1.05);
                    }
                    if (vnn_vn == 0) h_rmain->GetYaxis()->SetRangeUser(0.7, 1.1);
                }
                if (ihar == 3) {
                    if (vnn_vn == 1) {
                        if (m_if_have_jet == 1) h_rmain->GetYaxis()->SetRangeUser(-8, 2);
                        else h_rmain->GetYaxis()->SetRangeUser(0.9, 1.3);
                    }
                    if (vnn_vn == 0) h_rmain->GetYaxis()->SetRangeUser(1.0, 5.0);
                }
                if (ihar == 4) {
                    if (vnn_vn == 1) {
                        if (m_if_have_jet == 1) h_rmain->GetYaxis()->SetRangeUser(0, 10);
                        else h_rmain->GetYaxis()->SetRangeUser(0.6, 1.2);
                    }
                    if (vnn_vn == 0) h_rmain->GetYaxis()->SetRangeUser(0.0, 5.0);
                }

                if (m_multType == 1) {
                    if (ihar == 2) {
                        if (vnn_vn == 1) {
                            if (m_if_have_jet == 1) h_rmain->GetYaxis()->SetRangeUser(0.7, 1.2);
                            else h_rmain->GetYaxis()->SetRangeUser(0.9, 1.05);
                        }
                        if (vnn_vn == 0) h_rmain->GetYaxis()->SetRangeUser(0.7, 1.1);
                    }
                    if (ihar == 3) {
                        if (vnn_vn == 1) {
                            if (m_if_have_jet == 1) h_rmain->GetYaxis()->SetRangeUser(-5, 3.0);
                            else h_rmain->GetYaxis()->SetRangeUser(0.9, 1.3);
                        }
                        if (vnn_vn == 0) h_rmain->GetYaxis()->SetRangeUser(1.0, 5.0);
                    }
                    if (ihar == 4) {
                        if (vnn_vn == 1) {
                            if (m_if_have_jet == 1) h_rmain->GetYaxis()->SetRangeUser(0.5, 8);
                            else h_rmain->GetYaxis()->SetRangeUser(0.6, 1.2);
                        }
                        if (vnn_vn == 0) h_rmain->GetYaxis()->SetRangeUser(0.0, 5.0);
                    }
                }
            }

            if (min > 0) min = 0;
            float histHigh = max + 8 * (max - min) / 9;
            float histLow  = min - 1 * (max - min) / 9;

            // if (m_if_have_jet == 1 && ihar == 2 && vnn_vn == 1) histHigh = 0.16;
            Can->cd(1);
            h_main->GetYaxis()->SetRangeUser(histLow, histHigh);
            if (min >= 0) h_main->GetYaxis()->SetRangeUser(0, histHigh);
            if (ihar == 2 && vnn_vn == 1) h_main->GetYaxis()->SetRangeUser(0, 0.15);
            if (ihar == 3 && vnn_vn == 1) h_main->GetYaxis()->SetRangeUser(-0.03, 0.08);
            if (ihar == 4 && vnn_vn == 1) h_main->GetYaxis()->SetRangeUser(0, 0.04);

            if (ihar == 2 && vnn_vn == 1 && m_if_have_jet == 1) h_main->GetYaxis()->SetRangeUser(0, 0.2);
            if (ihar == 3 && vnn_vn == 1 && m_if_have_jet == 1) h_main->GetYaxis()->SetRangeUser(-0.12, 0.16);
            if (ihar == 4 && vnn_vn == 1 && m_if_have_jet == 1) h_main->GetYaxis()->SetRangeUser(0, 0.2);

            if (m_multType == 1) {
                if (ihar == 2 && vnn_vn == 1 && m_if_have_jet == 1) h_main->GetYaxis()->SetRangeUser(0, 0.12);
                if (ihar == 3 && vnn_vn == 1 && m_if_have_jet == 1) h_main->GetYaxis()->SetRangeUser(-0.08, 0.15);
                if (ihar == 4 && vnn_vn == 1 && m_if_have_jet == 1) h_main->GetYaxis()->SetRangeUser(0, 0.12);
            }

            if (hist_sys && vnn_vn == 1 && m_do_sysError) {
                if (idraw == 0) {
                    hist_sys->Draw();
                    hist    ->Draw("same");
                }
                else {
                    hist_sys->Draw("same");
                    hist    ->Draw("same");
                }
            }
            else {
                if (idraw == 0) hist->Draw();
                else            hist->Draw("same");
            }

            Common::myMarkerText(0.19 + idraw * 0.18, 0.65, col[isty], sty[isty], data_label, 1.25, 0.043);


            idraw++; isty++;
        }
        //------------------------------------------------------------------------------
        // drawPaperPoints();

        int size = 18;
        float X = 0.18, Y = 0.88;
        Common::myText2(X       , Y      , 1, "ATLAS"   , size, 73);
        Common::myText2(X + 0.14, Y      , 1, Common::Internal, size, 43);
        Common::myText2(X       , Y - .06, 1, DataSetEnums::DATASETLABEL [m_data_type] + " " + DataSetEnums::DATASETENERGY[m_data_type], size, 43);
        if (Bins::GetPtbIndexForPtaIndex(ipt1) == ipt2) {
            Common::myText2(X     , Y - .12, 1, Bins::label_ptab(ipt1, ipt2, m_correlation_type) , size, 43);
        }
        else {
            Common::myText2(X     , Y - .12, 1, Bins::label_pta (ipt1, m_correlation_type) , size, 43);
            Common::myText2(X     , Y - .12, 1, Bins::label_ptb (ipt2, m_correlation_type) , size, 43);
        }
        Common::myText2(X     , Y - .18, 1, Bins::label_eta      (ideta)        , size, 43);
        if (i_vn_type != Bins::VN_DIRECT) {
            if (m_multType == 0) Common::myText2(X + 0.2, Y - .18, 1, Bins::label_cent_peri  (icent_periph), size, 43);
            if (m_multType == 1) Common::myText2(X + 0.2, Y - .18, 1, Bins::label_FCalEt_peri(icent_periph), size, 43);
        }

        Common::myText2(0.7, Y, 1, "Template Fit", size, 63);
    }
}


void Plots_Vn_pT(int i_vn_type, int ihar, int periphLow, int periphHigh) {
    char name [600], name1[600];
    const int ipt1   = Bins::GetPtaIndex(0.5, 5.0);
    const int icent  = Bins::GetCentIndex(60, 150);
    const int ich    = Bins::COMBINED_CHARGE;
    int icent_periph;
    if (i_vn_type == Bins::VN_DIRECT) icent_periph = Bins::NO_PERIPHERAL_BIN;
    else                              icent_periph = Bins::GetCentIndex(periphLow, periphHigh);
    m_ptb = Bins::GetPtbIndex({0, 0.5, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.5, 4.0, 6.0, 8.0});
    if (ihar == 3) m_ptb = Bins::GetPtbIndex({0, 0.5, 1.0, 1.5, 2.0, 3.0, 4.0, 6.0, 8.0});
    if (ihar == 4) m_ptb = Bins::GetPtbIndex({0, 0.5, 1.0, 1.5, 2.0, 3.0});

    if (m_if_have_jet == 1) m_ptb = Bins::GetPtbIndex({0, 0.5, 1.0, 2.0, 3.0});

    for (int vnn_vn : {1}) {
        if (vnn_vn == 1) sprintf(name, "can_v%d_SystemCompare_pTbdep_pta%d_cent%d_ch%d_eta%d_pericent%d_type%d_ifHaveJet%d_multType%d_fgTag%d"  , ihar      , ipt1, icent, ich, ideta, icent_periph, i_vn_type, m_if_have_jet, m_multType, m_fgTag);
        else             sprintf(name, "can_v%d%d_SystemCompare_pTbdep_pta%d_cent%d_ch%d_eta%d_pericent%d_type%d_ifHaveJet%d_multType%d_fgTag%d", ihar, ihar, ipt1, icent, ich, ideta, icent_periph, i_vn_type, m_if_have_jet, m_multType, m_fgTag);

        TCanvas *Can = Common::StandardCanvas2(name);
        m_can_vec.push_back(Can);

        //------------------------------------------------------------------------------
        //Fill
        float max = 0;
        float min = 0;
        for (auto& l_dataset : m_dataset) {
            TFile* l_InFile         = l_dataset.file;
            TFile* l_InFile_ref     = l_dataset.file_ref;
            std::string data_label  = l_dataset.label;
            int eventSet            = l_dataset.eventSet;

            if (vnn_vn == 1) sprintf(name, "h_v%d_ptb_pta%d_cent%d_ch%d_eta%d_pericent%d_type%d__%s",    ihar      , ipt1, icent, ich, ideta, icent_periph, i_vn_type, data_label.c_str());
            else             sprintf(name, "h_v%d%d_ptb_pta%d_cent%d_ch%d_eta%d_pericent%d_type%d__%s",  ihar, ihar, ipt1, icent, ich, ideta, icent_periph, i_vn_type, data_label.c_str());
            TH1D* hist = Bins::PtbdepHist(m_ptb, name, m_correlation_type); //bins.h

            for (unsigned int ipt2 = 0; ipt2 < m_ptb.size(); ipt2++) {
                double pt_lo = hist->GetBinLowEdge(ipt2 + 1);
                if (pt_lo < 0.5 - 0.001) continue;
                if (vnn_vn == 0) sprintf(name, "v_{%d,%d}"                  , ihar, ihar);
                else             sprintf(name, "v_{%d}(#it{p}_{T}^{#it{b}})", ihar      );
                hist->GetYaxis()->SetTitle(name);
                hist->GetXaxis()->SetTitle("#it{p}_{T}^{#it{b}} [GeV]");
                Common::FormatHist(hist, m_format);

                std::pair<float, float> vnn = Bins::GetVnn  (icent, ipt1, m_ptb[ipt2], ich, ideta, ihar, icent_periph, l_InFile);
                if (vnn_vn == 1)        vnn = Bins::GetVnPtb(icent, ipt1, m_ptb[ipt2], ich, ideta, ihar, icent_periph, l_InFile, l_InFile_ref);
                hist->SetBinContent(ipt2 + 1, vnn.first );
                hist->SetBinError  (ipt2 + 1, vnn.second);
            }

            if (hist->GetMaximum() > max && hist->GetMaximum() <  0.5) max = hist->GetMaximum();
            if (hist->GetMinimum() < min && hist->GetMinimum() > -0.5) min = hist->GetMinimum();
            l_dataset.hist = hist;

            // -----------------------------------------------------------------------------
            // Systematic errors
            if (m_do_sysError) {
                TH1* h_vn_sys = nullptr;
                if (eventSet == 3) h_vn_sys = m_systerrors_inclusive->AddSystErrorPtbDepHist(ihar, icent, hist, SystErrors::SYSTEMATIC_TOTAL);
                if (eventSet == 2) h_vn_sys = m_systerrors_AllEvents->AddSystErrorPtbDepHist(ihar, icent, hist, SystErrors::SYSTEMATIC_TOTAL);
                if (eventSet == 0) h_vn_sys = m_systerrors_NoJet    ->AddSystErrorPtbDepHist(ihar, icent, hist, SystErrors::SYSTEMATIC_TOTAL);
                if (eventSet == 1) h_vn_sys = m_systerrors_WithJet  ->AddSystErrorPtbDepHist(ihar, icent, hist, SystErrors::SYSTEMATIC_TOTAL);
                l_dataset.hist_sys = h_vn_sys;
            }
            // -----------------------------------------------------------------------------
        }
        //------------------------------------------------------------------------------


        //Draw
        //------------------------------------------------------------------------------
        int idraw = 0, isty = 0;
        TH1* h_main = nullptr;
        TH1* h_rmain = nullptr;
        for (auto& l_dataset : m_dataset) {
            std::string base        = l_dataset.base;
            std::string data_label  = l_dataset.label;
            TH1D *hist              = (TH1D*) l_dataset.hist;
            TH1D* hist_sys          = (TH1D*) l_dataset.hist_sys;
            int eventSet            = l_dataset.eventSet;
            if (idraw == 0) h_main = hist;

            int col    [6] = {kBlue    , kRed    , kGreen + 1, kOrange + 1, kCyan + 1, kMagenta    };
            int col_sys[6] = {kBlue - 9, kRed - 9, kGreen - 9, kOrange - 9, kCyan - 9, kMagenta - 9};
            int sty[6] = {25,      20,         22,        20,          20,       20};
            Common::format(hist, col[isty], sty[isty]);

            if (hist_sys && vnn_vn == 1 && m_do_sysError) {
                Common::format(hist_sys, col_sys[isty], sty[isty]);
                hist_sys->SetLineWidth(8);
                hist_sys->SetMarkerSize(0.1);
                if (idraw == 0) h_main = hist_sys;
            }

            if (idraw > 0) {
                Can->cd(2);

                TH1D* hist_ratio = (TH1D*)hist->Clone(Common::UniqueName().c_str());
                Common::FormatHist(hist_ratio, m_format);
                hist_ratio->Divide(m_dataset[0].hist);
                sprintf(name, "ratio (%s/Inclusive)", data_label.c_str());
                hist_ratio->GetYaxis()->SetTitle(name);
                ReassignRatioStatErrorPT(hist_ratio, base, ihar, icent, icent_periph, m_ptb);
                h_rmain = hist_ratio;

                if (m_do_sysError) {
                    TH1* h_vn_ratiosys = nullptr;
                    if (eventSet == 2) h_vn_ratiosys = m_ratiosys_AllEvents->AddSystErrorPtbDepHist(ihar, icent, hist_ratio, RatioSystErrors::SYSTEMATIC_TOTAL);
                    if (eventSet == 0) h_vn_ratiosys = m_ratiosys_NoJet    ->AddSystErrorPtbDepHist(ihar, icent, hist_ratio, RatioSystErrors::SYSTEMATIC_TOTAL);
                    if (eventSet == 1) h_vn_ratiosys = m_ratiosys_WithJet  ->AddSystErrorPtbDepHist(ihar, icent, hist_ratio, RatioSystErrors::SYSTEMATIC_TOTAL);

                    Common::format(h_vn_ratiosys, col_sys[isty], sty[isty]);
                    h_vn_ratiosys->SetLineWidth(15);
                    h_vn_ratiosys->SetMarkerSize(0.1);

                    if (idraw == 1) {
                        h_rmain = h_vn_ratiosys;
                        h_vn_ratiosys->Draw();
                        hist_ratio   ->Draw("same");
                    }
                    else {
                        h_vn_ratiosys->Draw("same");
                        hist_ratio   ->Draw("same");
                    }
                }
                else {
                    if (idraw == 1) hist_ratio->Draw();
                    else            hist_ratio->Draw("same");
                }

                if (idraw == 1) {
                    double line_min = 0.1;
                    double line_max = h_rmain->GetBinLowEdge(h_rmain->GetNbinsX() + 1);
                    TLine *line  = new TLine(line_min,  1, line_max,  1);
                    line->SetLineStyle(2);
                    line->Draw();
                }

                if (ihar == 2) {
                    h_rmain->GetXaxis()->SetRangeUser(0.0, 8.0);
                    if (vnn_vn == 1) {
                        if (m_if_have_jet == 1) {
                            h_rmain->GetXaxis()->SetRangeUser(0.0, 3.0);
                            h_rmain->GetYaxis()->SetRangeUser(0.4, 2.0);
                        }
                        else h_rmain->GetYaxis()->SetRangeUser(0.9, 1.2);
                    }
                    if (vnn_vn == 0) h_rmain->GetYaxis()->SetRangeUser(0.7, 1.1);
                }
                if (ihar == 3) {
                    h_rmain->GetXaxis()->SetRangeUser(0.0, 8.0);
                    if (vnn_vn == 1) {
                        if (m_if_have_jet == 1) {
                            h_rmain->GetXaxis()->SetRangeUser(0.0, 3.0);
                            h_rmain->GetYaxis()->SetRangeUser(0.0, 4.0);
                        }
                        else h_rmain->GetYaxis()->SetRangeUser(0.4, 1.3);
                    }
                    if (vnn_vn == 0) h_rmain->GetYaxis()->SetRangeUser(1.0, 5.0);
                }
                if (ihar == 4) {
                    h_rmain->GetXaxis()->SetRangeUser(0.0, 3.0);
                    if (vnn_vn == 1) {
                        if (m_if_have_jet == 1) h_rmain->GetYaxis()->SetRangeUser(-10, 10.0);
                        else h_rmain->GetYaxis()->SetRangeUser(0.0, 3.0);
                    }
                    if (vnn_vn == 0) h_rmain->GetYaxis()->SetRangeUser(0.0, 5.0);
                }
            }

            float histHigh = max + 8 * (max - min) / 9;
            float histLow  = min - 1 * (max - min) / 9;
            Can->cd(1);
            h_main->GetYaxis()->SetRangeUser(0, histHigh);

            if (ihar == 2) h_main->GetXaxis()->SetRangeUser(0.0, 8.0);
            if (ihar == 3) h_main->GetXaxis()->SetRangeUser(0.0, 8.0);
            if (ihar == 4) h_main->GetXaxis()->SetRangeUser(0.0, 3.0);

            if (m_if_have_jet == 1 && ihar == 2) {
                if (vnn_vn == 0) h_main->GetYaxis()->SetRangeUser(0, 0.012);
                if (vnn_vn == 1) {
                    h_main->GetXaxis()->SetRangeUser(0.0, 3.0);
                    h_main->GetYaxis()->SetRangeUser(0, 0.17);
                }
            }
            if (m_if_have_jet == 1 && ihar == 4) {
                if (vnn_vn == 0) h_main->GetYaxis()->SetRangeUser(0, 0.012);
                if (vnn_vn == 1) h_main->GetYaxis()->SetRangeUser(-0.4, 0.5);
            }

            if (hist_sys && vnn_vn == 1 && m_do_sysError) {
                if (idraw == 0) {
                    hist_sys->Draw();
                    hist    ->Draw("same");
                }
                else {
                    hist_sys->Draw("same");
                    hist    ->Draw("same");
                }
            }
            else {
                if (idraw == 0) hist->Draw();
                else            hist->Draw("same");
            }

            Common::myMarkerText(0.19 + idraw * 0.18, 0.65, col[isty], sty[isty], data_label, 1.25, 0.043);

            idraw++; isty++;
        }
        //------------------------------------------------------------------------------


        int size = 18;
        float X = 0.16, Y = 0.88;
        Common::myText2(X       , Y      , 1, "ATLAS"   , size, 73);
        Common::myText2(X + 0.14, Y      , 1, Common::Internal, size, 43);
        Common::myText2(X       , Y - .06, 1, DataSetEnums::DATASETLABEL [m_data_type] + " " + DataSetEnums::DATASETENERGY[m_data_type], size, 43);
        Common::myText2(X       , Y - .12, 1, Bins::label_eta(ideta)                   , size, 43);
        Common::myText2(X + 0.25, Y - .12, 1, Bins::label_pta(ipt1, m_correlation_type), size, 43);
        if (m_multType == 0) Common::myText2(X, Y - 0.18, 1, Bins::label_cent  (icent), size, 43);
        if (m_multType == 1) Common::myText2(X, Y - 0.18, 1, Bins::label_FCalEt(icent), size, 43);
        if (i_vn_type != Bins::VN_DIRECT) {
            if (m_multType == 0) Common::myText2(X + 0.25, Y - 0.18, 1, Bins::label_cent_peri  (icent_periph), size, 43);
            if (m_multType == 1) Common::myText2(X + 0.25, Y - 0.18, 1, Bins::label_FCalEt_peri(icent_periph), size, 43);
        }

        Common::myText2(0.7, Y, 1, "Template Fit", size, 63);
    }
}

void ReassignRatioStatErrorMult(TH1* InputHist, std::string base, int ihar, int ipt2, int icent_periph, std::vector<int> centbins) {
    char name[600];
    sprintf(name, "01RootFiles/%s_fgTag0_TemplateFits_pedestal_vnRatioErrors.root" , base.c_str());
    TFile* ratioFile = new TFile(name , "read");
    Common::CheckFile(ratioFile, name);

    sprintf(name, "h_ratio_v%d_pericent%d_pta6_ptb%.2d_ch2_deta01", ihar, icent_periph, ipt2);
    TH1* hist_ratio = (TH1D*) ratioFile->Get(name);

    for (int i = 1; i <= InputHist->GetNbinsX(); i++) {
        InputHist->SetBinError(i, hist_ratio->GetBinError(centbins[i - 1] + 1));
    }
}

void ReassignRatioStatErrorPT(TH1* InputHist, std::string base, int ihar, int icent, int icent_periph, std::vector<int> ptbbins) {
    char name[600];
    sprintf(name, "01RootFiles/%s_fgTag0_TemplateFits_pedestal_vnRatioErrors.root" , base.c_str());
    TFile* ratioFile = new TFile(name , "read");
    Common::CheckFile(ratioFile, name);

    for (int i = 1; i <= InputHist->GetNbinsX(); i++) {
        double pt_lo = InputHist->GetBinLowEdge(i);
        if (pt_lo < 0.5 - 0.001) continue;
        sprintf(name, "h_ratio_v%d_pericent%d_pta6_ptb%.2d_ch2_deta01", ihar, icent_periph, ptbbins[i - 1]);
        TH1* hist_ratio = (TH1D*) ratioFile->Get(name);
        InputHist->SetBinError(i, hist_ratio->GetBinError(icent + 1));
    }
}


void parsePaperPoints()
{
    Double_t xAxis6a[18] = {0, 5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150};
    h_pp13_stat = new TH1D("h_pp13_stat", "", 17, xAxis6a);
    h_pp13_stat->SetBinContent( 5, 0.06097218);
    h_pp13_stat->SetBinContent( 6, 0.06235108);
    h_pp13_stat->SetBinContent( 7, 0.06340845);
    h_pp13_stat->SetBinContent( 8, 0.06282482);
    h_pp13_stat->SetBinContent( 9, 0.06445208);
    h_pp13_stat->SetBinContent(10, 0.06488898);
    h_pp13_stat->SetBinContent(11, 0.06439517);
    h_pp13_stat->SetBinContent(12, 0.06557894);
    h_pp13_stat->SetBinContent(13, 0.06435218);
    h_pp13_stat->SetBinContent(14, 0.06346405);
    h_pp13_stat->SetBinContent(15, 0.06326561);
    h_pp13_stat->SetBinContent(16, 0.06616382);
    h_pp13_stat->SetBinContent(17, 0.06340954);
    h_pp13_stat->SetBinError  ( 5, 0.0004991889);
    h_pp13_stat->SetBinError  ( 6, 0.0003098063);
    h_pp13_stat->SetBinError  ( 7, 0.000257127 );
    h_pp13_stat->SetBinError  ( 8, 0.0002583414);
    h_pp13_stat->SetBinError  ( 9, 0.0001485571);
    h_pp13_stat->SetBinError  (10, 0.0001677945);
    h_pp13_stat->SetBinError  (11, 0.0002111122);
    h_pp13_stat->SetBinError  (12, 0.0001349971);
    h_pp13_stat->SetBinError  (13, 0.000176549 );
    h_pp13_stat->SetBinError  (14, 0.000243023 );
    h_pp13_stat->SetBinError  (15, 0.0003509521);
    h_pp13_stat->SetBinError  (16, 0.0005021617);
    h_pp13_stat->SetBinError  (17, 0.0008209348);
    h_pp13_stat->SetEntries(13);
    h_pp13_stat->SetMarkerStyle(20);
    h_pp13_stat->SetMarkerColor(kBlue);
    h_pp13_stat->SetLineColor(kBlue);
    // h_pp13_stat->Draw("same");

    Double_t xAxis3[18] = {0, 5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150};
    h_pp13_syst = new TH1D("h_pp13_syst", "", 17, xAxis3);
    h_pp13_syst->SetBinContent( 5, 0.06097218);
    h_pp13_syst->SetBinContent( 6, 0.06235108);
    h_pp13_syst->SetBinContent( 7, 0.06340845);
    h_pp13_syst->SetBinContent( 8, 0.06282482);
    h_pp13_syst->SetBinContent( 9, 0.06445208);
    h_pp13_syst->SetBinContent(10, 0.06488898);
    h_pp13_syst->SetBinContent(11, 0.06439517);
    h_pp13_syst->SetBinContent(12, 0.06557894);
    h_pp13_syst->SetBinContent(13, 0.06435218);
    h_pp13_syst->SetBinContent(14, 0.06346405);
    h_pp13_syst->SetBinContent(15, 0.06326561);
    h_pp13_syst->SetBinContent(16, 0.06616382);
    h_pp13_syst->SetBinContent(17, 0.06340954);
    h_pp13_syst->SetBinError  ( 5, 0.002299609);
    h_pp13_syst->SetBinError  ( 6, 0.002097321);
    h_pp13_syst->SetBinError  ( 7, 0.001647619);
    h_pp13_syst->SetBinError  ( 8, 0.00121736 );
    h_pp13_syst->SetBinError  ( 9, 0.001415015);
    h_pp13_syst->SetBinError  (10, 0.001418656);
    h_pp13_syst->SetBinError  (11, 0.001414548);
    h_pp13_syst->SetBinError  (12, 0.001424598);
    h_pp13_syst->SetBinError  (13, 0.001414196);
    h_pp13_syst->SetBinError  (14, 0.00140714 );
    h_pp13_syst->SetBinError  (15, 0.00140562 );
    h_pp13_syst->SetBinError  (16, 0.001429812);
    h_pp13_syst->SetBinError  (17, 0.001406721);
    h_pp13_syst->SetEntries(13);
    h_pp13_syst->SetLineColor(kBlue - 9);
    h_pp13_syst->SetLineWidth(10);
    h_pp13_syst->SetMarkerStyle(20);
    h_pp13_syst->SetMarkerSize(0.1);
}
