#include "Riostream.h"
#include "TFile.h"
#include "TF1.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include <TStyle.h>

#include "bins.h"

void Plots_jetEta(DEFAULT_SETTINGS) {
    string base = BASENAME;
    char name [600];
    TFile *input;
    sprintf(name , "../02AnaCorr/01Rootfiles/%s.root", base.c_str());
    input  = new TFile(name);
    if (input->IsZombie()) {std::cout << name << " Not Found" << std::endl; throw std::exception();}

    TH1D *h_JetEta10[Bins::NCENT + Bins::NCENT_ADD];

    char hname[100];
    //Read in All Histograms
    for (int icent = 0; icent < Bins::NCENT; icent++) {
        sprintf(hname, "h_JetEta10_cent%.2d", icent);
        h_JetEta10[icent] = (TH1D*)input->Get(hname);
        if (!h_JetEta10[icent]) {std::cout << hname << " Not found" << std::endl; throw std::exception();}
    }

    //Add more multiplicity bins
    for (int icent = Bins::NCENT; icent < Bins::NCENT + Bins::NCENT_ADD; icent++) {
        for (int icent_add = Bins::cent_add_lo[icent - Bins::NCENT]; icent_add < Bins::cent_add_up[icent - Bins::NCENT]; icent_add++) {
            TH1D* h_JetEta10_add = h_JetEta10[icent_add];

            if (icent_add == Bins::cent_add_lo[icent - Bins::NCENT]) {
                sprintf(hname, "h_JetEta10_cent%.2d", icent); h_JetEta10[icent] = (TH1D*)h_JetEta10_add->Clone(hname); h_JetEta10[icent]->SetTitle(hname);
            }
            else {
                h_JetEta10[icent]->Add(h_JetEta10_add);
            }
        }
    }

    //Write All Histograms
    std::vector<TCanvas*> m_can_vec;
    std::vector<int> m_centralities = Bins::GetCentIndex({0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150});

    for (int icent = 0; icent < (int) m_centralities.size(); icent++) {
        int cent_lo = Bins::GetCentVals(m_centralities[icent]).first;
        int cent_hi = Bins::GetCentVals(m_centralities[icent]).second;
        if (cent_lo > 150) continue;
        if (cent_hi > 160) continue;
        // if (cent_lo < 10)  continue;
        if (cent_hi - cent_lo <= 5 || cent_hi - cent_lo >= 20) continue;

        sprintf(name, "can_h_JetEta10_cent%.2d_%.2d", cent_lo, cent_hi);
        TCanvas* c1 = new TCanvas(name, name, 700, 500);
        c1->SetLeftMargin  (0.12);
        c1->SetTopMargin   (0.04);
        c1->SetBottomMargin(0.12);
        c1->SetRightMargin (0.04);

        h_JetEta10[m_centralities[icent]]->Scale(1. / h_JetEta10[m_centralities[icent]]->Integral() / h_JetEta10[m_centralities[icent]]->GetBinWidth(1), "nosw2");
        h_JetEta10[m_centralities[icent]]->GetYaxis()->SetTitleOffset(1.0);
        h_JetEta10[m_centralities[icent]]->GetXaxis()->SetTitleOffset(1.0);

        h_JetEta10[m_centralities[icent]]->Draw();

        // c1->SetLogy();

        float X, Y, SIZE;
        X = 0.18, Y = 0.9, SIZE = 24;
        Common::myText2(X       , Y      , 1, "ATLAS"         , SIZE, 73);
        Common::myText2(X + 0.14, Y      , 1, Common::Internal, SIZE, 43);
        Common::myText2(X       , Y - .06, 1, DataSetEnums::DATASETLABEL[1] + " " + DataSetEnums::DATASETENERGY[1], SIZE, 43);
        if (l_multiplicity_type == 0) sprintf(name, "%d#leq#it{N}_{ ch}^{ rec}<%d", cent_lo, cent_hi);
        if (l_multiplicity_type == 1) sprintf(name, "%d#leq E_{T}^{ FCal}<%d", cent_lo, cent_hi);
        Common::myText2(X, Y - .12, 1, name, SIZE, 63);

        m_can_vec.push_back(c1);
    }
    Common::SaveCanvas(m_can_vec, base);
}
