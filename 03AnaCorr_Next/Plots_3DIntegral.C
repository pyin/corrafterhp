#include "Riostream.h"
#include "TFile.h"
#include "TF1.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TCanvas.h"
#include <TStyle.h>
#include <TProfile.h>

#include "bins.h"
#include "DatasetEnums.h"

void Plots_3DIntegral() {
    char name [600];
    TFile *input;
    sprintf(name , "../02AnaCorr/03GlobalEtaPhi/Data_LowAndIntermediateMu_eff1_trig2_pileup8_multtype0_globalEtaPhi.root");
    input  = new TFile(name);
    if (input->IsZombie()) {std::cout << name << " Not Found" << std::endl; throw std::exception();}

    TFile *tempinput;
    sprintf(name , "../02AnaCorr/Condor/01Rootfiles/Data_LowAndIntermediateMu_eff1_trig2_pileup8_multtype0_globalEtaPhi.root");
    // sprintf(name , "../02AnaCorr/Condor/01Rootfiles/Data_LowAndIntermediateMu_eff1_trig2_multtype0.root");
    tempinput  = new TFile(name);
    if (tempinput->IsZombie()) {std::cout << name << " Not Found" << std::endl; throw std::exception();}

    sprintf(name , "01RootFiles/Data_LowAndIntermediateMu_eff1_trig2_pileup8_multtype0_globalEtaPhi_new.root");
    TFile *output = new TFile(name, "recreate");

    TH1D *hcent;
    TH3D *h_EtaPhi_EbyE         [Bins::NCENT + Bins::NCENT_ADD];
    TH3D *h_EtaPhi_ptWeight_EbyE[Bins::NCENT + Bins::NCENT_ADD];

    // TH3D *h_EtaPhi_EbyE_new         [Bins::NCENT + Bins::NCENT_ADD];
    TH3D *h_EtaPhi_ptWeight_EbyE_new[Bins::NCENT + Bins::NCENT_ADD];

    TProfile *h_etaDept_ptWeight_EbyE[8];

    char hname[100];
    //Read in All Histograms
    for (int icent = 0; icent < Bins::NCENT; icent++) {
        sprintf(hname, "h_EtaPhi_EbyE_cent%d", icent);
        h_EtaPhi_EbyE[icent] = (TH3D*)input->Get(hname);
        if (!h_EtaPhi_EbyE[icent]) {std::cout << hname << " Not found" << std::endl; throw std::exception();}

        sprintf(hname, "h_EtaPhi_ptWeight_EbyE_cent%d", icent);
        h_EtaPhi_ptWeight_EbyE[icent] = (TH3D*)input->Get(hname);
        if (!h_EtaPhi_ptWeight_EbyE[icent]) {std::cout << hname << " Not found" << std::endl; throw std::exception();}
    }

    sprintf(hname, "hcent");
    hcent = (TH1D*)tempinput->Get(hname);
    if (!hcent) {std::cout << hname << " Not found" << std::endl; throw std::exception();}

    //Add more multiplicity bins
    for (int icent = Bins::NCENT; icent < Bins::NCENT + Bins::NCENT_ADD; icent++) {
        for (int icent_add = Bins::cent_add_lo[icent - Bins::NCENT]; icent_add < Bins::cent_add_up[icent - Bins::NCENT]; icent_add++) {
            TH3D* h_EtaPhi_EbyE_add        = h_EtaPhi_ptWeight_EbyE       [icent_add];
            TH3D* h_EtaPhi_ptWeight_EbyE_add    = h_EtaPhi_ptWeight_EbyE   [icent_add];

            if (icent_add == Bins::cent_add_lo[icent - Bins::NCENT]) {
                sprintf(hname, "h_EtaPhi_EbyE_cent%d"         , icent); h_EtaPhi_EbyE         [icent] = (TH3D*)h_EtaPhi_EbyE_add         ->Clone(hname); h_EtaPhi_EbyE         [icent]->SetTitle(hname);
                sprintf(hname, "h_EtaPhi_ptWeight_EbyE_cent%d", icent); h_EtaPhi_ptWeight_EbyE[icent] = (TH3D*)h_EtaPhi_ptWeight_EbyE_add->Clone(hname); h_EtaPhi_ptWeight_EbyE[icent]->SetTitle(hname);
            }
            else {
                h_EtaPhi_EbyE         [icent]->Add(h_EtaPhi_EbyE_add         );
                h_EtaPhi_ptWeight_EbyE[icent]->Add(h_EtaPhi_ptWeight_EbyE_add);
            }
        }
    }

    //Write All Histograms
    std::vector<TCanvas*> m_can_vec;
    std::vector<int> m_centralities = Bins::GetCentIndex({0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150});
    int col[10] = {kRed, kBlue, kGreen + 1, kCyan + 1, kYellow + 1, kOrange + 1, kAzure + 1, kMagenta, kGray, kBlack};

    TProfile *main = nullptr;

    for (int icent = 0; icent < (int) m_centralities.size(); icent++) {
        float X, Y, SIZE;
        int cent_lo = Bins::GetCentVals(m_centralities[icent]).first;
        int cent_hi = Bins::GetCentVals(m_centralities[icent]).second;
        if (cent_lo > 150) continue;
        if (cent_hi > 160) continue;
        if (cent_hi - cent_lo <= 5 || cent_hi - cent_lo >= 20) continue;

        int nEvents = hcent->GetBinContent(m_centralities[icent] + 1);
        if (m_centralities[icent] == 40) nEvents = hcent->GetBinContent(39) + hcent->GetBinContent(40);
        if (m_centralities[icent] == 41) nEvents = hcent->GetBinContent(37) + hcent->GetBinContent(38);
        if (m_centralities[icent] == 42) nEvents = hcent->GetBinContent(35) + hcent->GetBinContent(36);
        if (m_centralities[icent] == 43) nEvents = hcent->GetBinContent(33) + hcent->GetBinContent(34);
        if (m_centralities[icent] == 44) nEvents = hcent->GetBinContent(31) + hcent->GetBinContent(32);
        if (m_centralities[icent] == 45) nEvents = hcent->GetBinContent(29) + hcent->GetBinContent(30);

        sprintf(hname, "h_EtaPhi_ptWeight_EbyE_new_cent%d", m_centralities[icent]);
        h_EtaPhi_ptWeight_EbyE_new[m_centralities[icent]] = new TH3D(hname, hname, 8, -2.5, 2.5, 8, -acos(-1.0), acos(-1.0), 200, 0.0, 20.0);

        sprintf(name, "can_etaDept_ptWeight_EbyE_new_cent%.2d_%.2d", cent_lo, cent_hi);
        TCanvas* c1 = new TCanvas(name, name, 700, 500);
        c1->SetLeftMargin  (0.12);
        c1->SetTopMargin   (0.04);
        c1->SetBottomMargin(0.12);
        c1->SetRightMargin (0.04);
        m_can_vec.push_back(c1);
        c1->SetLogy();

        if (icent == 0) main = new TProfile("temp", "temp", 200, 0.0, 20.0);

        for (int x = 1; x <= 8; x++) {
            double eta = h_EtaPhi_ptWeight_EbyE_new[m_centralities[icent]]->GetXaxis()->GetBinCenter(x);
            sprintf(hname, "h_etaDept_ptWeight_EbyE_new_cent%d_eta%d", m_centralities[icent], x);
            h_etaDept_ptWeight_EbyE[x - 1] = new TProfile(hname, hname, 200, 0.0, 20.0);
            for (int y = 1; y <= 8; y++) {
                double phi = h_EtaPhi_ptWeight_EbyE_new[m_centralities[icent]]->GetYaxis()->GetBinCenter(y);
                for (int z = 1; z <= 200; z++) {
                    double pt   = h_EtaPhi_ptWeight_EbyE_new[m_centralities[icent]]->GetZaxis()->GetBinCenter(z);
                    double val2 = h_EtaPhi_ptWeight_EbyE[m_centralities[icent]]->Integral(x, x, y, y, z, 200);

                    h_EtaPhi_ptWeight_EbyE_new[m_centralities[icent]]->Fill       (eta, phi, pt, val2);
                    h_EtaPhi_ptWeight_EbyE_new[m_centralities[icent]]->SetBinError(eta, phi, pt, 0   );

                    h_etaDept_ptWeight_EbyE[x - 1]->Fill(pt, val2);
                    if (icent == 0 && x == 1) main->Fill(pt, val2);
                }
            }

            h_etaDept_ptWeight_EbyE[x - 1]->Scale(1. / nEvents);
            h_etaDept_ptWeight_EbyE[x - 1]->SetMarkerSize(0.5);
            h_etaDept_ptWeight_EbyE[x - 1]->SetMarkerColor(col[x - 1]);
            h_etaDept_ptWeight_EbyE[x - 1]->SetLineColor(col[x - 1]);

            h_etaDept_ptWeight_EbyE[x - 1]->Draw(x == 1 ? "" : "same");


            if (x == 1) {
                h_etaDept_ptWeight_EbyE[x - 1]->GetYaxis()->SetRangeUser(5e-8,100);
                h_etaDept_ptWeight_EbyE[x - 1]->GetXaxis()->SetRangeUser(0.4, 200);
            }

            if (icent == 0 && x == 1) {
                main->Scale(1. / nEvents);
                main->SetMarkerSize(0.5);
                main->SetMarkerColor(kBlack);
                main->SetLineColor(kBlack);
            }
        }

        if (icent != 0) main->Draw("same");

        X = 0.18, Y = 0.9, SIZE = 24;
        Common::myText2(X       , Y      , 1, "ATLAS"         , SIZE, 73);
        Common::myText2(X + 0.14, Y      , 1, Common::Internal, SIZE, 43);
        Common::myText2(X       , Y - .06, 1, DataSetEnums::DATASETLABEL[1] + " " + DataSetEnums::DATASETENERGY[1], SIZE, 43);
        X = 0.60, Y = 0.88, SIZE = 24;
        sprintf(name, "%d#leq#it{N}_{ ch}^{ rec}<%d", cent_lo, cent_hi);
        Common::myText2(X, Y, 1, name, SIZE, 63);
    }

    output->cd();
    output->Write();

    Common::SaveCanvas(m_can_vec, "");
}
