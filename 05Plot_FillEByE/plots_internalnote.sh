

root -b -l -q "Plots_TemplateFits.C+(2,0,5,1,9,5,1,1,0,0,0,2,1,0,0,    0,     1)"  #inclusive
mv figs/*.png  figs/results/templateFit/inclusive
root -b -l -q "Plots_TemplateFits.C+(2,0,5,1,0,5,1,1,2,9,4,2,1,2,0,    0,     1)"
mv figs/*.png  figs/results/templateFit/AllEvents
root -b -l -q "Plots_TemplateFits.C+(2,0,5,1,0,5,1,1,2,9,4,1,1,2,0,    0,     1)"
mv figs/*.png  figs/results/templateFit/WithJets
root -b -l -q "Plots_TemplateFits.C+(2,0,5,1,0,5,1,1,2,9,4,0,1,2,0,    0,     1)"
mv figs/*.png  figs/results/templateFit/NoJets
root -b -l -q "Plots_TemplateFits.C+(2,0,5,1,3,5,1,1,2,9,4,2,1,1,0,    1,     1)"
mv figs/*.png  figs/results/templateFit/JH




root -b -l -q "Plots_Bootstrip_1D2PC.C+(2,0,5,1,9,5,1,1,0,0,0,2,1,0,0,    0)"
mv figs/*.png  figs/bootstrapping/inclusive
root -b -l -q "Plots_Bootstrip_1D2PC.C+(2,0,5,1,0,5,1,1,2,9,4,2,1,2,0,    0)"
mv figs/*.png  figs/bootstrapping/allevents
root -b -l -q "Plots_Bootstrip_1D2PC.C+(2,0,5,1,0,5,1,1,2,9,4,1,1,2,0,    0)"
mv figs/*.png  figs/bootstrapping/withjets
root -b -l -q "Plots_Bootstrip_1D2PC.C+(2,0,5,1,0,5,1,1,2,9,4,0,1,2,0,    0)"
mv figs/*.png  figs/bootstrapping/nojets
root -b -l -q "Plots_Bootstrip_1D2PC.C+(2,0,5,1,3,5,1,1,2,9,4,2,1,1,0,    1)"
mv figs/*.png  figs/bootstrapping/jh



root -b -l -q "Plots_Bootstrip_vnn.C+(2,0,5,1,9,5,1,1,0,0,0,2,1,0,0,    0)"
mv figs/*.png  figs/bootstrapping/inclusive
root -b -l -q "Plots_Bootstrip_vnn.C+(2,0,5,1,0,5,1,1,2,9,4,2,1,2,0,    0)"
mv figs/*.png  figs/bootstrapping/allevents
root -b -l -q "Plots_Bootstrip_vnn.C+(2,0,5,1,0,5,1,1,2,9,4,1,1,2,0,    0)"
mv figs/*.png  figs/bootstrapping/withjets
root -b -l -q "Plots_Bootstrip_vnn.C+(2,0,5,1,0,5,1,1,2,9,4,0,1,2,0,    0)"
mv figs/*.png  figs/bootstrapping/nojets
root -b -l -q "Plots_Bootstrip_vnn.C+(2,0,5,1,3,5,1,1,2,9,4,2,1,1,0,    1)"
mv figs/*.png  figs/bootstrapping/jh




root -b -l -q Plots_TrackEffSystematic.C++
mv figs/*.png  figs/systematics/TrackEffSystematic
root -b -l -q Plots_PeriphBinSystematic.C++
mv figs/*.png  figs/systematics/PeriphBinSystematic
root -b -l -q Plots_EvtMixingSystematic.C++
mv figs/*.png  figs/systematics/EvtMixing




root -b -l -q Plots_Vn_withSystematic.C++
mv figs/*.png  figs/results/vn/


root -b -l -q Plots_Ntrk_allcase.C++
mv figs/*.png  figs/datasets



root -b -l -q "Plots_TemplateFits_paper_allLegTogether.C+(2,0,5,1,9,5,1,1,0,0,0,2,1,0,0,    0,     1)"
root -b -l -q "Plots_TemplateFits_paper_allLegTogether.C+(2,0,5,1,0,5,1,1,2,9,4,2,1,2,0,    0,     1)"
root -b -l -q "Plots_TemplateFits_paper_allLegTogether.C+(2,0,5,1,0,5,1,1,2,9,4,1,1,2,0,    0,     1)"
root -b -l -q "Plots_TemplateFits_paper_allLegTogether.C+(2,0,5,1,0,5,1,1,2,9,4,0,1,2,0,    0,     1)"
root -b -l -q "Plots_TemplateFits_paper_allLegTogether.C+(2,0,5,1,3,5,1,1,2,9,4,2,1,1,0,    1,     1)"
