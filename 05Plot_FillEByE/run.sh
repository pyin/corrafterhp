#!/bin/bash

# ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
# asetup 21.2.170,AnalysisBase
# asetup 21.2,latest,AnalysisBase
# lsetup "root 6.18.04-x86_64-centos7-gcc8-opt"

root -b -l -q "S01_RebinCentrality.C+(  $1,$2,$3,$4,$5,$6,$7,$8,$9,${10},${11},${12},${13},${14},${15})"
root -b -l -q "S07a_FitPTY_Template.C+( $1,$2,$3,$4,$5,$6,$7,$8,$9,${10},${11},${12},${13},${14},${15},        ${16})"

# 0: 10-30
# 1: 20-40
# 2: 10-40
# 3: 40-150
# 4: 60-150

touch done_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_${9}_${10}_${11}_${12}_${13}_${14}_${15}_${16}.txt
