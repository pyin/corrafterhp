#include "Riostream.h"
#include "TFile.h"
#include "TH1.h"
#include "TF1.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "bins.h"
#include "TROOT.h"
#include "TGaxis.h"

#include <TStyle.h>

using namespace Bins;
using namespace DataSetEnums;


std::map<int, int> centbins_to_ptcent = {
    {Bins::GetCentIndex(10,  30), 0},
    {Bins::GetCentIndex(20,  40), 1},
    {Bins::GetCentIndex(10,  40), 2},
    {Bins::GetCentIndex(40, 150), 3},
    {Bins::GetCentIndex(60, 150), 4}
};

std::vector<TCanvas*> m_can_vec;

void Draw1D(int icent1, int icent2);
void Draw1D_pTDept(int iptb, int icent1, int icent2);

TFile *InFile;
TFile *BSErrFile;
char name [600];
char name1[600];
int m_data_type;
int m_correlation_type;
int m_if_have_jet;
int m_if_have_uecorr;
int m_fgTag;
int m_useBSErr;


void Plots_TemplateFits_paper_allLegTogether(DEFAULT_SETTINGS, int fgTag = 0, int useBSErr = 0) {
    gStyle->SetOptTitle(0);
    gStyle->SetOptStat(0);
    std::string base = BASENAME;
    m_data_type        = l_data_type;
    m_correlation_type = l_correlation_type;
    m_if_have_jet      = l_if_have_jet;
    m_if_have_uecorr   = l_if_have_uecorr;
    m_fgTag            = fgTag;
    m_useBSErr         = useBSErr;

    sprintf(name , "01RootFiles/%s_fgTag%d_TemplateFits_pedestal.root", base.c_str(), fgTag);
    InFile = new TFile(name , "read");
    std::cout << "input  file: " << name  << std::endl;

    if (m_useBSErr == 1) {
        sprintf(name , "01RootFiles/%s_fgTag%d_BootstrapError.root", base.c_str(), fgTag);
        BSErrFile = new TFile(name , "read");
        std::cout << "BS error  file: " << name  << std::endl;
    }


    //------------------------------------------------------------------------------
    std::vector<int> centbins_peripheral = {Bins::GetCentIndex(10, 30)};
    std::vector<int> centbins;

    int icent1 = Bins::GetCentIndex(40, 150);
    int icent2 = Bins::GetCentIndex(10,  30);

    if (m_if_have_jet == 2 && m_correlation_type == 1) {
        Draw1D(Bins::GetCentIndex(50,  60), icent2); Draw1D(Bins::GetCentIndex(90, 100), icent2); Draw1D(Bins::GetCentIndex(40, 150), icent2);
        Draw1D_pTDept(1, icent1, icent2); Draw1D_pTDept(3, icent1, icent2); Draw1D_pTDept(5, icent1, icent2);
    }
    else {
        Draw1D(Bins::GetCentIndex(40, 150), icent2);
        if (m_if_have_jet == 2 && m_correlation_type == 2) {Draw1D_pTDept(1, icent1, icent2); Draw1D_pTDept(3, icent1, icent2); Draw1D_pTDept(5, icent1, icent2);}
    }

    std::cout << "Saving" << std::endl;
    std::string base2 = "pedestal" + base;
    Common::SaveCanvas(m_can_vec, base);
}


void Draw1D(int icent1, int icent2) {
    int cent_lo  = Bins::GetCentVals(icent1).first ;
    int cent_hi  = Bins::GetCentVals(icent1).second;
    int centPeriph_lo = Bins::GetCentVals(icent2).first;
    int centPeriph_hi = Bins::GetCentVals(icent2).second;

    TH1D *h_central, *h_rescaledperipheral, *h_fit_func;
    TF1  *f_pedestal, *f_vnn_combined;

    sprintf(name , "cent%.2d_pericent%.2d", icent1, icent2);
    sprintf(name1, "h_central_%s"           , name); h_central            = (TH1D*)InFile->Get(name1); Common::CheckObject2(h_central           , name1, InFile);
    sprintf(name1, "h_rescaledperipheral_%s", name); h_rescaledperipheral = (TH1D*)InFile->Get(name1); Common::CheckObject2(h_rescaledperipheral, name1, InFile);
    sprintf(name1, "h_fit_func_%s"          , name); h_fit_func           = (TH1D*)InFile->Get(name1); Common::CheckObject2(h_fit_func          , name1, InFile);
    sprintf(name1, "f_pedestal_%s"          , name); f_pedestal           = (TF1* )InFile->Get(name1); Common::CheckObject2(f_pedestal          , name1, InFile);
    sprintf(name1, "f_vnn_combined_%s"      , name); f_vnn_combined       = (TF1* )InFile->Get(name1); Common::CheckObject2(f_vnn_combined      , name1, InFile);
    if (h_central->Integral() == 0) {std::cout << "Skipping " << name << "as integral is 0" << std::endl; return;}

    if (m_useBSErr == 1) {
        // std::cout << "multdept:  assign error from bootstrapping" << std::endl;
        sprintf(name, "fg_nchDept_BS_cent%.2d", icent1); TH1D* h_central_err = (TH1D*)BSErrFile->Get(name); Common::CheckObject2(h_central_err, name, BSErrFile);
        for (int i = 1; i <= 24; i++) {
            h_central->SetBinError(i, h_central_err->GetBinError(i));
        }
    }

    h_central->GetXaxis()->SetTitle("#Delta#phi");
    h_central->GetYaxis()->SetTitle("#it{C}(#Delta#phi)");
    h_central->GetXaxis()->SetTitleOffset(1.1);
    h_central->GetYaxis()->SetTitleOffset(1.1);

    // float max = h_central->GetBinContent(h_central->GetMaximumBin());
    // float min = h_central->GetBinContent(h_central->GetMinimumBin());
    // h_central->SetMaximum(max + (max - min) / 3);
    // h_central->SetMinimum(min - (max - min) / 5);
    // if (m_if_have_jet == 1 && m_correlation_type == 2) h_central->SetMinimum(min - (max - min) / 8);
    // if (m_if_have_jet == 2 && m_correlation_type == 1) h_central->SetMinimum(min - (max - min) / 8);
    // h_central->GetYaxis()->SetRangeUser(0.972, 1.042);
    if (m_if_have_jet == 2 && m_correlation_type == 1) h_central->GetYaxis()->SetRangeUser(0.905, 1.149);
    else                                               h_central->GetYaxis()->SetRangeUser(0.972, 1.042);

    sprintf(name , "cent%.2d_%.2d_pericent%.2d_%.2d", cent_lo, cent_hi, centPeriph_lo, centPeriph_hi);
    sprintf(name1, "can_%s", name);
    TCanvas *Can = new TCanvas(name1, name1, 700, 500);
    Can->SetLeftMargin  (0.15);
    Can->SetTopMargin   (0.05);
    Can->SetBottomMargin(0.15);
    Can->SetRightMargin (0.05);
    m_can_vec.push_back(Can);

    h_central->GetYaxis()->SetNdivisions (505);

    h_central           ->Draw();
    h_rescaledperipheral->Draw("same");
    h_fit_func          ->Draw("same");
    f_pedestal          ->Draw("same");
    f_vnn_combined      ->Draw("same");

    float X = 0.04, Y = 0.90, SIZE = 28;
    if (m_if_have_jet == 2 && m_correlation_type == 0) Common::myText2(X, Y, 1, "h-h", SIZE, 63);
    if (m_if_have_jet == 2 && m_correlation_type == 2) Common::myText2(X, Y, 1, "h^{UE}-h^{UE}: AllEvents"    , SIZE, 63);
    if (m_if_have_jet == 0 && m_correlation_type == 2) Common::myText2(X, Y, 1, "h^{UE}-h^{UE}: NoJets"  , SIZE, 63);
    if (m_if_have_jet == 1 && m_correlation_type == 2) Common::myText2(X, Y, 1, "h^{UE}-h^{UE}: WithJets", SIZE, 63);
    if (m_if_have_jet == 2 && m_correlation_type == 1) Common::myText2(X, Y, 1, "h^{UE}-h^{J}: #it{p}_{T}^{G} >40 GeV", SIZE, 63);
    Common::myText2(X, Y - 0.11, 1, label_cent_corr(icent1), SIZE, 63);

    if (m_if_have_jet == 2 && m_correlation_type == 1 && cent_lo == 40 && cent_hi == 150) {
        Common::myText2(X       , Y - 0.25, 1, "ATLAS"   , SIZE, 73);
        Common::myText2(X + 0.18, Y - 0.25, 1, Common::Internal, SIZE, 43);
        Common::myText2(X       , Y - 0.34, 1, DATASETLABEL[m_data_type] + " " + DATASETENERGY[m_data_type], SIZE, 43);
        Common::myText2(X       , Y - 0.43, 1, label_eta(Bins::GetDetaIndex(2.0, 5.0)) + ", " + label_ptab(Bins::GetPtaIndex(0.5, 4.0), Bins::GetPtbIndex(0.5, 4.0)), SIZE, 43);
        // Common::myText2(X + 0.20, Y - 0.43, 1, label_ptab(Bins::GetPtaIndex(0.5, 4.0), Bins::GetPtbIndex(0.5, 4.0)), SIZE, 43);
        // Common::myText2(X       , Y - 0.52, 1, label_cent_peri(icent2), SIZE, 43);
    }

    if (m_if_have_jet == 2 && m_correlation_type == 1 && cent_lo == 50 && cent_hi == 60) {
        TLegend *leg1 = new TLegend(0.18, 0.45, 0.50, 0.72);
        leg1->SetTextFont(43);
        leg1->SetTextSize(28);
        leg1->SetBorderSize(0);
        leg1->SetFillStyle (0);
        leg1->AddEntry(h_central           , "#it{C}(#Delta#phi)"                 , "p");
        leg1->AddEntry(h_rescaledperipheral, "#it{G}+#it{FC}^{periph}(#Delta#phi)", "p");
        leg1->AddEntry(h_fit_func          , "Fit"                                , "l");
        leg1->Draw();
    }

    if (m_if_have_jet == 2 && m_correlation_type == 1 && cent_lo == 90 && cent_hi == 100) {
        TLegend *leg1 = new TLegend(0.18, 0.54, 0.50, 0.72);
        leg1->SetTextFont(43);
        leg1->SetTextSize(28);
        leg1->SetBorderSize(0);
        leg1->SetFillStyle (0);
        leg1->AddEntry(f_vnn_combined, "#it{C}^{ridge}(#Delta#phi)+#it{FC}^{periph}(0)", "l");
        leg1->AddEntry(f_pedestal    , "#it{G}+#it{FC}^{periph}(0)"                    , "l");
        leg1->Draw();
    }
}








void Draw1D_pTDept(int iptb, int icent1, int icent2) {
    int cent_lo  = Bins::GetCentVals(icent1).first ;
    int cent_hi  = Bins::GetCentVals(icent1).second;
    int centPeriph_lo = Bins::GetCentVals(icent2).first;
    int centPeriph_hi = Bins::GetCentVals(icent2).second;


    TH1D *h_central, *h_rescaledperipheral, *h_fit_func;
    TF1  *f_pedestal, *f_vnn_combined;

    sprintf(name , "ptb%.2d_cent%.2d_pericent%.2d", iptb, icent1, icent2);
    sprintf(name1, "h_central_%s"           , name); h_central            = (TH1D*)InFile->Get(name1); Common::CheckObject2(h_central           , name1, InFile);
    sprintf(name1, "h_rescaledperipheral_%s", name); h_rescaledperipheral = (TH1D*)InFile->Get(name1); Common::CheckObject2(h_rescaledperipheral, name1, InFile);
    sprintf(name1, "h_fit_func_%s"          , name); h_fit_func           = (TH1D*)InFile->Get(name1); Common::CheckObject2(h_fit_func          , name1, InFile);
    sprintf(name1, "f_pedestal_%s"          , name); f_pedestal           = (TF1* )InFile->Get(name1); Common::CheckObject2(f_pedestal          , name1, InFile);
    sprintf(name1, "f_vnn_combined_%s"      , name); f_vnn_combined       = (TF1* )InFile->Get(name1); Common::CheckObject2(f_vnn_combined      , name1, InFile);
    if (h_central->Integral() == 0) {std::cout << "Skipping " << name << "as integral is 0" << std::endl; return;}

    if (m_useBSErr == 1) {
        // std::cout << "ptdept:  assign error from bootstrapping" << std::endl;
        sprintf(name, "fg_pTbDept_BS_ptb%d_cent%.2d", iptb, centbins_to_ptcent[icent1]); TH1D* h_central_err = (TH1D*)BSErrFile->Get(name); Common::CheckObject2(h_central_err, name, BSErrFile);
        for (int i = 1; i <= 24; i++) {
            h_central->SetBinError(i, h_central_err->GetBinError(i));
        }
    }

    h_central->GetXaxis()->SetTitle("#Delta#phi");
    h_central->GetYaxis()->SetTitle("#it{C}(#Delta#phi)");
    h_central->GetXaxis()->SetTitleOffset(1.1);
    h_central->GetYaxis()->SetTitleOffset(1.1);

    h_central->GetYaxis()->SetRangeUser(0.905, 1.149);

    if (m_if_have_jet == 2 && m_correlation_type == 1) h_central->GetYaxis()->SetRangeUser(0.905, 1.149);
    else                                               h_central->GetYaxis()->SetRangeUser(0.945, 1.092);

    sprintf(name , "ptb%d_cent%.2d_%.2d_pericent%.2d_%.2d_fgTag%d", iptb, cent_lo, cent_hi, centPeriph_lo, centPeriph_hi, m_fgTag);
    sprintf(name1, "can_fits_%s", name);
    TCanvas *Can = new TCanvas(name1, name1, 700, 500);
    Can->SetLeftMargin  (0.15);
    Can->SetTopMargin   (0.05);
    Can->SetBottomMargin(0.15);
    Can->SetRightMargin (0.05);
    m_can_vec.push_back(Can);

    h_central->GetYaxis()->SetNdivisions (505);

    h_central           ->Draw();
    h_rescaledperipheral->Draw("same");
    h_fit_func          ->Draw("same");
    f_pedestal          ->Draw("same");
    f_vnn_combined      ->Draw("same");

    float X = 0.05, Y = 0.90, SIZE = 25;
    if (m_if_have_jet == 2 && m_correlation_type == 2) Common::myText2(X, Y, 1, "h^{UE}-h^{UE}: AllEvents"    , SIZE, 63);
    if (m_if_have_jet == 0 && m_correlation_type == 2) Common::myText2(X, Y, 1, "h^{UE}-h^{UE}: NoJets"  , SIZE, 63);
    if (m_if_have_jet == 1 && m_correlation_type == 2) Common::myText2(X, Y, 1, "h^{UE}-h^{UE}: WithJets", SIZE, 63);
    if (m_if_have_jet == 2 && m_correlation_type == 1) Common::myText2(X, Y, 1, "h^{UE}-h^{J}: #it{p}_{T}^{G} >40 GeV", SIZE, 63);
    if (iptb == 1) Common::myText2(X, Y - 0.1, 1, label_ptb(Bins::GetPtbIndex(1.0, 1.5)), SIZE, 63);
    if (iptb == 3) Common::myText2(X, Y - 0.1, 1, label_ptb(Bins::GetPtbIndex(2.0, 3.0)), SIZE, 63);
    if (iptb == 4) Common::myText2(X, Y - 0.1, 1, label_ptb(Bins::GetPtbIndex(3.0, 4.0)), SIZE, 63);
    if (iptb == 5) Common::myText2(X, Y - 0.1, 1, label_ptb(Bins::GetPtbIndex(4.0, 6.0)), SIZE, 63);

    if (m_if_have_jet == 2 && m_correlation_type == 1) {
        if (iptb == 1) {
            Common::myText2(X       , Y - 0.25, 1, "ATLAS"   , SIZE, 73);
            Common::myText2(X + 0.16, Y - 0.25, 1, Common::Internal, SIZE, 43);
            Common::myText2(X       , Y - 0.34, 1, DATASETLABEL[m_data_type] + " " + DATASETENERGY[m_data_type], SIZE, 43);
            Common::myText2(X       , Y - 0.43, 1, label_eta(Bins::GetDetaIndex(2.0, 5.0)) + ",", SIZE, 43);
            Common::myText2(X + 0.18, Y - 0.43, 1, label_pta(Bins::GetPtaIndex(0.5, 4.0)), SIZE, 43);
            Common::myText2(X       , Y - 0.52, 1, label_cent_corr(icent1), SIZE, 43);
            // Common::myText2(X       , Y - 0.61, 1, label_cent_peri(icent2), SIZE, 43);
        }

        if (iptb == 3) {
            TLegend *leg1 = new TLegend(0.16, 0.45, 0.50, 0.72);
            leg1->SetTextFont(43);
            leg1->SetTextSize(27);
            leg1->SetBorderSize(0);
            leg1->SetFillStyle (0);
            leg1->AddEntry(h_central           , "#it{C}(#Delta#phi)"                 , "p");
            leg1->AddEntry(h_rescaledperipheral, "#it{G}+#it{FC}^{periph}(#Delta#phi)", "p");
            leg1->AddEntry(h_fit_func          , "Fit"                                , "l");
            leg1->Draw();
        }

        if (iptb == 5) {
            TLegend *leg1 = new TLegend(0.18, 0.54, 0.50, 0.72);
            leg1->SetTextFont(43);
            leg1->SetTextSize(27);
            leg1->SetBorderSize(0);
            leg1->SetFillStyle (0);
            leg1->AddEntry(f_vnn_combined, "#it{C}^{ridge}(#Delta#phi)+#it{FC}^{periph}(0)", "l");
            leg1->AddEntry(f_pedestal    , "#it{G}+#it{FC}^{periph}(0)"                    , "l");
            leg1->Draw();
        }
    }
}
