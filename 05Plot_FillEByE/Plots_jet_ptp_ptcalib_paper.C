#include "Riostream.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "vector"
#include "TF1.h"
#include "TLine.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TEllipse.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMath.h"

#include "bins.h"

vector<TCanvas*> m_can_vec;


void Plots_jet_ptp_ptcalib_paper() {
    char name[600];
    float X, Y, SIZE;
    sprintf(name, "../02AnaCorr/01Rootfiles/backup_data_tig9Global_repo_Periph020_____goodOn20230101/Data_pp_mu5_eff1_trig0_pileup5_trkqual1_rejME1_jet2_jc9_jetcut4_ifHaveJet2_ifUECorr1_corrtype2_multtype0.root");
    TFile *input1  = new TFile(name);
    std::cout << "Opening: " << name << std::endl;
    sprintf(name, "../02AnaCorr/01Rootfiles/backup_pythia8_simAna_internalnote/Data_pp_mu5_eff1_trig3_pileup5_trkqual1_rejME1_jet2_jc9_jetcut4_ifHaveJet2_ifUECorr0_corrtype1_multtype0_PythiaSim.root");
    TFile *input2  = new TFile(name);
    std::cout << "Opening: " << name << std::endl;

    string histName = "h2_PFlowJets_calibPT_PTAbvN";

    TH2D* hist1[Bins::NCENT + Bins::NCENT_ADD];
    TH2D* hist2[Bins::NCENT + Bins::NCENT_ADD];

    char hname[100];
    //Read in Histograms
    for (int icent = 15; icent < Bins::NCENT; icent++) {
        sprintf(hname, "%s_cent%.2d", histName.c_str(), icent);
        hist1[icent] = (TH2D*)input1->Get(hname);
        if (!hist1[icent]) {std::cout << hname << " Not found" << std::endl; throw std::exception();}
        sprintf(hname, "%s_cent%.2d", histName.c_str(), icent);
        hist2[icent] = (TH2D*)input2->Get(hname);
        if (!hist2[icent]) {std::cout << hname << " Not found" << std::endl; throw std::exception();}
    }

    //Add more multiplicity bins
    for (int icent = Bins::NCENT; icent < Bins::NCENT + Bins::NCENT_ADD; icent++) {
        for (int icent_add = Bins::cent_add_lo[icent - Bins::NCENT]; icent_add < Bins::cent_add_up[icent - Bins::NCENT]; icent_add++) {
            TH2D* hist1_add = hist1[icent_add];
            TH2D* hist2_add = hist2[icent_add];

            if (icent_add == Bins::cent_add_lo[icent - Bins::NCENT]) {
                sprintf(hname, "%s_cent%.2d", histName.c_str(), icent); hist1[icent] = (TH2D*)hist1_add->Clone(hname); hist1[icent]->SetTitle(hname);
                sprintf(hname, "%s_cent%.2d", histName.c_str(), icent); hist2[icent] = (TH2D*)hist2_add->Clone(hname); hist2[icent]->SetTitle(hname);
            }
            else {
                hist1[icent]->Add(hist1_add);
                hist2[icent]->Add(hist2_add);
            }
        }
    }


    int icent   = Bins::GetCentIndex(10, 30);
    int cent_lo = Bins::GetCentVals(icent).first;
    int cent_hi = Bins::GetCentVals(icent).second;
    hist1[icent]->GetXaxis()->SetTitleOffset(1.1);
    hist1[icent]->GetYaxis()->SetTitleOffset(1.1);
    hist1[icent]->GetXaxis()->SetTitle("#it{p}_{T}^{calib} [GeV]");
    hist1[icent]->GetYaxis()->SetTitle("#it{p}_{T}^{G} [GeV]");
    hist2[icent]->GetXaxis()->SetTitleOffset(1.1);
    hist2[icent]->GetYaxis()->SetTitleOffset(1.1);
    hist2[icent]->GetXaxis()->SetTitle("#it{p}_{T} (generated) [GeV]");
    hist2[icent]->GetYaxis()->SetTitle("#it{p}_{T}^{G} [GeV]");


    // sprintf(name, "pfx1_%s_cent%.2d_%.2d", histName.c_str(), cent_lo, cent_hi);
    // TH1D* pjx1 = hist1[icent]->ProjectionX(name, 41, 200);
    // sprintf(name, "pfx2_%s_cent%.2d_%.2d", histName.c_str(), cent_lo, cent_hi);
    // TH1D* pjx2 = hist2[icent]->ProjectionX(name, 41, 200);
    // sprintf(name, "spfx1_%s_cent%.2d_%.2d", histName.c_str(), cent_lo, cent_hi);
    // TH1D* spjx1 = hist1[icent]->ProjectionX(name, 41, 200);
    // sprintf(name, "spfx2_%s_cent%.2d_%.2d", histName.c_str(), cent_lo, cent_hi);
    // TH1D* spjx2 = hist2[icent]->ProjectionX(name, 41, 200);
    // sprintf(name, "tempall_pfx1_%s_cent%.2d_%.2d", histName.c_str(), cent_lo, cent_hi);
    // TH1D* tempall_pjx1 = hist1[icent]->ProjectionX(name, 1, 200);
    // sprintf(name, "tempall_pfx2_%s_cent%.2d_%.2d", histName.c_str(), cent_lo, cent_hi);
    // TH1D* tempall_pjx2 = hist2[icent]->ProjectionX(name, 1, 200);
    // sprintf(name, "scaleall_pfx2_%s_cent%.2d_%.2d", histName.c_str(), cent_lo, cent_hi);
    // TH1D* scaleall_pjx2 = hist2[icent]->ProjectionX(name, 1, 200);

    // for (int i = 1; i <= 200; i++) {
    //     float current_pythia = spjx2->GetBinContent(i);
    //     if (current_pythia < 1.) continue;

    //     float tempall_data   = tempall_pjx1->GetBinContent(i);
    //     float tempall_pythia = tempall_pjx2->GetBinContent(i);

    //     if (tempall_data > 0. && tempall_pythia > 0) {
    //         spjx2->SetBinContent(i, current_pythia * tempall_data / tempall_pythia);
    //     }
    // }

    // for (int i = 1; i <= 200; i++) {
    //     float current_pythia = tempall_pjx2->GetBinContent(i);

    //     float tempall_data   = tempall_pjx1->GetBinContent(i);
    //     float tempall_pythia = tempall_pjx2->GetBinContent(i);

    //     if (tempall_data > 0. && tempall_pythia > 0) {
    //         scaleall_pjx2->SetBinContent(i, current_pythia * tempall_data / tempall_pythia);
    //     }
    // }

    // sprintf(name, "can_pjxWithScale_overlay_%s_cent%.2d_%.2d", histName.c_str(), cent_lo, cent_hi);
    // TCanvas *c5 = new TCanvas(name, name, 700, 500);
    // c5->SetLeftMargin  (0.15);
    // c5->SetTopMargin   (0.05);
    // c5->SetBottomMargin(0.15);
    // c5->SetRightMargin (0.05);
    // c5->SetLogy();
    // m_can_vec.push_back(c5);

    // spjx1->Scale(1. / spjx1->Integral());
    // spjx2->Scale(1. / spjx2->Integral());

    // spjx1->GetXaxis()->SetRangeUser(0, 150);
    // spjx1->GetYaxis()->SetRangeUser(1e-5, 4e-1);

    // spjx1->SetLineColor(kBlue);
    // spjx2->SetLineColor(kRed);

    // spjx1->Draw("hist");
    // spjx2->Draw("hist,same");

    // TLegend *leg2 = new TLegend(.20, .2, .35, .4);
    // leg2->SetTextSize(.04);
    // leg2->SetBorderSize(0);
    // leg2->SetFillColor(0);
    // leg2->AddEntry(spjx1, "Data"   , "l");
    // leg2->AddEntry(spjx2, "PYTHIA8", "l");
    // leg2->Draw("same");

    // X = 0.05, Y = 0.9, SIZE = 24;
    // Common::myText2(X, Y      , 1, "ATLAS"         , SIZE, 73); Common::myText2(X + 0.18, Y      , 1, Common::Internal, SIZE, 43);
    // Common::myText2(X, Y - .08, 1, DataSetEnums::DATASETLABEL[2] + " " + DataSetEnums::DATASETENERGY[2], SIZE, 43);
    // // sprintf(name, "%d#leq#it{N}_{ch}^{rec,corr}<%d", cent_lo, cent_hi);
    // // Common::myText2(X, Y - .16, 1, name, SIZE, 43);




    sprintf(name, "can_Data_%s_cent%.2d_%.2d", histName.c_str(), cent_lo, cent_hi);
    TCanvas *c1 = new TCanvas(name, name, 700, 500);
    c1->SetLeftMargin  (0.15);
    c1->SetTopMargin   (0.05);
    c1->SetBottomMargin(0.15);
    c1->SetRightMargin (0.15);
    c1->SetLogz();
    m_can_vec.push_back(c1);

    hist1[icent]->GetXaxis()->SetRangeUser(0, 100);
    hist1[icent]->GetYaxis()->SetRangeUser(1, 100);
    hist1[icent]->Draw("colz");

    sprintf(name, "pfx_data_%s_cent%.2d_%.2d", histName.c_str(), cent_lo, cent_hi);
    TProfile* pfx1 = hist1[icent]->ProfileX(name);
    pfx1->SetMarkerSize(0.5);
    pfx1->Draw("same");

    TF1* f1 = new TF1("f1", "[0]+[1]*x", 40, 100);
    f1->SetLineWidth(5);
    pfx1->Fit("f1", "RQ");
    // sprintf(name, "%.3f + %.3f*x", f1->GetParameter(0), f1->GetParameter(1)); Common::myText2(0.5, 0.1, 1, name, 24, 43);

    TF1* f1_full = new TF1("f1_full", "[0]+[1]*x", 0, 40);
    f1_full->SetParameters(f1->GetParameter(0), f1->GetParameter(1));
    f1_full->SetLineColor(kRed);
    f1_full->SetLineStyle(7);
    f1_full->SetLineWidth(2);
    f1_full->Draw("same");

    sprintf(name, "Slope: %.3f #pm %.3f"        , f1->GetParameter(1), f1->GetParError(1)); Common::myText2(0.05, 0.54, 1, name, 24, 43);
    sprintf(name, "Intercept: %.2f #pm %.2f GeV", f1->GetParameter(0), f1->GetParError(0)); Common::myText2(0.05, 0.62, 1, name, 24, 43);


    X = 0.05, Y = 0.9, SIZE = 24;
    Common::myText2(X, Y      , 1, "ATLAS"         , SIZE, 73); Common::myText2(X + 0.18, Y      , 1, Common::Internal, SIZE, 43);
    Common::myText2(X, Y - .08, 1, DataSetEnums::DATASETLABEL[2] + " " + DataSetEnums::DATASETENERGY[2], SIZE, 43);
    sprintf(name, "%d#leq#it{N}_{ch}^{rec,corr}<%d", cent_lo, cent_hi);
    Common::myText2(X, Y - .16, 1, name, SIZE, 43);










    sprintf(name, "can_PYTHIA_%s_cent%.2d_%.2d", histName.c_str(), cent_lo, cent_hi);
    TCanvas *c2 = new TCanvas(name, name, 700, 500);
    c2->SetLeftMargin  (0.15);
    c2->SetTopMargin   (0.05);
    c2->SetBottomMargin(0.15);
    c2->SetRightMargin (0.15);
    c2->SetLogz();
    m_can_vec.push_back(c2);

    hist2[icent]->GetXaxis()->SetRangeUser(0, 100);
    hist2[icent]->GetYaxis()->SetRangeUser(1, 100);
    hist2[icent]->Draw("colz");

    sprintf(name, "pfx_PYTHIA8_%s_cent%.2d_%.2d", histName.c_str(), cent_lo, cent_hi);
    TProfile* pfx2 = hist2[icent]->ProfileX(name);
    pfx2->SetMarkerSize(0.5);
    pfx2->Draw("same");

    TF1* f2 = new TF1("f2", "[0]+[1]*x", 40, 100);
    f2->SetLineWidth(5);
    pfx2->Fit("f2", "RQ");
    // sprintf(name, "%.3f + %.3f*x", f2->GetParameter(0), f2->GetParameter(1)); Common::myText2(0.5, 0.1, 1, name, 24, 43);


    TF1* f2_full = new TF1("f2_full", "[0]+[1]*x", 0, 40);
    f2_full->SetParameters(f1->GetParameter(0), f1->GetParameter(1));
    f2_full->SetLineColor(kRed);
    f2_full->SetLineStyle(7);
    f2_full->SetLineWidth(2);
    f2_full->Draw("same");

    sprintf(name, "Slope: %.3f #pm %.3f"        , f2->GetParameter(1), f2->GetParError(1)); Common::myText2(0.05, 0.54, 1, name, 24, 43);
    sprintf(name, "Intercept: %.2f #pm %.2f GeV", f2->GetParameter(0), f2->GetParError(0)); Common::myText2(0.05, 0.62, 1, name, 24, 43);


    X = 0.05, Y = 0.9, SIZE = 24;
    Common::myText2(X, Y      , 1, "ATLAS ", SIZE, 73);
    Common::myText2(X + 0.18, Y      , 1, "Simulation", SIZE, 43);
    Common::myText2(X + 0.36, Y      , 1, Common::Internal, SIZE, 43);
    Common::myText2(X       , Y - 0.08, 1, "PYTHIA 8", SIZE, 43);
    sprintf(name, "%d#leq#it{N}_{ch}^{truth}<%d", cent_lo, cent_hi);
    Common::myText2(X, Y - .16, 1, name, SIZE, 43);





    Common::SaveCanvas(m_can_vec, "");
}
