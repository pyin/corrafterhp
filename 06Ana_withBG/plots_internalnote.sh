

root -b -l -q "Plots_2D_2PC.C+(2,0,5,1,0,5,1,1,0,0,0,2,1,0,0,    0)"  #inclusive
mv figs/*.png  figs/results/2D_2PC/Inclusive
root -b -l -q "Plots_2D_2PC.C+(2,0,5,1,0,5,1,1,2,9,4,2,1,2,0,    0)"
mv figs/*.png  figs/results/2D_2PC/AllEvents
root -b -l -q "Plots_2D_2PC.C+(2,0,5,1,0,5,1,1,2,9,4,1,1,2,0,    0)"
mv figs/*.png  figs/results/2D_2PC/WithJets
root -b -l -q "Plots_2D_2PC.C+(2,0,5,1,0,5,1,1,2,9,4,0,1,2,0,    0)"
mv figs/*.png  figs/results/2D_2PC/NoJets
root -b -l -q "Plots_2D_2PC.C+(2,0,5,1,3,5,1,1,2,9,4,2,1,1,0,    1)"
mv figs/*.png  figs/results/2D_2PC/JH


root -b -l -q "Plots_fg_bg_inclusive.C+(2,0,5,1,0,5,1,1,0,0,0,2,1,0,0,    0)"  #inclusive
mv figs/*.png  figs/results/fgbgoverlay/Inclusive
root -b -l -q "Plots_fg_bg_inclusive.C+(2,0,5,1,0,5,1,1,2,9,4,2,1,2,0,    0)"
mv figs/*.png  figs/results/fgbgoverlay/AllEvents
root -b -l -q "Plots_fg_bg_inclusive.C+(2,0,5,1,0,5,1,1,2,9,4,1,1,2,0,    0)"
mv figs/*.png  figs/results/fgbgoverlay/WithJets
root -b -l -q "Plots_fg_bg_inclusive.C+(2,0,5,1,0,5,1,1,2,9,4,0,1,2,0,    0)"
mv figs/*.png  figs/results/fgbgoverlay/NoJets
root -b -l -q "Plots_fg_bg_inclusive.C+(2,0,5,1,3,5,1,1,2,9,4,2,1,1,0,    1)"
mv figs/*.png  figs/results/fgbgoverlay/JH

