#ifndef __SYSTERRORS__
#define __SYSTERRORS__
// #include "ClosureCorr.C"
#include "bins.h"

class SystErrors {


private:
    TGraphErrors *m_gr_Tracking_systematic[Bins::NHAR_DFT + 1][2] = {{nullptr}};
    TGraphErrors *m_gr_Pileup_systematic  [Bins::NHAR_DFT + 1][2] = {{nullptr}};
    TGraphErrors *m_gr_periph_systematic  [Bins::NHAR_DFT + 1][2] = {{nullptr}};
    TGraphErrors *m_gr_EvtMix_systematic  [Bins::NHAR_DFT + 1][2] = {{nullptr}};
    TGraphErrors *m_gr_UECorr_systematic  [Bins::NHAR_DFT + 1][2] = {{nullptr}};
    TGraphErrors *m_gr_MC_systematic      [Bins::NHAR_DFT + 1][2] = {{nullptr}};
    TGraphErrors *m_gr_MinJetpT_systematic[Bins::NHAR_DFT + 1][2] = {{nullptr}};
    TGraphErrors *m_gr_Total_systematic   [Bins::NHAR_DFT + 1][2] = {{nullptr}};

    TH1D         *m_h_pTSpectra                               [Bins::NCENT + Bins::NCENT_ADD] = {nullptr};


    double InterPolate(double low, double up, double val_low, double val_up, double x);

    //HARDCODED Uncertainties for pp13
    double TrackEffSystematic_pp13 (int ihar, double pt, double mult, int evtSet, int deptType);
    double PileupSystematic_pp13   (int ihar, double pt, double mult, int evtSet, int deptType);
    double PeriphBinSystematic_pp13(int ihar, double pt, double mult, int evtSet, int deptType);
    double EvtMixingSystematic_pp13(int ihar, double pt, double mult, int evtSet, int deptType);
    double UECorrSystematic_pp13   (int ihar, double pt, double mult, int evtSet, int deptType);
    double MCSystematic_pp13       (int ihar, double pt, double mult, int evtSet, int deptType);
    double MinJetpTSystematic_pp13 (int ihar, double pt, double mult, int evtSet, int deptType);
    double TotalSystematic_pp13    (int ihar, double pt, double mult, int evtSet, int deptType);


    //Returns errors as % not as fraction!
    //divide returned value by 100 to get fraction
    //only for differential pt bins
    double TrackEffSystematic (int ihar, double pt, int icent, int deptType) {return m_gr_Tracking_systematic[ihar][deptType]->Eval(deptType == MULTDEPT ? icent : pt);}
    double PileupSystematic   (int ihar, double pt, int icent, int deptType) {return m_gr_Pileup_systematic  [ihar][deptType]->Eval(deptType == MULTDEPT ? icent : pt);}
    double PeriphBinSystematic(int ihar, double pt, int icent, int deptType) {return m_gr_periph_systematic  [ihar][deptType]->Eval(deptType == MULTDEPT ? icent : pt);}
    double EvtMixingSystematic(int ihar, double pt, int icent, int deptType) {return m_gr_EvtMix_systematic  [ihar][deptType]->Eval(deptType == MULTDEPT ? icent : pt);}
    double UECorrSystematic   (int ihar, double pt, int icent, int deptType) {return m_gr_UECorr_systematic  [ihar][deptType]->Eval(deptType == MULTDEPT ? icent : pt);}
    double MCSystematic       (int ihar, double pt, int icent, int deptType) {return m_gr_MC_systematic      [ihar][deptType]->Eval(deptType == MULTDEPT ? icent : pt);}
    double MinJetpTSystematic (int ihar, double pt, int icent, int deptType) {return m_gr_MinJetpT_systematic[ihar][deptType]->Eval(deptType == MULTDEPT ? icent : pt);}
    double TotalSystematic    (int ihar, double pt, int icent, int deptType) {return m_gr_Total_systematic   [ihar][deptType]->Eval(deptType == MULTDEPT ? icent : pt);}

public:
    SystErrors(std::string l_basename = "Data_LowAndIntermediateMu_eff1_trig2", int l_data_type = DataSetEnums::DATA_LOW_AND_INTERMEDIATE_MU, int evtSet = 2);

    ~SystErrors() {
        for (int ihar : {Bins::v2, Bins::v3, Bins::v4}) {
            for (int deptType = 0; deptType < 2; deptType++) {
                if (m_gr_MinJetpT_systematic[ihar][deptType]) delete m_gr_MinJetpT_systematic[ihar][deptType];
                if (m_gr_MC_systematic      [ihar][deptType]) delete m_gr_MC_systematic      [ihar][deptType];
                if (m_gr_UECorr_systematic  [ihar][deptType]) delete m_gr_UECorr_systematic  [ihar][deptType];
                if (m_gr_EvtMix_systematic  [ihar][deptType]) delete m_gr_EvtMix_systematic  [ihar][deptType];
                if (m_gr_periph_systematic  [ihar][deptType]) delete m_gr_periph_systematic  [ihar][deptType];
                if (m_gr_Pileup_systematic  [ihar][deptType]) delete m_gr_Pileup_systematic  [ihar][deptType];
                if (m_gr_Tracking_systematic[ihar][deptType]) delete m_gr_Tracking_systematic[ihar][deptType];
                if (m_gr_Total_systematic   [ihar][deptType]) delete m_gr_Total_systematic   [ihar][deptType];
            }
        }
    }

    enum {
        SYSTEMATIC_TRACKEFF  = 0,
        SYSTEMATIC_PILEUP    = 1,
        SYSTEMATIC_PERIPHBIN = 2,
        SYSTEMATIC_EVTMIX    = 3,
        SYSTEMATIC_UECORR    = 4,
        SYSTEMATIC_MC        = 5,
        SYSTEMATIC_MINJETPT  = 6,
        SYSTEMATIC_TOTAL     = 7,
    };

    enum {
        MULTDEPT  = 0,
        PTDEPT    = 1,
    };



    //Returns a histogram containing the % systematic uncertainties as bin contents
    TH1* GetSystErrorPtbDepHist (int ihar, int centbin, const TH1 *const InputHist, int systematic_type = SYSTEMATIC_TOTAL);

    //Returns a histogram containing the systematic uncertainties as bin errors
    TH1* AddSystErrorPtbDepHist (int ihar, int centbin, const TH1 *const InputHist, int systematic_type = SYSTEMATIC_TOTAL);

    //Returns a histogram containing the systematic uncertainties
    //as bin errors added in quadrature to original errors
    TH1* AddSystErrorPtbDepHist2(int ihar, int centbin, const TH1 *const InputHist, int systematic_type = SYSTEMATIC_TOTAL);

    //Same as above functions but for pTb dependence
    TH1* GetSystErrorCentDepHist (int ihar, int ptbbin, const TH1 *const InputHist, int systematic_type = SYSTEMATIC_TOTAL);
    TH1* AddSystErrorCentDepHist (int ihar, int ptbbin, const TH1 *const InputHist, int systematic_type = SYSTEMATIC_TOTAL);
    TH1* AddSystErrorCentDepHist2(int ihar, int ptbbin, const TH1 *const InputHist, int systematic_type = SYSTEMATIC_TOTAL);

};

void DumpCentDep(int ihar, int ptbbin, TH1*hist1, TH1*hist2, std::string s) {
    for (int ibin = 1; ibin <= hist1->GetNbinsX(); ibin++) {
        cout << __PRETTY_FUNCTION__ << "  :: " << s << " ihar=" << ihar << " ipt2=" << ptbbin << " cent="
             << hist1->GetBinCenter(ibin) << " "
             << hist1->GetBinContent(ibin) << " "
             << hist2->GetBinContent(ibin) << " "
             << hist2->GetBinContent(ibin) - hist1->GetBinContent(ibin) << " "
             << std::endl;
    }
}

void DumpPtbDep(int ihar, int centbin, TH1*hist1, TH1*hist2, std::string s) {
    for (int ibin = 1; ibin <= hist1->GetNbinsX(); ibin++) {
        cout << __PRETTY_FUNCTION__ << "  :: " << s << " ihar=" << ihar << " icent=" << centbin << " ptb="
             << hist1->GetBinCenter(ibin) << " "
             << hist1->GetBinContent(ibin) << " "
             << hist2->GetBinContent(ibin) << " "
             << hist2->GetBinContent(ibin) - hist1->GetBinContent(ibin) << " "
             << std::endl;
    }
}


//Returns a histogram containing the systematic uncertainties
//as bin errors added in quadrature to original errors
TH1* SystErrors::AddSystErrorPtbDepHist2(int ihar, int centbin, const TH1 *const InputHist, int systematic_type)
{
    TH1* hist = AddSystErrorPtbDepHist(ihar, centbin, InputHist, systematic_type);
    for (int ibin = 1; ibin <= hist->GetNbinsX(); ibin++) {
        double err1 = hist     ->GetBinError(ibin);
        double err2 = InputHist->GetBinError(ibin);
        err1 = sqrt(err1 * err1 + err2 * err2);
        hist->SetBinError(ibin, err1);
    }
    return hist;
}

//Returns a histogram containing the systematic uncertainties as bin errors
TH1* SystErrors::AddSystErrorPtbDepHist(int ihar, int centbin, const TH1 *const InputHist, int systematic_type)
{
    char name[600];
    sprintf(name, "%s_syst1", InputHist->GetName());
    TH1* hist = (TH1*)InputHist->Clone(name);
    TH1* h_error = GetSystErrorPtbDepHist(ihar, centbin, InputHist, systematic_type);
    for (int ibin = 1; ibin <= hist->GetNbinsX(); ibin++) {
        hist->SetBinError(ibin, h_error->GetBinContent(ibin)*hist->GetBinContent(ibin) / 100.0); //divide by 100 to convert %error to fractional error
    }
    delete h_error;
    return hist;
}

//Returns a histogram containing the % systematic uncertainties as bin contents
TH1* SystErrors::GetSystErrorPtbDepHist(int ihar, int centbin, const TH1 *const InputHist, int systematic_type)
{
    TH1* hist = nullptr;
    //Output histogram that stores the systematic uncertainties
    if (InputHist == nullptr) {
        std::vector<int> ptb_bin_vals = Bins::GetPtbIndex({0.0, 0.5, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.5, 4.0, 6.0, 8.0});
        if (ihar == 3) ptb_bin_vals = Bins::GetPtbIndex({0.0, 0.5, 1.0, 1.5, 2.0, 3.0, 4.0, 6.0, 8.0});
        if (ihar == 4) ptb_bin_vals = Bins::GetPtbIndex({0.0, 0.5, 1.0, 1.5, 2.0, 3.0});
        hist = Bins::PtbdepHist(ptb_bin_vals, Common::UniqueName(), 0); //Bins.h
    }
    else {
        hist = (TH1*)InputHist->Clone(Common::UniqueName().c_str());
        hist->Reset();
    }

    //loop over the output histogram bins
    if (Bins::NTRACK_PPB_LO[centbin] != 60 || Bins::NTRACK_PPB_HI[centbin] != 150) {
        std::cout << "Unidentified multiplicity bin: [" << Bins::NTRACK_PPB_LO[centbin] << ", " << Bins::NTRACK_PPB_HI[centbin] << "). We only have [60,150) for sysError." << std::endl;
        throw std::exception();
    }
    else {
        for (int ibin = 1; ibin <= hist->GetNbinsX(); ibin++) {
            double pt2lo = hist->GetBinLowEdge(ibin    );
            double pt2hi = hist->GetBinLowEdge(ibin + 1);
            double pt2Mid = (pt2lo + pt2hi) / 2;
            if (pt2lo < 0.5 - 0.001) continue;

            double val = 0;
            if      (systematic_type == SYSTEMATIC_TOTAL    ) val = TotalSystematic    (ihar, pt2Mid, -1, PTDEPT);
            else if (systematic_type == SYSTEMATIC_TRACKEFF ) val = TrackEffSystematic (ihar, pt2Mid, -1, PTDEPT);
            else if (systematic_type == SYSTEMATIC_PILEUP   ) val = PileupSystematic   (ihar, pt2Mid, -1, PTDEPT);
            else if (systematic_type == SYSTEMATIC_PERIPHBIN) val = PeriphBinSystematic(ihar, pt2Mid, -1, PTDEPT);
            else if (systematic_type == SYSTEMATIC_EVTMIX   ) val = EvtMixingSystematic(ihar, pt2Mid, -1, PTDEPT);
            else if (systematic_type == SYSTEMATIC_UECORR   ) val = UECorrSystematic   (ihar, pt2Mid, -1, PTDEPT);
            else if (systematic_type == SYSTEMATIC_MC       ) val = MCSystematic       (ihar, pt2Mid, -1, PTDEPT);
            else if (systematic_type == SYSTEMATIC_MINJETPT ) val = MinJetpTSystematic (ihar, pt2Mid, -1, PTDEPT);
            else {
                std::cout << __PRETTY_FUNCTION__ << " :: unknown systematic type=" << systematic_type << std::endl;
                throw std::exception();
            }

            hist->SetBinContent(ibin, val);
            hist->SetBinError  (ibin, 0  );
        }
    }

    return hist;
}


//Returns a histogram containing the systematic uncertainties
//as bin errors added in quadrature to original errors
TH1* SystErrors::AddSystErrorCentDepHist2(int ihar, int ptbbin, const TH1 *const InputHist, int systematic_type)
{
    TH1* hist = AddSystErrorCentDepHist(ihar, ptbbin, InputHist, systematic_type);
    for (int ibin = 1; ibin <= hist->GetNbinsX(); ibin++) {
        double err1 = hist     ->GetBinError(ibin);
        double err2 = InputHist->GetBinError(ibin);
        err1 = sqrt(err1 * err1 + err2 * err2);
        hist->SetBinError(ibin, err1);
    }
    return hist;
}

//Returns a histogram containing the systematic uncertainties as bin errors
TH1* SystErrors::AddSystErrorCentDepHist(int ihar, int ptbbin, const TH1 *const InputHist, int systematic_type)
{
    char name[600];
    sprintf(name, "%s_syst1", InputHist->GetName());
    TH1* hist = (TH1*)InputHist->Clone(name);
    TH1* h_error = GetSystErrorCentDepHist(ihar, ptbbin, InputHist, systematic_type);
    for (int ibin = 1; ibin <= hist->GetNbinsX(); ibin++) {
        hist->SetBinError(ibin, h_error->GetBinContent(ibin)*hist->GetBinContent(ibin) / 100.0);
    }
    delete h_error;
    return hist;
}

//Returns a histogram containing the % systematic uncertainties as bin contents
TH1* SystErrors::GetSystErrorCentDepHist(int ihar, int ptbbin, const TH1 *const InputHist, int systematic_type)
{
    //Output histogram that stores the systematic uncertainties
    TH1*hist;
    if (InputHist == nullptr) {
        std::vector<int> multbins = Bins::GetCentIndex({0, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150});
        if (ihar != 2) multbins = Bins::GetCentIndex({0, 20, 40, 60, 80, 100, 120, 140});
        hist = Bins::CentdepHist(multbins, Common::UniqueName().c_str()); //Bins.h
    }
    else {
        hist = (TH1*)InputHist->Clone(Common::UniqueName().c_str());
        hist->Reset();
    }

    //loop over the output histogram bins
    if (Bins::PT2_LO[ptbbin] != 0.5 || Bins::PT2_HI[ptbbin] != 5.0) {
        std::cout << "Unidentified pT bin: [" << Bins::PT2_LO[ptbbin] << ", " << Bins::PT2_HI[ptbbin] << "). We only have [0.5,5.0) for sysError." << std::endl;
        throw std::exception();
    }
    else {
        for (int ibin = 1; ibin <= hist->GetNbinsX(); ibin++) {
            double cent1 = hist->GetBinLowEdge(ibin    );
            double cent2 = hist->GetBinLowEdge(ibin + 1);
            double centMid = (cent1 + cent2) / 2;

            double val = 0;
            if      (systematic_type == SYSTEMATIC_TOTAL    ) val = TotalSystematic    (ihar, -1, centMid, MULTDEPT);
            else if (systematic_type == SYSTEMATIC_TRACKEFF ) val = TrackEffSystematic (ihar, -1, centMid, MULTDEPT);
            else if (systematic_type == SYSTEMATIC_PILEUP   ) val = PileupSystematic   (ihar, -1, centMid, MULTDEPT);
            else if (systematic_type == SYSTEMATIC_PERIPHBIN) val = PeriphBinSystematic(ihar, -1, centMid, MULTDEPT);
            else if (systematic_type == SYSTEMATIC_EVTMIX   ) val = EvtMixingSystematic(ihar, -1, centMid, MULTDEPT);
            else if (systematic_type == SYSTEMATIC_UECORR   ) val = UECorrSystematic   (ihar, -1, centMid, MULTDEPT);
            else if (systematic_type == SYSTEMATIC_MC       ) val = MCSystematic       (ihar, -1, centMid, MULTDEPT);
            else if (systematic_type == SYSTEMATIC_MINJETPT ) val = MinJetpTSystematic (ihar, -1, centMid, MULTDEPT);
            else {
                std::cout << __PRETTY_FUNCTION__ << " :: unknown systematic type=" << systematic_type << std::endl;
                throw std::exception();
            }
            hist->SetBinContent(ibin, val);
            hist->SetBinError  (ibin, 0  );
        }
    }

    return hist;
}



SystErrors::SystErrors(std::string l_basename, int l_data_type, int evtSet)
{
    char name [600];
    char name2[600];
    char name3[600];
    TFile *file_original = gFile;
    if (!file_original) {
        std::cout << __PRETTY_FUNCTION__ << " :: Warning no active gFile; creating temporary file" << std::endl;
        sprintf(name, "01RootFiles/temp_%s.root", l_basename.c_str());
        file_original = new TFile(name, "recreate");
    }

    /*-----------------------------------------------------------------------------
     *  Histogtrams storing the systematic uncertainties as function of pT
     *-----------------------------------------------------------------------------*/
    TH1D *h_Tracking_systematic[Bins::NHAR_DFT + 1][2] = {{nullptr}};
    TH1D *h_Pileup_systematic  [Bins::NHAR_DFT + 1][2] = {{nullptr}};
    TH1D *h_periph_systematic  [Bins::NHAR_DFT + 1][2] = {{nullptr}};
    TH1D *h_EvtMix_systematic  [Bins::NHAR_DFT + 1][2] = {{nullptr}};
    TH1D *h_UECorr_systematic  [Bins::NHAR_DFT + 1][2] = {{nullptr}};
    TH1D *h_MC_systematic      [Bins::NHAR_DFT + 1][2] = {{nullptr}};
    TH1D *h_MinJetpT_systematic[Bins::NHAR_DFT + 1][2] = {{nullptr}};
    TH1D *h_Total_systematic   [Bins::NHAR_DFT + 1][2] = {{nullptr}};

    for (int ihar : {Bins::v2, Bins::v3, Bins::v4}) {
        TH1D* htemp_multDept;
        TH1D* htemp_pTDept;
        std::vector<int> multbins = Bins::GetCentIndex({0, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150});
        if (ihar != 2) multbins = Bins::GetCentIndex({0, 20, 40, 60, 80, 100, 120, 140});
        htemp_multDept = Bins::CentdepHist(multbins, Common::UniqueName().c_str()); //Bins.h
        std::vector<int> ptb_bin_vals = Bins::GetPtbIndex({0.0, 0.5, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.5, 4.0, 6.0, 8.0});
        if (ihar == 3) ptb_bin_vals = Bins::GetPtbIndex({0.0, 0.5, 1.0, 1.5, 2.0, 3.0, 4.0, 6.0, 8.0});
        if (ihar == 4) ptb_bin_vals = Bins::GetPtbIndex({0.0, 0.5, 1.0, 1.5, 2.0, 3.0});
        htemp_pTDept = Bins::PtbdepHist(ptb_bin_vals, Common::UniqueName().c_str(), 0);

        if (evtSet == 1) ptb_bin_vals = Bins::GetPtbIndex({0.5, 1.0, 2.0, 3.0});

        if (ihar == Bins::v2) {
            htemp_multDept = new TH1D(Common::UniqueName().c_str(), "", 30, 0, 150);
            htemp_pTDept   = new TH1D(Common::UniqueName().c_str(), "", 80, 0, 8);
        }
        h_Tracking_systematic[ihar][0] = (TH1D*)htemp_multDept->Clone(Common::UniqueName().c_str());
        h_Pileup_systematic  [ihar][0] = (TH1D*)htemp_multDept->Clone(Common::UniqueName().c_str());
        h_periph_systematic  [ihar][0] = (TH1D*)htemp_multDept->Clone(Common::UniqueName().c_str());
        h_EvtMix_systematic  [ihar][0] = (TH1D*)htemp_multDept->Clone(Common::UniqueName().c_str());
        h_UECorr_systematic  [ihar][0] = (TH1D*)htemp_multDept->Clone(Common::UniqueName().c_str());
        h_MC_systematic      [ihar][0] = (TH1D*)htemp_multDept->Clone(Common::UniqueName().c_str());
        h_MinJetpT_systematic[ihar][0] = (TH1D*)htemp_multDept->Clone(Common::UniqueName().c_str());
        h_Total_systematic   [ihar][0] = (TH1D*)htemp_multDept->Clone(Common::UniqueName().c_str());

        h_Tracking_systematic[ihar][1] = (TH1D*)htemp_pTDept->Clone(Common::UniqueName().c_str());
        h_Pileup_systematic  [ihar][1] = (TH1D*)htemp_pTDept->Clone(Common::UniqueName().c_str());
        h_periph_systematic  [ihar][1] = (TH1D*)htemp_pTDept->Clone(Common::UniqueName().c_str());
        h_EvtMix_systematic  [ihar][1] = (TH1D*)htemp_pTDept->Clone(Common::UniqueName().c_str());
        h_UECorr_systematic  [ihar][1] = (TH1D*)htemp_pTDept->Clone(Common::UniqueName().c_str());
        h_MC_systematic      [ihar][1] = (TH1D*)htemp_pTDept->Clone(Common::UniqueName().c_str());
        h_MinJetpT_systematic[ihar][1] = (TH1D*)htemp_pTDept->Clone(Common::UniqueName().c_str());
        h_Total_systematic   [ihar][1] = (TH1D*)htemp_pTDept->Clone(Common::UniqueName().c_str());
        delete htemp_multDept;
        delete htemp_pTDept;
    }

    /*-----------------------------------------------------------------------------
     *The histogram containing the pT spectra for this centrality
     *The pT spectra is needed to generate the correction when the pT binning is
     *coarser than the pT binning used in the correction functions
     *The corrections for coarser pT bins are produced as spectra-weighted mean of the correction in fine pT bins
     *-----------------------------------------------------------------------------*/
    sprintf(name , "01RootFiles/%s_fgTag0_RebinCentrality.root", l_basename.c_str());
    TFile *f_pTSpectra = new TFile(name, "read");
    file_original->cd();
    for (int icent = 16; icent < Bins::NCENT + Bins::NCENT_ADD; icent++) {
        sprintf(name, "dist_pT_afterRej_ref_cent%.2d", icent);
        Common::CheckFile(f_pTSpectra, name);
        TH1D* h_pTSpectra = (TH1D*)f_pTSpectra->Get(name);
        std::cout << name << std::endl;
        if (!Common::CheckObject(h_pTSpectra, name)) throw std::exception();
        m_h_pTSpectra[icent] = (TH1D*)h_pTSpectra->Clone(Common::UniqueName().c_str());
    }
    //close and delete the spectra file
    f_pTSpectra->Close();
    delete f_pTSpectra;
    cout << "::Done Reading Spectra" << std::endl;

    if (l_data_type == DataSetEnums::DATA_LOW_AND_INTERMEDIATE_MU) {
        for (int ihar : {Bins::v2, Bins::v3, Bins::v4}) {
            for (int ibin = 1; ibin <= h_Tracking_systematic[ihar][0]->GetNbinsX(); ibin++) {
                double mult = h_Tracking_systematic[ihar][0]->GetBinCenter(ibin);
                double val_trk    = TrackEffSystematic_pp13 (ihar, -1, mult, evtSet, MULTDEPT);
                double val_pileup = PileupSystematic_pp13   (ihar, -1, mult, evtSet, MULTDEPT);
                double val_periph = PeriphBinSystematic_pp13(ihar, -1, mult, evtSet, MULTDEPT);
                double val_mix    = EvtMixingSystematic_pp13(ihar, -1, mult, evtSet, MULTDEPT);
                double val_UECorr = UECorrSystematic_pp13   (ihar, -1, mult, evtSet, MULTDEPT);
                double val_mc     = MCSystematic_pp13       (ihar, -1, mult, evtSet, MULTDEPT);
                double val_jetpt  = MinJetpTSystematic_pp13 (ihar, -1, mult, evtSet, MULTDEPT);
                double val_tot    = TotalSystematic_pp13    (ihar, -1, mult, evtSet, MULTDEPT);


                h_Tracking_systematic[ihar][0]->SetBinContent(ibin, val_trk   );
                h_Pileup_systematic  [ihar][0]->SetBinContent(ibin, val_pileup);
                h_periph_systematic  [ihar][0]->SetBinContent(ibin, val_periph);
                h_EvtMix_systematic  [ihar][0]->SetBinContent(ibin, val_mix   );
                h_UECorr_systematic  [ihar][0]->SetBinContent(ibin, val_UECorr);
                h_MC_systematic      [ihar][0]->SetBinContent(ibin, val_mc    );
                h_MinJetpT_systematic[ihar][0]->SetBinContent(ibin, val_jetpt );
                h_Total_systematic   [ihar][0]->SetBinContent(ibin, val_tot   );
            }

            for (int ibin = 1; ibin <= h_Tracking_systematic[ihar][1]->GetNbinsX(); ibin++) {
                double pt = h_Tracking_systematic[ihar][1]->GetBinCenter(ibin);
                double val_trk    = TrackEffSystematic_pp13 (ihar, pt, -1, evtSet, PTDEPT);
                double val_pileup = PileupSystematic_pp13   (ihar, pt, -1, evtSet, PTDEPT);
                double val_periph = PeriphBinSystematic_pp13(ihar, pt, -1, evtSet, PTDEPT);
                double val_mix    = EvtMixingSystematic_pp13(ihar, pt, -1, evtSet, PTDEPT);
                double val_UECorr = UECorrSystematic_pp13   (ihar, pt, -1, evtSet, PTDEPT);
                double val_mc     = MCSystematic_pp13       (ihar, pt, -1, evtSet, PTDEPT);
                double val_jetpt  = MinJetpTSystematic_pp13 (ihar, pt, -1, evtSet, PTDEPT);
                double val_tot    = TotalSystematic_pp13    (ihar, pt, -1, evtSet, PTDEPT);


                h_Tracking_systematic[ihar][1]->SetBinContent(ibin, val_trk   );
                h_Pileup_systematic  [ihar][1]->SetBinContent(ibin, val_pileup);
                h_periph_systematic  [ihar][1]->SetBinContent(ibin, val_periph);
                h_EvtMix_systematic  [ihar][1]->SetBinContent(ibin, val_mix   );
                h_UECorr_systematic  [ihar][1]->SetBinContent(ibin, val_UECorr);
                h_MC_systematic      [ihar][1]->SetBinContent(ibin, val_mc    );
                h_MinJetpT_systematic[ihar][1]->SetBinContent(ibin, val_jetpt );
                h_Total_systematic   [ihar][1]->SetBinContent(ibin, val_tot   );
            }
        }
    }
    else {
        std::cout << __PRETTY_FUNCTION__ << " :: unknown data type=" << l_data_type << std::endl;
    }


    //Convert the systematics to TGraphs
    file_original->cd();
    for (int ihar : {Bins::v2, Bins::v3, Bins::v4}) {
        for (int deptType = 0; deptType < 2; deptType++) {
            std::cout << "Converting:  ihar =" << ihar << "  deptType =" << deptType << std::endl;

            m_gr_Tracking_systematic[ihar][deptType] = Common::h2gr(h_Tracking_systematic[ihar][deptType]);
            m_gr_Pileup_systematic  [ihar][deptType] = Common::h2gr(h_Pileup_systematic  [ihar][deptType]);
            m_gr_periph_systematic  [ihar][deptType] = Common::h2gr(h_periph_systematic  [ihar][deptType]);
            m_gr_EvtMix_systematic  [ihar][deptType] = Common::h2gr(h_EvtMix_systematic  [ihar][deptType]);
            m_gr_UECorr_systematic  [ihar][deptType] = Common::h2gr(h_UECorr_systematic  [ihar][deptType]);
            m_gr_MC_systematic      [ihar][deptType] = Common::h2gr(h_MC_systematic      [ihar][deptType]);
            m_gr_MinJetpT_systematic[ihar][deptType] = Common::h2gr(h_MinJetpT_systematic[ihar][deptType]);
            m_gr_Total_systematic   [ihar][deptType] = Common::h2gr(h_Total_systematic   [ihar][deptType]);

            delete h_Tracking_systematic[ihar][deptType]; h_Tracking_systematic[ihar][deptType] = nullptr;
            delete h_Pileup_systematic  [ihar][deptType]; h_Pileup_systematic  [ihar][deptType] = nullptr;
            delete h_periph_systematic  [ihar][deptType]; h_periph_systematic  [ihar][deptType] = nullptr;
            delete h_EvtMix_systematic  [ihar][deptType]; h_EvtMix_systematic  [ihar][deptType] = nullptr;
            delete h_UECorr_systematic  [ihar][deptType]; h_UECorr_systematic  [ihar][deptType] = nullptr;
            delete h_MC_systematic      [ihar][deptType]; h_MC_systematic      [ihar][deptType] = nullptr;
            delete h_MinJetpT_systematic[ihar][deptType]; h_MinJetpT_systematic[ihar][deptType] = nullptr;
            delete h_Total_systematic   [ihar][deptType]; h_Total_systematic   [ihar][deptType] = nullptr;
        }
    }
}




double SystErrors::InterPolate(double low, double up, double val_low, double val_up, double x)
{
    if (x < low || x > up) {
        std::cout << "InterPolate() Error " << low << " " << up << "  " << x << std::endl;
        throw std::exception();
    }
    return val_low + (x - low) * (val_up - val_low) / (up - low);
}

double SystErrors::TotalSystematic_pp13(int ihar, double pt, double mult, int evtSet, int deptType)
{
    double a = TrackEffSystematic_pp13 (ihar, pt, mult, evtSet, deptType);
    double b = PileupSystematic_pp13   (ihar, pt, mult, evtSet, deptType);
    double c = PeriphBinSystematic_pp13(ihar, pt, mult, evtSet, deptType);
    double d = EvtMixingSystematic_pp13(ihar, pt, mult, evtSet, deptType);
    double e = UECorrSystematic_pp13   (ihar, pt, mult, evtSet, deptType);
    double f = MCSystematic_pp13       (ihar, pt, mult, evtSet, deptType);
    double g = MinJetpTSystematic_pp13 (ihar, pt, mult, evtSet, deptType);
    return sqrt(a * a + b * b + c * c + d * d);
}

double SystErrors::TrackEffSystematic_pp13(int ihar, double pt, double mult, int evtSet, int deptType)
{
    if (deptType == MULTDEPT) {
        if (evtSet == 2) {
            if (ihar == Bins::v2) return 0.35;
            if (ihar == Bins::v3) return 0.6;
            if (ihar == Bins::v4) return 0.6;
            return 0;
        }
        if (evtSet == 0) {
            if (ihar == Bins::v2) return 0.35;
            if (ihar == Bins::v3) return 0.6;
            if (ihar == Bins::v4) return 0.6;
            return 0;
        }
        if (evtSet == 1) {
            if (ihar == Bins::v2) return 0.35;
            if (ihar == Bins::v3) return 0.6;
            if (ihar == Bins::v4) return 0.6;
            return 0;
        }
    }
    if (deptType == PTDEPT) {
        if (evtSet == 2) {
            if (ihar == Bins::v2) return 0.2;
            if (ihar == Bins::v3) return 0.5;
            if (ihar == Bins::v4) return 0.5;
            return 0;
        }
        if (evtSet == 0) {
            if (ihar == Bins::v2) return 0.2;
            if (ihar == Bins::v3) return 0.45;
            if (ihar == Bins::v4) return 0.45;
            return 0;
        }
        if (evtSet == 1) {
            if (ihar == Bins::v2) return 0.2;
            if (ihar == Bins::v3) return 0.5;
            if (ihar == Bins::v4) return 0.5;
            return 0;
        }
    }
    return 0;
}

double SystErrors::PileupSystematic_pp13(int ihar, double pt, double mult, int evtSet, int deptType)
{
    if (deptType == MULTDEPT) {
        if (evtSet == 2) {
            if (ihar == Bins::v2) {
                if (mult < 100) return 0.3;
                else            return InterPolate(100, 150, 0.3, 3.0, mult);
            }
            if (ihar == Bins::v3) {
                if (mult < 100) return 0.3;
                else            return InterPolate(100, 150, 0.3, 3.0, mult);
            }
            if (ihar == Bins::v4) {
                if (mult < 100) return 0.3;
                else            return InterPolate(100, 150, 0.3, 3.0, mult);
            }
            return 0;
        }
        if (evtSet == 0) {
            if (ihar == Bins::v2) {
                if (mult < 100) return 0.3;
                else            return InterPolate(100, 150, 0.3, 3.0, mult);
            }
            if (ihar == Bins::v3) {
                if (mult < 100) return 0.3;
                else            return InterPolate(100, 150, 0.3, 3.0, mult);
            }
            if (ihar == Bins::v4) {
                if (mult < 100) return 0.3;
                else            return InterPolate(100, 150, 0.3, 3.0, mult);
            }
            return 0;
        }
        if (evtSet == 1) {
            if (ihar == Bins::v2) {
                if (mult < 100) return 0.3;
                else            return InterPolate(100, 150, 0.3, 3.0, mult);
            }
            if (ihar == Bins::v3) {
                if (mult < 100) return 0.3;
                else            return InterPolate(100, 150, 0.3, 3.0, mult);
            }
            if (ihar == Bins::v4) {
                if (mult < 100) return 0.3;
                else            return InterPolate(100, 150, 0.3, 3.0, mult);
            }
            return 0;
        }
    }
    if (deptType == PTDEPT) {
        if (evtSet == 2) {
            if (ihar == Bins::v2) return 1.0;
            if (ihar == Bins::v3) return 1.0;
            if (ihar == Bins::v4) return 1.0;
            return 0;
        }
        if (evtSet == 0) {
            if (ihar == Bins::v2) return 1.0;
            if (ihar == Bins::v3) return 1.0;
            if (ihar == Bins::v4) return 1.0;
            return 0;
        }
        if (evtSet == 1) {
            if (ihar == Bins::v2) return 1.0;
            if (ihar == Bins::v3) return 1.0;
            if (ihar == Bins::v4) return 1.0;
            return 0;
        }
    }
    return 0;
}

double SystErrors::PeriphBinSystematic_pp13(int ihar, double pt, double mult, int evtSet, int deptType)
{
    if (deptType == MULTDEPT) {
        if (evtSet == 2) {
            if (ihar == Bins::v2) {
                if (mult < 40) return InterPolate( 0,  40, 6.5, 1.0, mult);
                else           return InterPolate(40, 150, 1.0, 0.5, mult);
            }
            if (ihar == Bins::v3) {
                if (mult < 80) return InterPolate( 0,  80, 40.0, 8.0, mult);
                else           return InterPolate(80, 140,  8.0, 5.0, mult);
            }
            if (ihar == Bins::v4) return InterPolate( 0, 140, 30.0, 4.0, mult);
            return 0;
        }
        if (evtSet == 0) {
            if (ihar == Bins::v2) {
                if (mult < 40) return InterPolate( 0,  40, 6.5, 1.0, mult);
                else           return InterPolate(40, 150, 1.0, 0.5, mult);
            }
            if (ihar == Bins::v3) {
                if (mult < 80) return InterPolate( 0,  80, 40.0, 8.0, mult);
                else           return InterPolate(80, 140,  8.0, 5.0, mult);
            }
            if (ihar == Bins::v4) return InterPolate( 0, 140, 26.0, 6.0, mult);
            return 0;
        }
        if (evtSet == 1) {
            if (ihar == Bins::v2) {
                if (mult < 80) return InterPolate( 0,  80, 24.5, 3.0, mult);
                else           return InterPolate(80, 150,  3.0, 1.5, mult);
            }
            if (ihar == Bins::v3) return 220;
            if (ihar == Bins::v4) return 165;
            return 0;
        }
    }
    if (deptType == PTDEPT) {
        if (evtSet == 2) {
            if (ihar == Bins::v2) {
                if (pt < 2.4) return InterPolate(0  , 2.4, 1.5,  4.5, pt);
                else          return InterPolate(2.4, 8.0, 4.5, 24.5, pt);
            }
            if (ihar == Bins::v3) return 8;
            if (ihar == Bins::v4) return 16;
            return 0;
        }
        if (evtSet == 0) {
            if (ihar == Bins::v2) {
                if (pt < 2.4) return InterPolate(0  , 2.4, 1.5,  4.5, pt);
                else          return InterPolate(2.4, 8.0, 4.5, 24.5, pt);
            }
            if (ihar == Bins::v3) return 8;
            if (ihar == Bins::v4) return 16;
            return 0;
        }
        if (evtSet == 1) {
            if (ihar == Bins::v2) {
                if      (pt < 1.0) return InterPolate(0  , 1.0, 15, 15, pt);
                else if (pt < 2.0) return InterPolate(1.0, 2.0,  6,  6, pt);
                else if (pt < 3.0) return InterPolate(2.0, 3.0, 35, 35, pt);
                else return 0;
            }
            if (ihar == Bins::v3) {
                if      (pt < 1.0) return InterPolate(0  , 1.0, 116, 116, pt);
                else if (pt < 2.0) return InterPolate(1.0, 2.0,  60,  60, pt);
                else if (pt < 3.0) return InterPolate(2.0, 3.0, 162, 162, pt);
                else return 0;
            }
            if (ihar == Bins::v4) {
                if      (pt < 1.0) return InterPolate(0  , 1.0, 223, 223, pt);
                else if (pt < 2.0) return InterPolate(1.0, 2.0, 435, 435, pt);
                else if (pt < 3.0) return InterPolate(2.0, 3.0, 130, 130, pt);
                else return 0;
            }
            return 0;
        }
    }
    return 0;
}

double SystErrors::EvtMixingSystematic_pp13(int ihar, double pt, double mult, int evtSet, int deptType)
{
    if (deptType == MULTDEPT) {
        if (evtSet == 2) {
            if (ihar == Bins::v2) return 0.6;
            if (ihar == Bins::v3) return 3.0;
            if (ihar == Bins::v4) return 5.0;
            return 0;
        }
        if (evtSet == 0) {
            if (ihar == Bins::v2) return 0.5;
            if (ihar == Bins::v3) return 3.0;
            if (ihar == Bins::v4) return 5.0;
            return 0;
        }
        if (evtSet == 1) {
            if (ihar == Bins::v2) return 0.6;
            if (ihar == Bins::v3) return 3.0;
            if (ihar == Bins::v4) return 5.0;
            return 0;
        }
    }
    if (deptType == PTDEPT) {
        if (evtSet == 2) {
            if (ihar == Bins::v2) return 0.8;
            if (ihar == Bins::v3) return 3.0;
            if (ihar == Bins::v4) return 10.0;
            return 0;
        }
        if (evtSet == 0) {
            if (ihar == Bins::v2) return 0.8;
            if (ihar == Bins::v3) return 3.0;
            if (ihar == Bins::v4) return 8.0;
            return 0;
        }
        if (evtSet == 1) {
            if (ihar == Bins::v2) return 0.8;
            if (ihar == Bins::v3) return 3.0;
            if (ihar == Bins::v4) return 10.0;
            return 0;
        }
    }
    return 0;
}


double SystErrors::UECorrSystematic_pp13(int ihar, double pt, double mult, int evtSet, int deptType)
{
    // if (deptType == MULTDEPT) {
    //     if (evtSet == 2) {
    //         if (ihar == Bins::v2) {
    //             if (mult < 80) return InterPolate( 0,  80, 1.0, 1.0, mult);
    //             else           return InterPolate(80, 150, 1.0, 5.0, mult);
    //         }
    //         if (ihar == Bins::v3) return 3.3;
    //         if (ihar == Bins::v4) return 7.8;
    //         return 0;
    //     }
    //     if (evtSet == 0) {
    //         if (ihar == Bins::v2) {
    //             if (mult < 80) return InterPolate( 0,  80, 1.0, 1.0, mult);
    //             else           return InterPolate(80, 150, 1.0, 7.0, mult);
    //         }
    //         if (ihar == Bins::v3) {
    //             if (mult < 100) return InterPolate(  0, 100, 10.0, 1.2, mult);
    //             else            return InterPolate(100, 140,  1.2, 1.2, mult);
    //         }
    //         if (ihar == Bins::v4) return 7.8;
    //         return 0;
    //     }
    // }
    // if (deptType == PTDEPT) {
    //     if (evtSet == 2) {
    //         if (ihar == Bins::v2) {
    //             if (pt < 3.5) return InterPolate(0  , 3.5, 1.7, 1.7, pt);
    //             else          return InterPolate(3.5, 8.0, 1.7, 8.0, pt);
    //         }
    //         if (ihar == Bins::v3) return 2;
    //         if (ihar == Bins::v4) return 8;
    //         return 0;
    //     }
    //     if (evtSet == 0) {
    //         if (ihar == Bins::v2) return 1.5;
    //         if (ihar == Bins::v3) return 3.3;
    //         if (ihar == Bins::v4) return 8;
    //         return 0;
    //     }
    // }
    return 0;
}

double SystErrors::MCSystematic_pp13(int ihar, double pt, double mult, int evtSet, int deptType)
{
    return 0;
}

double SystErrors::MinJetpTSystematic_pp13(int ihar, double pt, double mult, int evtSet, int deptType)
{
    // if (deptType == MULTDEPT) {
    //     if (evtSet == 2) {
    //         if (ihar == Bins::v2) return 2.0;
    //         if (ihar == Bins::v3) {
    //             if (mult < 60) return InterPolate( 0,  60, 30.0, 3.5, mult);
    //             else           return InterPolate(60, 150,  3.5, 3.5, mult);
    //         }
    //         if (ihar == Bins::v4) return 9.5;
    //         return 0;
    //     }
    //     if (evtSet == 0) {
    //         if (ihar == Bins::v2) return 2.0;
    //         if (ihar == Bins::v3) {
    //             if (mult < 60) return InterPolate( 0,  60, 30.0, 3.5, mult);
    //             else           return InterPolate(60, 150,  3.5, 3.5, mult);
    //         }
    //         if (ihar == Bins::v4) return 7.5;
    //         return 0;
    //     }
    // }
    // if (deptType == PTDEPT) {
    //     if (evtSet == 2) {
    //         if (ihar == Bins::v2) {
    //             if (pt < 4.0) return InterPolate(0  , 4.0, 1.0, 1.0, pt);
    //             else          return InterPolate(4.0, 8.0, 1.0, 7.8, pt);
    //         }
    //         if (ihar == Bins::v3) return 4.0;
    //         if (ihar == Bins::v4) return 8.5;
    //         return 0;
    //     }
    //     if (evtSet == 0) {
    //         if (ihar == Bins::v2) {
    //             if (pt < 4.0) return InterPolate(0  , 4.0, 1.0, 1.0, pt);
    //             else          return InterPolate(4.0, 8.0, 1.0, 7.8, pt);
    //         }
    //         if (ihar == Bins::v3) {
    //             if (pt < 4.0) return InterPolate(0  , 4.0, 4.0,  4.0, pt);
    //             else          return InterPolate(4.0, 8.0, 4.0, 14.0, pt);
    //         }
    //         if (ihar == Bins::v4) return 10.0;
    //         return 0;
    //     }
    // }
    return 0;
}

#endif
