#!/bin/zsh

ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup 21.2.170,AnalysisBase
# lsetup "root 6.18.04-x86_64-centos7-gcc8-opt"

root -b -l -q "S01_RebinCentrality.C+($1,$2,$3,$4,$5,$6,$7,$8,$9,${10},${11},              ${12})"
root -b -l -q "S02_Rebin_pT.C+($1,$2,$3,$4,$5,$6,$7,$8,$9,${10},${11},                     ${12})"
root -b -l -q "S03_RebinCharge.C+($1,$2,$3,$4,$5,$6,$7,$8,$9,${10},${11},                  ${12})"
root -b -l -q "S03_Update.C+($1,$2,$3,$4,$5,$6,$7,$8,$9,${10},${11},                       ${12})"
root -b -l -q "S04_ProjectionX.C+($1,$2,$3,$4,$5,$6,$7,$8,$9,${10},${11},                  ${12})"
# root -b -l -q "S05_PTY1D.C+($1,$2,$3,$4,$5,$6,$7,$8,$9,${10},${11},                        ${12})"
# root -b -l -q "S06_ZYAM1D.C+($1,$2,$3,$4,$5,$6,$7,$8,$9,${10},${11},                       ${12})"
# root -b -l -q "S07a_FitPTY_Template.C+($1,$2,$3,$4,$5,$6,$7,$8,$9,${10},${11},     0,      ${12})"
# root -b -l -q "S07a_FitPTY_Template.C+($1,$2,$3,$4,$5,$6,$7,$8,$9,${10},${11},     1,      ${12})"
# root -b -l -q "S07b_FitPTY_Direct.C+($1,$2,$3,$4,$5,$6,$7,$8,$9,${10},${11},               ${12})"
# root -b -l -q "S07d_FitPTY_FCalTemplate.C+($1,$2,$3,$4,$5,$6,$7,$8,$9,${10}, 0,      ${11})"
# root -b -l -q "S07d_FitPTY_FCalTemplate.C+($1,$2,$3,$4,$5,$6,$7,$8,$9,${10}, 1,      ${11})"
# root -b -l -q "S07e_TemplateVnnRatioErrors.C+($1,$2,$3,$4,$5,$6,$7,$8,$9,${10}            )"


touch done_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_${9}_${10}_${11}_fgType${12}.txt
