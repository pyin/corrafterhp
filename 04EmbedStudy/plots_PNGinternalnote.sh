# ================================================================================
# Background plots
#
mkdir pngFigs
# # trigger plots
# mkdir pngFigs/datasets
# root -b -l -q Plots_trigger.C++
# root -b -l -q Plots_Ntrk.C++
# mv figs/*.png pngFigs/datasets/datasets

# # delta multiplicity
# mkdir pngFigs/methodology
# root -b -l -q Plots_UENch.C++
# mv figs/*.png pngFigs/methodology/UECorrection

# # pT distributions with particle rejection
# root -b -l -q "Plots_pTDist.C+(1,1,2,8,3,6,2,1,2,0)"
# mv figs/*.png pngFigs/methodology/pTDist

# # jet pT corrected vs original
# root -b -l -q "Plots_jetpT.C+(1,1,2,8,3,6,2,1,2,0)"
# root -b -l -q "Plots_UECorrection.C+(1,1,2,8,3,6,2,1,2,0)"
# mv figs/*.png pngFigs/methodology/UECorrection

# # 1D 2PC for methodology, using inclusive case
mkdir pngFigs/results
mkdir pngFigs/results/1d_2pc
# root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,0,0,0,0,0,0,   0,0)"
# mv figs/*.png pngFigs/results/1d_2pc/



# ================================================================================
# Systematics
#
mkdir pngFigs/systematics
# Tracking efficiency
mkdir pngFigs/systematics/trackEff
root -b -l -q "Plots_Vn_Systematics.C+(2,0)"
root -b -l -q "Plots_Vn_Systematics.C+(0,0)"
mv figs/*.png  pngFigs/systematics/trackEff/

# Pileup
mkdir pngFigs/systematics/pileup
root -b -l -q "Plots_Vn_Systematics.C+(2,1)"
root -b -l -q "Plots_Vn_Systematics.C+(0,1)"
mv figs/*.png  pngFigs/systematics/pileup

# peripheral bin
mkdir pngFigs/systematics/periphbin
root -b -l -q "Plots_Vn_Systematics.C+(2,2)"
root -b -l -q "Plots_Vn_Systematics.C+(0,2)"
mv figs/*.png  pngFigs/systematics/periphbin

# Event mixing
mkdir pngFigs/systematics/eventMixing
root -b -l -q "Plots_Vn_Systematics.C+(2,3)"
root -b -l -q "Plots_Vn_Systematics.C+(0,3)"
mv figs/*.png  pngFigs/systematics/eventMixing

# UE Correction
mkdir pngFigs/systematics/UECorr
root -b -l -q "Plots_Vn_Systematics.C+(2,4)"
root -b -l -q "Plots_Vn_Systematics.C+(0,4)"
mv figs/*.png  pngFigs/systematics/UECorr

# MinSndJetpT Correction
mkdir pngFigs/systematics/MinSndJetpT
root -b -l -q "Plots_Vn_Systematics.C+(2,6)"
root -b -l -q "Plots_Vn_Systematics.C+(0,6)"
mv figs/*.png  pngFigs/systematics/MinSndJetpT



# ================================================================================
# 2D 2PC
mkdir pngFigs/results/2d_2pc
root -b -l -q "Plots_2D_2PC.C+(1,1,2,8,3,6,2,1,2,0)"
root -b -l -q "Plots_2D_2PC.C+(1,1,2,8,3,6,0,1,2,0)"
root -b -l -q "Plots_2D_2PC.C+(1,1,2,8,3,6,1,1,2,0)"
mv figs/*.png pngFigs/results/2d_2pc/

# 1D 2PC
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,6,2,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,6,0,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,6,1,1,2,0,   0,0)"
mv figs/*.png pngFigs/results/1d_2pc/

# pt dependence
mkdir pngFigs/results/vn_ptDept
root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,1,1,2,  0,0,  1)"
root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,1,1,0,  0,0,  1)"
root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,1,1,1,  0,0,  0)"
mv figs/*.png pngFigs/results/vn_ptDept/

# multiplicity dependence
mkdir pngFigs/results/vn_multDept
root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,0,1,2,  0,0,  1)"
root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,0,1,0,  0,0,  1)"
root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,0,1,1,  0,0,  0)"
mv figs/*.png pngFigs/results/vn_multDept/




# # ================================================================================
# # 2D 2PC
# mkdir pngFigs/ETresults
# mkdir pngFigs/ETresults/2d_2pc
# root -b -l -q "Plots_2D_2PC.C+(1,1,2,8,3,6,2,1,2,1)"
# root -b -l -q "Plots_2D_2PC.C+(1,1,2,8,3,6,0,1,2,1)"
# root -b -l -q "Plots_2D_2PC.C+(1,1,2,8,3,6,1,1,2,1)"
# mv figs/*.png pngFigs/ETresults/2d_2pc/

# # 1D 2PC
# mkdir pngFigs/ETresults/1d_2pc
# root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,6,2,1,2,1,   0,0)"
# root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,6,0,1,2,1,   0,0)"
# root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,6,1,1,2,1,   0,0)"
# mv figs/*.png pngFigs/ETresults/1d_2pc/

# # pt dependence
# mkdir pngFigs/ETresults/vn_ptDept
# root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,1,1,2,  1,0,  0)"
# root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,1,1,0,  1,0,  0)"
# root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,1,1,1,  1,0,  0)"
# mv figs/*.png pngFigs/ETresults/vn_ptDept/

# # multiplicity dependence
# mkdir pngFigs/ETresults/vn_multDept
# root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,0,1,2,  1,0,  0)"
# root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,0,1,0,  1,0,  0)"
# root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,0,1,1,  1,0,  0)"
# mv figs/*.png pngFigs/ETresults/vn_multDept/
