#!/bin/zsh

# sh run.sh 1 0 1 2 8  3 2  2 1 1  0   0
# sh run.sh 1 0 1 2 8  3 2  2 1 1  0   1
# sh run.sh 1 0 1 2 8  3 2  2 1 1  0   2
# # sh run.sh 1 0 1 2 8  3 6  2 1 1  0   3
# # sh run.sh 1 0 1 2 8  3 6  2 1 1  0   4
# sh run.sh 1 0 1 2 8  3 2  2 1 1  0   5
# # sh run.sh 1 0 1 2 8  3 6  2 1 1  0   6


root -b -l -q "Plots_rawPairs.C+(1,0,1,2,8,3,2,2,1,1,0,   0)"
root -b -l -q "Plots_rawPairs.C+(1,0,1,2,8,3,2,2,1,1,0,   1)"
root -b -l -q "Plots_rawPairs.C+(1,0,1,2,8,3,2,2,1,1,0,   2)"
root -b -l -q "Plots_rawPairs.C+(1,0,1,2,8,3,2,2,1,1,0,   5)"
# root -b -l -q "Plots_rawPairs.C+(1,0,1,2,8,3,6,2,1,1,0,   6)"

touch done_run_all.txt
