# =============================================
# multiplicity dependence   fgTag=0
#
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,0,0,  0)"    # with ZYAM
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,0,1,  0)"    # without ZYAM
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,0,2,  0)"    # Fourier

# =============================================
# pT dependence             fgTag=0
#
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,1,0,  0)"    # with ZYAM
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,1,1,  0)"    # without ZYAM
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,1,2,  0)"    # Fourier




# =============================================
# multiplicity dependence   fgTag=1
#
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,0,0,  1)"    # with ZYAM
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,0,1,  1)"    # without ZYAM
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,0,2,  1)"    # Fourier

# =============================================
# pT dependence             fgTag=1
#
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,1,0,  1)"    # with ZYAM
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,1,1,  1)"    # without ZYAM
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,1,2,  1)"    # Fourier




# =============================================
# multiplicity dependence   fgTag=2
#
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,0,0,  2)"    # with ZYAM
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,0,1,  2)"    # without ZYAM
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,0,2,  2)"    # Fourier

# =============================================
# pT dependence             fgTag=2
#
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,1,0,  2)"    # with ZYAM
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,1,1,  2)"    # without ZYAM
root -b -l -q "Plots_Vn_CompareMultiCases.C+(0,1,2,  2)"    # Fourier
