//Compare the vnn values from the S07e_TemplateVnnRatioErrors.C to the values obtained from S07a_FitPTY_Template.C
#include "common.C"
#include "bins.h"

void S07e_Check1(int ihar=2,std::string target="AllEvents"){

  TFile *_file0 = nullptr;
  if(target=="AllEvents"){
    _file0=TFile::Open("01RootFiles/Data_LowAndIntermediateMu_eff1_trig2_pileup8_jet3_jetcut6_ifHaveJet2_ifUECorr1_corrtype2_multtype0_fgTag0_TemplateFits_pedestal_vnRatioErrors.root");
  }
  TFile *_file1 = TFile::Open("01RootFiles/Data_LowAndIntermediateMu_eff1_trig2_pileup8_jet3_jetcut6_ifHaveJet2_ifUECorr1_multtype0_fgTag0_TemplateFits_pedestal_vnn.root");
  TFile *_file2 = TFile::Open("01RootFiles/Data_LowAndIntermediateMu_eff1_trig2_pileup8_jet3_jetcut6_ifHaveJet2_ifUECorr1_corrtype2_multtype0_fgTag0_TemplateFits_pedestal_vnn.root");

  TH1D* hist_inclusive = nullptr;
  TH1D* hist_target    = nullptr;

  const int ipta=Bins::GetPtaIndex(0.5,5.0);
  const int iptb=Bins::GetPtbIndex(0.5,5.0);
  const int centbin_peripheral = Bins::GetCentIndex(0,20);

  //---------------------------------------------------------------------------
  //Plot vnn values
  TCanvas*C2=Common::StandardCanvas1c("C_Combined");
  for(int ican:{0,1}){
    TProfile *prof = nullptr;
    TH1D     *hist = nullptr;
    TCanvas*C1 = nullptr;
    char name1[600],name2[600];
    if(ican==0){
      C1=Common::StandardCanvas1c("C_inclusive");
      sprintf(name1,"p_inclusive_v%d%d_pericent%d_pta%d_ptb%d_ch2_deta01",ihar,ihar,centbin_peripheral,ipta,iptb);
      sprintf(name2,          "h_v%d%d_pericent%d_pta%d_ptb%d_ch2_deta01",ihar,ihar,centbin_peripheral,ipta,iptb);
      prof=(TProfile*)_file0->Get(name1);
      hist=(TH1D*)    _file1->Get(name2);
      if(!hist) {std::cout<<name1<<endl; throw std::exception();}
      if(!prof) {std::cout<<name2<<endl; throw std::exception();}
      Common::format(prof,1,24);
      Common::format(hist,2,25);
      hist_inclusive=hist;
    }
    else if(ican==1){
      C1=Common::StandardCanvas1c("C_target");
      sprintf(name1,"p_target_v%d%d_pericent%d_pta%d_ptb%d_ch2_deta01",ihar,ihar,centbin_peripheral,ipta,iptb);
      sprintf(name2,       "h_v%d%d_pericent%d_pta%d_ptb%d_ch2_deta01",ihar,ihar,centbin_peripheral,ipta,iptb);
      prof=(TProfile*)_file0->Get(name1);
      hist=(TH1D*)    _file2->Get(name2);
      if(!hist) {std::cout<<name1<<endl; throw std::exception();}
      if(!prof) {std::cout<<name2<<endl; throw std::exception();}
      Common::format(prof,4,24);
      Common::format(hist,6,25);
      hist_target=hist;
    }

    C1->cd();
    prof->Draw();
    hist->Draw("same");
    prof->GetYaxis()->SetRangeUser  (-.005,0.005);
    if(ihar==3) prof->GetYaxis()->SetRangeUser  (-.0015,0.0015);
    prof->GetXaxis()->SetTitleOffset(2);
    prof->GetYaxis()->SetTitleOffset(0.5);

    C2->cd();
    if(ican==0) prof->Draw();
    else        prof->Draw("same");
    hist->Draw("same");
  }
  //---------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  //Plot vnn Ratios
  TCanvas*C3=Common::StandardCanvas1c("C_ratio");
  TH1D* hist_ratio=(TH1D*)hist_target->Clone(Common::UniqueName().c_str());
  hist_ratio->Divide(hist_inclusive);

  char name[600];
  sprintf(name,"h_ratio_v%d%d_pericent%d_pta%d_ptb%d_ch2_deta01",ihar,ihar,centbin_peripheral,ipta,iptb);
  TH1D*hist=(TH1D*)_file0->Get(name);
  sprintf(name,"ratio (%s v_{%d,%d}/inclusive v_{%d,%d})",target.c_str(),ihar,ihar,ihar,ihar);
  hist      ->SetBinContent(centbin_peripheral+1,-1);
  hist      ->SetBinError  (centbin_peripheral+1,-1);
  hist_ratio->SetBinContent(centbin_peripheral+1,-1);
  hist_ratio->SetBinError  (centbin_peripheral+1,-1);
  hist->GetYaxis()->SetTitle(name);
  hist->GetXaxis()->SetTitleOffset(2);
  hist->GetYaxis()->SetTitleOffset(0.5);
  hist->GetYaxis()->SetRangeUser  (0.5,1.5);
  hist->GetXaxis()->SetRangeUser  (19,64);
  Common::format(hist,1,24);
  hist->Draw();
  hist->LabelsOption("v");
  Common::ShiftXaxis(hist_ratio,0.1);
  hist_ratio->Draw("same");


  TLegend *leg=Common::StandardLegend(.2,.2,.5,.5);
  leg->AddEntry(hist      ,"From Toys");
  leg->AddEntry(hist_ratio,"Direct division");
  leg->Draw();

  const int m_data_type=DataSetEnums::DATA_LOW_AND_INTERMEDIATE_MU;
  const int m_correlation_type=DataSetEnums::HADRON_HADRON_CORRELATIONS;
  int size = 24;
  float X = 0.18, Y = 0.88;
  Common::myText2(X       , Y      , 1, "ATLAS"   , size, 73);
  Common::myText2(X + 0.14, Y      , 1, Common::Internal, size, 43);
  Common::myText2(X       , Y - .06, 1, DataSetEnums::DATASETLABEL [m_data_type] + " " + DataSetEnums::DATASETENERGY[m_data_type], size, 43);
  if (Bins::GetPtbIndexForPtaIndex(ipta) == iptb) {
     Common::myText2(X     , Y - .12, 1, Bins::label_ptab(ipta, iptb, m_correlation_type) , size, 43);
  }
  else {
     Common::myText2(X     , Y - .12, 1, Bins::label_pta (ipta, m_correlation_type) , size, 43);
     Common::myText2(X     , Y - .12, 1, Bins::label_ptb (iptb, m_correlation_type) , size, 43);
  }
  Common::myText2(0.7, Y, 1, "Template Fit", size, 63);
  //if (m_if_have_jet == 2) Common::myText2(0.7, Y - 0.06, 1, "AllEvents", size, 63);
  //if (m_if_have_jet == 0) Common::myText2(0.7, Y - 0.06, 1, "NoJet"    , size, 63);
  //if (m_if_have_jet == 1) Common::myText2(0.7, Y - 0.06, 1, "WithJet"  , size, 63);
  //---------------------------------------------------------------------------
  C3->SaveAs( ("figs/"+target+"_har"+std::to_string(ihar)+".pdf").c_str());
}

