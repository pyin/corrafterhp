# ================================================================================
# Background plots
#
# trigger plot and multiplicity plots
root -b -l -q "Plots_trigger.C+(1,1,2,8,3,6,2,1,2,0)"
root -b -l -q Plots_Ntrk_inclusive.C++
mv figs/*.pdf figs/datasets

# FCalET vs multiplicity with profile
root -b -l -q "Plots_MultFCal.C+(1,1,1,5,3,6,2,1,2,1)"
root -b -l -q "Plots_MultFCal.C+(1,1,1,5,3,6,0,1,2,1)"
root -b -l -q "Plots_MultFCal.C+(1,1,1,5,3,6,1,1,2,1)"
# root -b -l -q "Plots_MultFCal.C+(1,1,2,5,3,6,2,1,2,1)"
# root -b -l -q "Plots_MultFCal.C+(1,1,2,5,3,6,0,1,2,1)"
# root -b -l -q "Plots_MultFCal.C+(1,1,2,5,3,6,1,1,2,1)"
mv figs/*.pdf figs/datasets/fcal_mult

# 1D 2PC for methodology, using inclusive case
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,6,2,1,0,0,   0,0)"
mv figs/*.pdf figs/results/1d_2pc/

# delta multiplicity
root -b -l -q Plots_UENch.C++
mv figs/*.pdf figs/methodology/UECorrection

# jet pT corrected vs original
root -b -l -q "Plots_jetpT.C+(1,1,2,8,3,6,2,1,2,0)"
root -b -l -q "Plots_UECorrection.C+(1,1,2,8,3,6,2,1,2,0)"
mv figs/*.pdf figs/methodology/UECorrection

# pT distributions with particle rejection
root -b -l -q "Plots_pTDist.C+(1,1,2,8,3,11,2,1,2,0)"
root -b -l -q "Plots_pTDist.C+(1,1,2,8,3,12,2,1,2,0)"
root -b -l -q "Plots_pTDist.C+(1,1,2,8,3,13,2,1,2,0)"
root -b -l -q "Plots_pTDist.C+(1,1,2,8,3, 7,2,1,2,0)"
root -b -l -q "Plots_pTDist.C+(1,1,2,8,3, 6,2,1,2,0)"
root -b -l -q "Plots_pTDist.C+(1,1,2,8,3, 8,2,1,2,0)"
mv figs/*.pdf figs/methodology/pTDist

# multiplicity distribution for different jet threshold
root -b -l -q "Plots_Ntrk_3case.C+( 6)"
root -b -l -q "Plots_Ntrk_3case.C+( 7)"
root -b -l -q "Plots_Ntrk_3case.C+( 8)"
root -b -l -q "Plots_Ntrk_3case.C+( 9)"
root -b -l -q "Plots_Ntrk_3case.C+(10)"
root -b -l -q "Plots_Ntrk_3case.C+(12)"
mv figs/*.pdf figs/methodology/Nch_differentJetCut

# jet eta overlay
root -b -l -q "Plots_overlayJetCases.C+(0,0)"
mv figs/*.pdf figs/methodology/jetMonitor

# deta vs dphi plot   and dphi projection
root -b -l -q Plots_jetdEtadPhi_LnSL.C++
mv figs/*.pdf figs/methodology/jetMonitor

# pT spectrum for all case and ratios
root -b -l -q "Plots_pTDist_AllCase2.C+(10)"
mv figs/*.pdf figs/methodology/pTSpectrumApp



# ================================================================================
# Systematics
#
# Tracking efficiency
root -b -l -q "Plots_Vn_Systematics.C+(2,0)"
root -b -l -q "Plots_Vn_Systematics.C+(0,0)"
mv figs/*.pdf  figs/systematics/trackEff/

# Pileup
root -b -l -q "Plots_Vn_Systematics.C+(2,1)"
root -b -l -q "Plots_Vn_Systematics.C+(0,1)"
mv figs/*.pdf  figs/systematics/pileup

# peripheral bin
root -b -l -q "Plots_Vn_Systematics.C+(2,2)"
root -b -l -q "Plots_Vn_Systematics.C+(0,2)"
root -b -l -q "Plots_Vn_Systematics.C+(1,2)"
mv figs/*.pdf  figs/systematics/periphbin

# peripheral bin - FCalET
root -b -l -q "Plots_Vn_Systematics_FCalET.C+(2,2)"
root -b -l -q "Plots_Vn_Systematics_FCalET.C+(0,2)"
root -b -l -q "Plots_Vn_Systematics_FCalET.C+(1,2)"
mv figs/*.pdf figs/systematics/periphbinFCalET

# Event mixing
root -b -l -q "Plots_Vn_Systematics.C+(2,3)"
root -b -l -q "Plots_Vn_Systematics.C+(0,3)"
mv figs/*.pdf  figs/systematics/eventMixing

# Crosscheck: UE Correction
root -b -l -q "Plots_Vn_Systematics.C+(2,4)"
root -b -l -q "Plots_Vn_Systematics.C+(0,4)"
root -b -l -q "Plots_Vn_Systematics.C+(1,4)"
mv figs/*.pdf  figs/systematics/UECorr

# Crosscheck: Jet constituent cut
root -b -l -q "Plots_Vn_CompareConstituentCut.C+(0,0,1,2,  0)"
root -b -l -q "Plots_Vn_CompareConstituentCut.C+(0,0,1,0,  0)"
root -b -l -q "Plots_Vn_CompareConstituentCut.C+(0,0,1,1,  0)"
root -b -l -q "Plots_Vn_CompareConstituentCut.C+(0,1,1,2,  0)"
root -b -l -q "Plots_Vn_CompareConstituentCut.C+(0,1,1,0,  0)"
root -b -l -q "Plots_Vn_CompareConstituentCut.C+(0,1,1,1,  0)"
mv figs/*.pdf figs/systematics/crosscheckJetConstituent

# Crosscheck: use FCalET as peripheral
root -b -l -q "Plots_Vn_CompareFCalPeriph.C+(0,0,1,2,  0)"
root -b -l -q "Plots_Vn_CompareFCalPeriph.C+(0,0,1,0,  0)"
root -b -l -q "Plots_Vn_CompareFCalPeriph.C+(0,0,1,1,  0)"
root -b -l -q "Plots_Vn_CompareFCalPeriph.C+(0,1,1,2,  0)"
root -b -l -q "Plots_Vn_CompareFCalPeriph.C+(0,1,1,0,  0)"
root -b -l -q "Plots_Vn_CompareFCalPeriph.C+(0,1,1,1,  0)"
mv figs/*.pdf figs/systematics/crosscheckFCalPeriph

# Crosscheck: Different jet threshold
root -b -l -q "Plots_Vn_jetCut_CompareMultiCases.C+(0,0,1,2,  0)"
root -b -l -q "Plots_Vn_jetCut_CompareMultiCases.C+(0,0,1,0,  0)"
root -b -l -q "Plots_Vn_jetCut_CompareMultiCases.C+(0,0,1,1,  0)"
root -b -l -q "Plots_Vn_jetCut_CompareMultiCases.C+(0,1,1,2,  0)"
root -b -l -q "Plots_Vn_jetCut_CompareMultiCases.C+(0,1,1,0,  0)"
root -b -l -q "Plots_Vn_jetCut_CompareMultiCases.C+(0,1,1,1,  0)"
mv figs/*.pdf figs/systematics/crosscheckJetThreshold



# ================================================================================
# Final result ratio sysErrors
# track efficiency
root -b -l -q  "Plots_Vn_RatioSystematics.C+(2,0,0)"
root -b -l -q  "Plots_Vn_RatioSystematics.C+(0,0,0)"
mv figs/*.pdf  figs/systematics/finalRatio/trackEff

# event mixing
root -b -l -q  "Plots_Vn_RatioSystematics.C+(2,3,0)"
root -b -l -q  "Plots_Vn_RatioSystematics.C+(0,3,0)"
mv figs/*.pdf  figs/systematics/finalRatio/eventMixing

# peripheral reference
root -b -l -q  "Plots_Vn_RatioSystematics.C+(2,2,0)"
root -b -l -q  "Plots_Vn_RatioSystematics.C+(0,2,0)"
root -b -l -q  "Plots_Vn_RatioSystematics.C+(1,2,0)"
mv figs/*.pdf  figs/systematics/finalRatio/periphbin

# peripheral reference FCalET
root -b -l -q  "Plots_Vn_RatioSystematics_FCalET.C+(2,2,1)"
root -b -l -q  "Plots_Vn_RatioSystematics_FCalET.C+(0,2,1)"
root -b -l -q  "Plots_Vn_RatioSystematics_FCalET.C+(1,2,1)"
mv figs/*.pdf  figs/systematics/finalRatio/periphbinFCalET



# ================================================================================
# 2D 2PC
root -b -l -q "Plots_2D_2PC.C+(1,1,2,8,3,6,2,1,2,0)"
root -b -l -q "Plots_2D_2PC.C+(1,1,2,8,3,6,0,1,2,0)"
root -b -l -q "Plots_2D_2PC.C+(1,1,2,8,3,6,1,1,2,0)"
mv figs/*.pdf figs/results/2d_2pc/

# 1D 2PC
# 6GeV
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,11,2,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,11,0,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,11,1,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,11,2,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,11,0,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,11,1,1,2,0,   0,0)"
# 7GeV
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,12,2,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,12,0,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,12,1,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,12,2,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,12,0,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,12,1,1,2,0,   0,0)"
# 8GeV
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,13,2,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,13,0,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,13,1,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,13,2,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,13,0,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,13,1,1,2,0,   0,0)"
# 9GeV
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,7,2,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,7,0,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,7,1,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,7,2,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,7,0,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,7,1,1,2,0,   0,0)"
# 10GeV
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,6,2,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,6,0,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,6,1,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,6,2,1,0,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,6,2,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,6,0,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,6,1,1,2,0,   0,0)"
# 12GeV
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,8,2,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,8,0,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,8,1,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,8,2,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,8,0,1,2,0,   0,0)"
root -b -l -q "Plots_TemplateFits_pTDeptBins.C+(1,1,2,8,3,8,1,1,2,0,   0,0)"
mv figs/*.pdf figs/results/1d_2pc/

# pt dependence
root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,1,1,2,  0,0,  1)"
root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,1,1,0,  0,0,  1)"
root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,1,1,1,  0,0,  1)"
mv figs/*.pdf figs/results/vn_ptDept/

# multiplicity dependence
root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,0,1,2,  0,0,  1)"
root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,0,1,0,  0,0,  1)"
root -b -l -q "Plots_Vn_refref_CompareMultiCases.C+(0,0,1,1,  0,0,  1)"
mv figs/*.pdf figs/results/vn_multDept/

# fcal dependence
root -b -l -q "Plots_Vn_refref_CompareMultiCases_FCal.C+(0,0,1,2,  1,0,  1)"
root -b -l -q "Plots_Vn_refref_CompareMultiCases_FCal.C+(0,0,1,0,  1,0,  1)"
root -b -l -q "Plots_Vn_refref_CompareMultiCases_FCal.C+(0,0,1,1,  1,0,  1)"
mv figs/*.pdf figs/results/vn_fcalDept/

# Summary plot
root -b -l -q "Plots_Vn_summary.C+(0,0,1,9,  0,0,  1)"
root -b -l -q "Plots_Vn_summary.C+(0,0,1,8,  0,0,  1)"
root -b -l -q "Plots_Vn_summary.C+(0,1,1,9,  0,0,  1)"
root -b -l -q "Plots_Vn_summary.C+(0,1,1,8,  0,0,  1)"
root -b -l -q "Plots_Vn_summary_FCal.C+(0,0,1,9,  1,0,  1)"
root -b -l -q "Plots_Vn_summary_FCal.C+(0,0,1,8,  1,0,  1)"
mv figs/*.pdf figs/results/summary/

# Crosscheck: Compare Inclusive with published result
root -b -l -q "Plots_Vn_CompOldNew.C+(0,0,1,2,  0,0,  1)"
root -b -l -q "Plots_Vn_CompOldNew.C+(0,1,1,2,  0,0,  1)"
root -b -l -q "Plots_Vn_CompOldNew.C+(0,0,1,3,  0,0,  1)"
root -b -l -q "Plots_Vn_CompOldNew.C+(0,1,1,3,  0,0,  1)"
root -b -l -q "Plots_Vn_CompOldNew2.C+(0,0,1,2,  0,0,  1)"
root -b -l -q "Plots_Vn_CompOldNew2.C+(0,1,1,2,  0,0,  1)"
mv figs/*.pdf figs/results/compPublished/


# h_central_cent39_pericent46_pta6_ptb37_ch2_deta01->SetMarkerColor(kRed)
# h_central_cent38_pericent46_pta6_ptb37_ch2_deta01->SetMarkerColor(kBlue)
# h_central_cent37_pericent46_pta6_ptb37_ch2_deta01->SetMarkerColor(kGreen + 1)
# h_central_cent36_pericent46_pta6_ptb37_ch2_deta01->SetMarkerColor(kCyan + 1)
# h_central_cent35_pericent46_pta6_ptb37_ch2_deta01->SetMarkerColor(kOrange + 1)
# h_central_cent34_pericent46_pta6_ptb37_ch2_deta01->SetMarkerColor(kMagenta)
# h_central_cent39_pericent46_pta6_ptb37_ch2_deta01->Draw()
# h_central_cent38_pericent46_pta6_ptb37_ch2_deta01->Draw("same")
# h_central_cent37_pericent46_pta6_ptb37_ch2_deta01->Draw("same")
# h_central_cent36_pericent46_pta6_ptb37_ch2_deta01->Draw("same")
# h_central_cent35_pericent46_pta6_ptb37_ch2_deta01->Draw("same")
# h_central_cent34_pericent46_pta6_ptb37_ch2_deta01->Draw("same")





# HJ correlation
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,6,2,1,1,0,   0,1)"
root -b -l -q "Plots_TemplateFits.C+(1,1,2,8,3,6,1,1,1,0,   0,1)"








root -b -l -q "Plots_jetpTNchCorr.C+(sui yi)"           # plot jet Nch mean and RMS on different panel
root -b -l -q "Plots_jetpTNchCorrOverlay.C+(sui yi)"    # plot jet Nch mean and RMS overlay with truth
