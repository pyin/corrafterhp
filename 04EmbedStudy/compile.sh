#!/bin/zsh

ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
localSetupROOT 6.00.00-x86_64-slc6-gcc48-opt


for file in  S01_RebinCentrality.C \
             S02_Rebin_pT.C \
             S03_RebinCharge.C \
             S04_ProjectionX.C \
             S04b_PeripheralScales.C \
             S05_PTY1D.C \
             S06_ZYAM1D.C \
             S07a_FitPTY_Template.C \
             S07b_FitPTY_Direct.C \
             S07c_FitPTY_PeriSub.C
  do
  echo $file
    root -b <<EOF
    .L $file+
    .q
EOF
  done
