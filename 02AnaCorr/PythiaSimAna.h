#ifndef _PythiaSimAna_HH_
#define _PythiaSimAna_HH_

#include <vector>
#include "Riostream.h"
#include "TChain.h"
#include <string>
#include "Event.h"

#include "map"
#include "DatasetEnums.h"
#include "bins.h"
#include "TH3.h"
#include <TProfile.h>
#include <TProfile2D.h>
// #include "GoodRunsLists/GoodRunsListSelectionTool.h"

#include "CheckHotSpot.h"


using namespace std;

class TH1;
class TH2;
class TProfile;

struct triple {
    std::string first;
    Bool_t*     second;
    int         third;
};

struct PFlowJetType {
    double pT_orig;
    double pT;
    double pT_sumAbvN;
    double eta;
    double phi;
    double Nconstit;
    double pT_corrected;
    double pT_avgCorr;
    double pT_correction;
    double Nconstit_corrected;
    double Nconstit_correction;
    double newjvt;
    double pT_uncalib;
    double sumTrkPT;
    double sumAllPT;

    bool pflow_above1stCut;
    bool pflow_above2ndCut;
    bool pflow_passJVT;
    bool inBrightBand;
    bool haveNeighborPflowJet;
    bool haveBalncJet;

    vector<double> jetIngredientPT ;
    vector<double> jetIngredientETA;
    vector<double> jetIngredientPHI;
    vector<int>    jetIngredientCH ;
    vector<int>    jetIngredientPID;
};

struct Particle {
    double pT;
    double eta;
    double phi;
    int    pid;
    int    charge;
};

class PythiaSimAna {
    enum {
        //pool bins:
        // nc = Bins::NCENT,
        nc = 200,
        nz = 10,
        ZMAX = 100,
        plnbr = nc * nz,
    };

    enum {
        PP_MIN_BIAS            = 2,
        PP_MIN_BIAS_MODIFIED   = 4,
        HI_LOOSE               = 8,
        HI_TIGHT               = 16,
        // HI_TIGHT_TIGHTER_D0_Z0 = 16,
        // HI_LOOSE_7SCT_HITS     = 32,
        HI_TIGHT_LOOSE_D0_Z0   = 32,
        HI_LOOSE_TIGHT_D0_Z0   = 64,
        HI_LOOSE_TIGHTER_D0_Z0 = 128
    };

public:
    PythiaSimAna();

    void run();

    int  m_from           = 0    ;
    int  m_to             = 0    ;
    int  m_total          = 0    ;
    bool m_monitor_mode   = false;

    int m_data_type         = DataSetEnums::DATA_LOWMU                ;
    int m_do_bootstrapping  = DataSetEnums::NO_BS                     ;
    int m_mu_limit          = DataSetEnums::NO_MU_LIMIT               ;
    int m_apply_efficiency  = DataSetEnums::DEFAULT_EFFICIENCY        ;
    int m_trig_type         = DataSetEnums::ALL_TRIGS                 ;
    int m_pileup_reject     = DataSetEnums::NO_PILEUP_REJECTION       ;
    int m_trk_quality       = DataSetEnums::TRKQUAL_PP_MIN_BIAS       ;
    int m_muon_elec         = DataSetEnums::NO_MUONELECTRON_REJECTION ;
    int m_jet_reject_type   = DataSetEnums::NO_JET_REJECT             ;
    int m_jet_ptcorr        = DataSetEnums::WITH_AVGCORRONLY          ;
    int m_jet_reject_cuts   = DataSetEnums::NO_JET_CUTS               ;
    int m_if_have_jet       = DataSetEnums::MINBIAS                   ;
    int m_if_have_uecorr    = DataSetEnums::WITH_MULTCORR             ;
    int m_correlation_type  = DataSetEnums::HADRON_HADRON_CORRELATIONS;
    int m_multiplicity_type = DataSetEnums::USE_NTRK                  ;


private:
    TChain *fChain = nullptr;


    void Init();
    void InitTriggers();
    void InitHistos();
    void SaveHistos();
    void InitJetCuts();

    void parsePFlowJets();
    void parseTruthParticles();
    void ReconstructPFlowJet();
    void parsePFlowObj();
    void reparsePFlowJet();
    void Fill(Event* event1, Event* event2, int mixtype);
    void FillJetMonitors();
    void FillEvtMonitors();
    void FillPFOMonitors();

    bool PassTrigger(double ntrk);
    bool PassQuality(int quality);
    bool inBrightBand(double eta);
    bool isSeperateFromPFlowJets(double partEta);
    bool isMuonElectron(int pIndex);
    bool RejectJet_Cone(double trk_pt, double trk_eta, double trk_phi, double trk_eff);
    bool FillMixed(Event* event1, Event* event_jet = nullptr, Event* event_orgn = nullptr);

    int get_zPool(double z);
    int GetCentBinNtrk(double ntrk);
    std::pair<int, float> CountLowPTPFlowParticle(PFlowJetType pj);

    double GetNtrkCent();
    double GetNtrkCentBeforeQual();
    double MultiplicityCorrection();
    double ConvertPhi(double phi);
    double CalculateDeltaPhi(double phi1, double phi2);
    double EvaluateTrackingEff(double pt, double eta);

    // GoodRunsListSelectionTool m_grlTool;

    // ------------------------------------
    // Jet cut variables
    //
    float dR = -1;
    float min_jet_pt     = 15.;
    float min_jet_pt_2nd = 10.;
    int   min_jet_Nconstituents = 0; //Minimum Number of constituents
    float pfJetPt_1st    = 25.;
    float pfJetPt_2nd    = 15.;
    float pfJetPt_blnc   = 15.;
    float pfJetPt_rjct   = 15.;
    float pfo_threshold  =  0.;
    // ------------------------------------

    int nmix;
    int m_cent_i;
    int m_TEbin;
    int m_centIndex_UECorr;
    int depth[nc];
    int FCalET_cent;
    float nCurrentBlue = 0;
    float totalEnergy;
    float mubar;
    double ntrk_cent;
    double ntrk_cent_orig;

    double m_zvtx;
    double ntrk_correction = 0;

    vector<PFlowJetType> pflowJetContainer;
    vector<Particle>     truthParticleContainer;
    vector<Particle>      chargeParticleContainer;

    vector<EVENT_PTR> pool[plnbr];

    TH1* h_trig;
    TH1* h1_Nch_perTrig_orig [200];
    TH1* h1_Nch_perTrig_MBHLT[200];
    TH1* h1_Nch_perTrig_HLT  [200];
    TH1* h1_Nch_perTrig_HLT2 [200];
    TH1* h1_Nch_perTrig      [200];
    TH1* h1_Nch_HLT_mb_sp1100_trk70_hmt_L1TE5;
    TH1* hcent;
    TH1* h_runNumber[7];
    TH1* h1_multiplicity[10];
    TH1* h_PoissonRndm;
    TH1* hcent_orig;
    TH1* h1_dvtxz;
    TH1* h_nvtx;
    TH1* hNtrk_origional;
    TH1* hNtrk_corrected;
    TH1* hNtrk_perRun[28];
    TH1* hNtrk_origional_beforeTrigger;
    TH1* hNtrk_corrected_beforeTrigger;
    TH1* h_MultCorrection;
    TH1* h1_pT_perPID[15];
    TH1* h_size_vexz[Bins::NCENT];
    TH1* h1_pfobj_etabar[Bins::NCENT];
    TH1* h_dz           [Bins::NCENT];
    TH1* dist_pT_trig      [Bins::NCENT];
    TH1* dist_pT_cc_poolJet[Bins::NCENT];
    TH1* N_trigger_jetcut [Bins::NCENT];    //pT spectra of tracks within the jet cone.
    TH1* h1_pflowJetdphi[Bins::NCENT];
    TH1* h1_Muon_PT    [Bins::NCENT];
    TH1* h1_Electron_PT[Bins::NCENT];
    TH1* h1_JetNewPt   [Bins::NCENT];
    TH1* h1_NJet1st[Bins::NCENT];
    TH1* h1_NJet2nd[Bins::NCENT];
    TH1* h1_NJet_re[Bins::NCENT];
    TH1* h1_NJet2nd_re[Bins::NCENT];
    TH1* h1_JetOrigPt_diffRun[28];
    TH1* h1_PFlowJets_NchNew[Bins::NCENT];
    TH1* h1_sumphi       [Bins::NCENT];
    TH1* h1_nPairs[4][Bins::NCENT];
    TH1* h1_red_deta_to_jet[Bins::NCENT];
    TH1* h1_red_dphi_to_jet[Bins::NCENT];
    TH1* h1_blue_deta_to_jet[Bins::NCENT];
    TH1* h1_blue_dphi_to_jet[Bins::NCENT];
    TH1* h1_jetPhi_orig[Bins::NCENT];
    TH1* h1_jetPhi[Bins::NCENT];
    TH1* h1_Red_Pt [Bins::NCENT];
    TH1* h1_Blue_Pt[Bins::NCENT];
    TH1* h1_Nch_perPID_Red [Bins::NCENT];
    TH1* h1_Nch_perPID_Blue[Bins::NCENT];
    TH1* fg_nchDept[Bins::NCENT];
    TH1* cc_nchDept[Bins::NCENT];
    TH1* fg_pTbDept[Bins::NPT][Bins::NPTCENT];
    TH1* cc_pTbDept[Bins::NPT][Bins::NPTCENT];
    // -----------------------------------
    // global monitor plots
    //    record how many event processed
    TH1* hFillEntries;
    TH1* hFillMixEntries;
    TH1* hEntries_nch;
    TH1* hEntries_ptb[Bins::NPTCENT];
    TH1* hFillEntries_cc;
    // -----------------------------------


    TH2* h2_Mult_CorrMult;
    TH2* h2_Mult_CorrMult2;
    TH2* h2_Mult_FCal;
    TH2* h2_Nch_beforeQual_afterQual;
    TH2* h2_BHM_NchTE;
    TH2* h2_GoodCaloEvt_NchTE;
    TH2* h2_pfo_finerbin_EtaPhi     ;
    TH2* h2_pfo_finerbin_EtaPhi_AbvN;
    TH2* h2_pfo_finerbin2_EtaPhi     ;
    TH2* h2_pfo_finerbin2_EtaPhi_AbvN;
    TH2* h2_trk_EtaPt[28];
    TH2* h2_JetPt_dNcon;
    TH2* h2_nchCovXY[Bins::NCENT];
    TH2* h2_pTbCovXY[Bins::NPT][Bins::NPTCENT];
    TH2* h2_etabar_ch_pfobj[Bins::NCENT];
    TH2* fg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* bg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* cg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* cc_unmatched[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* fg_after_correction[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* h2_Pt_Nch[Bins::NCENT];
    TH2* h2_Pt_Nch_pos[Bins::NCENT];
    TH2* h2_Pt_Nch_neg[Bins::NCENT];
    TH2* h2_Trk_EtaPhi_orig[Bins::NCENT];
    TH2* h2_Trk_EtaPhi     [Bins::NCENT];
    TH2* h2_Trk_EtaPhi_AbvN[Bins::NCENT];
    TH2* h2_Red_EtaPhi     [Bins::NCENT];
    TH2* h2_Red_EtaPhi_diffPt[Bins::NCENT][Bins::NPT1];
    TH2* h2_Red_chargePos_EtaPhi[Bins::NCENT];
    TH2* h2_Red_chargeNeg_EtaPhi[Bins::NCENT];
    TH2* h2_Blue_EtaPhi    [Bins::NCENT];
    TH2* h2_Blue_EtaPhi_diffPt[Bins::NCENT][Bins::NPT1];
    TH2* h2_Blue_chargePos_EtaPhi[Bins::NCENT][Bins::NPT1];
    TH2* h2_Blue_chargeNeg_EtaPhi[Bins::NCENT][Bins::NPT1];
    TH2* h2_Muon_EtaPhi    [Bins::NCENT];
    TH2* h2_Electron_EtaPhi[Bins::NCENT];
    TH2* h2_nBlue_nRed[Bins::NCENT];
    TH2* h2_fgBlue_bgBlue[Bins::NCENT];
    TH2* h2_PFlowJetsPT_uncalib_calib[Bins::NCENT];
    TH2* h2_PFlowJets_PT_ETA[Bins::NCENT];
    TH2* h2_PFlowJets_calibPT_PTAbvN[Bins::NCENT];
    TH2* h2_PFlowJets_calibPT_SumPFO[Bins::NCENT];
    TH2* h2_PFlowJets_TrkFailQualEtaPhi[Bins::NCENT];
    TH2* h2_PFlowJets_EtaPhi         [Bins::NCENT];
    TH2* h2_PFlowJets_EtaSumHEC      [Bins::NCENT];
    TH2* h2_PFlowJets_EtaSumGap      [Bins::NCENT];
    TH2* h2_PFlowJets_EtaPhi_rejected[Bins::NCENT];
    TH2* h2_PFlowJets_EtaPhi_jvtRej  [Bins::NCENT];
    TH2* h2_PFlowJets_EtaPhi_orig    [Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaSumHEC      [Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaSumGap      [Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaPhi_rejected[Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaPhi_jvtRej  [Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaPhi_orig    [Bins::NCENT];
    TH2* h2_PFlowJets_NchOrig_NchCor[Bins::NCENT];
    TH2* h2_PFlowJets_EtaJvt      [Bins::NCENT];
    TH2* h2_PFlowJets_EtaT_orig   [Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaJvt   [Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaT_orig[Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaPhi[Bins::NCENT];
    TH2* h2_PFlowJets_EtaPhi_diffRun[28][Bins::NCENT];
    TH2* h2_pflowobj_EtaPhi     [Bins::NCENT];
    TH2* h2_pflowobj_EtaPhi_AbvN[Bins::NCENT];
    TH2* h2_jetSumCalibPt_dPtRatio[Bins::NCENT];
    TH2* h2_jet_detadphi[Bins::NCENT];
    TH2* h2_jeteta_blnceta[Bins::NCENT];
    TH2* h2_dataJetPtpCalibPtRatio[Bins::NCENT];
    TH2* h2_dataJetPtpCalibPtRatio_all;

    TProfile* p_Ntrk;
    TProfile* p_v2[Bins::NCENT];
    TProfile* p_v2_jetcut[Bins::NCENT];

    TProfile2D* pf2_eta_jetpt_avgfrac;

    TH1D* h1_PflowJets2nd_TimePeak[Bins::NCENT];
    TH2* h_efficiency = nullptr;
    TH2D* h2_EtaPhi_EbyE         [9][Bins::NCENT];
    TH2D* h2_EtaPhi_ptWeight_EbyE[9][Bins::NCENT];
    TH1F* h_UE_pTThreshold[9][Bins::NCENT];    // value of pT correction
    TH1D* h1_pflowjet_Phi[25][10];
    TH1D* h1_eta;
    TH1D* h1_phi;
    TH1D* h1_pt;
    TH1D* h1_temp_nchfg[Bins::NCENT];
    TH1D* h1_temp_nchcc[Bins::NCENT];
    TH1D* h1_temp_pTbfg[Bins::NPT][Bins::NPTCENT];
    TH1D* h1_temp_pTbcc[Bins::NPT][Bins::NPTCENT];


    // Declaration of leaf types -- truth PYTHIA
    vector<int>    *particle_id  = 0;
    vector<double> *particle_pt  = 0;
    vector<double> *particle_eta = 0;
    vector<double> *particle_phi = 0;
    vector<double> *jet_pt       = 0;
    vector<double> *jet_eta      = 0;
    vector<double> *jet_phi      = 0;
};

void PythiaSimAna::Init() {
    fChain = new TChain("HeavyIonD3PD");

    std::cout << "-----> Initiating PYTHIA events!" << std::endl;
    fChain = new TChain("PyTree");
    fChain->Add("/usatlas/u/pyin/workarea/PYTHIA8/02Analysis/01RootFiles/pytree_Tune14Monash_HardQCDon_MPIoff_ISRon_above15_50M/*.root");
    fChain->SetBranchAddress("particle_id" , &particle_id );
    fChain->SetBranchAddress("particle_pt" , &particle_pt );
    fChain->SetBranchAddress("particle_eta", &particle_eta);
    fChain->SetBranchAddress("particle_phi", &particle_phi);
    fChain->SetBranchAddress("jet_pt"      , &jet_pt      );
    fChain->SetBranchAddress("jet_eta"     , &jet_eta     );
    fChain->SetBranchAddress("jet_phi"     , &jet_phi     );
    fChain->SetBranchStatus("*"              , 0);
    fChain->SetBranchStatus("particle*", 1);
    fChain->SetBranchStatus("jet*", 1);
}
#endif
