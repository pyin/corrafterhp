#ifndef _EmbedTruthEvent_HH_
#define _EmbedTruthEvent_HH_

#include <vector>
#include "Riostream.h"
#include "TChain.h"
#include <string>
#include "Event.h"

#include "map"
#include "DatasetEnums.h"
#include "bins.h"
#include "TH3.h"
#include <TProfile.h>
#include <TProfile2D.h>
// #include "GoodRunsLists/GoodRunsListSelectionTool.h"

#include "CheckHotSpot.h"


using namespace std;

class TH1;
class TH2;
class TProfile;

struct triple {
    std::string first;
    Bool_t*     second;
    int         third;
};

struct PFlowJetType {
    double pT_orig;
    double pT;
    double eta;
    double phi;
    double jvt;
    double pT_corrected;
    double pT_correction1;
    double pT_correction2;
    double pT_correction3;
    double Nconstit_corrected;
    double Nconstit_correction;
    int   Nconstit;

    bool pflow_above1stCut;
    bool pflow_above2ndCut;
    bool pflow_passJVT;
    bool inPFlowBrightBand;
    bool haveNeighborPflowJet;
    bool haveBalncJet;
    bool nearTruthJet;
    bool overlapDataJet;

    vector<int> pflowJetTrkIndex;

    vector<double> jetIngredientPT ;
    vector<double> jetIngredientETA;
    vector<double> jetIngredientPHI;
    vector<int>    jetIngredientCH ;
};

struct Particle {
    double pT;
    double eta;
    double phi;
    double t;
    int    charge;
};

class EmbedTruthEvent {
    enum {
        //pool bins:
        // nc = Bins::NCENT,
        nc = 200,
        nz = 10,
        ZMAX = 100,
        plnbr = nc * nz,
    };

    enum {
        PP_MIN_BIAS            = 2,
        PP_MIN_BIAS_MODIFIED   = 4,
        HI_LOOSE               = 8,
        HI_TIGHT               = 16,
        // HI_TIGHT_TIGHTER_D0_Z0 = 16,
        // HI_LOOSE_7SCT_HITS     = 32,
        HI_TIGHT_LOOSE_D0_Z0   = 32,
        HI_LOOSE_TIGHT_D0_Z0   = 64,
        HI_LOOSE_TIGHTER_D0_Z0 = 128
    };

public:
    EmbedTruthEvent();

    void run();

    int  m_from           = 0    ;
    int  m_to             = 0    ;
    int  m_total          = 0    ;
    bool m_monitor_mode   = false;

    int m_data_type         = DataSetEnums::DATA_LOWMU                ;
    int m_do_bootstrapping  = DataSetEnums::NO_BS                     ;
    int m_mu_limit          = DataSetEnums::NO_MU_LIMIT               ;
    int m_apply_efficiency  = DataSetEnums::DEFAULT_EFFICIENCY        ;
    int m_trig_type         = DataSetEnums::ALL_TRIGS                 ;
    int m_pileup_reject     = DataSetEnums::NO_PILEUP_REJECTION       ;
    int m_trk_quality       = DataSetEnums::TRKQUAL_PP_MIN_BIAS       ;
    int m_muon_elec         = DataSetEnums::NO_MUONELECTRON_REJECTION ;
    int m_jet_reject_type   = DataSetEnums::NO_JET_REJECT             ;
    int m_jet_ptcorr        = DataSetEnums::WITH_AVGCORRONLY          ;
    int m_jet_reject_cuts   = DataSetEnums::NO_JET_CUTS               ;
    int m_if_have_jet       = DataSetEnums::MINBIAS                   ;
    int m_if_have_uecorr    = DataSetEnums::WITH_MULTCORR             ;
    int m_correlation_type  = DataSetEnums::HADRON_HADRON_CORRELATIONS;
    int m_multiplicity_type = DataSetEnums::USE_NTRK                  ;


private:
    TChain *fChain = nullptr;
    TChain *truthChain = nullptr;
    TChain *dataJetChain = nullptr;


    void Init();
    void InitTriggers();
    void InitHistos();
    void SaveHistos();
    void InitJetCuts();
    void ReconstructPFlowJet();
    void InitUEpT();

    void reparsePFlowJet();
    void reparseDataPFlowJet();
    void readinRecoDataJets();
    void parseTruthParticles();
    void readinRecoTruthJet();
    void parseDataParticles();
    void parseEmbedding();
    void parseDataJetsDirect();
    void Fill(Event* event1, Event* event2, int mixtype);
    void FillJetMonitors();
    void FillEvtMonitors();
    void processDataPsi2V2();
    void processEmbedPsi2V2();

    bool Pileup();
    bool PassTrigger(double ntrk);
    bool inPFlowBrightBand(double pflowJetEta, double pflowJetPhi);
    bool isSeperateFromEmbedPFlowJets(float partEta);
    bool isSeperateFromDataPFlowJets(float partEta);
    bool isMuonElectron(int pIndex);
    bool RejectJet_Cone(float trk_pt, float trk_eta, float trk_phi, float trk_eff);
    bool FillMixed(Event* event1, Event* event_jet = nullptr, Event* event_orgn = nullptr);
    bool ifPassGRL(UInt_t rNum, UInt_t lbNum);
    bool PassQuality(int quality);

    int get_zPool(float z);
    int GetCentBinNtrk(float ntrk);
    std::pair<int, float> CountLowPTParticle(PFlowJetType pj);
    std::pair<int, float> CountLowPTDataPFlowParticle(PFlowJetType pj);

    double GetNtrkCent();
    double GetNtrkCentBeforeQual();
    double MultiplicityCorrection();
    double ConvertPhi(double phi);
    double CalculateDeltaPhi(double phi1, double phi2);
    double EvaluateTrackingEff(float pt, float eta);
    double findUECorr(TH2D* h, double jet_eta, double jet_phi, int type, int repass);
    double multiplicityInterpolate(int etabin, int phibin, int type);
    double interpolate(double low, double up, double val_low, double val_up, double x);



    // GoodRunsListSelectionTool m_grlTool;

    // ------------------------------------
    // Jet cut variables
    //
    float dR = -1;
    float min_jet_pt     = 15.;
    float min_jet_pt_2nd = 10.;
    int   min_jet_Nconstituents = 0; //Minimum Number of constituents
    float pfJetPt_1st    = 25.;
    float pfJetPt_2nd    = 15.;
    float pfJetPt_blnc   = 15.;
    // ------------------------------------

    int nmix;
    int m_cent_i = 0;
    int m_TEbin;
    int m_centIndex_UECorr;
    int depth[nc];
    int FCalET_cent;
    float nCurrentBlue = 0;
    float totalEnergy;
    double ntrk_cent;
    double ntrk_cent_orig;
    double embedPsi2 = 0;
    double dataPsi2  = 0;

    double m_zvtx;
    double ntrk_correction = 0;

    vector<PFlowJetType>  recoPFlowJetContainer;
    vector<PFlowJetType>  recoDataPFlowJetContainer;
    vector<PFlowJetType>  recoTruthPFlowJetContainer;
    vector<PFlowJetType>  directTruthJetContainer;
    vector<PFlowJetType>  directDataJetContainer;
    vector<Particle>      truthParticleContainer;
    vector<Particle>      fullParticleContainer;
    vector<Particle>      dataParticleContainer;
    vector<Particle>      chargeParticleContainer;

    std::vector<triple> m_trig_map_All;
    std::vector<triple> m_trig_map_minbias;
    std::vector<triple> m_trig_map_minbias_additional;
    std::vector<triple> m_trig_map_minbias_nojettrig;
    std::vector<triple> m_trig_map_minbias_nojet45;
    std::vector<triple> m_trig_map_HMT;
    std::vector<triple> m_trig_map_HMT_NOTE;
    std::vector<triple> m_trig_map_HMT_L1TE5;
    std::vector<triple> m_trig_map_HMT_L1TE10;
    std::vector<triple> m_trig_map_HMT_L1TE15;
    std::vector<triple> m_trig_map_HMT_L1TE20;
    std::vector<triple> m_trig_map_HMT_L1TE30PLUS;

    vector<EVENT_PTR> pool[plnbr];

    TH1* h_trig;
    TH1* hcent;
    TH1* hcent_orig;
    TH1* hzvtx[Bins::NCENT + 1];
    TH1* h_nvtx;
    TH1* h1_multiplicity[13];
    TH1* h1_Nch_perPID;
    TH1* h1_pT_perPID[15];
    TH1* h1_truthJetPTSpectrum[2];
    TH1* h1_dataJetPTSpectrum [3];
    TH1* h1_recoJetPTSpectrum [2];
    TH1* hNtrk_origional;
    TH1* hNtrk_corrected;
    TH1* hNtrk_origional_beforeTrigger;
    TH1* hNtrk_corrected_beforeTrigger;
    TH1* h1_NEvt1stPFlowJet;
    TH1* h_MultCorrection;
    TH1* h_size_vexz[Bins::NCENT];
    TH1* h_JetEta2nd[Bins::NCENT];
    TH1* dist_pT_trig[Bins::NCENT];
    TH1* dist_pT_cc_poolJet  [Bins::NCENT];
    TH1* h_jet_pTFrac   [Bins::NCENT];      // pT of all particle in jet cone / pT of jet
    TH1* N_trigger_jetcut [Bins::NCENT];    //pT spectra of tracks within the jet cone.
    TH1* h_bg_constituents[Bins::NCENT];    // value of constituents correction
    TH1* h_bg_ptWeight    [Bins::NCENT];    // value of pT correction
    TH1* h1_jetNch_orig[Bins::NCENT][14];
    TH1* h1_jetNch_UECR[Bins::NCENT][14];
    TH1* h1_pflowJetdphi[Bins::NCENT];
    TH1* h1_NPFlowJet[Bins::NCENT];
    TH1* h1_jetdphi_truth[Bins::NCENT];
    TH1* h1_jetdphi_reco [Bins::NCENT];
    TH1* h1_recoJetRoundTruth_Phi_1st[Bins::NCENT];
    // -----------------------------------
    // global monitor plots
    //    record how many event processed
    TH1* hFillEntries;
    TH1* hFillMixEntries;
    // -----------------------------------


    TH2* h_efficiency = nullptr;
    TH2* h2_Mult_CorrMult;
    TH2* h2_Mult_FCal;
    TH2* h2_Mult_NBlue;
    TH2* h2_Mult_NRed;
    TH2* h2_NRed_NBlue[Bins::NCENT];
    TH2* fg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* bg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* cg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* cc_unmatched[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* fg_after_correction[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* h2_NjetDirect_NjetReco[Bins::NCENT];
    TH2* h2_fgBlue_bgBlue[Bins::NCENT];
    TH2* h2_FullPsi2_FCalPsi2    [Bins::NCENT];
    TH2* h2_DataPsi2_EmbedPsi2   [Bins::NCENT];
    TH2* h2_FullPsi2V2_FCalPsi2V2[Bins::NCENT];
    TH2* h2_eta_pflowObjPT[Bins::NCENT];
    TH2* h2_jet_direcPTCalib_recoPT [Bins::NCENT];
    TH2* h2_jet_direcPTCalib_dpTRatio[Bins::NCENT];
    TH2* h2_jet_eta_dphiToPsi2_inclusive[Bins::NCENT];
    TH2* h2_tjet_deltaPT_dphiToPsi2_1st[Bins::NCENT];
    TH2* h2_tjet_deltaPT_dphiToPsi2_2nd[Bins::NCENT];
    TH2* h2_tjet_noCut_dPT_dphiToPsi2_1st[Bins::NCENT];
    TH2* h2_tjet_OLCut_dPT_dphiToPsi2_1st[Bins::NCENT];
    TH2* h2_jet_dPT_dphiToPsi2_1st[Bins::NCENT];
    TH2* h2_jet_dPT_dphiToDataPsi2_1st[Bins::NCENT];
    TH2* h2_jet_OlBlnc_dPT_dphiToDataPsi2_1st[Bins::NCENT];
    TH2* h2_jet_OlIsoBlnc_dPT_dphiToDataPsi2_1st[Bins::NCENT];

    TProfile2D* pf2_eta_jetpt_avgfrac;

    TProfile* p_Ntrk;
    TProfile* p_MultBins_withCorrMult;
    TProfile* p_CorrMultBins_withMult;
    TProfile* p_jetdNch[Bins::NCENT];
    TProfile* p_jetdpT [Bins::NCENT];

    TH2D* h2_EtaPhi_EbyE         [9][Bins::NCENT];
    TH2D* h2_EtaPhi_ptWeight_EbyE[9][Bins::NCENT];
    TH1D* h1_PflowJets2nd_TimePeak[Bins::NCENT];
    TH1F* h_UE_pTThreshold[9][Bins::NCENT];    // value of pT correction

    TProfile* pf_evt_Qx_all[9];
    TProfile* pf_evt_Qy_all[9];



    // Declaration of leaf types -- reco data jet tree
    UInt_t             RunNumber2   = 0;
    ULong64_t          eventNumber2 = 0;
    std::vector<float> *recoDataJet_pt ;
    std::vector<float> *recoDataJet_eta;
    std::vector<float> *recoDataJet_phi;
    std::vector<std::vector<float>> *recoDataJet_constituent_pT;
    std::vector<std::vector<float>> *recoDataJet_constituent_eta;
    std::vector<std::vector<float>> *recoDataJet_constituent_phi;



    // Declaration of leaf types -- truth PYTHIA
    vector<int>    *particle_id  = 0;
    vector<double> *particle_pt  = 0;
    vector<double> *particle_eta = 0;
    vector<double> *particle_phi = 0;
    vector<double> *jet_pt       = 0;
    vector<double> *jet_eta      = 0;
    vector<double> *jet_phi      = 0;



    // Declaration of leaf types
    vector<float>   *trk_pt            = nullptr;
    vector<float>   *trk_eta           = nullptr;
    vector<float>   *trk_phi           = nullptr;
    vector<float>   *trk_charge        = nullptr;
    vector<int>     *trk_qual          = nullptr;

    vector<float>   *neutralPFlowObj_pt   = nullptr;
    vector<float>   *neutralPFlowObj_eta  = nullptr;
    vector<float>   *neutralPFlowObj_phi  = nullptr;
    vector<float>   *neutralPFlowObj_t    = nullptr;
    vector<float>   *neutralPFlowObj_eEM  = nullptr;
    vector<float>   *neutralPFlowObj_HEC0 = nullptr;
    vector<float>   *neutralPFlowObj_HEC1 = nullptr;
    vector<float>   *neutralPFlowObj_HEC2 = nullptr;
    vector<float>   *neutralPFlowObj_HEC3 = nullptr;
    vector<float>   *neutralPFlowObj_EME1 = nullptr;
    vector<float>   *neutralPFlowObj_EME2 = nullptr;
    vector<float>   *neutralPFlowObj_EME3 = nullptr;
    vector<float>   *chargedPFlowObj_pt   = nullptr;
    vector<float>   *chargedPFlowObj_eta  = nullptr;
    vector<float>   *chargedPFlowObj_phi  = nullptr;

    vector<float>   *vtx_z                = nullptr;
    vector<float>   *MET_sumet            = nullptr;

    vector<int  >   *muon_trk_index    = nullptr;
    vector<int  >   *elec_trk_index    = nullptr;

    vector<float>   *AntiKt4EMPFlowJets_pt      = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_eta     = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_phi     = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_t       = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_pt_orig = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_newjvt  = nullptr;
    vector<vector<int>>   *AntiKt4EMPFlowJets_jetTrk_index  = nullptr;

    UInt_t          RunNumber = 0;
    ULong64_t       eventNumber = 0;
    UInt_t          lbn       = 0;
    UInt_t          bcid      = 0;

    Float_t         ActIntPerXing = 0;
    Float_t         Trkz0RMS = 0;

    // Minbias trigger
    Bool_t          b_HLT_mb_sptrk              = false;
    Bool_t          b_HLT_noalg_mb_L1MBTS_1     = false;
    Bool_t          b_HLT_noalg_mb_L1MBTS_1_1   = false;

    Bool_t          b_HLT_noalg_mb_L1MBTS_2     = false;
    Bool_t          b_HLT_noalg_mb_L1RD0_FILLED = false;
    Bool_t          b_HLT_j10_L1MBTS_2          = false;
    Bool_t          b_HLT_j10_L1RD0_FILLED      = false;
    Bool_t          b_HLT_j20_L1J12             = false;
    Bool_t          b_HLT_j20_L1MBTS_2          = false;
    Bool_t          b_HLT_j20_L1RD0_FILLED      = false;
    Bool_t          b_HLT_j45_L1J12             = false;
    Bool_t          b_HLT_noalg_mb_L1TE5        = false;
    Bool_t          b_HLT_noalg_mb_L1TE10       = false;
    Bool_t          b_HLT_noalg_mb_L1TE20       = false;
    Bool_t          b_HLT_noalg_mb_L1TE30       = false;

    // HMT trigger
    Bool_t          b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1200_trk100_hmt_L1TE5                     = false;
    Bool_t          b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE15                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1200_trk90_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1400_trk100_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE15                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE40                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE5                     = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5           = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10          = false;
    Bool_t          b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2500_trk140_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2700_trk150_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2900_trk160_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp600_trk40_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp600_trk40_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20             = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE20                      = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20             = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp900_trk50_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE20                      = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1                  = false;
};

void EmbedTruthEvent::Init() {
    std::cout << "-----> Initiating PYTHIA events!" << std::endl;
    truthChain = new TChain("PyTree");
    truthChain->Add("/usatlas/u/pyin/workarea/PYTHIA8/02Analysis/01RootFiles/pytree_Tune14Monash_HardQCDon_MPIoff_ISRon_above15_50M_CalibScaleDown/*.root");
    truthChain->SetBranchAddress("particle_id" , &particle_id );
    truthChain->SetBranchAddress("particle_pt" , &particle_pt );
    truthChain->SetBranchAddress("particle_eta", &particle_eta);
    truthChain->SetBranchAddress("particle_phi", &particle_phi);
    truthChain->SetBranchAddress("jet_pt"      , &jet_pt      );
    truthChain->SetBranchAddress("jet_eta"     , &jet_eta     );
    truthChain->SetBranchAddress("jet_phi"     , &jet_phi     );
    truthChain->SetBranchStatus("*"              , 0);
    truthChain->SetBranchStatus("particle*", 1);
    truthChain->SetBranchStatus("jet*", 1);


    std::cout << "-----> Initiating Data events!" << std::endl;
    fChain = new TChain("HeavyIonD3PD");
    fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2015_13TeV_set1.20220916_MYSTREAM/*.root");
    fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2016_13TeV_set1.20220916_MYSTREAM/*.root");
    fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set1.20220916_MYSTREAM/*.root");
    fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set2.20220916_MYSTREAM/*.root");
    fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set3.20220916_MYSTREAM/*.root");
    fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2018_13TeV_set1.20220916_MYSTREAM/*.root");


    std::cout << "-----> Initiating reco data jet trees!" << std::endl;
    dataJetChain = new TChain("recoDataJets");
    dataJetChain->Add("/usatlas/u/pyin/workarea/CorrAfterHP/02AnaCorr/01Rootfiles/recoDataJetTrees/*.root");
    dataJetChain->SetBranchAddress("RunNumber"      , &RunNumber2  );
    dataJetChain->SetBranchAddress("eventNumber"    , &eventNumber2);
    dataJetChain->SetBranchAddress("recoDataJet_pt" , &recoDataJet_pt );
    dataJetChain->SetBranchAddress("recoDataJet_eta", &recoDataJet_eta);
    dataJetChain->SetBranchAddress("recoDataJet_phi", &recoDataJet_phi);
    dataJetChain->SetBranchAddress("recoDataJet_constituent_pT" , &recoDataJet_constituent_pT );
    dataJetChain->SetBranchAddress("recoDataJet_constituent_eta", &recoDataJet_constituent_eta);
    dataJetChain->SetBranchAddress("recoDataJet_constituent_phi", &recoDataJet_constituent_phi);
    dataJetChain->SetBranchStatus("*"            , 0);
    dataJetChain->SetBranchStatus("RunNumber"    , 1);
    dataJetChain->SetBranchStatus("eventNumber"  , 1);
    dataJetChain->SetBranchStatus("recoDataJet_*", 1);


    // ------------------------------------------------------------
    // Set branch addresses and branch pointers
    //
    fChain->SetMakeClass(1);
    fChain->SetBranchAddress("RunNumber"  , &RunNumber  );
    fChain->SetBranchAddress("eventNumber", &eventNumber);
    fChain->SetBranchAddress("lbn"       , &lbn       );
    fChain->SetBranchAddress("trk_pt"    , &trk_pt    );
    fChain->SetBranchAddress("trk_eta"   , &trk_eta   );
    fChain->SetBranchAddress("trk_phi"   , &trk_phi   );
    fChain->SetBranchAddress("trk_charge", &trk_charge);
    fChain->SetBranchAddress("trk_qual"  , &trk_qual  );
    fChain->SetBranchAddress("vtx_z"     , &vtx_z     );
    fChain->SetBranchAddress("MET_sumet" , &MET_sumet );

    fChain->SetBranchAddress("neutralPFlowObj_pt"  , &neutralPFlowObj_pt  );
    fChain->SetBranchAddress("neutralPFlowObj_eta" , &neutralPFlowObj_eta );
    fChain->SetBranchAddress("neutralPFlowObj_phi" , &neutralPFlowObj_phi );
    fChain->SetBranchAddress("neutralPFlowObj_eEM" , &neutralPFlowObj_eEM );
    fChain->SetBranchAddress("neutralPFlowObj_t"   , &neutralPFlowObj_t   );
    fChain->SetBranchAddress("neutralPFlowObj_HEC0", &neutralPFlowObj_HEC0);
    fChain->SetBranchAddress("neutralPFlowObj_HEC1", &neutralPFlowObj_HEC1);
    fChain->SetBranchAddress("neutralPFlowObj_HEC2", &neutralPFlowObj_HEC2);
    fChain->SetBranchAddress("neutralPFlowObj_HEC3", &neutralPFlowObj_HEC3);
    fChain->SetBranchAddress("chargedPFlowObj_pt"  , &chargedPFlowObj_pt  );
    fChain->SetBranchAddress("chargedPFlowObj_eta" , &chargedPFlowObj_eta );
    fChain->SetBranchAddress("chargedPFlowObj_phi" , &chargedPFlowObj_phi );

    fChain->SetBranchAddress("AntiKt4EMPFlowJets_pt"     , &AntiKt4EMPFlowJets_pt     );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_eta"    , &AntiKt4EMPFlowJets_eta    );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_phi"    , &AntiKt4EMPFlowJets_phi    );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_t"      , &AntiKt4EMPFlowJets_t      );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_pt_orig", &AntiKt4EMPFlowJets_pt_orig);
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_newjvt" , &AntiKt4EMPFlowJets_newjvt );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_jetTrk_index" , &AntiKt4EMPFlowJets_jetTrk_index );

    fChain->SetBranchAddress("ActIntPerXing"     , &ActIntPerXing );
    fChain->SetBranchAddress("Trkz0RMS"          , &Trkz0RMS      );
    fChain->SetBranchAddress("muon_trk_index"    , &muon_trk_index);
    fChain->SetBranchAddress("electron_trk_index", &elec_trk_index);

    fChain->SetBranchAddress("b_HLT_mb_sptrk"           , &b_HLT_mb_sptrk           );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1"  , &b_HLT_noalg_mb_L1MBTS_1  );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1_1", &b_HLT_noalg_mb_L1MBTS_1_1);

    fChain->SetBranchAddress("b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10", &b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5", &b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1", &b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1TE10", &b_HLT_mb_sp1000_trk70_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1TE5", &b_HLT_mb_sp1000_trk70_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE10", &b_HLT_mb_sp1100_trk70_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE20", &b_HLT_mb_sp1100_trk70_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE30", &b_HLT_mb_sp1100_trk70_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE5", &b_HLT_mb_sp1100_trk70_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk100_hmt_L1TE5", &b_HLT_mb_sp1200_trk100_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1", &b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE10", &b_HLT_mb_sp1200_trk80_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE15", &b_HLT_mb_sp1200_trk80_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE20", &b_HLT_mb_sp1200_trk80_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE30", &b_HLT_mb_sp1200_trk80_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE5", &b_HLT_mb_sp1200_trk80_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk90_hmt_L1TE5", &b_HLT_mb_sp1200_trk90_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk100_hmt_L1TE20", &b_HLT_mb_sp1400_trk100_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1", &b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE10", &b_HLT_mb_sp1400_trk90_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE15", &b_HLT_mb_sp1400_trk90_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE20", &b_HLT_mb_sp1400_trk90_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE30", &b_HLT_mb_sp1400_trk90_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE40", &b_HLT_mb_sp1400_trk90_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE5", &b_HLT_mb_sp1400_trk90_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE10", &b_HLT_mb_sp1600_trk100_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE15", &b_HLT_mb_sp1600_trk100_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE20", &b_HLT_mb_sp1600_trk100_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE25", &b_HLT_mb_sp1600_trk100_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE30", &b_HLT_mb_sp1600_trk100_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE40", &b_HLT_mb_sp1600_trk100_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE5", &b_HLT_mb_sp1600_trk100_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE50", &b_HLT_mb_sp1600_trk100_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE10", &b_HLT_mb_sp1700_trk110_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE20", &b_HLT_mb_sp1700_trk110_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE30", &b_HLT_mb_sp1700_trk110_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE40", &b_HLT_mb_sp1700_trk110_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE50", &b_HLT_mb_sp1700_trk110_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE60", &b_HLT_mb_sp1700_trk110_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE10", &b_HLT_mb_sp1800_trk110_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE15", &b_HLT_mb_sp1800_trk110_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE20", &b_HLT_mb_sp1800_trk110_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE25", &b_HLT_mb_sp1800_trk110_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE30", &b_HLT_mb_sp1800_trk110_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE10", &b_HLT_mb_sp1900_trk120_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE20", &b_HLT_mb_sp1900_trk120_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE30", &b_HLT_mb_sp1900_trk120_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE40", &b_HLT_mb_sp1900_trk120_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE50", &b_HLT_mb_sp1900_trk120_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE60", &b_HLT_mb_sp1900_trk120_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE70", &b_HLT_mb_sp1900_trk120_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE10", &b_HLT_mb_sp2100_trk120_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE15", &b_HLT_mb_sp2100_trk120_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE20", &b_HLT_mb_sp2100_trk120_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE25", &b_HLT_mb_sp2100_trk120_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE30", &b_HLT_mb_sp2100_trk120_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE20", &b_HLT_mb_sp2100_trk130_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE30", &b_HLT_mb_sp2100_trk130_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE40", &b_HLT_mb_sp2100_trk130_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE50", &b_HLT_mb_sp2100_trk130_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE60", &b_HLT_mb_sp2100_trk130_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE70", &b_HLT_mb_sp2100_trk130_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE20", &b_HLT_mb_sp2200_trk140_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE30", &b_HLT_mb_sp2200_trk140_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE40", &b_HLT_mb_sp2200_trk140_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE50", &b_HLT_mb_sp2200_trk140_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE60", &b_HLT_mb_sp2200_trk140_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE70", &b_HLT_mb_sp2200_trk140_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE15", &b_HLT_mb_sp2300_trk130_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE20", &b_HLT_mb_sp2300_trk130_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE25", &b_HLT_mb_sp2300_trk130_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE30", &b_HLT_mb_sp2300_trk130_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE40", &b_HLT_mb_sp2300_trk130_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE20", &b_HLT_mb_sp2400_trk150_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE30", &b_HLT_mb_sp2400_trk150_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE40", &b_HLT_mb_sp2400_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE50", &b_HLT_mb_sp2400_trk150_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE60", &b_HLT_mb_sp2400_trk150_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE70", &b_HLT_mb_sp2400_trk150_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10", &b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40", &b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2500_trk140_hmt_L1TE40", &b_HLT_mb_sp2500_trk140_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40", &b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2700_trk150_hmt_L1TE40", &b_HLT_mb_sp2700_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40", &b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2900_trk160_hmt_L1TE40", &b_HLT_mb_sp2900_trk160_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1", &b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10", &b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5", &b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp600_trk40_hmt_L1TE10", &b_HLT_mb_sp600_trk40_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp600_trk40_hmt_L1TE5", &b_HLT_mb_sp600_trk40_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1", &b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1", &b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE10", &b_HLT_mb_sp700_trk50_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE20", &b_HLT_mb_sp700_trk50_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE5", &b_HLT_mb_sp700_trk50_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1", &b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5", &b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk50_hmt_L1TE5", &b_HLT_mb_sp900_trk50_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1", &b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE10", &b_HLT_mb_sp900_trk60_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE20", &b_HLT_mb_sp900_trk60_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE5", &b_HLT_mb_sp900_trk60_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1", &b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1);

    //--------------------------------------------------------------------
    fChain->SetBranchStatus("*"                   , 0);
    fChain->SetBranchStatus("RunNumber"           , 1);
    fChain->SetBranchStatus("eventNumber"         , 1);
    fChain->SetBranchStatus("lbn"                 , 1);
    fChain->SetBranchStatus("trk_*"               , 1);
    fChain->SetBranchStatus("vtx_z"               , 1);
    fChain->SetBranchStatus("b_*"                 , 1);
    fChain->SetBranchStatus("neutralPFlowObj_*"   , 1);
    fChain->SetBranchStatus("chargedPFlowObj_*"   , 1);
    fChain->SetBranchStatus("ActIntPerXing"       , 1);
    fChain->SetBranchStatus("Trkz0RMS"            , 1);
    fChain->SetBranchStatus("MET_sumet"           , 1);
    fChain->SetBranchStatus("muon_trk_index"      , 1);
    fChain->SetBranchStatus("electron_trk_index"  , 1);
    fChain->SetBranchStatus("AntiKt4EMPFlowJets_*", 1);
}
#endif
