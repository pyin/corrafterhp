// CheckHotSpot.h
#ifndef CHECKHOTSPOT_H // include guard
#define CHECKHOTSPOT_H

bool isHotSpot(float eta, float phi, float HEC0Frac, float HEC1Frac, float HEC2Frac, float HEC3Frac);

#endif /* CHECKHOTSPOT_H */
