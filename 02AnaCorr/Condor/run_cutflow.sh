#!/bin/bash

ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh ""
asetup 21.2.170,AnalysisBase
# asetup 21.2,latest,AnalysisBase
# localSetupROOT 6.20.06-x86_64-centos7-gcc8-opt


echo "Starting the run"
root -b -l <<EOF
  gSystem->Load("libfastjet.so");
  .L CheckHotSpot.C+
  .L CutFlow.C+

  CutFlow* agp = new CutFlow();
  agp->m_total             =$1;
  agp->m_from              =$2;
  agp->m_to                =$3;

  agp->m_data_type         =$4;
  agp->m_do_bootstrapping  =$5;
  agp->m_mu_limit          =$6;
  agp->m_apply_efficiency  =$7;
  agp->m_trig_type         =$8;
  agp->m_pileup_reject     =$9;
  agp->m_trk_quality       =${10};
  agp->m_muon_elec         =${11};
  agp->m_jet_reject_type   =${12};
  agp->m_jet_ptcorr        =${13};
  agp->m_jet_reject_cuts   =${14};
  agp->m_if_have_jet       =${15};
  agp->m_if_have_uecorr    =${16};
  agp->m_correlation_type  =${17};
  agp->m_multiplicity_type =${18};

  agp->run();
  .q;
EOF
# tot=`echo $1 | sed -e 's/-//g'`
# file=03Done/done_tot${tot}_from${2}_to${3}_datatype${4}_eff${5}_trig${6}_pileupreject${7}_jetrejecttype${8}_jetrejectcut${9}_corrtype${10}_multtype${11}.txt
# touch $file
echo "DONE RUNNING THE SCRIPT"
