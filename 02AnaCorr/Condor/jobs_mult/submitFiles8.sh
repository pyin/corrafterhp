SubmitFileName="submit_pp_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_${9}_${10}_${11}_${12}_${13}_${14}_${15}"
echo $SubmitFileName
rm $SubmitFileName
echo "Universe       = vanilla"                                                                                                             >> $SubmitFileName
echo "Notification   = Never"                                                                                                               >> $SubmitFileName
echo "Executable     = /usatlas/u/pyin/workarea/CorrAfterHP/02AnaCorr/Condor/run.sh"                                                        >> $SubmitFileName
echo "Rank           = CPU_Speed"                                                                                                           >> $SubmitFileName
echo "request_memory = 6000M"                                                                                                               >> $SubmitFileName
echo "Priority       = +20"                                                                                                                 >> $SubmitFileName
echo "GetEnv         = True"                                                                                                                >> $SubmitFileName
# echo "x509userproxy  = \$ENV(X509_USER_PROXY)"                                                                                              >> $SubmitFileName
echo "Initialdir     = /usatlas/u/pyin/workarea/CorrAfterHP/02AnaCorr/Condor"                                                               >> $SubmitFileName
echo "Output         = /usatlas/scratch/pyin/log/pp_"${1}${2}${3}${4}${5}${6}${7}${8}${9}${10}${11}${12}${13}${14}${15}".\$(Process).out"   >> $SubmitFileName
echo "Error          = /usatlas/scratch/pyin/log/pp_"${1}${2}${3}${4}${5}${6}${7}${8}${9}${10}${11}${12}${13}${14}${15}".\$(Process).err"   >> $SubmitFileName
echo "Log            = /usatlas/scratch/pyin/log/pp_"${1}${2}${3}${4}${5}${6}${7}${8}${9}${10}${11}${12}${13}${14}${15}".\$(Process).log"   >> $SubmitFileName
echo "Notify_user    = py2246@columbia.edu"                                                                                                 >> $SubmitFileName
echo "MIN = \$\$([ \$(Process) * 10000000 ])"                                                                                               >> $SubmitFileName
echo "MAX = \$\$([ (\$(Process) + 1) * 10000000 ])"                                                                                         >> $SubmitFileName
echo 'Arguments      = "0 $(MIN) $(MAX)' ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9} ${10} ${11} ${12} ${13} ${14} ${15}'"'                >> $SubmitFileName
echo "Queue 50"                                                                                                                            >> $SubmitFileName

