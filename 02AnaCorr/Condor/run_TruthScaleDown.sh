#!/bin/zsh

ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh ""
asetup 21.2.170,AnalysisBase


echo "Starting the run"
root -b -l <<EOF
  gSystem->Load("libfastjet.so");

  .L TruthJetCalibScaleDown.C+

  TruthJetCalibScaleDown* agp = new TruthJetCalibScaleDown();
  agp->m_idx =$1;

  agp->run();

  .q;
EOF
echo "DONE RUNNING THE SCRIPT"
