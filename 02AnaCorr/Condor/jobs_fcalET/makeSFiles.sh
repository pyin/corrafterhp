
# ---------------------------------------------------
#                                          Inclusive
#
# sh submitFiles.sh 1 1 2 5 3 6 2 1 0 1   # Default

# sh submitFiles.sh 1 2 2 5 3 6 2 1 0 1   # Efficiency SysError
# sh submitFiles.sh 1 1 2 5 3 6 2 1 0 1   # Pileup SysError
# sh submitFiles.sh 1 1 2 0 3 6 2 1 0 1   # Pileup SysError
# sh submitFiles.sh 1 1 2 8 3 6 2 0 0 1   # UE correction
# sh submitFiles.sh 1 1 2 8 3 7 2 1 0 1   # using 9 GeV
# sh submitFiles.sh 1 1 2 8 3 8 2 1 0 1   # using 12 GeV
# sh submitFiles.sh 1 1 2 8 3 9 2 1 0 1   # using 12 GeV


# ---------------------------------------------------
#                                          AllEvents
#
# sh submitFiles.sh 1 1 2 5 3 6 2 1 2 1   # Default

# sh submitFiles.sh 1 0 2 8 3 6 2 1 2 1   # Efficiency SysError
# sh submitFiles.sh 1 1 2 5 3 6 2 1 2 1   # Pileup SysError
# sh submitFiles.sh 1 1 2 0 3 6 2 1 2 1   # Pileup SysError
# sh submitFiles.sh 1 1 2 8 3 6 2 0 2 1   # UE correction
# sh submitFiles.sh 1 1 2 8 3 7 2 1 2 1   # using 9 GeV
# sh submitFiles.sh 1 1 2 8 3 8 2 1 2 1   # using 12 GeV
# sh submitFiles.sh 1 1 2 8 3 9 2 1 2 1   # using 12 GeV


# ---------------------------------------------------
#                                              NoJet
#
# sh submitFiles.sh 1 1 2 5 3 6 0 1 2 1   # Default

# sh submitFiles.sh 1 0 2 8 3 6 0 1 2 1   # Efficiency SysError
# sh submitFiles.sh 1 1 2 5 3 6 0 1 2 1   # Pileup SysError
# sh submitFiles.sh 1 1 2 0 3 6 0 1 2 1   # Pileup SysError
# sh submitFiles.sh 1 1 2 8 3 6 0 0 2 1   # UE correction
# sh submitFiles.sh 1 1 2 8 3 7 0 1 2 1   # using 9 GeV
# sh submitFiles.sh 1 1 2 8 3 8 0 1 2 1   # using 12 GeV
# sh submitFiles.sh 1 1 2 8 3 9 0 1 2 1   # using 12 GeV


# ---------------------------------------------------
#                                            WithJet
#
# sh submitFiles.sh 1 1 2 5 3 6 1 1 2 1   # Default




# sh submitFiles.sh 1 1 2 5 3 6 2 1 1 1   # Default
# sh submitFiles.sh 1 1 2 5 3 6 1 1 1 1   # Default










# ---------------------------------------------------
#                                          Inclusive
#
sh submitFiles.sh 4 1 2 5 3 6 2 1 0 1   # Default

# sh submitFiles.sh 4 0 2 8 3 6 2 1 0 1   # Efficiency SysError
# sh submitFiles.sh 4 1 2 5 3 6 2 1 0 1   # Pileup SysError
# sh submitFiles.sh 4 1 2 0 3 6 2 1 0 1   # Pileup SysError
# sh submitFiles.sh 4 1 2 8 3 6 2 0 0 1   # UE correction
# sh submitFiles.sh 4 1 2 8 3 7 2 1 0 1   # using 9 GeV
# sh submitFiles.sh 4 1 2 8 3 8 2 1 0 1   # using 12 GeV
# sh submitFiles.sh 4 1 2 8 3 9 2 1 0 1   # using 12 GeV


# ---------------------------------------------------
#                                          AllEvents
#
sh submitFiles.sh 4 1 2 5 3 6 2 1 2 1   # Default
# sh submitFiles.sh 4 1 1 5 3 6 2 1 2 1   # minbias

# sh submitFiles.sh 4 0 2 8 3 6 2 1 2 1   # Efficiency SysError
# sh submitFiles.sh 4 1 2 5 3 6 2 1 2 1   # Pileup SysError
# sh submitFiles.sh 4 1 2 0 3 6 2 1 2 1   # Pileup SysError
# sh submitFiles.sh 4 1 2 8 3 6 2 0 2 1   # UE correction
# sh submitFiles.sh 4 1 2 8 3 7 2 1 2 1   # using 9 GeV
# sh submitFiles.sh 4 1 2 8 3 8 2 1 2 1   # using 12 GeV
# sh submitFiles.sh 4 1 2 8 3 9 2 1 2 1   # using 12 GeV


# ---------------------------------------------------
#                                              NoJet
#
# sh submitFiles.sh 4 1 2 5 3 6 0 1 2 1   # Default
# sh submitFiles.sh 4 1 1 5 3 6 0 1 2 1   # minbias

# sh submitFiles.sh 4 0 2 8 3 6 0 1 2 1   # Efficiency SysError
# sh submitFiles.sh 4 1 2 5 3 6 0 1 2 1   # Pileup SysError
# sh submitFiles.sh 4 1 2 0 3 6 0 1 2 1   # Pileup SysError
# sh submitFiles.sh 4 1 2 8 3 6 0 0 2 1   # UE correction
# sh submitFiles.sh 4 1 2 8 3 7 0 1 2 1   # using 9 GeV
# sh submitFiles.sh 4 1 2 8 3 8 0 1 2 1   # using 12 GeV
# sh submitFiles.sh 4 1 2 8 3 9 0 1 2 1   # using 12 GeV


# ---------------------------------------------------
#                                            WithJet
#
sh submitFiles.sh 4 1 2 5 3 6 1 1 2 1   # Default
# sh submitFiles.sh 4 1 1 5 3 6 1 1 2 1   # minbias
