rm submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "Universe       = vanilla"                                                                                         >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "Notification   = Never"                                                                                           >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "Executable     = /usatlas/u/pyin/workarea/CorrAfterHP/02AnaCorr/Condor/run_globalEtaPhi.sh"                       >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "Rank           = CPU_Speed"                                                                                       >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "request_memory = 5000M"                                                                                           >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "Priority       = +20"                                                                                             >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "GetEnv         = True"                                                                                            >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
# echo "x509userproxy  = \$ENV(X509_USER_PROXY)"                                                                          >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "Initialdir     = /usatlas/u/pyin/workarea/CorrAfterHP/02AnaCorr/Condor"                                           >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "Output         = /usatlas/scratch/pyin/log/ppGlobal_"${1}${2}${3}${4}${5}${6}${7}${8}xxxxx${9}".\$(Process).out"  >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "Error          = /usatlas/scratch/pyin/log/ppGlobal_"${1}${2}${3}${4}${5}${6}${7}${8}xxxxx${9}".\$(Process).err"  >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "Log            = /usatlas/scratch/pyin/log/ppGlobal_"${1}${2}${3}${4}${5}${6}${7}${8}xxxxx${9}".\$(Process).log"  >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "Notify_user    = py2246@columbia.edu"                                                                             >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "MIN = \$\$([ \$(Process) * 2000000 ])"                                                                            >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "MAX = \$\$([ (\$(Process) + 1) * 2000000 ])"                                                                      >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo 'Arguments      = "0 $(MIN) $(MAX)' ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8}' 2 9 2 2 1 1 '${9}'"'                  >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}
echo "Queue 250"                                                                                                         >> submit_ppGlobal_${1}_${2}_${3}_${4}_${5}_${6}_${7}_${8}_x_x_x_x_x_x_${9}

