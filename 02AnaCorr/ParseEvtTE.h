#ifndef _ParseEvtTE_HH_
#define _ParseEvtTE_HH_

#include <vector>
#include "Riostream.h"
#include "TChain.h"
#include <string>
#include "Event.h"

#include "map"
#include "DatasetEnums.h"
#include "bins.h"
#include "TH3.h"
#include <TProfile.h>
#include <TProfile2D.h>


using namespace std;

class TH1;
class TH2;
class TProfile;

class ParseEvtTE {
public:
    ParseEvtTE();

    void run();

    int  m_from           = 0    ;
    int  m_to             = 0    ;
    int  m_total          = 0    ;
    bool m_monitor_mode   = false;

    int m_data_type         = DataSetEnums::DATA_LOWMU                ;
    int m_do_bootstrapping  = DataSetEnums::NO_BS                     ;
    int m_mu_limit          = DataSetEnums::NO_MU_LIMIT               ;
    int m_apply_efficiency  = DataSetEnums::DEFAULT_EFFICIENCY        ;
    int m_trig_type         = DataSetEnums::ALL_TRIGS                 ;
    int m_pileup_reject     = DataSetEnums::NO_PILEUP_REJECTION       ;
    int m_trk_quality       = DataSetEnums::TRKQUAL_PP_MIN_BIAS       ;
    int m_muon_elec         = DataSetEnums::NO_MUONELECTRON_REJECTION ;
    int m_jet_reject_type   = DataSetEnums::NO_JET_REJECT             ;
    int m_jet_ptcorr        = DataSetEnums::WITH_AVGCORRONLY          ;
    int m_jet_reject_cuts   = DataSetEnums::NO_JET_CUTS               ;
    int m_if_have_jet       = DataSetEnums::MINBIAS                   ;
    int m_if_have_uecorr    = DataSetEnums::WITH_UECORR               ;
    int m_correlation_type  = DataSetEnums::HADRON_HADRON_CORRELATIONS;
    int m_multiplicity_type = DataSetEnums::USE_NTRK                  ;


private:
    TChain *fChain = nullptr;
    TChain *oChain = nullptr;


    void Init();
    void InitHistos();

    void readInL1TEs(int i);


    Float_t evt_L1TE = -1;

    // ------------------------------------------
    // output tree variables
    //
    TTree* outTree;
    // ------------------------------------------


    // Declaration of leaf types
    UInt_t          RunNumber   = 0;
    ULong64_t       eventNumber = 0;
    Float_t         L1TE        = 0;


    UInt_t          RunNumberOld   = 0;
    ULong64_t       eventNumberOld = 0;
};

void ParseEvtTE::Init() {
    std::cout << "-----> Initiating Data events!" << std::endl;
    fChain = new TChain("HeavyIonD3PD");
    // fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2015_13TeV_set1_L1TE_PS.20220805_MYSTREAM/*.root");
    // fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2016_13TeV_set1_L1TE_PS.20220805_MYSTREAM/*.root");
    // fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set1_L1TE_PS.20220805_MYSTREAM/*.root");
    // fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set2_L1TE_PS.20220805_MYSTREAM/*.root");
    // fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set3_L1TE_PS.20220805_MYSTREAM/*.root");
    fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2018_13TeV_set1_L1TE_PS.20220805_MYSTREAM/*.root");


    // ------------------------------------------------------------
    // Set branch addresses and branch pointers
    //
    fChain->SetMakeClass(1);
    fChain->SetBranchAddress("RunNumber"  , &RunNumber  );
    fChain->SetBranchAddress("eventNumber", &eventNumber);
    fChain->SetBranchAddress("L1TE"       , &L1TE       );

    //--------------------------------------------------------------------
    fChain->SetBranchStatus("*"                   , 0);
    fChain->SetBranchStatus("RunNumber"           , 1);
    fChain->SetBranchStatus("eventNumber"         , 1);
    fChain->SetBranchStatus("L1TE"                , 1);



    oChain = new TChain("HeavyIonD3PD");
    // oChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2015_13TeV_set1.20220222_MYSTREAM/*.root");
    // oChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2016_13TeV_set1.20220222_MYSTREAM/*.root");
    // oChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set1.20220222_MYSTREAM/*.root");
    // oChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set2.20220222_MYSTREAM/*.root");
    // oChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set3.20220222_MYSTREAM/*.root");
    oChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2018_13TeV_set1.20220222_MYSTREAM/*.root");

    oChain->SetMakeClass(1);
    oChain->SetBranchAddress("RunNumber"  , &RunNumberOld  );
    oChain->SetBranchAddress("eventNumber", &eventNumberOld);

    //--------------------------------------------------------------------
    oChain->SetBranchStatus("*"                   , 0);
    oChain->SetBranchStatus("RunNumber"           , 1);
    oChain->SetBranchStatus("eventNumber"         , 1);
}
#endif
