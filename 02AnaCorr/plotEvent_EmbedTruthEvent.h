#ifndef _EmbedTruthEvent_HH_
#define _EmbedTruthEvent_HH_

#include <vector>
#include "Riostream.h"
#include "TChain.h"
#include <string>
#include "Event.h"

#include "map"
#include "DatasetEnums.h"
#include "bins.h"
#include "TH3.h"
#include <TProfile.h>
#include <TProfile2D.h>
// #include "GoodRunsLists/GoodRunsListSelectionTool.h"


using namespace std;

class TH1;
class TH2;
class TProfile;

struct triple {
    std::string first;
    Bool_t*     second;
    int         third;
};

struct TrkJetType {
    double pT;
    double eta;
    double phi;
    double Nconstit;
    double pT_corrected;
    double pT_correction;
    double Nconstit_corrected;
    double Nconstit_correction;
    double minDistWithCalo;

    bool trk_above1stCut;
    bool trk_above2ndCut;
    bool haveNeighborTrkJet;     // isolation cut. avoid any >15 GeV jet that have another >10 GeV jets around.
    bool haveBalncJet;

    vector<double> jetIngredientPT ;
    vector<double> jetIngredientETA;
    vector<double> jetIngredientPHI;
    vector<int>    jetIngredientIDX;
};

struct CaloJetType {
    float pT;
    float eta;
    float phi;
    float t;
    float t_corr;
    float jvf;
    float jvt;
    float pT_uncalib;
    int   TightBad;

    bool calo_above1stCut;
    bool calo_above2ndCut;
    bool calo_passJVT;
    bool inCaloBrightBand;
};

struct PFlowJetType {
    float pT;
    float eta;
    float phi;
    float pT_corrected;
    float pT_correction1;
    float pT_correction2;
    float pT_correction3;
    float Nconstit_corrected;
    float Nconstit_correction;
    int   TightBad;
    int   pflowJetOrigSize;
    int   Nconstit;

    bool pflow_above1stCut;
    bool pflow_above2ndCut;
    bool pflow_passJVT;
    bool inPFlowBrightBand;
    bool haveNeighborPflowJet;
    bool haveBalncJet;
    bool nearTruthJet;
    bool overlapDataJet;

    vector<int> pflowJetTrkIndex;
    vector<float> pflowJetLayerEnergies;

    vector<double> jetIngredientPT ;
    vector<double> jetIngredientETA;
    vector<double> jetIngredientPHI;
};

struct Particle {
    float pT;
    float eta;
    float phi;
    int   charge;
};

class EmbedTruthEvent {
    enum {
        //pool bins:
        // nc = Bins::NCENT,
        nc = 200,
        nz = 10,
        ZMAX = 100,
        plnbr = nc * nz,
    };

    enum {
        PP_MIN_BIAS            = 2,
        PP_MIN_BIAS_MODIFIED   = 4,
        HI_LOOSE               = 8,
        HI_TIGHT               = 16,
        // HI_TIGHT_TIGHTER_D0_Z0 = 16,
        // HI_LOOSE_7SCT_HITS     = 32,
        HI_TIGHT_LOOSE_D0_Z0   = 32,
        HI_LOOSE_TIGHT_D0_Z0   = 64,
        HI_LOOSE_TIGHTER_D0_Z0 = 128
    };

public:
    EmbedTruthEvent();

    void run();

    int  m_from           = 0    ;
    int  m_to             = 0    ;
    int  m_total          = 0    ;
    bool m_monitor_mode   = false;

    int m_data_type         = DataSetEnums::DATA_LOWMU                ;
    int m_do_bootstrapping  = DataSetEnums::NO_BS                     ;
    int m_mu_limit          = DataSetEnums::NO_MU_LIMIT               ;
    int m_apply_efficiency  = DataSetEnums::DEFAULT_EFFICIENCY        ;
    int m_trig_type         = DataSetEnums::ALL_TRIGS                 ;
    int m_pileup_reject     = DataSetEnums::NO_PILEUP_REJECTION       ;
    int m_trk_quality       = DataSetEnums::TRKQUAL_PP_MIN_BIAS       ;
    int m_muon_elec         = DataSetEnums::NO_MUONELECTRON_REJECTION ;
    int m_jet_reject_type   = DataSetEnums::NO_JET_REJECT             ;
    int m_jet_ptcorr        = DataSetEnums::PFLOWJETCORR_PSI2FULLETA  ;
    int m_jet_reject_cuts   = DataSetEnums::NO_JET_CUTS               ;
    int m_if_have_jet       = DataSetEnums::MINBIAS                   ;
    int m_if_have_uecorr    = DataSetEnums::WITH_UECORR               ;
    int m_correlation_type  = DataSetEnums::HADRON_HADRON_CORRELATIONS;
    int m_multiplicity_type = DataSetEnums::USE_NTRK                  ;


private:
    TChain *fChain = nullptr;
    TChain *truthChain = nullptr;


    void Init();
    void InitTriggers();
    void InitHistos();
    void SaveHistos();
    void InitJetCuts();
    void ReconstructTrkJet();
    void ReconstructPFlowJet();
    void ReconstructDataPFlowJet();
    void ReconstructTruthPFlowJet();
    void InitUEpT();

    void reparsePFlowJet();
    void parseTruthParticles();
    void parseDataParticles();
    void parseEmbedding();
    void Fill(Event* event1, Event* event2, int mixtype);
    void FillJetMonitors();
    void FillEventMonitors(Event* eventBlue1, Event* eventBlue2);

    bool Pileup();
    bool PassTrigger(double ntrk_corr, double ntrk_orig);
    bool inCaloBrightBand(double caloJetEta, double caloJetPhi);
    bool inPFlowBrightBand(double pflowJetEta, double pflowJetPhi);
    bool isSeperateFromEmbedPFlowJets(float partEta);
    bool isSeperateFromDataPFlowJets(float partEta);
    bool isMuonElectron(int pIndex);
    bool RejectJet_Cone(float trk_pt, float trk_eta, float trk_phi, float trk_eff);
    bool FillMixed(Event* event1, Event* event_jet = nullptr, Event* event_orgn = nullptr);
    bool ifPassGRL(UInt_t rNum, UInt_t lbNum);
    bool PassQuality(int quality);

    int get_zPool(float z);
    int GetCentBinNtrk(float ntrk);
    std::pair<int, float> CountLowPTParticle(PFlowJetType pj);

    double GetNtrkCent();
    double GetNtrkCentBeforeQual();
    double MultiplicityCorrection();
    double ConvertPhi(double phi);
    double CalculateDeltaPhi(float phi1, float phi2);
    double EvaluateTrackingEff(float pt, float eta);
    double findUECorr(TH2D* h, double jet_eta, double jet_phi, int type, int repass);
    double multiplicityInterpolate(int etabin, int phibin, int type);
    double interpolate(double low, double up, double val_low, double val_up, double x);
    double findMinDistWithCaloJet(double trkJet_eta, double trkJet_phi);


    float modulationCorrectionFactor(double jet_phi, int repass);

    // GoodRunsListSelectionTool m_grlTool;

    // ------------------------------------
    // Jet cut variables
    //
    float dR = -1;
    float min_jet_pt     = 15.;
    float min_jet_pt_2nd = 10.;
    int   min_jet_Nconstituents = 0; //Minimum Number of constituents
    float caloJetPt_1st  = 27.5;
    float caloJetPt_2nd  = 20.5;
    float pfJetPt_1st    = 25.;
    float pfJetPt_2nd    = 15.;
    float pfJetPt_blnc   = 15.;
    // ------------------------------------

    int nmix;
    int m_cent_i = 0;
    int m_TEbin;
    int m_centIndex_UECorr;
    int depth[nc];
    int FCalET_cent;
    float nCurrentBlue = 0;
    float totalEnergy;
    double ntrk_cent;
    double ntrk_cent_orig;

    double m_zvtx;
    double ntrk_correction = 0;

    bool drawCurrent = false;
    int jettype = 9;
    int evtNumber = 0;

    vector<PFlowJetType>  recoPFlowJetContainer;
    vector<PFlowJetType>  recoDataPFlowJetContainer;
    vector<PFlowJetType>  recoTruthPFlowJetContainer;
    vector<Particle>      truthParticleContainer;
    vector<Particle>      fullParticleContainer;
    vector<Particle>      dataParticleContainer;
    vector<Particle>      chargeParticleContainer;

    Float_t Calo_Et[2][3]   ;
    Float_t Calo_Qx[2][3][3];
    Float_t Calo_Qy[2][3][3];

    std::vector<triple> m_trig_map_minbias;
    std::vector<triple> m_trig_map_HMT;
    std::vector<triple> m_trig_map_HMT_L1MBTS;
    std::vector<triple> m_trig_map_HMT_L1TE10;
    std::vector<triple> m_trig_map_HMT_L1TE5;
    std::vector<triple> m_trig_map_HMT_L1TE15;
    std::vector<triple> m_trig_map_HMT_L1TE20;
    std::vector<triple> m_trig_map_HMT_L1TE30PLUS;
    std::vector<triple> m_trig_map_HMT_higherTRK;
    std::vector<triple> m_trig_map_HMT_no0ETA24;
    std::vector<triple> m_trig_map_All;
    std::vector<triple> m_trig_map_minbias1;
    std::vector<triple> m_trig_map_minbias2;
    std::vector<triple> m_trig_map_minbias3;
    std::vector<triple> m_trig_map_HMT_PUSUP;
    std::vector<triple> m_trig_map_HMT_TE5TE10;
    std::vector<triple> m_trig_map_HMT_L1TE5_no0ETA24;
    std::vector<triple> m_trig_map_HMT_L1TE10_no0ETA24;
    std::vector<triple> m_trig_map_HMT_L1TE15_no0ETA24;
    std::vector<triple> m_trig_map_HMT_L1TE20_no0ETA24;
    std::vector<triple> m_trig_map_HMT_L1TE30PLUS_no0ETA24;

    vector<EVENT_PTR> pool[plnbr];

    TH1* h_trig;
    TH1* hcent;
    TH1* h_mu;
    TH1* h_mu_afterTrig;
    TH1* h_PoissonRndm;
    TH1* hcent_orig;
    TH1* hzvtx[Bins::NCENT + 1];
    TH1* h_nvtx;
    TH1* h1_multiplicity[10];
    TH1* hNtrk_origional;
    TH1* hNtrk_corrected;
    TH1* hNtrk_origional_beforeTrigger;
    TH1* hNtrk_corrected_beforeTrigger;
    TH1* h1_NEvt1stTrkJet;
    TH1* h1_NEvt1stPFlowJet;
    TH1* h_MultCorrection;
    TH1* h1_NTrkJet1st[5];
    TH1* h1_NPFlowJet1st[5];
    TH1* h_size_vexz[Bins::NCENT];
    TH1* h1_offlineTE[Bins::NCENT];
    TH1* h1_trkDistance[Bins::NCENT];
    TH1* h_dz       [Bins::NCENT];
    TH1* h_JetEta2nd[Bins::NCENT];
    TH1* dist_pT_original    [Bins::NCENT];
    TH1* dist_pT_corred      [Bins::NCENT];
    TH1* dist_pT_afterRej_jet[Bins::NCENT];
    TH1* dist_pT_cc_poolJet  [Bins::NCENT];
    TH1* dist_pT_afterRej_ref[Bins::NCENT];
    TH1* h_jet_pTFrac   [Bins::NCENT];      // pT of all particle in jet cone / pT of jet
    TH1* N_trigger_jetcut [Bins::NCENT];    //pT spectra of tracks within the jet cone.
    TH1* h_bg_constituents[Bins::NCENT];    // value of constituents correction
    TH1* h_bg_ptWeight    [Bins::NCENT];    // value of pT correction
    TH1* h1_jetNch_orig[Bins::NCENT][14];
    TH1* h1_jetNch_UECR[Bins::NCENT][14];
    TH1* h1_CaloJets_PT_orig[Bins::NCENT];
    TH1* h1_CaloJets_PT     [Bins::NCENT];
    TH1* h1_CaloJetsPT_inBright [Bins::NCENT];
    TH1* h1_CaloJetsPT_outBright[Bins::NCENT];
    TH1* h1_sizeOfWhite[Bins::NCENT];
    TH1* h1_trkJetdphi [Bins::NCENT];
    TH1* h1_caloJetdphi[Bins::NCENT];
    TH1* h1_pflowJetdphi[Bins::NCENT];
    // -----------------------------------
    // global monitor plots
    //    record how many event processed
    TH1* hFillEntries;
    TH1* hFillMixEntries;
    // -----------------------------------


    TH2* h_efficiency = nullptr;
    TH2* h2_Mult_CorrMult;
    TH2* h2_Mult_FCal;
    TH2* fg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* bg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* cg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* cc_unmatched[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* fg_after_correction[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* h2_Trk_EtaPhi_orig[Bins::NCENT];
    TH2* h2_Trk_EtaPhi     [Bins::NCENT];
    TH2* h2_Muon_EtaPhi    [Bins::NCENT];
    TH2* h2_Electron_EtaPhi[Bins::NCENT];
    TH2* h2_CorrJetpT_JetpT[Bins::NCENT];   // corrected jet pT  vs original jet pT
    TH2* h2_JetpTCorr_JetpT[Bins::NCENT];   // jet pT correction vs original jet pT
    TH2* h2_CorrJetNch_JetNch[Bins::NCENT]; // corrected jet Nch  vs original jet Nch
    TH2* h2_JetNchCorr_JetNch[Bins::NCENT]; // jet Nch correction vs original jet Nch
    TH2* h2_jetNch_UENchCorr [Bins::NCENT];
    TH2* h2_jetPT_jetNch_orig[Bins::NCENT];
    TH2* h2_jetPT_jetNch_UECR[Bins::NCENT];
    TH2* h2_NTrkJet2nd_NTrkJet1st[Bins::NCENT];         // N_2nd energy jet (default 10 GeV) vs N_15GeVJet
    TH2* h2_dR_CaloJettoTrkJet[Bins::NCENT];
    TH2* h2_CaloJetPT_TrkJetPT    [Bins::NCENT];
    TH2* h2_CaloJetPT_TrkJetPTCorr[Bins::NCENT];
    TH2* h2_dR_PflowJettoCaloJet[4][Bins::NCENT];
    TH2* h2_Mismatch_RunNumEta[Bins::NCENT];
    TH2* h2_Mismatch_EtaPhi[Bins::NCENT];
    TH2* h2_fgBlue_bgBlue[Bins::NCENT];
    TH2* h2_N2ndTrkJet_N2ndCaloJet[Bins::NCENT];
    TH2* h2_PFlowJetPT_TrkJetPT    [Bins::NCENT];
    TH2* h2_PFlowJetPT_TrkJetPTCorr[Bins::NCENT];
    TH2* h2_dR_PFlowJettoTrkJet[Bins::NCENT];
    TH2* h2_dR_CaloJettoPflowJet[4][Bins::NCENT];
    TH2* h2_CaloJetPT_PflowJetPT[Bins::NCENT];
    TH2* h2_Ratio_CaloJetPT_PflowJetPT[Bins::NCENT];
    TH2* h2_PFlow_EmbedPT_TruthPT[Bins::NCENT];
    TH2* h2_PFlowJets_EventAngleFCalET[Bins::NCENT];
    TH2* h2_PFlowJets_EventAngleQ2    [Bins::NCENT];
    TH2* h2_TruthPFlowJets_EventAngleFCalET[Bins::NCENT];
    TH2* h2_PFlowJets_Blue1AngleFCalET[Bins::NCENT];
    TH2* h2_PFlowJets_Blue2AngleFCalET[Bins::NCENT];
    TH2* h2_PFlowJets_Blue1AngleBlue2Angle[Bins::NCENT];
    TH2* h2_PFlowJetCorr_v2  [Bins::NCENT];
    TH2* h2_PFlowJetCorr_psi2[Bins::NCENT];
    TH2* h2_PFlowJetCorr_fct[Bins::NCENT];
    TH2* h2_etadept_v2  [Bins::NCENT];
    TH2* h2_etadept_psi2[Bins::NCENT];
    TH2* h2_etadept_fct [Bins::NCENT];
    TH2* h2_FullPsi2_FCalPsi2    [Bins::NCENT];
    TH2* h2_FullPsi2V2_FCalPsi2V2[Bins::NCENT];
    TH2* h2_pTcorr_deltaPT[Bins::NCENT];
    TH2* h2_jetpT_deltaPT[Bins::NCENT];
    TH2* h2_pTcorr_deltaPT_beforeOS[Bins::NCENT];
    TH2* h2_jetpT_deltaPT_beforeOS [Bins::NCENT];
    TH2* h2_pTcorr_deltaPT_beforeMC[Bins::NCENT];
    TH2* h2_jetpT_deltaPT_beforeMC [Bins::NCENT];
    TH2* h2_jetpT_deltaPT_original [Bins::NCENT];

    TProfile* p_Ntrk;
    TProfile* p_MultBins_withCorrMult;
    TProfile* p_CorrMultBins_withMult;
    TProfile* p_v2[Bins::NCENT];
    TProfile* p_v2_jetcut[Bins::NCENT];
    TProfile* p_jetdNch[Bins::NCENT];
    TProfile* p_jetdpT [Bins::NCENT];

    TH1D* h1_CaloJets2nd_TimePeak [Bins::NCENT];
    TH1D* h1_PflowJets2nd_TimePeak[Bins::NCENT];

    TH2D* h_EtaPhi_EbyE         [9][Bins::NCENT];
    TH2D* h_EtaPhi_ptWeight_EbyE[9][Bins::NCENT];
    TH2D* h2_Multiplicity_EbyE  [9][Bins::NCENT];
    TH1* h_UE_pTThreshold[9][Bins::NCENT];    // value of pT correction
    TH3* h_EtaPhiPt[9][Bins::NCENT];



    // Declaration of leaf types -- truth PYTHIA
    vector<int>    *particle_id  = 0;
    vector<double> *particle_pt  = 0;
    vector<double> *particle_eta = 0;
    vector<double> *particle_phi = 0;
    vector<double> *particle_m   = 0;
    vector<double> *particle_tau = 0;



    // Declaration of leaf types
    vector<float>   *trk_pt            = nullptr;
    vector<float>   *trk_eta           = nullptr;
    vector<float>   *trk_phi           = nullptr;
    vector<float>   *trk_charge        = nullptr;
    vector<int>     *trk_qual          = nullptr;

    vector<float>   *vtx_z                = nullptr;
    vector<float>   *MET_sumet            = nullptr;

    vector<float>   *neutralPFlowObj_pt  = nullptr;
    vector<float>   *neutralPFlowObj_eta = nullptr;
    vector<float>   *neutralPFlowObj_phi = nullptr;
    vector<float>   *chargedPFlowObj_pt  = nullptr;
    vector<float>   *chargedPFlowObj_eta = nullptr;
    vector<float>   *chargedPFlowObj_phi = nullptr;

    vector<int  >   *muon_trk_index    = nullptr;
    vector<int  >   *elec_trk_index    = nullptr;

    UInt_t          RunNumber = 0;
    UInt_t          lbn       = 0;
    UInt_t          bcid      = 0;

    Float_t         ActIntPerXing = 0;
    Float_t         Trkz0RMS = 0;

    Bool_t          b_HLT_mb_sp900_pusup500_trk60_hmt_L1TE5   = false;

    // Minbias trigger
    Bool_t          b_HLT_mb_sptrk              = false;
    Bool_t          b_HLT_noalg_mb_L1MBTS_1     = false;
    Bool_t          b_HLT_noalg_mb_L1MBTS_2     = false;
    Bool_t          b_HLT_noalg_mb_L1MBTS_1_1   = false;

    // HMT trigger
    Bool_t          b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10_0ETA24     = false;
    Bool_t          b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5_0ETA24      = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1TE10_0ETA24              = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1TE5_0ETA24               = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5_0ETA24     = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10_0ETA24     = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15_0ETA24     = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5_0ETA24      = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5_0ETA24      = false;
    Bool_t          b_HLT_mb_sp1200_trk100_hmt_L1TE5                     = false;
    Bool_t          b_HLT_mb_sp1200_trk100_hmt_L1TE5_0ETA24              = false;
    Bool_t          b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE10_0ETA24              = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE15                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE15_0ETA24              = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE5_0ETA24               = false;
    Bool_t          b_HLT_mb_sp1200_trk90_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1200_trk90_hmt_L1TE5_0ETA24               = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10_0ETA24     = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15_0ETA24     = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20_0ETA24     = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5_0ETA24      = false;
    Bool_t          b_HLT_mb_sp1400_trk100_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1400_trk100_hmt_L1TE20_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1400_trk100_hmt_L1TE20_0ETA25             = false;
    Bool_t          b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE10_0ETA24              = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE10_0ETA25              = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE15                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE15_0ETA24              = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE20_0ETA24              = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE40                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE5_0ETA24               = false;
    Bool_t          b_HLT_mb_sp1500_pusup700_trk100_hmt_L1TE20_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE10_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE15_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE20_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE25_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE5                     = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE10_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE15_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE20_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE25_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE30_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10_0ETA24    = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15_0ETA24    = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20_0ETA24    = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25_0ETA24    = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30_0ETA24    = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE10_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE15_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE20_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE25_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE30_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15_0ETA24   = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20_0ETA24   = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25_0ETA24   = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30_0ETA24   = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40_0ETA24   = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5           = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE15_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE20_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE25_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE30_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE40_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10          = false;
    Bool_t          b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE20_0ETA24   = false;
    Bool_t          b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2500_trk140_hmt_L1TE20_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2500_trk140_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE20_0ETA24   = false;
    Bool_t          b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2700_trk150_hmt_L1TE20_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2700_trk150_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2900_trk160_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp600_trk40_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp600_trk40_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20             = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE20                      = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5_0ETA24       = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20             = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5_0ETA24       = false;
    Bool_t          b_HLT_mb_sp900_trk50_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp900_trk50_hmt_L1TE5_0ETA24                = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE20                      = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE5_0ETA24                = false;
    Bool_t          b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1                  = false;
};

void EmbedTruthEvent::Init() {
    std::cout << "-----> Initiating PYTHIA events!" << std::endl;
    truthChain = new TChain("PyTree");
    truthChain->Add("/usatlas/u/pyin/workarea/PYTHIA8/02Analysis/01RootFiles/pytree_MONASH13_HardQCD_MPIoff_ISRon_above15_60M/*.root");
    truthChain->SetBranchAddress("particle_id" , &particle_id );
    truthChain->SetBranchAddress("particle_pt" , &particle_pt );
    truthChain->SetBranchAddress("particle_eta", &particle_eta);
    truthChain->SetBranchAddress("particle_phi", &particle_phi);
    truthChain->SetBranchAddress("particle_m"  , &particle_m  );
    truthChain->SetBranchAddress("particle_tau", &particle_tau);
    truthChain->SetBranchStatus("*", 1);


    std::cout << "-----> Initiating Data events!" << std::endl;
    fChain = new TChain("HeavyIonD3PD");
    fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2015_13TeV_set1.20210926_MYSTREAM/*.root");
    // fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2016_13TeV_set1.20210925_MYSTREAM/*.root");
    // fChain->Add("root://dcgftp.usatlas.bnl.gov:1096//pnfs/usatlas.bnl.gov/users/pyin/rucio/user.pyin/user.pyin.TrigRates.PP2017_13TeV_set1.20210925_MYSTREAM/*.root");
    // fChain->Add("root://dcgftp.usatlas.bnl.gov:1096//pnfs/usatlas.bnl.gov/users/pyin/rucio/user.pyin/user.pyin.TrigRates.PP2017_13TeV_set2.20210925_MYSTREAM/*.root");
    // fChain->Add("root://dcgftp.usatlas.bnl.gov:1096//pnfs/usatlas.bnl.gov/users/pyin/rucio/user.pyin/user.pyin.TrigRates.PP2017_13TeV_set3.20210925_MYSTREAM/*.root");
    // fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2018_13TeV_set1.20210925_MYSTREAM/*.root");


    // ------------------------------------------------------------
    // Set branch addresses and branch pointers
    //
    fChain->SetMakeClass(1);
    fChain->SetBranchAddress("RunNumber" , &RunNumber );
    fChain->SetBranchAddress("lbn"       , &lbn       );
    fChain->SetBranchAddress("trk_pt"    , &trk_pt    );
    fChain->SetBranchAddress("trk_eta"   , &trk_eta   );
    fChain->SetBranchAddress("trk_phi"   , &trk_phi   );
    fChain->SetBranchAddress("trk_charge", &trk_charge);
    fChain->SetBranchAddress("trk_qual"  , &trk_qual  );
    fChain->SetBranchAddress("vtx_z"     , &vtx_z     );
    fChain->SetBranchAddress("MET_sumet" , &MET_sumet );

    fChain->SetBranchAddress("neutralPFlowObj_pt" , &neutralPFlowObj_pt );
    fChain->SetBranchAddress("neutralPFlowObj_eta", &neutralPFlowObj_eta);
    fChain->SetBranchAddress("neutralPFlowObj_phi", &neutralPFlowObj_phi);
    fChain->SetBranchAddress("chargedPFlowObj_pt" , &chargedPFlowObj_pt );
    fChain->SetBranchAddress("chargedPFlowObj_eta", &chargedPFlowObj_eta);
    fChain->SetBranchAddress("chargedPFlowObj_phi", &chargedPFlowObj_phi);

    fChain->SetBranchAddress("b_HLT_mb_sptrk"           , &b_HLT_mb_sptrk           );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1"  , &b_HLT_noalg_mb_L1MBTS_1  );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_2"  , &b_HLT_noalg_mb_L1MBTS_2  );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1_1", &b_HLT_noalg_mb_L1MBTS_1_1);

    fChain->SetBranchAddress("ActIntPerXing"     , &ActIntPerXing );
    fChain->SetBranchAddress("Trkz0RMS"          , &Trkz0RMS      );
    fChain->SetBranchAddress("muon_trk_index"    , &muon_trk_index);
    fChain->SetBranchAddress("electron_trk_index", &elec_trk_index);

    fChain->SetBranchAddress("Calo_Et", &Calo_Et);
    fChain->SetBranchAddress("Calo_Qx", &Calo_Qx);
    fChain->SetBranchAddress("Calo_Qy", &Calo_Qy);

    fChain->SetBranchAddress("b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10", &b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10);
    // fChain->SetBranchAddress("b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5", &b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5);
    // fChain->SetBranchAddress("b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1", &b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1TE10", &b_HLT_mb_sp1000_trk70_hmt_L1TE10);
    // fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1000_trk70_hmt_L1TE10_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1TE5", &b_HLT_mb_sp1000_trk70_hmt_L1TE5);
    // fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1000_trk70_hmt_L1TE5_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE10", &b_HLT_mb_sp1100_trk70_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE20", &b_HLT_mb_sp1100_trk70_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE30", &b_HLT_mb_sp1100_trk70_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE5", &b_HLT_mb_sp1100_trk70_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5);
    // fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10);
    // fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15);
    // fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5);
    // fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5);
    // fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk100_hmt_L1TE5", &b_HLT_mb_sp1200_trk100_hmt_L1TE5);
    // fChain->SetBranchAddress("b_HLT_mb_sp1200_trk100_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1200_trk100_hmt_L1TE5_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1", &b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE10", &b_HLT_mb_sp1200_trk80_hmt_L1TE10);
    // fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1200_trk80_hmt_L1TE10_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE15", &b_HLT_mb_sp1200_trk80_hmt_L1TE15);
    // fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1200_trk80_hmt_L1TE15_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE20", &b_HLT_mb_sp1200_trk80_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE30", &b_HLT_mb_sp1200_trk80_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE5", &b_HLT_mb_sp1200_trk80_hmt_L1TE5);
    // fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1200_trk80_hmt_L1TE5_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk90_hmt_L1TE5", &b_HLT_mb_sp1200_trk90_hmt_L1TE5);
    // fChain->SetBranchAddress("b_HLT_mb_sp1200_trk90_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1200_trk90_hmt_L1TE5_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10);
    // fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15);
    // fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20);
    // fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5);
    // fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk100_hmt_L1TE20", &b_HLT_mb_sp1400_trk100_hmt_L1TE20);
    // fChain->SetBranchAddress("b_HLT_mb_sp1400_trk100_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1400_trk100_hmt_L1TE20_0ETA24);
    // fChain->SetBranchAddress("b_HLT_mb_sp1400_trk100_hmt_L1TE20.0ETA25", &b_HLT_mb_sp1400_trk100_hmt_L1TE20_0ETA25);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1", &b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE10", &b_HLT_mb_sp1400_trk90_hmt_L1TE10);
    // fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1400_trk90_hmt_L1TE10_0ETA24);
    // fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE10.0ETA25", &b_HLT_mb_sp1400_trk90_hmt_L1TE10_0ETA25);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE15", &b_HLT_mb_sp1400_trk90_hmt_L1TE15);
    // fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1400_trk90_hmt_L1TE15_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE20", &b_HLT_mb_sp1400_trk90_hmt_L1TE20);
    // fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1400_trk90_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE30", &b_HLT_mb_sp1400_trk90_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE40", &b_HLT_mb_sp1400_trk90_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE5", &b_HLT_mb_sp1400_trk90_hmt_L1TE5);
    // fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1400_trk90_hmt_L1TE5_0ETA24);
    // fChain->SetBranchAddress("b_HLT_mb_sp1500_pusup700_trk100_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1500_pusup700_trk100_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10);
    // fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15);
    // fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20);
    // fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25);
    // fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25.0ETA24", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE10", &b_HLT_mb_sp1600_trk100_hmt_L1TE10);
    // fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1600_trk100_hmt_L1TE10_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE15", &b_HLT_mb_sp1600_trk100_hmt_L1TE15);
    // fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1600_trk100_hmt_L1TE15_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE20", &b_HLT_mb_sp1600_trk100_hmt_L1TE20);
    // fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1600_trk100_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE25", &b_HLT_mb_sp1600_trk100_hmt_L1TE25);
    // fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE25.0ETA24", &b_HLT_mb_sp1600_trk100_hmt_L1TE25_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE30", &b_HLT_mb_sp1600_trk100_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE40", &b_HLT_mb_sp1600_trk100_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE5", &b_HLT_mb_sp1600_trk100_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE50", &b_HLT_mb_sp1600_trk100_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE10", &b_HLT_mb_sp1700_trk110_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE20", &b_HLT_mb_sp1700_trk110_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE30", &b_HLT_mb_sp1700_trk110_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE40", &b_HLT_mb_sp1700_trk110_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE50", &b_HLT_mb_sp1700_trk110_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE60", &b_HLT_mb_sp1700_trk110_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10);
    // fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15);
    // fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20);
    // fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25);
    // fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25.0ETA24", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30);
    // fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30.0ETA24", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE10", &b_HLT_mb_sp1800_trk110_hmt_L1TE10);
    // fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1800_trk110_hmt_L1TE10_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE15", &b_HLT_mb_sp1800_trk110_hmt_L1TE15);
    // fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1800_trk110_hmt_L1TE15_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE20", &b_HLT_mb_sp1800_trk110_hmt_L1TE20);
    // fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1800_trk110_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE25", &b_HLT_mb_sp1800_trk110_hmt_L1TE25);
    // fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE25.0ETA24", &b_HLT_mb_sp1800_trk110_hmt_L1TE25_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE30", &b_HLT_mb_sp1800_trk110_hmt_L1TE30);
    // fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE30.0ETA24", &b_HLT_mb_sp1800_trk110_hmt_L1TE30_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE10", &b_HLT_mb_sp1900_trk120_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE20", &b_HLT_mb_sp1900_trk120_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE30", &b_HLT_mb_sp1900_trk120_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE40", &b_HLT_mb_sp1900_trk120_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE50", &b_HLT_mb_sp1900_trk120_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE60", &b_HLT_mb_sp1900_trk120_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE70", &b_HLT_mb_sp1900_trk120_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10);
    // fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10.0ETA24", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15);
    // fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15.0ETA24", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20);
    // fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25);
    // fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25.0ETA24", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30);
    // fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30.0ETA24", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE10", &b_HLT_mb_sp2100_trk120_hmt_L1TE10);
    // fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE10.0ETA24", &b_HLT_mb_sp2100_trk120_hmt_L1TE10_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE15", &b_HLT_mb_sp2100_trk120_hmt_L1TE15);
    // fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE15.0ETA24", &b_HLT_mb_sp2100_trk120_hmt_L1TE15_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE20", &b_HLT_mb_sp2100_trk120_hmt_L1TE20);
    // fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2100_trk120_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE25", &b_HLT_mb_sp2100_trk120_hmt_L1TE25);
    // fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE25.0ETA24", &b_HLT_mb_sp2100_trk120_hmt_L1TE25_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE30", &b_HLT_mb_sp2100_trk120_hmt_L1TE30);
    // fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE30.0ETA24", &b_HLT_mb_sp2100_trk120_hmt_L1TE30_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE20", &b_HLT_mb_sp2100_trk130_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE30", &b_HLT_mb_sp2100_trk130_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE40", &b_HLT_mb_sp2100_trk130_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE50", &b_HLT_mb_sp2100_trk130_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE60", &b_HLT_mb_sp2100_trk130_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE70", &b_HLT_mb_sp2100_trk130_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE20", &b_HLT_mb_sp2200_trk140_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE30", &b_HLT_mb_sp2200_trk140_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE40", &b_HLT_mb_sp2200_trk140_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE50", &b_HLT_mb_sp2200_trk140_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE60", &b_HLT_mb_sp2200_trk140_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE70", &b_HLT_mb_sp2200_trk140_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15);
    // fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15.0ETA24", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20);
    // fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25);
    // fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25.0ETA24", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30);
    // fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30.0ETA24", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40);
    // fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40.0ETA24", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE15", &b_HLT_mb_sp2300_trk130_hmt_L1TE15);
    // fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE15.0ETA24", &b_HLT_mb_sp2300_trk130_hmt_L1TE15_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE20", &b_HLT_mb_sp2300_trk130_hmt_L1TE20);
    // fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2300_trk130_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE25", &b_HLT_mb_sp2300_trk130_hmt_L1TE25);
    // fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE25.0ETA24", &b_HLT_mb_sp2300_trk130_hmt_L1TE25_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE30", &b_HLT_mb_sp2300_trk130_hmt_L1TE30);
    // fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE30.0ETA24", &b_HLT_mb_sp2300_trk130_hmt_L1TE30_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE40", &b_HLT_mb_sp2300_trk130_hmt_L1TE40);
    // fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE40.0ETA24", &b_HLT_mb_sp2300_trk130_hmt_L1TE40_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE20", &b_HLT_mb_sp2400_trk150_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE30", &b_HLT_mb_sp2400_trk150_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE40", &b_HLT_mb_sp2400_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE50", &b_HLT_mb_sp2400_trk150_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE60", &b_HLT_mb_sp2400_trk150_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE70", &b_HLT_mb_sp2400_trk150_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10", &b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10);
    // fChain->SetBranchAddress("b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40", &b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40);
    // fChain->SetBranchAddress("b_HLT_mb_sp2500_trk140_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2500_trk140_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2500_trk140_hmt_L1TE40", &b_HLT_mb_sp2500_trk140_hmt_L1TE40);
    // fChain->SetBranchAddress("b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40", &b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40);
    // fChain->SetBranchAddress("b_HLT_mb_sp2700_trk150_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2700_trk150_hmt_L1TE20_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp2700_trk150_hmt_L1TE40", &b_HLT_mb_sp2700_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40", &b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2900_trk160_hmt_L1TE40", &b_HLT_mb_sp2900_trk160_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1", &b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10", &b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5", &b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp600_trk40_hmt_L1TE10", &b_HLT_mb_sp600_trk40_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp600_trk40_hmt_L1TE5", &b_HLT_mb_sp600_trk40_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1", &b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1", &b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE10", &b_HLT_mb_sp700_trk50_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE20", &b_HLT_mb_sp700_trk50_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE5", &b_HLT_mb_sp700_trk50_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1", &b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5", &b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5);
    // fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5.0ETA24", &b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5);
    // fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5.0ETA24", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk50_hmt_L1TE5", &b_HLT_mb_sp900_trk50_hmt_L1TE5);
    // fChain->SetBranchAddress("b_HLT_mb_sp900_trk50_hmt_L1TE5.0ETA24", &b_HLT_mb_sp900_trk50_hmt_L1TE5_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1", &b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE10", &b_HLT_mb_sp900_trk60_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE20", &b_HLT_mb_sp900_trk60_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE5", &b_HLT_mb_sp900_trk60_hmt_L1TE5);
    // fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE5.0ETA24", &b_HLT_mb_sp900_trk60_hmt_L1TE5_0ETA24);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1", &b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1);

    //--------------------------------------------------------------------
    fChain->SetBranchStatus("*"                   , 0);
    fChain->SetBranchStatus("RunNumber"           , 1);
    fChain->SetBranchStatus("lbn"                 , 1);
    fChain->SetBranchStatus("trk_*"               , 1);
    fChain->SetBranchStatus("vtx_z"               , 1);
    fChain->SetBranchStatus("b_*"                 , 1);
    fChain->SetBranchStatus("Calo_*"              , 1);
    fChain->SetBranchStatus("neutralPFlowObj_*"   , 1);
    fChain->SetBranchStatus("chargedPFlowObj_*"   , 1);
    fChain->SetBranchStatus("ActIntPerXing"       , 1);
    fChain->SetBranchStatus("Trkz0RMS"            , 1);
    fChain->SetBranchStatus("MET_sumet"           , 1);
    fChain->SetBranchStatus("muon_trk_index"      , 1);
    fChain->SetBranchStatus("electron_trk_index"  , 1);
    // fChain->SetBranchStatus("b_*.0ETA*"           , 0);
}
#endif
