#ifndef __DatasetEnums_h__
#define __DatasetEnums_h__
namespace DataSetEnums {
//-------------------------------------------------------------------------------------
enum {
    DATA_LOWMU                   = 0,
    DATA_LOW_AND_INTERMEDIATE_MU = 1,
    DATA_pp                      = 2,
    DATA_pp_1516                 = 3,
    DATA_pp_1718                 = 4,
    DATA_pp_FullV6               = 8,

    DEFAULT_TYPE = DATA_LOW_AND_INTERMEDIATE_MU,
};

// -----------------------------------------
// p+p  @ 13 TeV: Data_LowMu,
//                Data_LowAndIntermediateMu,
// p+p  @  5 TeV: Data_5TeV,
// p+Pb @  8 TeV: Data_pPb_2016_8TeV2
// -----------------------------------------
std::map<int, std::string> Filename = {
    {DATA_LOWMU                  , "Data_LowMu"               },
    {DATA_LOW_AND_INTERMEDIATE_MU, "Data_LowAndIntermediateMu"},
    {DATA_pp                     , "Data_pp"                  },
    {DATA_pp_1516                , "Data_pp_1516"             },
    {DATA_pp_1718                , "Data_pp_1718"             },
    {DATA_pp_FullV6              , "Data_pp_FullV6"           },
};


std::map<int, std::string> Filename_eff = {
    {DATA_LOWMU                  , "02EfficiencyFiles/tracking_efficiencies_20150709.root"   } ,
    {DATA_LOW_AND_INTERMEDIATE_MU, "02EfficiencyFiles/tracking_efficiencies_20150709.root"   } ,
    {DATA_pp                     , "02EfficiencyFiles/tracking_efficiencies_20150709.root"   } ,
    {DATA_pp_1516                , "02EfficiencyFiles/tracking_efficiencies_20150709.root"   } ,
    {DATA_pp_1718                , "02EfficiencyFiles/tracking_efficiencies_20150709.root"   } ,
    {DATA_pp_FullV6              , "02EfficiencyFiles/tracking_efficiencies_20150709.root"   } ,
};


std::map<int, std::string> Histname_eff = {
    {DATA_LOWMU                  , "Primary_efficiency_PtEta_A2SP"} ,
    {DATA_LOW_AND_INTERMEDIATE_MU, "Primary_efficiency_PtEta_A2SP"} ,
    {DATA_pp                     , "Primary_efficiency_PtEta_A2SP"} ,
    {DATA_pp_1516                , "Primary_efficiency_PtEta_A2SP"} ,
    {DATA_pp_1718                , "Primary_efficiency_PtEta_A2SP"} ,
    {DATA_pp_FullV6              , "Primary_efficiency_PtEta_A2SP"} ,
};


std::map<int, std::string> DATASETENERGY = {
    {DATA_LOWMU                  , "#sqrt{#it{s}}=13 TeV"}, //, L_{Tot}#approx14 nb^{-1}"} };
    {DATA_LOW_AND_INTERMEDIATE_MU, "#sqrt{#it{s}}=13 TeV"},
    {DATA_pp                     , "#sqrt{#it{s}}=13 TeV, 15.8 pb^{-1}"} ,
    {DATA_pp_1516                , "#sqrt{#it{s}}=13 TeV"} ,
    {DATA_pp_1718                , "#sqrt{#it{s}}=13 TeV"} ,
    {DATA_pp_FullV6              , "#sqrt{#it{s}}=13 TeV"} ,
};


std::map<int, std::string> DATASETLABEL = {
    {DATA_LOWMU                  , "#it{pp}"} ,
    {DATA_LOW_AND_INTERMEDIATE_MU, "#it{pp}"} ,
    {DATA_pp                     , "#it{pp}"} ,
    {DATA_pp_1516                , "#it{pp}"} ,
    {DATA_pp_1718                , "#it{pp}"} ,
    {DATA_pp_FullV6              , "#it{pp}"} ,
};
//-------------------------------------------------------------------------------------


// -------------------------------------------------------------------------------------
enum {
    NO_BS     = 0,
    YES_BS_1  = 1,
    YES_BS_2  = 2,
    YES_BS_3  = 3,
    YES_BS_4  = 4,
    YES_BS_5  = 5,
    YES_BS_6  = 6,
    YES_BS_7  = 7,
    YES_BS_8  = 8,
    YES_BS_9  = 9,
    YES_BS_10 = 10,
};
// -------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {
    NO_MU_LIMIT = 0,

    MUBAR0p02   = 1,
    MUBAR0p007  = 2,

    Z0RMS0p25    = 4,
    Z0RMS0p25MU3 = 5,

    Z0RMS0p25MUBAR0p03 = 6,

    Z0RMS0p2MU3 = 51,
    Z0RMS0p3MU3 = 52,

    MURANGE8    = 8, // mu: 0.0 - 1.0
    MURANGE9    = 9, // mu: 0.0 - 3.0
    MURANGE10   = 10, // mu: old conf note mu, 0.002-0.04 and 0.05 - 0.6   combine: 0.002 - 0.6
};

// std::map<int, std::string> MURANGENAME = {
//     {NO_MU_LIMIT, "No limit (old set)"},
//     // {MURANGE1   , "#mu < 0.5"},
//     // {MURANGE2   , "0.5 #leq #mu < 1.0"},
//     // {MURANGE3   , "1.0 #leq #mu < 1.5"},
//     // {MURANGE4   , "1.5 #leq #mu < 2.0"},
//     // {MURANGE5   , "2.0 #leq #mu < 2.5"},
//     // {MURANGE6   , "2.5 #leq #mu < 3.0"},
//     // {MURANGE7   , "1.5 #leq #mu < 2.5"},
//     {MURANGE8   , "#mu < 1.0"},
//     {MURANGE9   , "#mu < 3.0"},
// };
//-------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {
    NO_EFFICIENCY      = 0, //No eff whatsoever
    DEFAULT_EFFICIENCY = 1, //Efficiency for PTY only
    SYSERRO_EFFICIENCY = 2, //Efficiency for PTY only
};

std::map<int, std::string> EFFLABEL = {
    {NO_EFFICIENCY     , "No Eff Correction"},
    {DEFAULT_EFFICIENCY, "Eff Corrected"},
    {SYSERRO_EFFICIENCY, "SysError eff"}
};
//-------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {
    ALL_TRIGS               = 0,
    MINBIAS_ONLY            = 1,
    HMT                     = 2,
    MOREMINBIAS_HMT         = 3,
    MBNOJETTRIG_HMT         = 4,
    MBNOJET45_HMT           = 5,
    MB_ADDJ10_ADDLESSTE10_HMT = 6,



    MINBIAS_HMTNOTE         = 9,
    MINBIAS_HMTNOTE_TE5TE10 = 10,

    MB_HMT_no0ETA24         = 22,
    HMT_L1TE5               = 24,
    HMT_L1TE10              = 25,
    HMT_L1TE15              = 26,
    HMT_L1TE20              = 27,
    HMT_L1TE30PLUS          = 28,
};
std::map<int, std::string> TRIGLABEL = {
    {ALL_TRIGS              , "All Triggers"   },
    {MINBIAS_ONLY           , "Min Bias"       },
};
//-------------------------------------------------------------------------------------



//-------------------------------------------------------------------------------------
enum {
    NO_PILEUP_REJECTION                  = 0,
    REJECTION_NTRK5_ZVTXLT15             = 1,
    REJECTION_NTRK10_ZVTXLT15            = 2,
    REJECTION_SUMPT5_ZVTXLT15            = 3,
    REJECTION_SUMPT10_ZVTXLT15           = 4,
    REJECTION_REMOVE_2VTX_EVENTS         = 5,
    REJECTION_REMOVE_CLOSE15_2VTX_EVENTS = 6,
    REJECTION_REMOVE_CLOSE10_2VTX_EVENTS = 7,
    REJECTION_REMOVE_CLOSE5_2VTX_EVENTS  = 8,
    REJECTION_REMOVE_CLOSE5_3VTXNMORE    = 9,
};
//-------------------------------------------------------------------------------------



//-------------------------------------------------------------------------------------
enum {
    OLD_REQUIREPOSITIVE            = 0,
    TRKQUAL_PP_MIN_BIAS_MODIFIED   = 1,
    TRKQUAL_PP_MIN_BIAS            = 2,
    TRKQUAL_HI_LOOSE               = 3,
    TRKQUAL_HI_TIGHT               = 4,
    // TRKQUAL_HI_TIGHT_TIGHTER_D0_Z0 = 5,
    // TRKQUAL_HI_LOOSE_7SCT_HITS     = 6,
    TRKQUAL_HI_TIGHT_LOOSE_D0_Z0   = 5,
    TRKQUAL_HI_LOOSE_TIGHT_D0_Z0   = 6,
    TRKQUAL_HI_LOOSE_TIGHTER_D0_Z0 = 7,
};
//-------------------------------------------------------------------------------------


// -------------------------------------------------------------------------------------
enum {
    NO_MUONELECTRON_REJECTION = 0,
    REJECT_MUONELECTRON       = 1
};
// -------------------------------------------------------------------------------------


// -------------------------------------------------------------------------------------
enum {
    NO_JET_REJECT  = 0,
    TRKJET         = 1,
    PFLOWJET       = 2,
    EMBED_USETRUTHJETONLY = 3,
    EMBEDDINGPFLOW = 4,
    EMBEDDINGPFLOW_TruthOnly = 5,
    PYTHIA8ONLY = 6,
};
// -------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {
    NO_CORR = 0,

    WITH_AVGCORRONLY = 9,
};
//-------------------------------------------------------------------------------------


// -------------------------------------------------------------------------------------
enum {
    NO_JET_CUTS = 0,

    DETA4_PFLOW2515_RJORIG15_ABV4 = 2,
    DETA4_PFLOW3015_RJORIG15_ABV4 = 3,
    DETA4_PFLOW4015_RJORIG15_ABV4 = 4,
    DETA4_PFLOW5015_RJORIG15_ABV4 = 5,

    DETA4_PFLOW2515_UPPER30_RJORIG15_ABV4 = 6,
    DETA4_PFLOW3015_UPPER40_RJORIG15_ABV4 = 7,
    DETA4_PFLOW4015_UPPER50_RJORIG15_ABV4 = 8,
    DETA4_PFLOW5015_UPPER99_RJORIG15_ABV4 = 9,

    DETA4_PFLOW2520_RJORIG15_ABV4 = 12,

    DETA4_PFLOW3515_RJORIG15_ABV4 = 35,

    DETA4_PFLOW4015_RJORIG15_ABV2 = 42,
    DETA4_PFLOW4015_RJORIG15_ABV3 = 43,
    DETA4_PFLOW4015_RJORIG15_ABV3p5 = 44,
    DETA4_PFLOW4015_RJORIG15_ABV4p5 = 45,

    DETA4_PFLOW2515_RJORIG15_ABV0 = 50,
    DETA4_PFLOW2510_RJORIG15_ABV2 = 52,
    DETA4_PFLOW2510_RJORIG15_ABV3 = 53,
    DETA4_PFLOW2510_RJORIG15_ABV4 = 54,
    DETA4_PFLOW2510_RJORIG15_ABV5 = 55,

    DETA4_PFLOW2010_RJORIG15_ABV4 = 62,
    DETA4_PFLOW3010_RJORIG15_ABV4 = 63,
    DETA4_PFLOW4010_RJORIG15_ABV4 = 64,
    DETA4_PFLOW5010_RJORIG15_ABV4 = 65,

    DETA4_PFLOW2010_RJORIG15_ABV5 = 72,
    DETA4_PFLOW3010_RJORIG15_ABV5 = 73,
    DETA4_PFLOW4010_RJORIG15_ABV5 = 74,
    DETA4_PFLOW5010_RJORIG15_ABV5 = 75,

    DETA4_PFLOW2010_RJORIG15_ABV0 = 82,
    DETA4_PFLOW3010_RJORIG15_ABV0 = 83,
    DETA4_PFLOW4010_RJORIG15_ABV0 = 84,
    DETA4_PFLOW5010_RJORIG15_ABV0 = 85,


    DETA4_PFLOW3010_RJORIG15_ABV10 = 94,
    DETA4_PFLOW4010_RJORIG15_ABV10 = 95,
    DETA4_PFLOW5010_RJORIG15_ABV10 = 96,
    DETA4_PFLOW2510_RJORIG15_ABV20 = 97,
    DETA4_PFLOW2510_RJORIG15_ABV15 = 98,
    DETA4_PFLOW2510_RJORIG15_ABV10 = 99,
};
//-------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {
    NOJET   = 0,
    WITHJET = 1,
    MINBIAS = 2,
};
//-------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {    // only for multiplicity correction only
    NO_MULTCORR   = 0,
    WITH_MULTCORR = 1,
};
//-------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {
    HADRON_HADRON_CORRELATIONS = 0,
    HADRON_JET_CORRELATIONS    = 1,
    HADRON_NOJET_CORRELATIONS  = 2,
};
//-------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {
    USE_NTRK    = 0,
    USE_FCAL_ET = 1,
};
//-------------------------------------------------------------------------------------



#define DEFAULT_SETTINGS \
  int l_data_type         = DataSetEnums::DEFAULT_TYPE, \
  int l_do_bootstrapping  = DataSetEnums::NO_BS,\
  int l_mu_limit          = DataSetEnums::NO_MU_LIMIT,\
  int l_apply_efficiency  = DataSetEnums::DEFAULT_EFFICIENCY,\
  int l_trig_type         = DataSetEnums::ALL_TRIGS,\
  int l_pileup_reject     = DataSetEnums::NO_PILEUP_REJECTION, \
  int l_trk_quality       = DataSetEnums::TRKQUAL_PP_MIN_BIAS, \
  int l_muon_elec         = DataSetEnums::NO_MUONELECTRON_REJECTION, \
  int l_jet_reject_type   = DataSetEnums::NO_JET_REJECT, \
  int l_jet_ptcorr        = DataSetEnums::WITH_AVGCORRONLY, \
  int l_jet_reject_cuts   = DataSetEnums::NO_JET_CUTS, \
  int l_if_have_jet       = DataSetEnums::MINBIAS, \
  int l_if_have_uecorr    = DataSetEnums::WITH_MULTCORR, \
  int l_correlation_type  = DataSetEnums::HADRON_HADRON_CORRELATIONS, \
  int l_multiplicity_type = DataSetEnums::USE_NTRK \

#define SET_DEFAULT_SETTINGS \
  int l_data_type         = DataSetEnums::DEFAULT_TYPE,\
  int l_do_bootstrapping  = DataSetEnums::NO_BS,\
  int l_mu_limit          = DataSetEnums::NO_MU_LIMIT,\
  int l_apply_efficiency  = DataSetEnums::DEFAULT_EFFICIENCY,\
  int l_trig_type         = DataSetEnums::ALL_TRIGS,\
  int l_pileup_reject     = DataSetEnums::NO_PILEUP_REJECTION, \
  int l_trk_quality       = DataSetEnums::TRKQUAL_PP_MIN_BIAS, \
  int l_muon_elec         = DataSetEnums::NO_MUONELECTRON_REJECTION, \
  int l_jet_reject_type   = DataSetEnums::NO_JET_REJECT, \
  int l_jet_ptcorr        = DataSetEnums::WITH_AVGCORRONLY, \
  int l_jet_reject_cuts   = DataSetEnums::NO_JET_CUTS, \
  int l_if_have_jet       = DataSetEnums::MINBIAS, \
  int l_if_have_uecorr    = DataSetEnums::WITH_MULTCORR, \
  int l_correlation_type  = DataSetEnums::HADRON_HADRON_CORRELATIONS, \
  int l_multiplicity_type = DataSetEnums::USE_NTRK, \

#define PASS_SETTINGS \
  l_data_type       ,l_do_bootstrapping ,l_mu_limit         ,l_apply_efficiency ,\
  l_trig_type       ,l_pileup_reject    ,l_trk_quality      ,l_muon_elec ,\
  l_jet_reject_type ,l_jet_ptcorr       ,l_jet_reject_cuts  ,\
  l_if_have_jet     ,l_if_have_uecorr   ,\
  l_correlation_type,l_multiplicity_type

#define PASS_SETTINGS2 \
  m_data_type       ,m_do_bootstrapping ,m_mu_limit         ,m_apply_efficiency ,\
  m_trig_type       ,m_pileup_reject    ,m_trk_quality      ,m_muon_elec ,\
  m_jet_reject_type ,m_jet_ptcorr       ,m_jet_reject_cuts  ,\
  m_if_have_jet     ,m_if_have_uecorr   ,\
  m_correlation_type,m_multiplicity_type

#define ACCEPT_SETTINGS\
  int l_data_type       , int l_do_bootstrapping , int l_mu_limit   , int l_apply_efficiency ,\
  int l_trig_type       , int l_pileup_reject    , int l_trk_quality, int l_muon_elec ,\
  int l_jet_reject_type , int l_jet_ptcorr       , int l_jet_reject_cuts  ,\
  int l_if_have_jet     , int l_if_have_uecorr   ,\
  int l_correlation_type, int l_multiplicity_type

#define PRINT_SETTINGS\
  std::cout << l_data_type        << "," << l_do_bootstrapping  << "," << l_mu_limit        << "," << l_apply_efficiency  << ","\
            << l_trig_type        << "," << l_pileup_reject     << "," << l_trk_quality     << "," << l_muon_elec         << ","\
            << l_jet_reject_type  << "," << l_jet_ptcorr        << "," << l_jet_reject_cuts << ","\
            << l_if_have_jet      << "," << l_if_have_uecorr    << ","\
            << l_correlation_type << "," << l_multiplicity_type << std::endl;


#define BASENAME  DataSetEnums::BaseName(PASS_SETTINGS)

std::string BaseName(ACCEPT_SETTINGS) {
    std::cout << "**************************************" << std::endl;
    PRINT_SETTINGS;
    std::cout << "**************************************" << std::endl;

    char name[600] = "";
    sprintf(name, "%s", Filename[l_data_type].c_str());
    std::string ret = name;

    if (l_do_bootstrapping != NO_BS) {sprintf(name, "%s_doBS%d", ret.c_str(), l_do_bootstrapping); ret = name;}

    sprintf(name, "%s_mu%d"       , ret.c_str(), l_mu_limit         ); ret = name;
    sprintf(name, "%s_eff%d"      , ret.c_str(), l_apply_efficiency ); ret = name;
    sprintf(name, "%s_trig%d"     , ret.c_str(), l_trig_type        ); ret = name;
    sprintf(name, "%s_pileup%d"   , ret.c_str(), l_pileup_reject    ); ret = name;
    sprintf(name, "%s_trkqual%d"  , ret.c_str(), l_trk_quality      ); ret = name;
    sprintf(name, "%s_rejME%d"    , ret.c_str(), l_muon_elec        ); ret = name;
    sprintf(name, "%s_jet%d"      , ret.c_str(), l_jet_reject_type  ); ret = name;
    sprintf(name, "%s_jc%d"       , ret.c_str(), l_jet_ptcorr       ); ret = name;
    sprintf(name, "%s_jetcut%d"   , ret.c_str(), l_jet_reject_cuts  ); ret = name;
    sprintf(name, "%s_ifHaveJet%d", ret.c_str(), l_if_have_jet      ); ret = name;
    sprintf(name, "%s_ifUECorr%d" , ret.c_str(), l_if_have_uecorr   ); ret = name;
    sprintf(name, "%s_corrtype%d" , ret.c_str(), l_correlation_type ); ret = name;
    sprintf(name, "%s_multtype%d" , ret.c_str(), l_multiplicity_type); ret = name;

    return ret;
}


void CheckConfigurables(ACCEPT_SETTINGS) {
    bool pass = true;
    if (pass == false)  throw std::exception();
}


};//namespace DataSetEnums
#endif
