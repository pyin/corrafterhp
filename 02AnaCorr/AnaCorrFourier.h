#ifndef _AnaCorrFourier_HH_
#define _AnaCorrFourier_HH_

#include <vector>
#include "Riostream.h"
#include "TChain.h"
#include <string>
#include "Event.h"

#include "map"
#include "DatasetEnums.h"
#include "bins.h"
#include "TH3.h"
#include <TProfile.h>
#include <TProfile2D.h>
// #include "GoodRunsLists/GoodRunsListSelectionTool.h"

#include "CheckHotSpot.h"


using namespace std;

class TH1;
class TH2;
class TProfile;

struct triple {
    std::string first;
    Bool_t*     second;
    int         third;
};

struct PFlowJetType {
    double pT_orig;
    double pT;
    double pT_sumAbvN;
    double eta_orig;
    double phi_orig;
    double eta;
    double phi;
    double t;
    double t_corr;
    double Nconstit;
    double pT_corrected;
    double pT_avgCorr;
    double pT_correction;
    double Nconstit_corrected;
    double Nconstit_correction;
    double newjvt;
    double pT_uncalib;
    double sumTrkPT;
    double sumAllPT;
    int    pflowJetOrigSize;

    bool pflow_above1stCut;
    bool pflow_above2ndCut;
    bool pflow_passJVT;
    bool inBrightBand;
    bool haveNeighborPflowJet;
    bool haveBalncJet;

    vector<int> pflowJetTrkIndex;
    vector<float> pflowJetTrk_pT;
    vector<float> pflowJetTrk_eta;
    vector<float> pflowJetTrk_phi;
    vector<float> pflowJetNPO_pT;
    vector<float> pflowJetNPO_eta;
    vector<float> pflowJetNPO_phi;
};

struct PFlowObj {
    double pT;
    double eta;
    double phi;
    double t;
    int    charge;
};

class AnaCorrFourier {
    enum {
        //pool bins:
        // nc = Bins::NCENT,
        nc = 200,
        nz = 10,
        ZMAX = 100,
        plnbr = nc * nz,
    };

    enum {
        PP_MIN_BIAS            = 2,
        PP_MIN_BIAS_MODIFIED   = 4,
        HI_LOOSE               = 8,
        HI_TIGHT               = 16,
        // HI_TIGHT_TIGHTER_D0_Z0 = 16,
        // HI_LOOSE_7SCT_HITS     = 32,
        HI_TIGHT_LOOSE_D0_Z0   = 32,
        HI_LOOSE_TIGHT_D0_Z0   = 64,
        HI_LOOSE_TIGHTER_D0_Z0 = 128
    };

public:
    AnaCorrFourier();

    void run();

    int  m_from           = 0    ;
    int  m_to             = 0    ;
    int  m_total          = 0    ;
    bool m_monitor_mode   = false;

    int m_data_type         = DataSetEnums::DATA_LOWMU                ;
    int m_do_bootstrapping  = DataSetEnums::NO_BS                     ;
    int m_mu_limit          = DataSetEnums::NO_MU_LIMIT               ;
    int m_apply_efficiency  = DataSetEnums::DEFAULT_EFFICIENCY        ;
    int m_trig_type         = DataSetEnums::ALL_TRIGS                 ;
    int m_pileup_reject     = DataSetEnums::NO_PILEUP_REJECTION       ;
    int m_trk_quality       = DataSetEnums::TRKQUAL_PP_MIN_BIAS       ;
    int m_muon_elec         = DataSetEnums::NO_MUONELECTRON_REJECTION ;
    int m_jet_reject_type   = DataSetEnums::NO_JET_REJECT             ;
    int m_jet_ptcorr        = DataSetEnums::WITH_AVGCORRONLY          ;
    int m_jet_reject_cuts   = DataSetEnums::NO_JET_CUTS               ;
    int m_if_have_jet       = DataSetEnums::MINBIAS                   ;
    int m_if_have_uecorr    = DataSetEnums::WITH_MULTCORR             ;
    int m_correlation_type  = DataSetEnums::HADRON_HADRON_CORRELATIONS;
    int m_multiplicity_type = DataSetEnums::USE_NTRK                  ;


private:
    TChain *fChain = nullptr;


    void Init();
    void InitTriggers();
    void InitHistos();
    void SaveHistos();
    void InitJetCuts();

    void parsePFlowJets();
    void parsePFlowObj();
    void reparsePFlowJet();
    void Fill(Event* event1, Event* event2, int mixtype);
    void FillJetMonitors();
    void FillEvtMonitors();
    void FillPFOMonitors();

    bool Pileup();
    bool PassTrigger(double ntrk);
    bool PassQuality(int quality);
    bool inBrightBand(double eta);
    bool isSeperateFromPFlowJets(double partEta);
    bool isMuonElectron(int pIndex);
    bool RejectJet_Cone(double trk_pt, double trk_eta, double trk_phi, double trk_eff);
    bool FillMixed(Event* event1, Event* event_jet = nullptr, Event* event_orgn = nullptr);
    bool ifPassGRL(UInt_t rNum, UInt_t lbNum);

    int get_zPool(double z);
    int GetCentBinNtrk(double ntrk);

    double GetNtrkCent();
    double GetNtrkCentBeforeQual();
    double MultiplicityCorrection();
    double ConvertPhi(double phi);
    double CalculateDeltaPhi(double phi1, double phi2);
    double EvaluateTrackingEff(double pt, double eta);


    float getSumGapPos(const std::set<float>& eta_vals, float jet_eta_max, float min_delta);
    float getSumGapNeg(const std::set<float>& eta_vals, float jet_eta_min, float min_delta);

    // GoodRunsListSelectionTool m_grlTool;

    // ------------------------------------
    // Jet cut variables
    //
    float dR = -1;
    float min_jet_pt     = 15.;
    float min_jet_pt_2nd = 10.;
    int   min_jet_Nconstituents = 0; //Minimum Number of constituents
    float pfJetPt_1st    = 25.;
    float pfJetPt_2nd    = 15.;
    float pfJetPt_blnc   = 15.;
    float pfJetPt_rjct   = 15.;
    float pfo_threshold  =  0.;
    // ------------------------------------

    int nmix;
    int m_cent_i;
    int m_TEbin;
    int m_centIndex_UECorr;
    int depth[nc];
    int FCalET_cent;
    float nCurrentBlue = 0;
    float totalEnergy;
    float sumGapPos;
    float sumGapNeg;
    float mubar;
    double ntrk_cent;
    double ntrk_cent_orig;

    double m_zvtx;
    double ntrk_correction = 0;

    vector<PFlowJetType> pflowJetContainer;
    vector<PFlowObj>     pflowobjContainer;
    vector<PFlowObj>     hotspotContainer ;

    std::vector<triple> m_trig_map_All;
    std::vector<triple> m_trig_map_minbias;
    std::vector<triple> m_trig_map_minbias_additional;
    std::vector<triple> m_trig_map_minbias_nojettrig;
    std::vector<triple> m_trig_map_minbias_nojet45;
    std::vector<triple> m_trig_map_minbias_addj10_addlessTE10;
    std::vector<triple> m_trig_map_HMT;
    std::vector<triple> m_trig_map_HMT_NOTE;
    std::vector<triple> m_trig_map_HMT_L1TE5;
    std::vector<triple> m_trig_map_HMT_L1TE10;
    std::vector<triple> m_trig_map_HMT_L1TE15;
    std::vector<triple> m_trig_map_HMT_L1TE20;
    std::vector<triple> m_trig_map_HMT_L1TE30PLUS;

    vector<EVENT_PTR> pool[9][plnbr];

    TH1* h_trig;
    TH1* h1_Nch_perTrig_orig [200];
    TH1* h1_Nch_perTrig_MBHLT[200];
    TH1* h1_Nch_perTrig_HLT  [200];
    TH1* h1_Nch_perTrig_HLT2 [200];
    TH1* h1_Nch_perTrig      [200];
    TH1* h1_Nch_HLT_mb_sp1100_trk70_hmt_L1TE5;
    TH1* hcent;
    TH1* h_runNumber[7];
    TH1* h1_multiplicity[10];
    TH1* h_mu[7];
    TH1* h_mu_afterTrig;
    TH1* h_PoissonRndm;
    TH1* hcent_orig;
    TH1* h1_dvtxz;
    TH1* h_nvtx;
    TH1* hNtrk_origional;
    TH1* hNtrk_corrected;
    TH1* hNtrk_perRun[28];
    TH1* h1_z0RMS[28][Bins::NCENT];
    TH1* hNtrk_origional_beforeTrigger;
    TH1* hNtrk_corrected_beforeTrigger;
    TH1* h_MultCorrection;
    TH1* h1_Nch_perVTX[5];
    TH1* h_size_vexz[Bins::NCENT];
    TH1* h1_pfobj_etabar[Bins::NCENT];
    TH1* h_dz[Bins::NCENT];
    TH1* dist_pT_trig      [Bins::NCENT];
    TH1* dist_pT_cc_poolJet[Bins::NCENT];
    TH1* h1_pflowJetdphi[Bins::NCENT];
    TH1* h1_Muon_PT    [Bins::NCENT];
    TH1* h1_Electron_PT[Bins::NCENT];
    TH1* h1_JetNewPt   [Bins::NCENT];
    TH1* h1_NJet1st[Bins::NCENT];
    TH1* h1_NJet2nd[Bins::NCENT];
    TH1* h1_NJet_re[Bins::NCENT];
    TH1* h1_NJet2nd_re[Bins::NCENT];
    TH1* h1_JetOrigPt_diffRun[28];
    TH1* h1_PFlowJets_NchNew[Bins::NCENT];
    TH1* h1_jetPhi_orig[Bins::NCENT];
    TH1* h1_jetPhi[Bins::NCENT];
    TH1* h1_Red_Pt [Bins::NCENT];
    TH1* h1_Blue_Pt[Bins::NCENT];
    TH1* h1_reCalcJet_deta[Bins::NCENT];
    TH1* h1_reCalcJet_dphi[Bins::NCENT];
    TH1* fg_nchDept[Bins::NCENT];
    TH1* cc_nchDept[Bins::NCENT];
    TH1* fg_pTbDept[Bins::NPT][Bins::NPTCENT];
    TH1* cc_pTbDept[Bins::NPT][Bins::NPTCENT];
    // -----------------------------------
    // global monitor plots
    //    record how many event processed
    TH1* hFillEntries;
    TH1* hFillMixEntries;
    TH1* hEntries_nch;
    TH1* hEntries_ptb[Bins::NPTCENT];
    TH1* hFillEntries_cc;
    // -----------------------------------


    TH2* h2_Mult_CorrMult;
    TH2* h2_Mult_FCal;
    TH2* h2_Nch_beforeQual_afterQual;
    TH2* h2_BHM_NchTE;
    TH2* h2_GoodCaloEvt_NchTE;
    TH2* h2_L1TE_Ntrk;
    TH2* h2_L1TE_Ntrk_jetEvt;
    TH2* h2_L1TE_Ntrk_mb;
    TH2* h2_L1TE_Ntrk_mb_jetEvt;
    TH2* h2_L1TE_SumET_mb;
    TH2* h2_L1TE_SumET_mb_jetEvt;
    TH2* h2_pfo_finerbin_EtaPhi     ;
    TH2* h2_pfo_finerbin_EtaPhi_AbvN;
    TH2* h2_pfo_finerbin2_EtaPhi     ;
    TH2* h2_pfo_finerbin2_EtaPhi_AbvN;
    TH2* h2_trk_EtaPt[28];
    TH2* h2_JetPt_dNcon;
    TH2* h2_nchCovXY[Bins::NCENT];
    TH2* h2_pTbCovXY[Bins::NPT][Bins::NPTCENT];
    TH2* h2_etabar_ch_pfobj[Bins::NCENT];
    TH2* fg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* bg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* cg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* fg_after_correction[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* h2_Trk_EtaPhi_orig[Bins::NCENT];
    TH2* h2_Trk_EtaPhi     [Bins::NCENT];
    TH2* h2_Trk_EtaPhi_AbvN[Bins::NCENT];
    TH2* h2_Red_EtaPhi     [Bins::NCENT];
    TH2* h2_Red_EtaPhi_diffPt[Bins::NCENT][Bins::NPT1];
    TH2* h2_Blue_EtaPhi    [Bins::NCENT];
    TH2* h2_Blue_EtaPhi_diffPt[Bins::NCENT][Bins::NPT1];
    TH2* h2_Blue_chargePos_EtaPhi[Bins::NCENT][Bins::NPT1];
    TH2* h2_Blue_chargeNeg_EtaPhi[Bins::NCENT][Bins::NPT1];
    TH2* h2_Muon_EtaPhi    [Bins::NCENT];
    TH2* h2_Electron_EtaPhi[Bins::NCENT];
    TH2* h2_nBlue_nRed[Bins::NCENT];
    TH2* h2_fgBlue_bgBlue[Bins::NCENT];
    TH2* h2_PFlowJetsPT_uncalib_calib[Bins::NCENT];
    TH2* h2_PFlowJets_PT_ETA[Bins::NCENT];
    TH2* h2_PFlowJets_calibPT_PTAbvN[Bins::NCENT];
    TH2* h2_PFlowJets_calibPT_SumPFO[Bins::NCENT];
    TH2* h2_PFlowJets_TrkFailQualEtaPhi[Bins::NCENT];
    TH2* h2_PFlowJets_EtaPhi         [Bins::NCENT];
    TH2* h2_PFlowJets_EtaSumHEC      [Bins::NCENT];
    TH2* h2_PFlowJets_EtaSumGap      [Bins::NCENT];
    TH2* h2_PFlowJets_EtaPhi_rejected[Bins::NCENT];
    TH2* h2_PFlowJets_EtaPhi_jvtRej  [Bins::NCENT];
    TH2* h2_PFlowJets_EtaPhi_orig    [Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaSumHEC      [Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaSumGap      [Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaPhi_rejected[Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaPhi_jvtRej  [Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaPhi_orig    [Bins::NCENT];
    TH2* h2_PFlowJets_NchOrig_NchCor[Bins::NCENT];
    TH2* h2_PFlowJets_NchOrig_NchCor_forEvtMult[Bins::NCENT];
    TH2* h2_PFlowJets_EtaJvt      [Bins::NCENT];
    TH2* h2_PFlowJets_EtaT_orig   [Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaJvt   [Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaT_orig[Bins::NCENT];
    TH2* h2_PFlowJets2nd_EtaPhi[10][Bins::NCENT];
    TH2* h2_PFlowJets_EtaPhi_diffRun[28][Bins::NCENT];
    TH2* h2_pflowobj_EtaPhi     [Bins::NCENT];
    TH2* h2_pflowobj_EtaPhi_AbvN[Bins::NCENT];
    TH2* h2_sumGapPos_sumGapNeg[Bins::NCENT];
    TH2* h2_sumGapPos_etaBar   [Bins::NCENT];
    TH2* h2_sumGapNeg_etaBar   [Bins::NCENT];
    TH2* h2_endGapPos_endGapNeg[Bins::NCENT];
    TH2* h2_sumGapPos_endGapPos[Bins::NCENT];
    TH2* h2_sumGapNeg_endGapNeg[Bins::NCENT];
    TH2* h2_jetSumCalibPt_dPtRatio[Bins::NCENT];
    TH2* h2_jet_detadphi[Bins::NCENT];
    TH2* h2_jeteta_blnceta[Bins::NCENT];
    TH2* h2_npfoHEC0Frac_EtaPhi[4];
    TH2* h2_npfoHEC1Frac_EtaPhi[4];
    TH2* h2_npfoHEC2Frac_EtaPhi[4];
    TH2* h2_npfoHEC3Frac_EtaPhi[4];

    TProfile2D* pf2_eta_jetpt_avgfrac;

    TH1D* h1_PflowJets2nd_TimePeak[Bins::NCENT];
    TH2* h_efficiency = nullptr;
    TH2D* h2_EtaPhi_EbyE         [9][Bins::NCENT];
    TH2D* h2_EtaPhi_ptWeight_EbyE[9][Bins::NCENT];
    TH1F* h_UE_pTThreshold[9][Bins::NCENT];    // value of pT correction
    TH1D* h1_pflowjet_Phi[25][10];
    TH1D* h1_eta;
    TH1D* h1_phi;
    TH1D* h1_pt;
    TH1D* h1_temp_nchfg[Bins::NCENT];
    TH1D* h1_temp_nchcc[Bins::NCENT];
    TH1D* h1_temp_pTbfg[Bins::NPT][Bins::NPTCENT];
    TH1D* h1_temp_pTbcc[Bins::NPT][Bins::NPTCENT];


    // Declaration of leaf types
    vector<float>   *trk_pt            = nullptr;
    vector<float>   *trk_eta           = nullptr;
    vector<float>   *trk_phi           = nullptr;
    vector<float>   *trk_charge        = nullptr;
    vector<int>     *trk_qual          = nullptr;

    vector<float>   *neutralPFlowObj_pt   = nullptr;
    vector<float>   *neutralPFlowObj_eta  = nullptr;
    vector<float>   *neutralPFlowObj_phi  = nullptr;
    vector<float>   *neutralPFlowObj_t    = nullptr;
    vector<float>   *neutralPFlowObj_eEM  = nullptr;
    vector<float>   *neutralPFlowObj_HEC0 = nullptr;
    vector<float>   *neutralPFlowObj_HEC1 = nullptr;
    vector<float>   *neutralPFlowObj_HEC2 = nullptr;
    vector<float>   *neutralPFlowObj_HEC3 = nullptr;
    vector<float>   *neutralPFlowObj_EME1 = nullptr;
    vector<float>   *neutralPFlowObj_EME2 = nullptr;
    vector<float>   *neutralPFlowObj_EME3 = nullptr;
    vector<float>   *chargedPFlowObj_pt   = nullptr;
    vector<float>   *chargedPFlowObj_eta  = nullptr;
    vector<float>   *chargedPFlowObj_phi  = nullptr;

    vector<float>   *vtx_z                = nullptr;
    vector<float>   *MET_sumet            = nullptr;

    vector<int  >   *muon_trk_index    = nullptr;
    vector<int  >   *elec_trk_index    = nullptr;

    Float_t Calo_Et[2][3]   ;
    Float_t Calo_Qx[2][3][1];
    Float_t Calo_Qy[2][3][1];

    vector<float>   *AntiKt4EMTopoJets_pt       = nullptr;
    vector<float>   *AntiKt4EMTopoJets_eta      = nullptr;
    vector<float>   *AntiKt4EMTopoJets_phi      = nullptr;
    vector<float>   *AntiKt4EMTopoJets_t        = nullptr;
    vector<float>   *AntiKt4EMTopoJets_pt_orig  = nullptr;
    vector<int  >   *AntiKt4EMTopoJets_JCTight  = nullptr;
    vector<float>   *AntiKt4EMTopoJets_jvt      = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_pt      = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_eta     = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_phi     = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_t       = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_pt_orig = nullptr;
    vector<int  >   *AntiKt4EMPFlowJets_JCTight = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_jvt     = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_newjvt  = nullptr;
    vector<vector<int>>   *AntiKt4EMPFlowJets_jetTrk_index = nullptr;
    vector<vector<float>> *AntiKt4EMPFlowJets_jetTRK_pT    = nullptr;
    vector<vector<float>> *AntiKt4EMPFlowJets_jetTRK_WgtPt = nullptr;
    vector<vector<float>> *AntiKt4EMPFlowJets_jetTRK_eta   = nullptr;
    vector<vector<float>> *AntiKt4EMPFlowJets_jetTRK_phi   = nullptr;
    vector<vector<float>> *AntiKt4EMPFlowJets_jetNPFO_pT   = nullptr;
    vector<vector<float>> *AntiKt4EMPFlowJets_jetNPFO_eta  = nullptr;
    vector<vector<float>> *AntiKt4EMPFlowJets_jetNPFO_phi  = nullptr;


    UInt_t          RunNumberMatch   = 0;
    ULong64_t       eventNumberMatch = 0;
    Float_t         L1TE             = 0;

    UInt_t          RunNumber   = 0;
    ULong64_t       eventNumber = 0;
    UInt_t          lbn         = 0;
    UInt_t          bcid        = 0;

    Float_t         ActIntPerXing = 0;
    Float_t         Trkz0RMS = 0;

    // Float_t         endGapPos = 0;
    // Float_t         endGapNeg = 0;

    // Minbias trigger
    Bool_t          b_HLT_mb_sptrk              = false;
    Bool_t          b_HLT_noalg_mb_L1MBTS_1     = false;
    Bool_t          b_HLT_noalg_mb_L1MBTS_1_1   = false;

    Bool_t          b_HLT_noalg_mb_L1MBTS_2     = false;
    Bool_t          b_HLT_noalg_mb_L1RD0_FILLED = false;
    Bool_t          b_HLT_j10_L1MBTS_2          = false;
    Bool_t          b_HLT_j10_L1RD0_FILLED      = false;
    Bool_t          b_HLT_j20_L1J12             = false;
    Bool_t          b_HLT_j20_L1MBTS_2          = false;
    Bool_t          b_HLT_j20_L1RD0_FILLED      = false;
    Bool_t          b_HLT_j45_L1J12             = false;
    Bool_t          b_HLT_noalg_mb_L1TE5        = false;
    Bool_t          b_HLT_noalg_mb_L1TE10       = false;
    Bool_t          b_HLT_noalg_mb_L1TE20       = false;
    Bool_t          b_HLT_noalg_mb_L1TE30       = false;

    // HMT trigger
    Bool_t          b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1200_trk100_hmt_L1TE5                     = false;
    Bool_t          b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE15                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1200_trk90_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1400_trk100_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE15                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE40                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE5                     = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5           = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10          = false;
    Bool_t          b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2500_trk140_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2700_trk150_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2900_trk160_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp600_trk40_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp600_trk40_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20             = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE20                      = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20             = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp900_trk50_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE20                      = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1                  = false;
};

void AnaCorrFourier::Init() {
    fChain = new TChain("HeavyIonD3PD");

    if (m_data_type == DataSetEnums::DATA_pp || m_data_type == DataSetEnums::DATA_pp_FullV6) {
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2015_13TeV_set1.20220916_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2016_13TeV_set1.20220916_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set1.20220916_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set2.20220916_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set3.20220916_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2018_13TeV_set1.20220916_MYSTREAM/*.root");
    }
    else if (m_data_type == DataSetEnums::DATA_pp_1516) {
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2015_13TeV_set1.20220916_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2016_13TeV_set1.20220916_MYSTREAM/*.root");
    }
    else if (m_data_type == DataSetEnums::DATA_pp_1718) {
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set1.20220916_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set2.20220916_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set3.20220916_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2018_13TeV_set1.20220916_MYSTREAM/*.root");
    }
    // else if (m_data_type == DataSetEnums::DATA_pp_FullV6_17) {
    //     fChain->Add("root://dcgftp.usatlas.bnl.gov:1096//pnfs/usatlas.bnl.gov/users/pyin/rucio/user.pyin/user.pyin.TrigRates.PP2017_13TeV_set1.20211213_MYSTREAM/*.root");
    //     fChain->Add("root://dcgftp.usatlas.bnl.gov:1096//pnfs/usatlas.bnl.gov/users/pyin/rucio/user.pyin/user.pyin.TrigRates.PP2017_13TeV_set2.20211213_MYSTREAM/*.root");
    //     fChain->Add("root://dcgftp.usatlas.bnl.gov:1096//pnfs/usatlas.bnl.gov/users/pyin/rucio/user.pyin/user.pyin.TrigRates.PP2017_13TeV_set3.20211213_MYSTREAM/*.root");
    // }
    else {std::cout << __PRETTY_FUNCTION__ << " Unknown Dataset" << std::endl; throw std::exception();}


    // ------------------------------------------------------------
    // Set branch addresses and branch pointers
    //
    fChain->SetMakeClass(1);
    fChain->SetBranchAddress("RunNumber"  , &RunNumber  );
    fChain->SetBranchAddress("eventNumber", &eventNumber);
    fChain->SetBranchAddress("lbn"        , &lbn        );
    fChain->SetBranchAddress("trk_pt"     , &trk_pt     );
    fChain->SetBranchAddress("trk_eta"    , &trk_eta    );
    fChain->SetBranchAddress("trk_phi"    , &trk_phi    );
    fChain->SetBranchAddress("trk_charge" , &trk_charge );
    fChain->SetBranchAddress("trk_qual"   , &trk_qual   );
    fChain->SetBranchAddress("vtx_z"      , &vtx_z      );
    fChain->SetBranchAddress("MET_sumet"  , &MET_sumet  );
    fChain->SetBranchAddress("L1TE"       , &L1TE       );

    fChain->SetBranchAddress("neutralPFlowObj_pt"  , &neutralPFlowObj_pt  );
    fChain->SetBranchAddress("neutralPFlowObj_eta" , &neutralPFlowObj_eta );
    fChain->SetBranchAddress("neutralPFlowObj_phi" , &neutralPFlowObj_phi );
    fChain->SetBranchAddress("neutralPFlowObj_eEM" , &neutralPFlowObj_eEM );
    fChain->SetBranchAddress("neutralPFlowObj_t"   , &neutralPFlowObj_t   );
    fChain->SetBranchAddress("neutralPFlowObj_HEC0", &neutralPFlowObj_HEC0);
    fChain->SetBranchAddress("neutralPFlowObj_HEC1", &neutralPFlowObj_HEC1);
    fChain->SetBranchAddress("neutralPFlowObj_HEC2", &neutralPFlowObj_HEC2);
    fChain->SetBranchAddress("neutralPFlowObj_HEC3", &neutralPFlowObj_HEC3);
    fChain->SetBranchAddress("neutralPFlowObj_EME1", &neutralPFlowObj_EME1);
    fChain->SetBranchAddress("neutralPFlowObj_EME2", &neutralPFlowObj_EME2);
    fChain->SetBranchAddress("neutralPFlowObj_EME3", &neutralPFlowObj_EME3);
    fChain->SetBranchAddress("chargedPFlowObj_pt"  , &chargedPFlowObj_pt  );
    fChain->SetBranchAddress("chargedPFlowObj_eta" , &chargedPFlowObj_eta );
    fChain->SetBranchAddress("chargedPFlowObj_phi" , &chargedPFlowObj_phi );

    fChain->SetBranchAddress("ActIntPerXing", &ActIntPerXing);
    fChain->SetBranchAddress("Trkz0RMS"     , &Trkz0RMS     );

    // fChain->SetBranchAddress("sumGapPos", &sumGapPos);
    // fChain->SetBranchAddress("sumGapNeg", &sumGapNeg);
    // fChain->SetBranchAddress("endGapPos", &endGapPos);
    // fChain->SetBranchAddress("endGapNeg", &endGapNeg);

    fChain->SetBranchAddress("muon_trk_index"    , &muon_trk_index);
    fChain->SetBranchAddress("electron_trk_index", &elec_trk_index);

    fChain->SetBranchAddress("Calo_Et", &Calo_Et);
    fChain->SetBranchAddress("Calo_Qx", &Calo_Qx);
    fChain->SetBranchAddress("Calo_Qy", &Calo_Qy);

    fChain->SetBranchAddress("AntiKt4EMPFlowJets_pt"          , &AntiKt4EMPFlowJets_pt          );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_eta"         , &AntiKt4EMPFlowJets_eta         );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_phi"         , &AntiKt4EMPFlowJets_phi         );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_t"           , &AntiKt4EMPFlowJets_t           );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_pt_orig"     , &AntiKt4EMPFlowJets_pt_orig     );
    // fChain->SetBranchAddress("AntiKt4EMPFlowJets_JCTight"     , &AntiKt4EMPFlowJets_JCTight     );
    // fChain->SetBranchAddress("AntiKt4EMPFlowJets_jvt"         , &AntiKt4EMPFlowJets_jvt         );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_newjvt"      , &AntiKt4EMPFlowJets_newjvt      );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_jetTrk_index", &AntiKt4EMPFlowJets_jetTrk_index);
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_jetTRK_pT"   , &AntiKt4EMPFlowJets_jetTRK_pT   );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_jetTRK_WgtPt", &AntiKt4EMPFlowJets_jetTRK_WgtPt);
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_jetTRK_eta"  , &AntiKt4EMPFlowJets_jetTRK_eta  );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_jetTRK_phi"  , &AntiKt4EMPFlowJets_jetTRK_phi  );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_jetNPFO_pT"  , &AntiKt4EMPFlowJets_jetNPFO_pT  );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_jetNPFO_eta" , &AntiKt4EMPFlowJets_jetNPFO_eta );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_jetNPFO_phi" , &AntiKt4EMPFlowJets_jetNPFO_phi );

    fChain->SetBranchAddress("b_HLT_mb_sptrk"           , &b_HLT_mb_sptrk           );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1"  , &b_HLT_noalg_mb_L1MBTS_1  );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1_1", &b_HLT_noalg_mb_L1MBTS_1_1);

    fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_2"      , &b_HLT_noalg_mb_L1MBTS_2      );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1RD0_FILLED"  , &b_HLT_noalg_mb_L1RD0_FILLED  );
    fChain->SetBranchAddress("b_HLT_j10_L1MBTS_2"           , &b_HLT_j10_L1MBTS_2           );
    fChain->SetBranchAddress("b_HLT_j10_L1RD0_FILLED"       , &b_HLT_j10_L1RD0_FILLED       );
    fChain->SetBranchAddress("b_HLT_j20_L1J12"              , &b_HLT_j20_L1J12              );
    fChain->SetBranchAddress("b_HLT_j20_L1MBTS_2"           , &b_HLT_j20_L1MBTS_2           );
    fChain->SetBranchAddress("b_HLT_j20_L1RD0_FILLED"       , &b_HLT_j20_L1RD0_FILLED       );
    fChain->SetBranchAddress("b_HLT_j45_L1J12"              , &b_HLT_j45_L1J12              );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1TE5"         , &b_HLT_noalg_mb_L1TE5         );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1TE10"        , &b_HLT_noalg_mb_L1TE10        );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1TE20"        , &b_HLT_noalg_mb_L1TE20        );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1TE30"        , &b_HLT_noalg_mb_L1TE30        );

    fChain->SetBranchAddress("b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10", &b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5", &b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1", &b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1TE10", &b_HLT_mb_sp1000_trk70_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1TE5", &b_HLT_mb_sp1000_trk70_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE10", &b_HLT_mb_sp1100_trk70_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE20", &b_HLT_mb_sp1100_trk70_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE30", &b_HLT_mb_sp1100_trk70_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE5", &b_HLT_mb_sp1100_trk70_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk100_hmt_L1TE5", &b_HLT_mb_sp1200_trk100_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1", &b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE10", &b_HLT_mb_sp1200_trk80_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE15", &b_HLT_mb_sp1200_trk80_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE20", &b_HLT_mb_sp1200_trk80_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE30", &b_HLT_mb_sp1200_trk80_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE5", &b_HLT_mb_sp1200_trk80_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk90_hmt_L1TE5", &b_HLT_mb_sp1200_trk90_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk100_hmt_L1TE20", &b_HLT_mb_sp1400_trk100_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1", &b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE10", &b_HLT_mb_sp1400_trk90_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE15", &b_HLT_mb_sp1400_trk90_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE20", &b_HLT_mb_sp1400_trk90_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE30", &b_HLT_mb_sp1400_trk90_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE40", &b_HLT_mb_sp1400_trk90_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE5", &b_HLT_mb_sp1400_trk90_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE10", &b_HLT_mb_sp1600_trk100_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE15", &b_HLT_mb_sp1600_trk100_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE20", &b_HLT_mb_sp1600_trk100_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE25", &b_HLT_mb_sp1600_trk100_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE30", &b_HLT_mb_sp1600_trk100_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE40", &b_HLT_mb_sp1600_trk100_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE5", &b_HLT_mb_sp1600_trk100_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE50", &b_HLT_mb_sp1600_trk100_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE10", &b_HLT_mb_sp1700_trk110_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE20", &b_HLT_mb_sp1700_trk110_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE30", &b_HLT_mb_sp1700_trk110_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE40", &b_HLT_mb_sp1700_trk110_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE50", &b_HLT_mb_sp1700_trk110_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE60", &b_HLT_mb_sp1700_trk110_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE10", &b_HLT_mb_sp1800_trk110_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE15", &b_HLT_mb_sp1800_trk110_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE20", &b_HLT_mb_sp1800_trk110_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE25", &b_HLT_mb_sp1800_trk110_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE30", &b_HLT_mb_sp1800_trk110_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE10", &b_HLT_mb_sp1900_trk120_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE20", &b_HLT_mb_sp1900_trk120_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE30", &b_HLT_mb_sp1900_trk120_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE40", &b_HLT_mb_sp1900_trk120_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE50", &b_HLT_mb_sp1900_trk120_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE60", &b_HLT_mb_sp1900_trk120_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE70", &b_HLT_mb_sp1900_trk120_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE10", &b_HLT_mb_sp2100_trk120_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE15", &b_HLT_mb_sp2100_trk120_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE20", &b_HLT_mb_sp2100_trk120_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE25", &b_HLT_mb_sp2100_trk120_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE30", &b_HLT_mb_sp2100_trk120_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE20", &b_HLT_mb_sp2100_trk130_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE30", &b_HLT_mb_sp2100_trk130_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE40", &b_HLT_mb_sp2100_trk130_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE50", &b_HLT_mb_sp2100_trk130_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE60", &b_HLT_mb_sp2100_trk130_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE70", &b_HLT_mb_sp2100_trk130_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE20", &b_HLT_mb_sp2200_trk140_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE30", &b_HLT_mb_sp2200_trk140_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE40", &b_HLT_mb_sp2200_trk140_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE50", &b_HLT_mb_sp2200_trk140_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE60", &b_HLT_mb_sp2200_trk140_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE70", &b_HLT_mb_sp2200_trk140_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE15", &b_HLT_mb_sp2300_trk130_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE20", &b_HLT_mb_sp2300_trk130_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE25", &b_HLT_mb_sp2300_trk130_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE30", &b_HLT_mb_sp2300_trk130_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE40", &b_HLT_mb_sp2300_trk130_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE20", &b_HLT_mb_sp2400_trk150_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE30", &b_HLT_mb_sp2400_trk150_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE40", &b_HLT_mb_sp2400_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE50", &b_HLT_mb_sp2400_trk150_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE60", &b_HLT_mb_sp2400_trk150_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE70", &b_HLT_mb_sp2400_trk150_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10", &b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40", &b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2500_trk140_hmt_L1TE40", &b_HLT_mb_sp2500_trk140_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40", &b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2700_trk150_hmt_L1TE40", &b_HLT_mb_sp2700_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40", &b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2900_trk160_hmt_L1TE40", &b_HLT_mb_sp2900_trk160_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1", &b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10", &b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5", &b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp600_trk40_hmt_L1TE10", &b_HLT_mb_sp600_trk40_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp600_trk40_hmt_L1TE5", &b_HLT_mb_sp600_trk40_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1", &b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1", &b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE10", &b_HLT_mb_sp700_trk50_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE20", &b_HLT_mb_sp700_trk50_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE5", &b_HLT_mb_sp700_trk50_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1", &b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5", &b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk50_hmt_L1TE5", &b_HLT_mb_sp900_trk50_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1", &b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE10", &b_HLT_mb_sp900_trk60_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE20", &b_HLT_mb_sp900_trk60_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE5", &b_HLT_mb_sp900_trk60_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1", &b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1);

    //--------------------------------------------------------------------
    fChain->SetBranchStatus("*"                   , 0);
    fChain->SetBranchStatus("RunNumber"           , 1);
    fChain->SetBranchStatus("eventNumber"         , 1);
    fChain->SetBranchStatus("lbn"                 , 1);
    fChain->SetBranchStatus("trk_*"               , 1);
    fChain->SetBranchStatus("vtx_z"               , 1);
    fChain->SetBranchStatus("b_*"                 , 1);
    fChain->SetBranchStatus("Calo_*"              , 1);
    fChain->SetBranchStatus("ActIntPerXing"       , 1);
    // fChain->SetBranchStatus("sumGap*"             , 1);
    // fChain->SetBranchStatus("endGap*"             , 1);
    fChain->SetBranchStatus("Trkz0RMS"            , 1);
    fChain->SetBranchStatus("MET_sumet"           , 1);
    fChain->SetBranchStatus("AntiKt4EMPFlowJets_*", 1);
    fChain->SetBranchStatus("neutralPFlowObj_*"   , 1);
    fChain->SetBranchStatus("chargedPFlowObj_*"   , 1);
    fChain->SetBranchStatus("muon_trk_index"      , 1);
    fChain->SetBranchStatus("electron_trk_index"  , 1);
    fChain->SetBranchStatus("L1TE"                , 1);
}
#endif
