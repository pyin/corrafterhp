#ifndef _TruthJetCalibScaleDown_HH_
#define _TruthJetCalibScaleDown_HH_

#include <vector>
#include "Riostream.h"
#include "TChain.h"
#include <string>
#include "Event.h"

#include "map"
#include "DatasetEnums.h"
#include "bins.h"
#include <TProfile.h>
#include <TProfile2D.h>

using namespace std;

struct Particle {
    double pT;
    double eta;
    double phi;
    double t;
    int    charge;
};

class TruthJetCalibScaleDown {
public:
    TruthJetCalibScaleDown();

    void run();

    int  m_idx = 0;


private:
    TChain *truthChain = nullptr;


    void Init();
    void InitHistos();
    void SaveHistos();
    void ReconstructTruthPFlowJet();
    void RerecoTruthPFlowJet();

    double ConvertPhi(double phi);
    double CalculateDeltaPhi(double phi1, double phi2);

    vector<Particle> truthParticleContainer;

    TProfile2D* pf2_eta_jetCalibpt_avgfrac;

    // ------------------------------------------
    // output tree variables
    //
    TTree* outTree;
    vector<double> jet_pt ;
    vector<double> jet_eta;
    vector<double> jet_phi;
    // ------------------------------------------


    // Declaration of leaf types
    vector<int>    *particle_id  = 0;
    vector<double> *particle_pt  = 0;
    vector<double> *particle_eta = 0;
    vector<double> *particle_phi = 0;
};

void TruthJetCalibScaleDown::Init() {
    std::cout << "-----> Initiating PYTHIA events!" << std::endl;
    truthChain = new TChain("PyTree");
    char name[600] = "";
    sprintf(name, "/usatlas/u/pyin/workarea/PYTHIA8/02Analysis/01RootFiles/pytree_Tune14Monash_HardQCDon_MPIoff_ISRon_above15_50M/pytree_%d.root", m_idx);
    std::cout << name << std::endl;
    truthChain->Add(name);
    truthChain->SetBranchAddress("particle_id" , &particle_id );
    truthChain->SetBranchAddress("particle_pt" , &particle_pt );
    truthChain->SetBranchAddress("particle_eta", &particle_eta);
    truthChain->SetBranchAddress("particle_phi", &particle_phi);
    truthChain->SetBranchStatus("*"        , 0);
    truthChain->SetBranchStatus("particle*", 1);
}
#endif
