#ifndef _RecoDataJet_HH_
#define _RecoDataJet_HH_

#include <vector>
#include "Riostream.h"
#include "TChain.h"
#include <string>
#include "Event.h"

#include "map"
#include "DatasetEnums.h"
#include "bins.h"
#include "TH3.h"
#include <TProfile.h>
#include <TProfile2D.h>

#include "CheckHotSpot.h"


using namespace std;

class TH1;
class TH2;
class TProfile;

struct triple {
    std::string first;
    Bool_t*     second;
    int         third;
};

struct Particle {
    double pT;
    double eta;
    double phi;
    double t;
    int    charge;
};

class RecoDataJet {
    enum {
        //pool bins:
        // nc = Bins::NCENT,
        nc = 200,
        nz = 10,
        ZMAX = 100,
        plnbr = nc * nz,
    };

    enum {
        PP_MIN_BIAS            = 2,
        PP_MIN_BIAS_MODIFIED   = 4,
        HI_LOOSE               = 8,
        HI_TIGHT               = 16,
        // HI_TIGHT_TIGHTER_D0_Z0 = 16,
        // HI_LOOSE_7SCT_HITS     = 32,
        HI_TIGHT_LOOSE_D0_Z0   = 32,
        HI_LOOSE_TIGHT_D0_Z0   = 64,
        HI_LOOSE_TIGHTER_D0_Z0 = 128
    };

public:
    RecoDataJet();

    void run();

    int  m_from           = 0    ;
    int  m_to             = 0    ;
    int  m_total          = 0    ;
    bool m_monitor_mode   = false;

    int m_data_type         = DataSetEnums::DATA_LOWMU                ;
    int m_do_bootstrapping  = DataSetEnums::NO_BS                     ;
    int m_mu_limit          = DataSetEnums::NO_MU_LIMIT               ;
    int m_apply_efficiency  = DataSetEnums::DEFAULT_EFFICIENCY        ;
    int m_trig_type         = DataSetEnums::ALL_TRIGS                 ;
    int m_pileup_reject     = DataSetEnums::NO_PILEUP_REJECTION       ;
    int m_trk_quality       = DataSetEnums::TRKQUAL_PP_MIN_BIAS       ;
    int m_muon_elec         = DataSetEnums::NO_MUONELECTRON_REJECTION ;
    int m_jet_reject_type   = DataSetEnums::NO_JET_REJECT             ;
    int m_jet_ptcorr        = DataSetEnums::WITH_AVGCORRONLY          ;
    int m_jet_reject_cuts   = DataSetEnums::NO_JET_CUTS               ;
    int m_if_have_jet       = DataSetEnums::MINBIAS                   ;
    int m_if_have_uecorr    = DataSetEnums::WITH_MULTCORR             ;
    int m_correlation_type  = DataSetEnums::HADRON_HADRON_CORRELATIONS;
    int m_multiplicity_type = DataSetEnums::USE_NTRK                  ;


private:
    TChain *fChain = nullptr;


    void Init();
    void InitHistos();
    void SaveHistos();
    void ReconstructDataPFlowJet();

    void parseDataParticles();
    void parseDataJetsDirect();

    bool PassTrigger(double ntrk_corr, double ntrk_orig);
    bool isMuonElectron(int pIndex);
    bool PassQuality(int quality);

    double ConvertPhi(double phi);
    double CalculateDeltaPhi(double phi1, double phi2);
    double EvaluateTrackingEff(float pt, float eta);

    vector<Particle> dataParticleContainer;

    TH2* h_efficiency = nullptr;

    // ------------------------------------------
    // output tree variables
    //
    TTree* outTree;
    UInt_t             RunNumber   = 0;
    ULong64_t          eventNumber = 0;
    std::vector<float> recoDataJet_pt ;
    std::vector<float> recoDataJet_eta;
    std::vector<float> recoDataJet_phi;
    std::vector<std::vector<float>> recoDataJet_constituent_pT ;
    std::vector<std::vector<float>> recoDataJet_constituent_eta;
    std::vector<std::vector<float>> recoDataJet_constituent_phi;
    std::vector<std::vector<int>>   recoDataJet_constituent_ch ;
    // ------------------------------------------


    // Declaration of leaf types
    vector<float>   *trk_pt            = nullptr;
    vector<float>   *trk_eta           = nullptr;
    vector<float>   *trk_phi           = nullptr;
    vector<float>   *trk_charge        = nullptr;
    vector<int>     *trk_qual          = nullptr;

    vector<float>   *neutralPFlowObj_pt   = nullptr;
    vector<float>   *neutralPFlowObj_eta  = nullptr;
    vector<float>   *neutralPFlowObj_phi  = nullptr;
    vector<float>   *neutralPFlowObj_t    = nullptr;
    vector<float>   *neutralPFlowObj_eEM  = nullptr;
    vector<float>   *neutralPFlowObj_HEC0 = nullptr;
    vector<float>   *neutralPFlowObj_HEC1 = nullptr;
    vector<float>   *neutralPFlowObj_HEC2 = nullptr;
    vector<float>   *neutralPFlowObj_HEC3 = nullptr;
    vector<float>   *neutralPFlowObj_EME1 = nullptr;
    vector<float>   *neutralPFlowObj_EME2 = nullptr;
    vector<float>   *neutralPFlowObj_EME3 = nullptr;
    vector<float>   *chargedPFlowObj_pt   = nullptr;
    vector<float>   *chargedPFlowObj_eta  = nullptr;
    vector<float>   *chargedPFlowObj_phi  = nullptr;

    vector<float>   *vtx_z                = nullptr;
    vector<float>   *MET_sumet            = nullptr;

    vector<int  >   *muon_trk_index    = nullptr;
    vector<int  >   *elec_trk_index    = nullptr;

    // UInt_t          RunNumber   = 0;
    // ULong64_t       eventNumber = 0;
    UInt_t          lbn       = 0;
    UInt_t          bcid      = 0;
    Float_t         ActIntPerXing = 0;
};

void RecoDataJet::Init() {
    std::cout << "-----> Initiating Data events!" << std::endl;
    fChain = new TChain("HeavyIonD3PD");
    fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2015_13TeV_set1.20220916_MYSTREAM/*.root");
    fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2016_13TeV_set1.20220916_MYSTREAM/*.root");
    fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set1.20220916_MYSTREAM/*.root");
    fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set2.20220916_MYSTREAM/*.root");
    fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set3.20220916_MYSTREAM/*.root");
    fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2018_13TeV_set1.20220916_MYSTREAM/*.root");


    // ------------------------------------------------------------
    // Set branch addresses and branch pointers
    //
    fChain->SetMakeClass(1);
    fChain->SetBranchAddress("RunNumber"  , &RunNumber  );
    fChain->SetBranchAddress("eventNumber", &eventNumber);
    fChain->SetBranchAddress("lbn"       , &lbn       );
    fChain->SetBranchAddress("trk_pt"    , &trk_pt    );
    fChain->SetBranchAddress("trk_eta"   , &trk_eta   );
    fChain->SetBranchAddress("trk_phi"   , &trk_phi   );
    fChain->SetBranchAddress("trk_charge", &trk_charge);
    fChain->SetBranchAddress("trk_qual"  , &trk_qual  );
    fChain->SetBranchAddress("vtx_z"     , &vtx_z     );
    fChain->SetBranchAddress("MET_sumet" , &MET_sumet );

    fChain->SetBranchAddress("neutralPFlowObj_pt"  , &neutralPFlowObj_pt  );
    fChain->SetBranchAddress("neutralPFlowObj_eta" , &neutralPFlowObj_eta );
    fChain->SetBranchAddress("neutralPFlowObj_phi" , &neutralPFlowObj_phi );
    fChain->SetBranchAddress("neutralPFlowObj_eEM" , &neutralPFlowObj_eEM );
    fChain->SetBranchAddress("neutralPFlowObj_t"   , &neutralPFlowObj_t   );
    fChain->SetBranchAddress("neutralPFlowObj_HEC0", &neutralPFlowObj_HEC0);
    fChain->SetBranchAddress("neutralPFlowObj_HEC1", &neutralPFlowObj_HEC1);
    fChain->SetBranchAddress("neutralPFlowObj_HEC2", &neutralPFlowObj_HEC2);
    fChain->SetBranchAddress("neutralPFlowObj_HEC3", &neutralPFlowObj_HEC3);
    fChain->SetBranchAddress("chargedPFlowObj_pt"  , &chargedPFlowObj_pt  );
    fChain->SetBranchAddress("chargedPFlowObj_eta" , &chargedPFlowObj_eta );
    fChain->SetBranchAddress("chargedPFlowObj_phi" , &chargedPFlowObj_phi );

    fChain->SetBranchAddress("ActIntPerXing"     , &ActIntPerXing );
    fChain->SetBranchAddress("muon_trk_index"    , &muon_trk_index);
    fChain->SetBranchAddress("electron_trk_index", &elec_trk_index);

    //--------------------------------------------------------------------
    fChain->SetBranchStatus("*"                   , 0);
    fChain->SetBranchStatus("RunNumber"           , 1);
    fChain->SetBranchStatus("eventNumber"         , 1);
    fChain->SetBranchStatus("lbn"                 , 1);
    fChain->SetBranchStatus("trk_*"               , 1);
    fChain->SetBranchStatus("vtx_z"               , 1);
    fChain->SetBranchStatus("b_*"                 , 1);
    fChain->SetBranchStatus("neutralPFlowObj_*"   , 1);
    fChain->SetBranchStatus("chargedPFlowObj_*"   , 1);
    fChain->SetBranchStatus("ActIntPerXing"       , 1);
    fChain->SetBranchStatus("MET_sumet"           , 1);
    fChain->SetBranchStatus("muon_trk_index"      , 1);
    fChain->SetBranchStatus("electron_trk_index"  , 1);
}
#endif
