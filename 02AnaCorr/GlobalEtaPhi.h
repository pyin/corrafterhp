#ifndef _GlobalEtaPhi_HH_
#define _GlobalEtaPhi_HH_

#include <vector>
#include "Riostream.h"
#include "TChain.h"
#include <string>

#include "Event.h"
#include "map"
#include "DatasetEnums.h"
#include "bins.h"
#include "TH3.h"
#include <TProfile2D.h>

#include "CheckHotSpot.h"

using namespace std;

class TH1;
class TH2;

struct triple {
    std::string first;
    Bool_t*     second;
    int         third;
};

struct PFlowJetType {
    double pT;
    double eta;
    double phi;
    double t;

    bool pflow_above1stCut;
    bool pflow_above2ndCut;
};

struct PFlowObj {
    float pT;
    float eta;
    float phi;
    float t;
    int   charge;
};

class GlobalEtaPhi {

    enum {
        //pool bins:
        // nc = Bins::NCENT,
        nc = 200,
        nz = 10,
        ZMAX = 100,
        plnbr = nc * nz,
    };

    enum {
        PP_MIN_BIAS            = 2,
        PP_MIN_BIAS_MODIFIED   = 4,
        HI_LOOSE               = 8,
        HI_TIGHT               = 16,
        // HI_TIGHT_TIGHTER_D0_Z0 = 16,
        // HI_LOOSE_7SCT_HITS     = 32,
        HI_TIGHT_LOOSE_D0_Z0   = 32,
        HI_LOOSE_TIGHT_D0_Z0   = 64,
        HI_LOOSE_TIGHTER_D0_Z0 = 128
    };

public:
    GlobalEtaPhi();

    void run();

    int  m_from           = 0    ;
    int  m_to             = 0    ;
    int  m_total          = 0    ;
    bool m_monitor_mode   = false;

    int m_data_type         = DataSetEnums::DATA_LOWMU                ;
    int m_do_bootstrapping  = DataSetEnums::NO_BS                     ;
    int m_mu_limit          = DataSetEnums::NO_MU_LIMIT               ;
    int m_apply_efficiency  = DataSetEnums::DEFAULT_EFFICIENCY        ;
    int m_trig_type         = DataSetEnums::ALL_TRIGS                 ;
    int m_pileup_reject     = DataSetEnums::NO_PILEUP_REJECTION       ;
    int m_trk_quality       = DataSetEnums::TRKQUAL_PP_MIN_BIAS       ;
    int m_muon_elec         = DataSetEnums::NO_MUONELECTRON_REJECTION ;
    int m_jet_reject_type   = DataSetEnums::NO_JET_REJECT             ;
    int m_jet_ptcorr        = DataSetEnums::WITH_AVGCORRONLY          ;
    int m_jet_reject_cuts   = DataSetEnums::NO_JET_CUTS               ;
    int m_if_have_jet       = DataSetEnums::MINBIAS                   ;
    int m_if_have_uecorr    = DataSetEnums::WITH_MULTCORR             ;
    int m_correlation_type  = DataSetEnums::HADRON_HADRON_CORRELATIONS;
    int m_multiplicity_type = DataSetEnums::USE_NTRK                  ;

private:
    TChain *fChain = nullptr;

    void InitTriggers();
    void Init();
    void InitHistos();
    void InitJetCuts();
    void SaveHistos();
    void parsePFlowJets();
    void parsePFlowObj();
    void FillJetMonitors();
    void processEvtPsi2V2();

    bool Pileup();
    bool PassTrigger(int ntrk_noeff);
    bool PassQuality(int quality);
    bool isSeperateFromPFlowJets(float partEta);
    bool isMuonElectron(int pIndex);
    bool ifPassGRL(UInt_t rNum, UInt_t lbNum);

    int GetNtrkCent();
    int GetCentBinNtrk(float ntrk);
    int get_zPool(float z);

    float EvaluateTrackingEff(float pt, float eta);
    float getSumGapPos(const std::set<float>& eta_vals, float jet_eta_max, float min_delta);
    float getSumGapNeg(const std::set<float>& eta_vals, float jet_eta_min, float min_delta);

    double ConvertPhi(double phi);
    double CalculateDeltaPhi(float phi1, float phi2);

    // ------------------------------------------
    //
    //
    float dR = 0.4;
    float pfJetPt_1st    = 25.;
    float pfJetPt_2nd    = 15.;
    float pfJetPt_blnc   = 15.;
    float pfJetPt_rjct   = 15.;

    int m_cent_i;
    int m_TEbin;

    float m_zvtx;

    double ntrk_cent;

    vector<PFlowJetType> pflowJetContainer;
    vector<PFlowObj>     pflowobjContainer;


    TH2  *h_efficiency = nullptr;
    TH2* temp_etaphi;
    TH2* temp_etaphi_ptWeight;

    TProfile2D* h_EtaPhi_EbyE         [9][Bins::NCENT];
    TProfile2D* h_EtaPhi_ptWeight_EbyE[9][Bins::NCENT];
    TH2D* h2_PFlowJets2nd_EtaT_orig[Bins::NCENT];
    TH2D* h2_evt_Qx_all[9];
    TH2D* h2_evt_Qy_all[9];

    TH1D* h_UE_pTThreshold[9][Bins::NCENT];    // value of pT correction

    std::vector<triple> m_trig_map_All;
    std::vector<triple> m_trig_map_minbias;
    std::vector<triple> m_trig_map_minbias_additional;
    std::vector<triple> m_trig_map_minbias_nojettrig;
    std::vector<triple> m_trig_map_minbias_nojet45;
    std::vector<triple> m_trig_map_minbias_addj10_addlessTE10;
    std::vector<triple> m_trig_map_HMT;
    std::vector<triple> m_trig_map_HMT_NOTE;
    std::vector<triple> m_trig_map_HMT_L1TE5;
    std::vector<triple> m_trig_map_HMT_L1TE10;
    std::vector<triple> m_trig_map_HMT_L1TE15;
    std::vector<triple> m_trig_map_HMT_L1TE20;
    std::vector<triple> m_trig_map_HMT_L1TE30PLUS;


    // Declaration of leaf types
    vector<float>   *trk_pt            = nullptr;
    vector<float>   *trk_eta           = nullptr;
    vector<float>   *trk_phi           = nullptr;
    vector<float>   *trk_charge        = nullptr;
    vector<int>     *trk_qual          = nullptr;

    vector<float>   *neutralPFlowObj_pt   = nullptr;
    vector<float>   *neutralPFlowObj_eta  = nullptr;
    vector<float>   *neutralPFlowObj_phi  = nullptr;
    vector<float>   *neutralPFlowObj_t    = nullptr;
    vector<float>   *neutralPFlowObj_eEM  = nullptr;
    vector<float>   *neutralPFlowObj_HEC0 = nullptr;
    vector<float>   *neutralPFlowObj_HEC1 = nullptr;
    vector<float>   *neutralPFlowObj_HEC2 = nullptr;
    vector<float>   *neutralPFlowObj_HEC3 = nullptr;
    vector<float>   *neutralPFlowObj_EME1 = nullptr;
    vector<float>   *neutralPFlowObj_EME2 = nullptr;
    vector<float>   *neutralPFlowObj_EME3 = nullptr;
    vector<float>   *chargedPFlowObj_pt   = nullptr;
    vector<float>   *chargedPFlowObj_eta  = nullptr;
    vector<float>   *chargedPFlowObj_phi  = nullptr;

    vector<float>   *vtx_z             = nullptr;
    vector<float>   *MET_sumet         = nullptr;

    vector<int  >   *muon_trk_index    = nullptr;
    vector<int  >   *elec_trk_index    = nullptr;

    vector<float>   *AntiKt4EMPFlowJets_pt      = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_eta     = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_phi     = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_t       = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_newjvt  = nullptr;
    vector<float>   *AntiKt4EMPFlowJets_pt_orig = nullptr;

    UInt_t          RunNumber = 0;
    UInt_t          lbn       = 0;
    UInt_t          bcid      = 0;

    Float_t         ActIntPerXing = 0;
    Float_t         Trkz0RMS = 0;

    // Minbias trigger
    Bool_t          b_HLT_mb_sptrk              = false;
    Bool_t          b_HLT_noalg_mb_L1MBTS_1     = false;
    Bool_t          b_HLT_noalg_mb_L1MBTS_1_1   = false;

    Bool_t          b_HLT_noalg_mb_L1MBTS_2     = false;
    Bool_t          b_HLT_noalg_mb_L1RD0_FILLED = false;
    Bool_t          b_HLT_j10_L1MBTS_2          = false;
    Bool_t          b_HLT_j10_L1RD0_FILLED      = false;
    Bool_t          b_HLT_j20_L1J12             = false;
    Bool_t          b_HLT_j20_L1MBTS_2          = false;
    Bool_t          b_HLT_j20_L1RD0_FILLED      = false;
    Bool_t          b_HLT_j45_L1J12             = false;
    Bool_t          b_HLT_noalg_mb_L1TE5        = false;
    Bool_t          b_HLT_noalg_mb_L1TE10       = false;
    Bool_t          b_HLT_noalg_mb_L1TE20       = false;
    Bool_t          b_HLT_noalg_mb_L1TE30       = false;


    // HMT trigger
    Bool_t          b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1200_trk100_hmt_L1TE5                     = false;
    Bool_t          b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE15                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1200_trk90_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1400_trk100_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE15                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE40                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE5                     = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5           = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10          = false;
    Bool_t          b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2500_trk140_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2700_trk150_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2900_trk160_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp600_trk40_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp600_trk40_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20             = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE20                      = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20             = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp900_trk50_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE20                      = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1                  = false;
};



void GlobalEtaPhi::Init() {
    fChain = new TChain("HeavyIonD3PD");

    if (m_data_type == DataSetEnums::DATA_pp || m_data_type == DataSetEnums::DATA_pp_FullV6) {
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2015_13TeV_set1.20220916_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2016_13TeV_set1.20220916_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set1.20220916_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set2.20220916_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set3.20220916_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2018_13TeV_set1.20220916_MYSTREAM/*.root");
    }
    else if (m_data_type == DataSetEnums::DATA_pp_1516) {
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2015_13TeV_set1.20220810_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2016_13TeV_set1.20220810_MYSTREAM/*.root");
    }
    else if (m_data_type == DataSetEnums::DATA_pp_1718) {
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set1.20220810_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set2.20220810_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2017_13TeV_set3.20220810_MYSTREAM/*.root");
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/reskimV6/user.pyin.TrigRates.PP2018_13TeV_set1.20220810_MYSTREAM/*.root");
    }
    else {std::cout << __PRETTY_FUNCTION__ << " Unknown Dataset" << std::endl; throw std::exception();}


    // ------------------------------------------------------------
    // Set branch addresses and branch pointers
    //
    fChain->SetMakeClass(1);
    fChain->SetBranchAddress("RunNumber" , &RunNumber );
    fChain->SetBranchAddress("lbn"       , &lbn       );
    fChain->SetBranchAddress("trk_pt"    , &trk_pt    );
    fChain->SetBranchAddress("trk_eta"   , &trk_eta   );
    fChain->SetBranchAddress("trk_phi"   , &trk_phi   );
    fChain->SetBranchAddress("trk_charge", &trk_charge);
    fChain->SetBranchAddress("trk_qual"  , &trk_qual  );
    fChain->SetBranchAddress("vtx_z"     , &vtx_z     );
    fChain->SetBranchAddress("MET_sumet" , &MET_sumet );

    fChain->SetBranchAddress("ActIntPerXing"     , &ActIntPerXing );
    fChain->SetBranchAddress("Trkz0RMS"          , &Trkz0RMS      );
    fChain->SetBranchAddress("muon_trk_index"    , &muon_trk_index);
    fChain->SetBranchAddress("electron_trk_index", &elec_trk_index);

    fChain->SetBranchAddress("neutralPFlowObj_pt"  , &neutralPFlowObj_pt  );
    fChain->SetBranchAddress("neutralPFlowObj_eta" , &neutralPFlowObj_eta );
    fChain->SetBranchAddress("neutralPFlowObj_phi" , &neutralPFlowObj_phi );
    fChain->SetBranchAddress("neutralPFlowObj_eEM" , &neutralPFlowObj_eEM );
    fChain->SetBranchAddress("neutralPFlowObj_t"   , &neutralPFlowObj_t   );
    fChain->SetBranchAddress("neutralPFlowObj_HEC0", &neutralPFlowObj_HEC0);
    fChain->SetBranchAddress("neutralPFlowObj_HEC1", &neutralPFlowObj_HEC1);
    fChain->SetBranchAddress("neutralPFlowObj_HEC2", &neutralPFlowObj_HEC2);
    fChain->SetBranchAddress("neutralPFlowObj_HEC3", &neutralPFlowObj_HEC3);
    fChain->SetBranchAddress("chargedPFlowObj_pt"  , &chargedPFlowObj_pt  );
    fChain->SetBranchAddress("chargedPFlowObj_eta" , &chargedPFlowObj_eta );
    fChain->SetBranchAddress("chargedPFlowObj_phi" , &chargedPFlowObj_phi );

    fChain->SetBranchAddress("AntiKt4EMPFlowJets_pt"     , &AntiKt4EMPFlowJets_pt     );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_eta"    , &AntiKt4EMPFlowJets_eta    );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_phi"    , &AntiKt4EMPFlowJets_phi    );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_t"      , &AntiKt4EMPFlowJets_t      );
    fChain->SetBranchAddress("AntiKt4EMPFlowJets_newjvt" , &AntiKt4EMPFlowJets_newjvt );

    fChain->SetBranchAddress("b_HLT_mb_sptrk"           , &b_HLT_mb_sptrk           );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1"  , &b_HLT_noalg_mb_L1MBTS_1  );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1_1", &b_HLT_noalg_mb_L1MBTS_1_1);

    fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_2"      , &b_HLT_noalg_mb_L1MBTS_2      );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1RD0_FILLED"  , &b_HLT_noalg_mb_L1RD0_FILLED  );
    fChain->SetBranchAddress("b_HLT_j10_L1MBTS_2"           , &b_HLT_j10_L1MBTS_2           );
    fChain->SetBranchAddress("b_HLT_j10_L1RD0_FILLED"       , &b_HLT_j10_L1RD0_FILLED       );
    fChain->SetBranchAddress("b_HLT_j20_L1J12"              , &b_HLT_j20_L1J12              );
    fChain->SetBranchAddress("b_HLT_j20_L1MBTS_2"           , &b_HLT_j20_L1MBTS_2           );
    fChain->SetBranchAddress("b_HLT_j20_L1RD0_FILLED"       , &b_HLT_j20_L1RD0_FILLED       );
    fChain->SetBranchAddress("b_HLT_j45_L1J12"              , &b_HLT_j45_L1J12              );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1TE5"         , &b_HLT_noalg_mb_L1TE5         );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1TE10"        , &b_HLT_noalg_mb_L1TE10        );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1TE20"        , &b_HLT_noalg_mb_L1TE20        );
    fChain->SetBranchAddress("b_HLT_noalg_mb_L1TE30"        , &b_HLT_noalg_mb_L1TE30        );

    fChain->SetBranchAddress("b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10", &b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5", &b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1", &b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1TE10", &b_HLT_mb_sp1000_trk70_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1TE5", &b_HLT_mb_sp1000_trk70_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE10", &b_HLT_mb_sp1100_trk70_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE20", &b_HLT_mb_sp1100_trk70_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE30", &b_HLT_mb_sp1100_trk70_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE5", &b_HLT_mb_sp1100_trk70_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk100_hmt_L1TE5", &b_HLT_mb_sp1200_trk100_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1", &b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE10", &b_HLT_mb_sp1200_trk80_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE15", &b_HLT_mb_sp1200_trk80_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE20", &b_HLT_mb_sp1200_trk80_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE30", &b_HLT_mb_sp1200_trk80_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE5", &b_HLT_mb_sp1200_trk80_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1200_trk90_hmt_L1TE5", &b_HLT_mb_sp1200_trk90_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk100_hmt_L1TE20", &b_HLT_mb_sp1400_trk100_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1", &b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE10", &b_HLT_mb_sp1400_trk90_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE15", &b_HLT_mb_sp1400_trk90_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE20", &b_HLT_mb_sp1400_trk90_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE30", &b_HLT_mb_sp1400_trk90_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE40", &b_HLT_mb_sp1400_trk90_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE5", &b_HLT_mb_sp1400_trk90_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE10", &b_HLT_mb_sp1600_trk100_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE15", &b_HLT_mb_sp1600_trk100_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE20", &b_HLT_mb_sp1600_trk100_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE25", &b_HLT_mb_sp1600_trk100_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE30", &b_HLT_mb_sp1600_trk100_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE40", &b_HLT_mb_sp1600_trk100_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE5", &b_HLT_mb_sp1600_trk100_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE50", &b_HLT_mb_sp1600_trk100_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE10", &b_HLT_mb_sp1700_trk110_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE20", &b_HLT_mb_sp1700_trk110_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE30", &b_HLT_mb_sp1700_trk110_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE40", &b_HLT_mb_sp1700_trk110_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE50", &b_HLT_mb_sp1700_trk110_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE60", &b_HLT_mb_sp1700_trk110_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE10", &b_HLT_mb_sp1800_trk110_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE15", &b_HLT_mb_sp1800_trk110_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE20", &b_HLT_mb_sp1800_trk110_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE25", &b_HLT_mb_sp1800_trk110_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE30", &b_HLT_mb_sp1800_trk110_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE10", &b_HLT_mb_sp1900_trk120_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE20", &b_HLT_mb_sp1900_trk120_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE30", &b_HLT_mb_sp1900_trk120_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE40", &b_HLT_mb_sp1900_trk120_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE50", &b_HLT_mb_sp1900_trk120_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE60", &b_HLT_mb_sp1900_trk120_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE70", &b_HLT_mb_sp1900_trk120_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE10", &b_HLT_mb_sp2100_trk120_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE15", &b_HLT_mb_sp2100_trk120_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE20", &b_HLT_mb_sp2100_trk120_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE25", &b_HLT_mb_sp2100_trk120_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE30", &b_HLT_mb_sp2100_trk120_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE20", &b_HLT_mb_sp2100_trk130_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE30", &b_HLT_mb_sp2100_trk130_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE40", &b_HLT_mb_sp2100_trk130_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE50", &b_HLT_mb_sp2100_trk130_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE60", &b_HLT_mb_sp2100_trk130_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE70", &b_HLT_mb_sp2100_trk130_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE20", &b_HLT_mb_sp2200_trk140_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE30", &b_HLT_mb_sp2200_trk140_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE40", &b_HLT_mb_sp2200_trk140_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE50", &b_HLT_mb_sp2200_trk140_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE60", &b_HLT_mb_sp2200_trk140_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE70", &b_HLT_mb_sp2200_trk140_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE15", &b_HLT_mb_sp2300_trk130_hmt_L1TE15);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE20", &b_HLT_mb_sp2300_trk130_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE25", &b_HLT_mb_sp2300_trk130_hmt_L1TE25);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE30", &b_HLT_mb_sp2300_trk130_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE40", &b_HLT_mb_sp2300_trk130_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE20", &b_HLT_mb_sp2400_trk150_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE30", &b_HLT_mb_sp2400_trk150_hmt_L1TE30);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE40", &b_HLT_mb_sp2400_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE50", &b_HLT_mb_sp2400_trk150_hmt_L1TE50);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE60", &b_HLT_mb_sp2400_trk150_hmt_L1TE60);
    fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE70", &b_HLT_mb_sp2400_trk150_hmt_L1TE70);
    fChain->SetBranchAddress("b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10", &b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40", &b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2500_trk140_hmt_L1TE40", &b_HLT_mb_sp2500_trk140_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40", &b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2700_trk150_hmt_L1TE40", &b_HLT_mb_sp2700_trk150_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40", &b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp2900_trk160_hmt_L1TE40", &b_HLT_mb_sp2900_trk160_hmt_L1TE40);
    fChain->SetBranchAddress("b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1", &b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10", &b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5", &b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp600_trk40_hmt_L1TE10", &b_HLT_mb_sp600_trk40_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp600_trk40_hmt_L1TE5", &b_HLT_mb_sp600_trk40_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1", &b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1", &b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE10", &b_HLT_mb_sp700_trk50_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE20", &b_HLT_mb_sp700_trk50_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE5", &b_HLT_mb_sp700_trk50_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1", &b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5", &b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk50_hmt_L1TE5", &b_HLT_mb_sp900_trk50_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1", &b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE10", &b_HLT_mb_sp900_trk60_hmt_L1TE10);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE20", &b_HLT_mb_sp900_trk60_hmt_L1TE20);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE5", &b_HLT_mb_sp900_trk60_hmt_L1TE5);
    fChain->SetBranchAddress("b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1", &b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1);


    //--------------------------------------------------------------------
    fChain->SetBranchStatus("*"         , 0);
    fChain->SetBranchStatus("RunNumber" , 1);
    fChain->SetBranchStatus("lbn"       , 1);
    fChain->SetBranchStatus("trk_pt"    , 1);
    fChain->SetBranchStatus("trk_eta"   , 1);
    fChain->SetBranchStatus("trk_phi"   , 1);
    fChain->SetBranchStatus("trk_qual"  , 1);
    fChain->SetBranchStatus("trk_charge", 1);
    fChain->SetBranchStatus("vtx_z"     , 1);
    fChain->SetBranchStatus("b_*"       , 1);

    fChain->SetBranchStatus("ActIntPerXing"       , 1);
    fChain->SetBranchStatus("Trkz0RMS"            , 1);
    fChain->SetBranchStatus("MET_sumet"           , 1);
    fChain->SetBranchStatus("neutralPFlowObj_*"   , 1);
    fChain->SetBranchStatus("chargedPFlowObj_*"   , 1);
    fChain->SetBranchStatus("muon_trk_index"      , 1);
    fChain->SetBranchStatus("electron_trk_index"  , 1);
    fChain->SetBranchStatus("AntiKt4EMPFlowJets_*", 1);

//--------------------------------------------------------------------
}



#endif
