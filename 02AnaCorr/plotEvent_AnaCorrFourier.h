#ifndef _AnaCorrFourier_HH_
#define _AnaCorrFourier_HH_

#include <vector>
#include "Riostream.h"
#include "TChain.h"
#include <string>
#include "Event.h"

#include "map"
#include "DatasetEnums.h"
#include "bins.h"
#include "TH3.h"
#include <TProfile.h>


using namespace std;

class TH1;
class TH2;
class TProfile;

struct triple {
    std::string first;
    Bool_t*     second;
    int         third;
};

class AnaCorrFourier {
    enum {
        //pool bins:
        // nc = Bins::NCENT,
        nc = 200,
        nz = 10,
        ZMAX = 100,
        plnbr = nc * nz,
    };

    enum {
        PP_MIN_BIAS            = 2,
        HI_LOOSE               = 4,
        HI_TIGHT               = 8,
        HI_TIGHT_TIGHTER_D0_Z0 = 16,
        HI_LOOSE_7SCT_HITS     = 32,
        HI_TIGHT_LOOSE_D0_Z0   = 64,
    };

public:
    AnaCorrFourier();

    void run();

    int  m_from           = 0    ;
    int  m_to             = 0    ;
    int  m_total          = 0    ;
    bool m_monitor_mode   = false;

    int m_data_type         = DataSetEnums::DATA_LOWMU                ;
    int m_mu_limit          = DataSetEnums::NO_MU_LIMIT               ;
    int m_apply_efficiency  = DataSetEnums::DEFAULT_EFFICIENCY        ;
    int m_trig_type         = DataSetEnums::ALL_TRIGS                 ;
    int m_pileup_reject     = DataSetEnums::NO_PILEUP_REJECTION       ;
    int m_trk_quality       = DataSetEnums::OLD_REQUIREPOSITIVE       ;
    int m_jet_reject_type   = DataSetEnums::NO_JET_REJECT             ;
    int m_jet_reject_cuts   = DataSetEnums::NO_JET_CUTS               ;
    int m_if_have_jet       = DataSetEnums::MINBIAS                   ;
    int m_if_have_uecorr    = DataSetEnums::WITH_UECORR               ;
    int m_correlation_type  = DataSetEnums::HADRON_HADRON_CORRELATIONS;
    int m_multiplicity_type = DataSetEnums::USE_NTRK                  ;


private:
    TChain *fChain = nullptr;


    void Init();
    void InitTriggers();
    void InitHistos();
    void SaveHistos();
    void InitJetCuts();
    void ReconstructJet();
    void InitUEpT();

    void parseJets();
    void Fill(Event* event1, Event* event2, int mixtype);
    void FillJetMonitors();

    bool Pileup();
    bool PassTrigger(int ntrk_noeff, bool isRun);
    bool PassQuality(int quality);
    bool inCaloBrightBand(double caloJetEta, double caloJetPhi);
    bool RejectJet_Cone(float trk_pt_, float trk_eta_, float trk_phi_, float trk_eff_);
    bool FillMixed(Event* event1, Event* event_jet = nullptr, Event* event_orgn = nullptr);

    int get_zPool(float z);
    int GetNtrkCent();
    int GetCentBinNtrk(float ntrk);
    std::pair<int, float> CountLowPTParticle(int ijet, float jet_eta);

    double MultiplicityCorrection();
    double ConvertPhi(double phi);
    double CalculateDeltaPhi(float phi1, float phi2);
    double EvaluateTrackingEff(float pt, float eta);
    double findUECorr(TH2D* h, double jet_eta, double jet_phi, int type);
    double multiplicityInterpolate(int etabin, int phibin, int type);
    double interpolate(double low, double up, double val_low, double val_up, double x);
    double findMinDistWithCaloJet(double trkJet_eta, double trkJet_phi);


    // ------------------------------------
    // Jet cut variables
    //
    float dR = -1;
    float min_jet_pt     = 1e10;
    float min_jet_pt_2nd = 1e10;
    int   min_jet_Nconstituents = 0; //Minimum Number of constituents
    float caloJetPt_1st  = 20.;
    float caloJetPt_2nd  = 10.;
    // ------------------------------------
    bool drawCurrent = false;
    int Ncurr = 0;
    int nmix;
    int m_cent_i;
    int m_centIndex_UECorr;
    int depth[nc];
    int ntrk_cent;
    int FCalET_cent;
    float nCurrentBlue = 0;

    double m_zvtx;
    double ntrk_correction = 0;
    vector<vector<double>> jetIngredientPT ;
    vector<vector<double>> jetIngredientETA;
    vector<vector<double>> jetIngredientPHI;
    vector<vector<int>>    jetIngredientIDX;

    vector<int> used_Partical;

    vector<double> m_jet_Nconstituents;
    vector<double> evt_jet_eta;
    vector<double> evt_jet_phi;
    vector<double> v_jetEnergyFractionA;
    vector<double> v_jetEnergyFractionB;

    vector<float>  reco_jet_pt ;
    vector<float>  reco_jet_eta;
    vector<float>  reco_jet_phi;
    vector<float>  reco_jet_Nch;

    vector<bool> m_jet_pass1;    // leading jet : eg. 15GeV
    vector<bool> m_jet_pass2;    // sub-leading jet : eg. 10GeV

    vector<triple> m_trig_map_minbias;
    vector<triple> m_trig_map_HMT;
    vector<triple> m_trig_map_HMT_L1MBTS;
    vector<triple> m_trig_map_HMT_L1TE10;
    vector<triple> m_trig_map_HMT_L1TE5;
    vector<triple> m_trig_map_HMT_L1TE15;
    vector<triple> m_trig_map_HMT_L1TE20;
    vector<triple> m_trig_map_HMT_L1TE30PLUS;
    vector<triple> m_trig_map_All;

    vector<EVENT_PTR> pool[plnbr];

    TH1* h_trig;
    TH1* hcent;
    TH1* h_mu;
    TH1* hcent_orig;
    TH1* hzvtx[Bins::NCENT + 1];
    TH1* h_nvtx;
    TH1* hNtrk_origional;
    TH1* hNtrk_corrected;
    TH1* hNtrk_origional_beforeTrigger;
    TH1* hNtrk_corrected_beforeTrigger;
    TH1* h_size_vexz[Bins::NCENT];
    TH1* h_dz       [Bins::NCENT];
    TH1* h_MultCorrection;
    TH1* h_JetEta2nd[Bins::NCENT];
    TH1* dist_pT_original    [Bins::NCENT];
    TH1* dist_pT_corred      [Bins::NCENT];
    TH1* dist_pT_afterRej_jet[Bins::NCENT];
    TH1* dist_pT_cc_poolJet  [Bins::NCENT];
    TH1* dist_pT_afterRej_ref[Bins::NCENT];
    TH1* h_jet_numJets  [Bins::NCENT];      // distribution of "# of 15 GeV jets in an event" in each centrality bin
    TH1* h_jet_pTFrac   [Bins::NCENT];      // pT of all particle in jet cone / pT of jet
    TH1* h_jet_counts;                      //# of 15 GeV jets in each centrality bin
    TH1* N_trigger_jetcut [Bins::NCENT];    //pT spectra of tracks within the jet cone.
    TH1* h_bg_constituents[Bins::NCENT];    // value of constituents correction
    TH1* h_bg_ptWeight    [Bins::NCENT];    // value of pT correction
    TH1* h_UE_pTThreshold [Bins::NCENT];    // value of pT correction
    TH1* h1_jetNch_orig[Bins::NCENT][14];
    TH1* h1_jetNch_UECR[Bins::NCENT][14];
    TH1* h1_CaloJets_PT[Bins::NCENT];
    // -----------------------------------
    // global monitor plots
    //    record how many event processed
    TH1* hFillEntries;
    TH1* hFillMixEntries;
    // -----------------------------------


    TH2* h_efficiency = nullptr;
    TH2* h2_Mult_CorrMult;
    TH2* h2_Mult_FCal;
    TH2* fg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* bg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* cg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* cc_unmatched[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* fg_after_correction[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* h2_CorrJetpT_JetpT[Bins::NCENT];   // corrected jet pT  vs original jet pT
    TH2* h2_JetpTCorr_JetpT[Bins::NCENT];   // jet pT correction vs original jet pT
    TH2* h2_CorrJetNch_JetNch[Bins::NCENT]; // corrected jet Nch  vs original jet Nch
    TH2* h2_JetNchCorr_JetNch[Bins::NCENT]; // jet Nch correction vs original jet Nch
    TH2* h2_jetNch_UENchCorr [Bins::NCENT];
    TH2* h2_jetPT_jetNch_orig[Bins::NCENT];
    TH2* h2_jetPT_jetNch_UECR[Bins::NCENT];
    TH2* h2_jetPT_jetNch_UECR_noOSCorr[Bins::NCENT];
    TH2* h2_jetPT_jetNch_jetNchUECRonly_noOSCorr[Bins::NCENT];
    TH2* h2_TrkJets_EtaPhi[Bins::NCENT];
    TH2* h2_TrkJets_noJetNear_EtaPhi[Bins::NCENT];
    TH2* h2_TrkJets2nd_EtaPhi[Bins::NCENT];
    TH2* h_2ndVS15Jet[Bins::NCENT];                     // N_2nd energy jet (default 10 GeV) vs N_15GeVJet
    TH2* h_jet_dEtadPhi_LnSL_sameCut[Bins::NCENT];      // deta:dphi distribution of leading and subleading jets
    TH2* h_jet_eta_LnSL_sameCut[Bins::NCENT];           // eta of leading jet vs subleading jet
    TH2* h_jet_phi_LnSL_sameCut[Bins::NCENT];           // phi of leading jet vs subleading jet
    TH2* h_jet_pT_LnSL_sameCut [Bins::NCENT];           // pT of leading jet vs subleading jet
    TH2* h_jet_dEtadPhi_LnSL_1st15[Bins::NCENT];        // deta:dphi distribution of leading and subleading jets
    TH2* h_jet_eta_LnSL_1st15[Bins::NCENT];             // eta of leading jet vs subleading jet
    TH2* h_jet_phi_LnSL_1st15[Bins::NCENT];             // phi of leading jet vs subleading jet
    TH2* h_jet_pT_LnSL_1st15 [Bins::NCENT];             // pT of leading jet vs subleading jet
    TH2* h2_Multiplicity_EbyE[Bins::NCENT];
    TH2* h2_CaloJets_EtaPhi[Bins::NCENT];
    TH2* h2_dR_CaloJettoTrkJet[Bins::NCENT];
    TH2* h2_CaloJetPT_TrkJetPT    [Bins::NCENT];
    TH2* h2_CaloJetPT_TrkJetPTCorr[Bins::NCENT];
    TH2* h2_fgBlue_bgBlue[Bins::NCENT];

    TH3* h_EtaPhiPt[Bins::NCENT];

    TProfile* p_Ntrk;
    TProfile* p_MultBins_withCorrMult;
    TProfile* p_CorrMultBins_withMult;
    TProfile* p_v2[Bins::NCENT];
    TProfile* p_v2_jetcut[Bins::NCENT];
    TProfile* p_jetdNch_noOSCorr[Bins::NCENT];
    TProfile* p_jetdpT_noOSCorr [Bins::NCENT];
    TProfile* p_jetdNch[Bins::NCENT];
    TProfile* p_jetdpT [Bins::NCENT];

    TH2D* h_EtaPhi_EbyE         [Bins::NCENT];
    TH2D* h_EtaPhi_ptWeight_EbyE[Bins::NCENT];


    // Declaration of leaf types
    vector<float>   *trk_pt            = nullptr;
    vector<float>   *trk_eta           = nullptr;
    vector<float>   *trk_phi           = nullptr;
    vector<float>   *trk_charge        = nullptr;
    vector<int>     *trk_qual          = nullptr;

    vector<float>   *vtx_z             = nullptr;
    vector<int>     *vx_nTracks        = nullptr;
    vector<float>   *vx_sumPt          = nullptr;
    vector<float>   *MET_sumet         = nullptr;

    vector<float>   **m_jet_pt  = nullptr;
    vector<float>   **m_jet_eta = nullptr;
    vector<float>   **m_jet_phi = nullptr;
    vector<float>   **m_jet_m   = nullptr;
    vector<float>   *AntiKt2PV0TrackJets_pt  = nullptr;
    vector<float>   *AntiKt2PV0TrackJets_eta = nullptr;
    vector<float>   *AntiKt2PV0TrackJets_phi = nullptr;
    vector<float>   *AntiKt2PV0TrackJets_m   = nullptr;
    vector<float>   *AntiKt3PV0TrackJets_pt  = nullptr;
    vector<float>   *AntiKt3PV0TrackJets_eta = nullptr;
    vector<float>   *AntiKt3PV0TrackJets_phi = nullptr;
    vector<float>   *AntiKt3PV0TrackJets_m   = nullptr;
    vector<float>   *AntiKt4PV0TrackJets_pt  = nullptr;
    vector<float>   *AntiKt4PV0TrackJets_eta = nullptr;
    vector<float>   *AntiKt4PV0TrackJets_phi = nullptr;
    vector<float>   *AntiKt4PV0TrackJets_m   = nullptr;
    vector<float>   *AntiKt4EMTopoJets_pt    = nullptr;
    vector<float>   *AntiKt4EMTopoJets_eta   = nullptr;
    vector<float>   *AntiKt4EMTopoJets_phi   = nullptr;
    vector<float>   *AntiKt4EMTopoJets_m     = nullptr;

    UInt_t          RunNumber = 0;
    UInt_t          lbn       = 0;
    UInt_t          bcid      = 0;

    Float_t         ActIntPerXing = 0;

    Bool_t          b_HLT_mb_perf_L1MBTS_2      = false;
    Bool_t          b_HLT_noalg_mb_L1MBTS_2     = false;
    Bool_t          b_HLT_noalg_mb_L1RD0_EMPTY  = false;
    Bool_t          b_HLT_noalg_mb_L1RD0_FILLED = false;
    Bool_t          b_HLT_mb_sptrk_L1MBTS_1     = false;

    Bool_t          b_HLT_mb_sp900_pusup500_trk60_hmt_L1TE5   = false;

    // Minbias trigger
    Bool_t          b_HLT_mb_sptrk              = false;
    Bool_t          b_HLT_noalg_mb_L1MBTS_1     = false;
    Bool_t          b_HLT_noalg_mb_L1MBTS_1_1   = false;

    // HMT trigger
    Bool_t          b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10_0ETA24     = false;
    Bool_t          b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5_0ETA24      = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1TE10_0ETA24              = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1000_trk70_hmt_L1TE5_0ETA24               = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1100_trk70_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5_0ETA24     = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10_0ETA24     = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15_0ETA24     = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5_0ETA24      = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5_0ETA24      = false;
    Bool_t          b_HLT_mb_sp1200_trk100_hmt_L1TE5                     = false;
    Bool_t          b_HLT_mb_sp1200_trk100_hmt_L1TE5_0ETA24              = false;
    Bool_t          b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE10_0ETA24              = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE15                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE15_0ETA24              = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1200_trk80_hmt_L1TE5_0ETA24               = false;
    Bool_t          b_HLT_mb_sp1200_trk90_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1200_trk90_hmt_L1TE5_0ETA24               = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10_0ETA24     = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15_0ETA24     = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20_0ETA24     = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40            = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5             = false;
    Bool_t          b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5_0ETA24      = false;
    Bool_t          b_HLT_mb_sp1400_trk100_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1400_trk100_hmt_L1TE20_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1400_trk100_hmt_L1TE20_0ETA25             = false;
    Bool_t          b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1                 = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE10                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE10_0ETA24              = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE10_0ETA25              = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE15                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE15_0ETA24              = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE20                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE20_0ETA24              = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE30                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE40                     = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE5                      = false;
    Bool_t          b_HLT_mb_sp1400_trk90_hmt_L1TE5_0ETA24               = false;
    Bool_t          b_HLT_mb_sp1500_pusup700_trk100_hmt_L1TE20_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE10_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE15_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE20_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE25_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE5                     = false;
    Bool_t          b_HLT_mb_sp1600_trk100_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1700_trk110_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30_0ETA24    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE10_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE15_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE20_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE25_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1800_trk110_hmt_L1TE30_0ETA24             = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp1900_trk120_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10_0ETA24    = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15_0ETA24    = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20_0ETA24    = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25_0ETA24    = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30_0ETA24    = false;
    Bool_t          b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5            = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE10                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE10_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE15_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE20_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE25_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2100_trk120_hmt_L1TE30_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2100_trk130_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2200_trk140_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15_0ETA24   = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20_0ETA24   = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25_0ETA24   = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30_0ETA24   = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40_0ETA24   = false;
    Bool_t          b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5           = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE15                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE15_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE20_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE25                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE25_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE30_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2300_trk130_hmt_L1TE40_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60           = false;
    Bool_t          b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70           = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE20                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE30                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE50                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE60                    = false;
    Bool_t          b_HLT_mb_sp2400_trk150_hmt_L1TE70                    = false;
    Bool_t          b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10          = false;
    Bool_t          b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE20_0ETA24   = false;
    Bool_t          b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2500_trk140_hmt_L1TE20_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2500_trk140_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE20_0ETA24   = false;
    Bool_t          b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2700_trk150_hmt_L1TE20_0ETA24             = false;
    Bool_t          b_HLT_mb_sp2700_trk150_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40          = false;
    Bool_t          b_HLT_mb_sp2900_trk160_hmt_L1TE40                    = false;
    Bool_t          b_HLT_mb_sp300_trk10_sumet50_hmt_L1TE10              = false;
    Bool_t          b_HLT_mb_sp300_trk10_sumet60_hmt_L1MBTS_1_1          = false;
    Bool_t          b_HLT_mb_sp300_trk10_sumet60_hmt_L1TE20              = false;
    Bool_t          b_HLT_mb_sp300_trk10_sumet70_hmt_L1MBTS_1_1          = false;
    Bool_t          b_HLT_mb_sp300_trk10_sumet80_hmt_L1MBTS_1_1          = false;
    Bool_t          b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp600_trk10_sumet50_hmt_L1TE10_0ETA25       = false;
    Bool_t          b_HLT_mb_sp600_trk10_sumet60_hmt_L1MBTS_1_1          = false;
    Bool_t          b_HLT_mb_sp600_trk10_sumet60_hmt_L1TE20_0ETA25       = false;
    Bool_t          b_HLT_mb_sp600_trk10_sumet70_hmt_L1MBTS_1_1          = false;
    Bool_t          b_HLT_mb_sp600_trk10_sumet80_hmt_L1MBTS_1_1          = false;
    Bool_t          b_HLT_mb_sp600_trk40_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp600_trk40_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20             = false;
    Bool_t          b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE20                      = false;
    Bool_t          b_HLT_mb_sp700_trk50_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5_0ETA24       = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10             = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20             = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5              = false;
    Bool_t          b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5_0ETA24       = false;
    Bool_t          b_HLT_mb_sp900_trk50_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp900_trk50_hmt_L1TE5_0ETA24                = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1                  = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE10                      = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE20                      = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE5                       = false;
    Bool_t          b_HLT_mb_sp900_trk60_hmt_L1TE5_0ETA24                = false;
    Bool_t          b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1                  = false;
};

void AnaCorrFourier::Init() {
    fChain = new TChain("HeavyIonD3PD");

    if     (m_data_type == DataSetEnums::DATA_LOWMU   ) {
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/13TeVData/ppLowMuData/*.root"  );
    }
    else if (m_data_type == DataSetEnums::DATA_LOW_AND_INTERMEDIATE_MU) {
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/13TeVData/ppLowMuData/*.root");             // mu ~ 0.04
        fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/13TeVData/ppIntermediateMuData/*.root");    // mu ~ 0.05 - 0.6
    }
    else if (m_data_type == DataSetEnums::DATA_5TEV    ) {
        char filename[600];
        for (int i = 0; i < 114; i++)
        {
            sprintf(filename, "/atlas/work/soumya/PP_Run2/DATA/PP_5TEV/links/%d.root", i);
            fChain->Add(filename);
        }
        // fChain->Add("/atlas/work/soumya/PP_Run2/DATA/PP_5TEV/links/*.root"   );
    }
    else if (m_data_type == DataSetEnums::DATA_pPb_2016_8TeV  ) {
        fChain->Add("/atlas/u/soumya/work/PP_Run2/DATA/pPb_2016_8TeV_2/links/*.root");
    }
    else if (m_data_type == DataSetEnums::DATA_pp_REPROCESSED ) {
        // fChain->Add("/usatlas/u/pyin/usatlasdata/ppData/13TeVData/newProcessed/*.root");
        fChain->Add("/atlasgpfs01/usatlas/data/msoumya/PP/links/*.root");
    }
    else {
        std::cout << __PRETTY_FUNCTION__ << " Unknown Dataset" << std::endl; throw std::exception();
    }

    // ------------------------------------------------------------
    // Set branch addresses and branch pointers
    //
    fChain->SetMakeClass(1);
    fChain->SetBranchAddress("RunNumber"         , &RunNumber         );
    if (m_data_type == DataSetEnums::DATA_LOWMU || m_data_type == DataSetEnums::DATA_LOW_AND_INTERMEDIATE_MU) {
        fChain->SetBranchAddress("trk_pt"            , &trk_pt            );
        fChain->SetBranchAddress("trk_eta"           , &trk_eta           );
        fChain->SetBranchAddress("trk_phi"           , &trk_phi           );
        fChain->SetBranchAddress("trk_charge"        , &trk_charge        );
        fChain->SetBranchAddress("trk_qual"          , &trk_qual          );
        fChain->SetBranchAddress("vtx_z"             , &vtx_z             );
        fChain->SetBranchAddress("MET_sumet"         , &MET_sumet         );
        if (m_data_type == DataSetEnums::DATA_LOWMU || m_data_type == DataSetEnums::DATA_LOW_AND_INTERMEDIATE_MU) {
            fChain->SetBranchAddress("b_HLT_mb_sptrk"             , &b_HLT_mb_sptrk             );
            fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1"    , &b_HLT_noalg_mb_L1MBTS_1    );
            fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1_1"  , &b_HLT_noalg_mb_L1MBTS_1_1  );

            fChain->SetBranchAddress("b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1" , &b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1 );
            fChain->SetBranchAddress("b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1" , &b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1 );
            fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1" , &b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1 );
            fChain->SetBranchAddress("b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1" , &b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1 );
            fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1" , &b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1 );
            fChain->SetBranchAddress("b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1" , &b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1 );
            fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1", &b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1);
            fChain->SetBranchAddress("b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1", &b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1);
            fChain->SetBranchAddress("b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1", &b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1);
            fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE10"    , &b_HLT_mb_sp1400_trk90_hmt_L1TE10    );
        }
    }
    else if (m_data_type == DataSetEnums::DATA_5TEV) {
        fChain->SetBranchAddress("trk_pt"            , &trk_pt            );
        fChain->SetBranchAddress("trk_eta"           , &trk_eta           );
        fChain->SetBranchAddress("trk_phi"           , &trk_phi           );
        fChain->SetBranchAddress("trk_charge"        , &trk_charge        );
        fChain->SetBranchAddress("trk_qual"          , &trk_qual          );
        fChain->SetBranchAddress("vtx_z"             , &vtx_z             );
        fChain->SetBranchAddress("b_HLT_mb_sptrk"                           , &b_HLT_mb_sptrk                           );
        fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1"                  , &b_HLT_noalg_mb_L1MBTS_1                  );
        fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1_1"                , &b_HLT_noalg_mb_L1MBTS_1_1                );
        fChain->SetBranchAddress("b_HLT_mb_sp900_pusup500_trk60_hmt_L1TE5"  , &b_HLT_mb_sp900_pusup500_trk60_hmt_L1TE5  );
        fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20);
    }
    else if (m_data_type == DataSetEnums::DATA_pPb_2016_8TeV) {
        fChain->SetBranchAddress("vtx_z"           , &vtx_z        );

        fChain->SetBranchAddress("trk_pt"          , &trk_pt    );
        fChain->SetBranchAddress("trk_eta"         , &trk_eta   );
        fChain->SetBranchAddress("trk_phi"         , &trk_phi   );
        fChain->SetBranchAddress("trk_charge"      , &trk_charge);
        fChain->SetBranchAddress("trk_qual"        , &trk_qual  );

        fChain->SetBranchAddress("b_HLT_mb_sptrk" , &b_HLT_mb_sptrk);
        fChain->SetBranchAddress("b_HLT_mb_sptrk_L1MBTS_1", &b_HLT_mb_sptrk_L1MBTS_1);
    }
    else if (m_data_type == DataSetEnums::DATA_pp_REPROCESSED) {
        fChain->SetBranchAddress("ActIntPerXing"     , &ActIntPerXing     );
        fChain->SetBranchAddress("trk_pt"            , &trk_pt            );
        fChain->SetBranchAddress("trk_eta"           , &trk_eta           );
        fChain->SetBranchAddress("trk_phi"           , &trk_phi           );
        fChain->SetBranchAddress("trk_charge"        , &trk_charge        );
        fChain->SetBranchAddress("trk_qual"          , &trk_qual          );
        fChain->SetBranchAddress("vtx_z"             , &vtx_z             );
        fChain->SetBranchAddress("MET_sumet"         , &MET_sumet         );

        fChain->SetBranchAddress("AntiKt4EMTopoJets_pt" , &AntiKt4EMTopoJets_pt );
        fChain->SetBranchAddress("AntiKt4EMTopoJets_eta", &AntiKt4EMTopoJets_eta);
        fChain->SetBranchAddress("AntiKt4EMTopoJets_phi", &AntiKt4EMTopoJets_phi);
        fChain->SetBranchAddress("AntiKt4EMTopoJets_m"  , &AntiKt4EMTopoJets_m  );

        fChain->SetBranchAddress("b_HLT_mb_sptrk"             , &b_HLT_mb_sptrk             );
        fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1"    , &b_HLT_noalg_mb_L1MBTS_1    );
        fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1_1"  , &b_HLT_noalg_mb_L1MBTS_1_1  );

        fChain->SetBranchAddress("b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10", &b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5", &b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1", &b_HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1TE10", &b_HLT_mb_sp1000_trk70_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1000_trk70_hmt_L1TE10_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1TE5", &b_HLT_mb_sp1000_trk70_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp1000_trk70_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1000_trk70_hmt_L1TE5_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5", &b_HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE10", &b_HLT_mb_sp1100_trk70_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE20", &b_HLT_mb_sp1100_trk70_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE30", &b_HLT_mb_sp1100_trk70_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp1100_trk70_hmt_L1TE5", &b_HLT_mb_sp1100_trk70_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5", &b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_trk100_hmt_L1TE5", &b_HLT_mb_sp1200_trk100_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_trk100_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1200_trk100_hmt_L1TE5_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1", &b_HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE10", &b_HLT_mb_sp1200_trk80_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1200_trk80_hmt_L1TE10_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE15", &b_HLT_mb_sp1200_trk80_hmt_L1TE15);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1200_trk80_hmt_L1TE15_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE20", &b_HLT_mb_sp1200_trk80_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE30", &b_HLT_mb_sp1200_trk80_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE5", &b_HLT_mb_sp1200_trk80_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_trk80_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1200_trk80_hmt_L1TE5_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_trk90_hmt_L1TE5", &b_HLT_mb_sp1200_trk90_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp1200_trk90_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1200_trk90_hmt_L1TE5_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk100_hmt_L1TE20", &b_HLT_mb_sp1400_trk100_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk100_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1400_trk100_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk100_hmt_L1TE20.0ETA25", &b_HLT_mb_sp1400_trk100_hmt_L1TE20_0ETA25);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1", &b_HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE10", &b_HLT_mb_sp1400_trk90_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1400_trk90_hmt_L1TE10_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE10.0ETA25", &b_HLT_mb_sp1400_trk90_hmt_L1TE10_0ETA25);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE15", &b_HLT_mb_sp1400_trk90_hmt_L1TE15);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1400_trk90_hmt_L1TE15_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE20", &b_HLT_mb_sp1400_trk90_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1400_trk90_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE30", &b_HLT_mb_sp1400_trk90_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE40", &b_HLT_mb_sp1400_trk90_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE5", &b_HLT_mb_sp1400_trk90_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp1400_trk90_hmt_L1TE5.0ETA24", &b_HLT_mb_sp1400_trk90_hmt_L1TE5_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1500_pusup700_trk100_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1500_pusup700_trk100_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25.0ETA24", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50", &b_HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE10", &b_HLT_mb_sp1600_trk100_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1600_trk100_hmt_L1TE10_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE15", &b_HLT_mb_sp1600_trk100_hmt_L1TE15);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1600_trk100_hmt_L1TE15_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE20", &b_HLT_mb_sp1600_trk100_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1600_trk100_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE25", &b_HLT_mb_sp1600_trk100_hmt_L1TE25);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE25.0ETA24", &b_HLT_mb_sp1600_trk100_hmt_L1TE25_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE30", &b_HLT_mb_sp1600_trk100_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE40", &b_HLT_mb_sp1600_trk100_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE5", &b_HLT_mb_sp1600_trk100_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp1600_trk100_hmt_L1TE50", &b_HLT_mb_sp1600_trk100_hmt_L1TE50);
        fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50);
        fChain->SetBranchAddress("b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60", &b_HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60);
        fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE10", &b_HLT_mb_sp1700_trk110_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE20", &b_HLT_mb_sp1700_trk110_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE30", &b_HLT_mb_sp1700_trk110_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE40", &b_HLT_mb_sp1700_trk110_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE50", &b_HLT_mb_sp1700_trk110_hmt_L1TE50);
        fChain->SetBranchAddress("b_HLT_mb_sp1700_trk110_hmt_L1TE60", &b_HLT_mb_sp1700_trk110_hmt_L1TE60);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25.0ETA24", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30.0ETA24", &b_HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE10", &b_HLT_mb_sp1800_trk110_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE10.0ETA24", &b_HLT_mb_sp1800_trk110_hmt_L1TE10_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE15", &b_HLT_mb_sp1800_trk110_hmt_L1TE15);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE15.0ETA24", &b_HLT_mb_sp1800_trk110_hmt_L1TE15_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE20", &b_HLT_mb_sp1800_trk110_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE20.0ETA24", &b_HLT_mb_sp1800_trk110_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE25", &b_HLT_mb_sp1800_trk110_hmt_L1TE25);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE25.0ETA24", &b_HLT_mb_sp1800_trk110_hmt_L1TE25_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE30", &b_HLT_mb_sp1800_trk110_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp1800_trk110_hmt_L1TE30.0ETA24", &b_HLT_mb_sp1800_trk110_hmt_L1TE30_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50);
        fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60);
        fChain->SetBranchAddress("b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70", &b_HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70);
        fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE10", &b_HLT_mb_sp1900_trk120_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE20", &b_HLT_mb_sp1900_trk120_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE30", &b_HLT_mb_sp1900_trk120_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE40", &b_HLT_mb_sp1900_trk120_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE50", &b_HLT_mb_sp1900_trk120_hmt_L1TE50);
        fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE60", &b_HLT_mb_sp1900_trk120_hmt_L1TE60);
        fChain->SetBranchAddress("b_HLT_mb_sp1900_trk120_hmt_L1TE70", &b_HLT_mb_sp1900_trk120_hmt_L1TE70);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70", &b_HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10.0ETA24", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15.0ETA24", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25.0ETA24", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30.0ETA24", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5", &b_HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE10", &b_HLT_mb_sp2100_trk120_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE10.0ETA24", &b_HLT_mb_sp2100_trk120_hmt_L1TE10_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE15", &b_HLT_mb_sp2100_trk120_hmt_L1TE15);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE15.0ETA24", &b_HLT_mb_sp2100_trk120_hmt_L1TE15_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE20", &b_HLT_mb_sp2100_trk120_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2100_trk120_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE25", &b_HLT_mb_sp2100_trk120_hmt_L1TE25);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE25.0ETA24", &b_HLT_mb_sp2100_trk120_hmt_L1TE25_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE30", &b_HLT_mb_sp2100_trk120_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk120_hmt_L1TE30.0ETA24", &b_HLT_mb_sp2100_trk120_hmt_L1TE30_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE20", &b_HLT_mb_sp2100_trk130_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE30", &b_HLT_mb_sp2100_trk130_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE40", &b_HLT_mb_sp2100_trk130_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE50", &b_HLT_mb_sp2100_trk130_hmt_L1TE50);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE60", &b_HLT_mb_sp2100_trk130_hmt_L1TE60);
        fChain->SetBranchAddress("b_HLT_mb_sp2100_trk130_hmt_L1TE70", &b_HLT_mb_sp2100_trk130_hmt_L1TE70);
        fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50);
        fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60);
        fChain->SetBranchAddress("b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70", &b_HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70);
        fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE20", &b_HLT_mb_sp2200_trk140_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE30", &b_HLT_mb_sp2200_trk140_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE40", &b_HLT_mb_sp2200_trk140_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE50", &b_HLT_mb_sp2200_trk140_hmt_L1TE50);
        fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE60", &b_HLT_mb_sp2200_trk140_hmt_L1TE60);
        fChain->SetBranchAddress("b_HLT_mb_sp2200_trk140_hmt_L1TE70", &b_HLT_mb_sp2200_trk140_hmt_L1TE70);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15.0ETA24", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25.0ETA24", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30.0ETA24", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40.0ETA24", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5", &b_HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE15", &b_HLT_mb_sp2300_trk130_hmt_L1TE15);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE15.0ETA24", &b_HLT_mb_sp2300_trk130_hmt_L1TE15_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE20", &b_HLT_mb_sp2300_trk130_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2300_trk130_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE25", &b_HLT_mb_sp2300_trk130_hmt_L1TE25);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE25.0ETA24", &b_HLT_mb_sp2300_trk130_hmt_L1TE25_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE30", &b_HLT_mb_sp2300_trk130_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE30.0ETA24", &b_HLT_mb_sp2300_trk130_hmt_L1TE30_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE40", &b_HLT_mb_sp2300_trk130_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp2300_trk130_hmt_L1TE40.0ETA24", &b_HLT_mb_sp2300_trk130_hmt_L1TE40_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50);
        fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60);
        fChain->SetBranchAddress("b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70", &b_HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70);
        fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE20", &b_HLT_mb_sp2400_trk150_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE30", &b_HLT_mb_sp2400_trk150_hmt_L1TE30);
        fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE40", &b_HLT_mb_sp2400_trk150_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE50", &b_HLT_mb_sp2400_trk150_hmt_L1TE50);
        fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE60", &b_HLT_mb_sp2400_trk150_hmt_L1TE60);
        fChain->SetBranchAddress("b_HLT_mb_sp2400_trk150_hmt_L1TE70", &b_HLT_mb_sp2400_trk150_hmt_L1TE70);
        fChain->SetBranchAddress("b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10", &b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40", &b_HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp2500_trk140_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2500_trk140_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2500_trk140_hmt_L1TE40", &b_HLT_mb_sp2500_trk140_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40", &b_HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp2700_trk150_hmt_L1TE20.0ETA24", &b_HLT_mb_sp2700_trk150_hmt_L1TE20_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp2700_trk150_hmt_L1TE40", &b_HLT_mb_sp2700_trk150_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40", &b_HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp2900_trk160_hmt_L1TE40", &b_HLT_mb_sp2900_trk160_hmt_L1TE40);
        fChain->SetBranchAddress("b_HLT_mb_sp300_trk10_sumet50_hmt_L1TE10", &b_HLT_mb_sp300_trk10_sumet50_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp300_trk10_sumet60_hmt_L1MBTS_1_1", &b_HLT_mb_sp300_trk10_sumet60_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sp300_trk10_sumet60_hmt_L1TE20", &b_HLT_mb_sp300_trk10_sumet60_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp300_trk10_sumet70_hmt_L1MBTS_1_1", &b_HLT_mb_sp300_trk10_sumet70_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sp300_trk10_sumet80_hmt_L1MBTS_1_1", &b_HLT_mb_sp300_trk10_sumet80_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1", &b_HLT_mb_sp400_trk40_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10", &b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5", &b_HLT_mb_sp600_pusup300_trk40_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp600_trk10_sumet50_hmt_L1TE10.0ETA25", &b_HLT_mb_sp600_trk10_sumet50_hmt_L1TE10_0ETA25);
        fChain->SetBranchAddress("b_HLT_mb_sp600_trk10_sumet60_hmt_L1MBTS_1_1", &b_HLT_mb_sp600_trk10_sumet60_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sp600_trk10_sumet60_hmt_L1TE20.0ETA25", &b_HLT_mb_sp600_trk10_sumet60_hmt_L1TE20_0ETA25);
        fChain->SetBranchAddress("b_HLT_mb_sp600_trk10_sumet70_hmt_L1MBTS_1_1", &b_HLT_mb_sp600_trk10_sumet70_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sp600_trk10_sumet80_hmt_L1MBTS_1_1", &b_HLT_mb_sp600_trk10_sumet80_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sp600_trk40_hmt_L1TE10", &b_HLT_mb_sp600_trk40_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp600_trk40_hmt_L1TE5", &b_HLT_mb_sp600_trk40_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1", &b_HLT_mb_sp600_trk45_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5", &b_HLT_mb_sp700_pusup350_trk50_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1", &b_HLT_mb_sp700_trk50_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE10", &b_HLT_mb_sp700_trk50_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE20", &b_HLT_mb_sp700_trk50_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp700_trk50_hmt_L1TE5", &b_HLT_mb_sp700_trk50_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1", &b_HLT_mb_sp700_trk55_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5", &b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5.0ETA24", &b_HLT_mb_sp900_pusup400_trk50_hmt_L1TE5_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5.0ETA24", &b_HLT_mb_sp900_pusup400_trk60_hmt_L1TE5_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp900_trk50_hmt_L1TE5", &b_HLT_mb_sp900_trk50_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp900_trk50_hmt_L1TE5.0ETA24", &b_HLT_mb_sp900_trk50_hmt_L1TE5_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1", &b_HLT_mb_sp900_trk60_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE10", &b_HLT_mb_sp900_trk60_hmt_L1TE10);
        fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE20", &b_HLT_mb_sp900_trk60_hmt_L1TE20);
        fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE5", &b_HLT_mb_sp900_trk60_hmt_L1TE5);
        fChain->SetBranchAddress("b_HLT_mb_sp900_trk60_hmt_L1TE5.0ETA24", &b_HLT_mb_sp900_trk60_hmt_L1TE5_0ETA24);
        fChain->SetBranchAddress("b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1", &b_HLT_mb_sp900_trk65_hmt_L1MBTS_1_1);
        fChain->SetBranchAddress("b_HLT_mb_sptrk", &b_HLT_mb_sptrk);
        fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1", &b_HLT_noalg_mb_L1MBTS_1);
        fChain->SetBranchAddress("b_HLT_noalg_mb_L1MBTS_1_1", &b_HLT_noalg_mb_L1MBTS_1_1);
    }
    else {
        std::cout << __PRETTY_FUNCTION__ << " Tree Branches not defined for this Dataset" << std::endl;
        throw std::exception();
    }

    //--------------------------------------------------------------------
    fChain->SetBranchStatus("*"             , 0);
    fChain->SetBranchStatus("RunNumber"     , 1);

    if (m_data_type == DataSetEnums::DATA_LOWMU                   ||
            m_data_type == DataSetEnums::DATA_LOW_AND_INTERMEDIATE_MU ||
            m_data_type == DataSetEnums::DATA_5TEV) {
        fChain->SetBranchStatus("b_*"        , 1);
        fChain->SetBranchStatus("trk_pt"     , 1);
        fChain->SetBranchStatus("trk_eta"    , 1);
        fChain->SetBranchStatus("trk_phi"    , 1);
        fChain->SetBranchStatus("trk_qual"   , 1);
        fChain->SetBranchStatus("trk_charge" , 1);
        fChain->SetBranchStatus("vtx_z"      , 1);
        fChain->SetBranchStatus("MET_sumet"  , 1);
    }
    else if (m_data_type == DataSetEnums::DATA_pPb_2016_8TeV) {
        fChain->SetBranchStatus("b_*"             , 1);
        fChain->SetBranchStatus("trk_pt"          , 1);
        fChain->SetBranchStatus("trk_eta"         , 1);
        fChain->SetBranchStatus("trk_phi"         , 1);
        fChain->SetBranchStatus("trk_charge"      , 1);
        fChain->SetBranchStatus("trk_qual"        , 1);
        fChain->SetBranchStatus("vtx_z"           , 1);
    }
    else if (m_data_type == DataSetEnums::DATA_pp_REPROCESSED) {
        fChain->SetBranchStatus("b_*"                , 1);
        fChain->SetBranchStatus("ActIntPerXing"      , 1);
        fChain->SetBranchStatus("trk_pt"             , 1);
        fChain->SetBranchStatus("trk_eta"            , 1);
        fChain->SetBranchStatus("trk_phi"            , 1);
        fChain->SetBranchStatus("trk_charge"         , 1);
        fChain->SetBranchStatus("trk_qual"           , 1);
        fChain->SetBranchStatus("vtx_z"              , 1);
        fChain->SetBranchStatus("MET_sumet"          , 1);
        fChain->SetBranchStatus("AntiKt4EMTopoJets_*", 1);
    }
    else {
        std::cout << __PRETTY_FUNCTION__ << " Tree Branches not defined for this Dataset" << std::endl;
        throw std::exception();
    }
}
#endif
